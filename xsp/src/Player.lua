Player = {}
function Player:new (...)
  o = {
  	i = -1,
  	j = -1,
  	ammo = 5,
  	ammoMax = 5,
  	step = 0,
  }
  setmetatable(o, self)
  self.__index = self
  return o
end

function Player:IsAtPos(_i,_j)
    return (self.i == _i) and (self.j == _j)
end

function Player:IsAtPosT(_pos)
  return (self.i == _pos[1]) and (self.j == _pos[2])
end