function scanMain()
    local set = 0
    local path = ""

    keepScreen(true)

    local colors = {}
    local colors2 = {}
    local x, y

    if scanOCGWidget() then
        if scanIsChapter() then
            path = path .. "level"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif scanIsSP() then
            path = path .. "level"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 特别演习 开活动时再解除注释
        if scanIsTBYX() then
            path = path .. "level"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- main
        colors = {
            { 1018, 381, 0xffffff },
            { 973, 263, 0xf7e384 },
            { 854, 322, 0x6bd3f7 }
        }
        if cmpColor(colors) then
            path = path .. "main"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 世界线选择
        if scanIsWorldLineSelect() then
            path = path .. "world_line_select"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- map

        -- if _scanIsMap() then
        if scanMapXianshi() or scanMapWuxianshi() then
            path = path .. "map"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- -- 自动寻敌
        -- if scanIsAutoSeekEnabled() then
        --     path = path .. "map:autoseek"
        --     if not SETTINGS.debug_path then
        --         keepScreen(false)
        --         BattleRecovery:UnregistByPath(path)
        --         return path
        --     end
        -- end

        -- formation
        if scanIsFormation() then
            path = path .. "formation"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- yanxi
        colors = {
            { 150, 16, 0xffffff },
            { 805, 585, 0xd6ebff },
            { 1032, 589, 0xd6ebff }
        }
        if cmpColor(colors) then
            path = path .. "yanxi"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsMeiri() then
            path = path .. "meiri"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end
    else
        -- FORK 没有石油/金币/钻石
        -- 特别演习 开活动时再解除注释
        if scanIsTBYX() then
            path = path .. "level"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- combat
        if scanIsCombat() then
            path = path .. "combat"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- getitems
        if scanIsGetItems() then
            path = path .. "getitems"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif scanIsGetItems2() then
            path = path .. "getitems"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif scanIsGetItems3() then
            path = path .. "getitems"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- yanxi:confirm
        colors = {
            { 467, 180, 0xffffff },
            { 659, 188, 0xffffff },
            { 651, 483, 0xffffff },
            { 486, 483, 0xd69652 }
        }
        if cmpColor(colors) then
            path = path .. "yanxi:confirm"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsReadProtoc() then
            path = path .. "ReadProtoc"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- victory
        if
        cmpColor(
                {
                    { 567, 331, 0xffffff },
                    { 624, 336, 0xffffff },
                    { 679, 322, 0xffffff }
                }
        ) and
                cmpColor(
                        {
                            { 596, 418, 0x23ade5 },
                            { 796, 476, 0x23ade5 },
                            { 775, 82, 0xd0d0d0 }
                        }
                ) == false
        then
            path = path .. "victory"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 舰队任务提示
        if scanIsFleetInform2Confirm1() then
            path = path .. "fleetinform:2confirm1"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- auto:confirm 是否自律
        if scanIsAutoConfirm() then
            path = path .. "auto:confirm"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- -- inform 普通通知，1个按钮
        if scanIsInform1Confirm() then
            path = path .. "inform1:confirm1"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- inform:2confirm1 短线，委托石油，搜索前往确认，2按钮1确认
        if scanIsInform2Confirm1() then
            path = path .. "inform:2confirm1"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- inform:3confirm3 退役
        if scanIsInform3Confirm3() then
            path = path .. "inform:3confirm3"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- level:confirm  舰队选择 立刻前往
        if scanIsLevelConfirm() then
            path = path .. "level:confirm"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- teamselection
        -- both normal and hard mode
        if scanIsFleetSelect() then
            if scanTeamChooseBtn_1() then
                path = path .. "teamselection"
                if not SETTINGS.debug_path then
                    keepScreen(false)
                    return path
                end
            else
                path = path .. "teamselection:moew"
                if not SETTINGS.debug_path then
                    keepScreen(false)
                    return path
                end
            end
        end

        if
        cmpColor(
                {
                    { 86, 67, 0xf7f39c },
                    { 377, 115, 0xeff39c },
                    { 1032, 573, 0xe79229 }
                }
        )
        then
            path = path .. "victory:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif
        cmpColor(
                {
                    { 65, 75, 0xf7e985 },
                    { 103, 105, 0xffe075 },
                    { 154, 68, 0xffe161 },
                    { 142, 105, 0xffff88 },
                    { 234, 108, 0xffff66 },
                    { 208, 95, 0xfbd465 }
                }
        )
        then
            -- S
            path = path .. "victory:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif
        cmpColor(
                {
                    { 71, 74, 0xffeb8c },
                    { 82, 93, 0xffff9c },
                    { 231, 92, 0xf7cb63 },
                    { 222, 67, 0xffffc6 }
                }
        )
        then
            -- A
            path = path .. "victory:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif
        cmpColor(
                {
                    { 67, 70, 0xc6edff },
                    { 96, 108, 0xaae5ff },
                    { 207, 109, 0x78d6ff },
                    { 273, 94, 0xdff5ff },
                    { 233, 87, 0x9ce0ff }
                }
        )
        then
            -- B
            path = path .. "victory:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif
        cmpColor(
                {
                    { 73, 65, 0xe7efe7 },
                    { 172, 108, 0xe7e7de },
                    { 221, 72, 0xeff7ef },
                    { 303, 66, 0xf5faee }
                }
        )
        then
            -- C 打破
            path = path .. "victory:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif
        cmpColor(
                {
                    { 70, 106, 0xe7ece3 },
                    { 110, 69, 0xebf3eb },
                    { 308, 107, 0xc4cabf },
                    { 203, 103, 0xc6cbc3 },
                    { 174, 68, 0xe9eee5 }
                }
        )
        then
            -- C 超时
            path = path .. "victory:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        elseif
        cmpColor(
                {
                    { 88, 84, 0xeff3e7 },
                    { 92, 109, 0xd6dbce },
                    { 308, 65, 0xf4faf6 },
                    { 297, 101, 0xc6cbc6 }
                }
        )
        then
            -- D
            path = path .. "victory:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 战斗失败提升提示
        if scanIsBattleFailTip() then
            path = path .. "failtip"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 合计奖励页面
        if scanIsTotalRewardsPage() then
            path = path .. "total_rewards"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 合计奖励页面带研修
        if scanIsTotalRewardsPage2() then
            path = path .. "total_rewards2"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- lootship
        colors = {
            { 63, 335, 0xffffff },
            { 51, 508, 0xffffff },
            { 60, 592, 0xffffff },
            { 973, 553, 0xffffff }
        }
        if cmpColor(colors) then
            path = path .. "lootship"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- renwu
        colors = {
            { 42, 143, 0xf7e784 },
            { 49, 143, 0xf7e384 },
            { 1108, 12, 0x313042 }
        }
        if cmpColor(colors) then
            path = path .. "renwu"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- dockyard (recycle)
        colors = {
            { 806, 600, 0xffffff },
            { 762, 572, 0x853d36 },
            { 907, 617, 0xc86056 },
            { 121, 28, 0xd7ebff },
            { 152, 25, 0xe0f1ff },
            { 677, 15, 0xc1e9fc },
            { 686, 29, 0xabd2f4 }
        }
        if cmpColor(colors) then
            path = path .. "dockyard"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- dockyard (recycle:common)
        colors = {
            { 806, 600, 0xffffff },
            { 762, 572, 0x853d36 },
            { 907, 617, 0xc86056 },
            { 121, 28, 0xd7ebff },
            { 152, 25, 0xe0f1ff },
            { 677, 15, 0xf7e99d },
            { 686, 30, 0xf3dd8c }
        }
        if cmpColor(colors) then
            path = path .. "dockyard"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- recycle:confirm
        if scanRecycleConfirmInfo() then
            path = path .. "recycle:confirm"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanEliteConfirm() then
            path = path .. "recycle:confirm:elite"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- recycle:gear
        colors = {
            { 901, 132, 0xd6514a },
            { 738, 464, 0xb5655a },
            { 910, 469, 0x5286c6 },
            { 214, 138, 0xffffff }
        }
        if cmpColor(colors) then
            path = path .. "recycle:gear"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- recycle:gear:material
        colors = {
            { 476, 493, 0xbd655a },
            { 779, 491, 0x5282c6 },
            { 733, 515, 0xffffff }
        }
        if cmpColor(colors) then
            path = path .. "recycle:gear:material"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- weituo:exp
        colors = {
            { 181, 103, 0xf7eb8c },
            { 202, 103, 0xffffef },
            { 178, 120, 0xffff94 },
            { 198, 133, 0xffdb6b }
        }
        if cmpColor(colors) then
            path = path .. "weituo:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- weituo:expended
        if scanIsWeituoExpanded() then
            path = path .. "weituo:expended"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- houzhai:exp
        if scanIsHouzhaiExpPage() then
            path = path .. "houzhai:exp"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 后宅 houzhai
        if scanIsHouzhai() then
            path = path .. "houzhai"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- activity 活动总览/每日签到
        x, y = myFindColor(
                { 0, 0, 1135, 638 },
                {
                    { x = 0, y = 0, color = 0x212031 },
                    { x = 0, y = 67, color = 0x212031 },
                    { x = 71, y = 6, color = 0xe7f3ff },
                    { x = 144, y = 15, color = 0xd6ebff },
                    { x = 115, y = 1, color = 0xf5faff },
                    { x = -36, y = 88, color = 0x516f9c }
                },
                95,
                0,
                0,
                0
        )
        if x > -1 then
            path = path .. "activity"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- announce
        x, y = myFindColor(
                { 0, 0, 1135, 638 },
                {
                    { x = 0, y = 0, color = 0xaec7fb },
                    { x = 92, y = 0, color = 0xaec7fb },
                    { x = -800, y = -5, color = 0xffffff },
                    { x = 34, y = -3, color = 0xaec7fb },
                    { x = 195, y = 546, color = 0xffffff }
                },
                95,
                0,
                0,
                0
        )
        if x > -1 then
            path = path .. "announce"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- ambush
        if scanIsAmbush() then
            path = path .. "ambush"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- login:bilibili
        if scanIslogin() then
            path = path .. "login:bilibili"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- zhanshuxueyuan
        colors = {
            { 214, 92, 0xbdb2ad },
            { 207, 201, 0xfff7e7 },
            { 118, 388, 0x94454a },
            { 49, 469, 0x395139 }
        }
        if cmpColor(colors) then
            path = path .. "zhanshuxueyuan"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- weituo
        colors = {
            { 119, 15, 0xeff3ff },
            { 146, 23, 0xeff3ff },
            { 817, 14, 0x313852 },
            { 48, 42, 0x292c4a }
        }
        if cmpColor(colors) then
            path = path .. "weituo"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- 个人信息 commander
        colors = {
            { 998, 17, 0x735d39 },
            { 1039, 29, 0xffffff },
            { 778, 428, 0x84e7ff },
            { 756, 568, 0x84e7ff }
        }
        if cmpColor(colors) then
            path = path .. "commander"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        colors = {
            { 935, 531, 0x4271ad },
            { 214, 106, 0x393431 },
            { 118, 405, 0x291010 },
            { 230, 555, 0x4a454a }
        }
        if cmpColor(colors) then
            path = path .. "book:selection"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- supplies (演习商店)
        colors = {
            { 94, 555, 0xf7cf42 },
            { 213, 566, 0x21aeef },
            { 664, 565, 0xffffff }
        }
        if cmpColor(colors) then
            path = path .. "supplies"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        colors = {
            { 693, 228, 0xff755a },
            { 477, 328, 0xffe794 },
            { 658, 327, 0x00b208 }
        }
        if cmpColor(colors) then
            path = path .. "houzhai:share"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        colors = {
            { 444, 287, 0xffe794 },
            { 489, 342, 0xffe794 },
            { 683, 292, 0x00b208 },
            { 651, 337, 0x00b208 }
        }
        if cmpColor(colors) then
            path = path .. "commander:share"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- preload  游戏封面页面
        local x_cri = -1
        local y_cri = -1
        x_cri, y_cri = myFindColor(
                { 1050, 570, 1141, 643 },
                {
                    { x = 0, y = 0, color = 0x005bac, offset = 0x101010 },
                    { x = 0, y = 15, color = 0x0056ab, offset = 0x101010 },
                    { x = 8, y = 7, color = 0xffffff, offset = 0x101010 }
                },
                95,
                0,
                0,
                0
        )
        if x_cri > -1 then
            local x_lic = -1
            local y_lic = -1
            x_lic, y_lic = myFindColor(
                    { 13, 583, 64, 627 },
                    {
                        { x = 0, y = 0, color = 0x0089be, offset = 0x101010 },
                        { x = 7, y = 7, color = 0x0089be, offset = 0x101010 },
                        { x = 4, y = 7, color = 0x0089be, offset = 0x101010 },
                        { x = -7, y = 6, color = 0x0089be, offset = 0x101010 }
                    },
                    95,
                    0,
                    0,
                    0
            )
            if x_lic > -1 then
                path = path .. "preload"
                if not SETTINGS.debug_path then
                    keepScreen(false)
                    BattleRecovery:UnregistByPath(path)
                    return path
                end
            end
        end

        -- hatch:select
        colors = {
            { 41, 23, 0x6b5d10 },
            { 904, 113, 0xff755a },
            { 225, 138, 0xffdb8c },
            { 221, 485, 0xffffff }
        }
        if cmpColor(colors) then
            path = path .. "hatch:select"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- hatch
        colors = {
            { 27, 23, 0xffba42 },
            { 645, 13, 0xffe79c },
            { 924, 32, 0xdeba63 },
            { 1067, 541, 0xffe79c }
        }
        if cmpColor(colors) then
            path = path .. "hatch"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- inform:repair
        if scanIsRepairInform() then
            path = path .. "inform:repair"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsUpdateInform() then
            path = path .. "inform:update"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsCertificate() then
            path = path .. "inform:certificate"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsOperationFile() then
            path = path .. "OpFile"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        --if scanIsFleet() then
        --    path = path .. "Fleet"
        --    if not SETTINGS.debug_path then
        --        keepScreen(false)
        --        BattleRecovery:UnregistByPath(path)
        --        return path
        --    end
        --end

        if scanSKIP() then
            path = path .. "SKIP"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanPause() then
            path = path .. "Pause"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        local btn_ov_x, btn_ov_y = scanBtnOverView()
        if btn_ov_x > -1 then
            path = path .. "OPERATION_MAP"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsOverView() then
            path = path .. "OVERVIEW"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        -- if scanIsOPERATION() then
        --     path = path .. "OPERATION"
        --     if not SETTINGS.debug_path then
        --         keepScreen(false)
        --         BattleRecovery:UnregistByPath(path)
        --         return path
        --     end
        -- end

        if scanBeaconListBtn() then
            path = path .. "MYBEACON"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsBeaconListPage() then
            path = path .. "BEACONLIST"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end

        if scanIsBattleComplete() then
            path = path .. "BattleComplete"
            if not SETTINGS.debug_path then
                keepScreen(false)
                BattleRecovery:UnregistByPath(path)
                return path
            end
        end
    end ------------ END of FORK

    -- -- yanxizuozhan
    -- colors = {{24,20,0xffd742},{974,21,0xff4573},{484,36,0xffffff},{565,38,0xf7ebf7},{862,68,0xffffff},{615,120,0xf7eff7},{110,627,0x9cbac6},{916,512,0xffffff}}
    -- if cmpColor(colors) then
    --   path = path..'yanxizuozhan'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- --  preload:startgame: conflict with 4-1
    --   -- colors = {{380,318,0xce7590},{563,320,0xce7590},{752,320,0xce7590},{758,408,0xce7590},{571,405,0xce7590},{380,405,0xce7590},{603,542,0x634717}}
    -- colors = {{393,313,0xce7590},{390,404,0xce7590},{737,314,0xce7590},{739,401,0xce7590}}
    -- if cmpColor(colors) then
    --     path = path..'preload:startgame'
    --     if not SETTINGS.debug_path then
    --       keepScreen(false)
    --       return path
    --     end
    -- end

    -- -- preload:servers
    -- colors = {{458,285,0xf7c76b},{449,340,0xf7c76b},{450,396,0xf7b239},{758,291,0xf7b239},{758,343,0xf7ae39},{766,396,0xf7b239}}
    -- if cmpColor(colors) then
    --   path = path..'preload:servers'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- -- main:weituo
    -- colors = {{976,23,0xad3452},{1051,344,0xa57500},{810,336,0x0871ad},{12,137,0x395994},{280,118,0xce4939}}
    -- if cmpColor(colors) then
    --   path = path..'main:weituo'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- -- xueyuan
    -- colors = {{24,20,{0xffd742,0xf4d761}},{975,21,{0xff4573,0xe95572}},{1127,43,0x21ffff},{587,456,0x395994},{268,518,0xfffbde}}
    -- if cmpColor(colors) then
    --   path = path..'xueyuan'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- -- tebieyanxi
    -- colors = {{24,20,{0xffd742,0xf4d761}},{975,21,{0xff4573,0xe95572}},{293,58,0x5acfff},{359,508,0xb565e7},{1127,494,0xff6563},{1126,593,0xff6563}}
    -- if cmpColor(colors) then
    --   path = path..'tebieyanxi'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- -- keyan_research
    -- colors = {{24,20,{0xffd742,0xf4d761}},{975,21,{0xff4573,0xe95572}},{193,418,0xdee7ef},{585,414,0xdeebef},{938,417,0xdee3e7},{86,568,0x314573}}
    -- if cmpColor(colors) then
    --   path = path..'keyan_research'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- -- 开发船坞 kaifachuanwu
    -- colors = {{23,20,0xffdb42},{1099,42,0x000000},{1105,21,0xefefef},{108,376,0x5a7194},{109,128,0x5a759c}}
    -- if cmpColor(colors) then
    --   path = path..'kaifachuanwu'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- -- inputbox
    -- colors = {{160,206,0xffffff},{540,217,0xffffff},{896,211,0xffffff},{112,505,0xffffff},{739,526,0xffffff},{1101,318,0xd6d6d6}}
    -- if cmpColor(colors) then
    --   path = path..'inputbox'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    -- -- update:reconnect
    -- colors = {{814,165,0xffdf42},{401,391,0xefebef},{643,392,0xffdf4a},{303,163,0xffdf7b}}
    -- if cmpColor(colors) then
    --   path = path..'update:reconnect'
    --   if not SETTINGS.debug_path then
    --     keepScreen(false)
    --     return path
    --   end
    -- end

    keepScreen(false)
    return path
end

function scanIsOilCoinGem()
    local x, y = myFindColor(
            { 479, 1, 1053, 56 },
            {
                { x = 0, y = 0, color = 0x363636 },
                { x = 20, y = 21, color = 0x6d6d6d },
                { x = 189, y = 7, color = 0xfff361 },
                { x = 203, y = 16, color = 0xcc8833 },
                { x = 378, y = 6, color = 0xfc4573 },
                { x = 382, y = 17, color = 0xf2456e }
            },
            95,
            0,
            0,
            0
    )
    return (x > -1)
end

function scanIsEventChapter()
    -- 活动关卡章节界面识别，每次活动单独制作识别
    local x, y = -1, -1
    -- local tab = {
    --     -- 神圣的悲喜剧章节界面
    --     {x = 0, y = 0, color = 0xdaebfc},
    --     {x = -12, y = -4, color = 0xdff0ff},
    --     {x = -42, y = -3, color = 0xd6eaff},
    --     {x = -68, y = 6, color = 0xcdcdcd},
    --     {x = -83, y = 9, color = 0xb69450},
    --     {x = -72, y = -20, color = 0xe5cc69}
    -- }
    -- local x, y = myFindColor({958, 558, 1135, 638}, tab, 95, 0, 0, 0)
    -- return x > -1

    --  local x, y = myFindColor({0, 0, 1135, 638},     --  最珍贵的宝物
    --  {
    --    {x=0,y=0,color=0x4e62b2},
    --    {x=-147,y=-17,color=0x76b4f5},
    --    {x=974,y=-13,color=0x5699f9},
    --    {x=-46,y=8,color=0x9ac3f8},
    --    {x=510,y=533,color=0xa6c7eb},
    --    {x=585,y=522,color=0xf7fcbe}
    --  },
    --  95, 0, 0, 0)

    -- local x, y = myFindColor({975, 569, 1127, 622},    --  激奏的Polaris
    --   {
    --     {x=0,y=0,color=0xe2b97a},
    --     {x=68,y=14,color=0xdff0ff},
    --     {x=-8,y=22,color=0xb98f56},
    --     {x=14,y=2,color=0xa9a198},
    --     {x=38,y=16,color=0xd0e9ff}
    --   },
    --   95, 0, 0, 0)

    -- local x, y = myFindColor({969, 567, 1127, 624},     --  虚拟与现实的双向镜
    -- {
    --   {x=0,y=0,color=0xfcf25d},
    --   {x=-17,y=21,color=0xf35555},
    --   {x=17,y=20,color=0x8e2b2b},
    --   {x=-2,y=20,color=0xffffff},
    --   {x=78,y=14,color=0xdcedfd}
    -- },
    -- 95, 0, 0, 0)

    --  浮樱影华
    --local x, y = myFindColor(
    --        { 969, 77, 1018, 128 },
    --        {
    --            { x = 0, y = 0, color = 0xa5e275, offset = 0x101010 },
    --            { x = 16, y = 0, color = 0x2aba58, offset = 0x101010 },
    --            { x = -8, y = 19, color = 0x2cb352, offset = 0x101010 }
    --        },
    --        95,
    --        0,
    --        0,
    --        0
    --)

    --  北境序曲
    --  local x, y = myFindColor({958, 69, 1029, 135},
    --   {
    --     {x=0,y=0,color=0xfdfdfd},
    --     {x=-12,y=1,color=0x8e5c45},
    --     {x=-5,y=-1,color=0x898991},
    --     {x=5,y=16,color=0x646474}
    --   },
    --   95, 0, 0, 0)

    --  斯图尔特的硝烟
    -- local x, y = myFindColor({0, 0, 1135, 638},
    -- {
    --   {x=0,y=0,color=0x252536},
    --   {x=0,y=42,color=0x252536},
    --   {x=-54,y=228,color=0x3b976e},
    --   {x=-75,y=224,color=0x8ba4ac},
    --   {x=401,y=212,color=0x5cbd8d}
    -- },
    -- 95, 0, 0, 0)

    --  微层混合
    -- local x, y = myFindColor({ 966, 77, 1017, 125 },
    --         {
    --             { x = 0, y = 0, color = 0xf9f9f9 },
    --             { x = 1, y = -4, color = 0xf8f8fa },
    --             { x = 6, y = -8, color = 0xf7fefe },
    --             { x = 4, y = 3, color = 0x3b6ad6 },
    --             { x = 0, y = -14, color = 0x3a5db3 }
    --         },
    --         95, 0, 0, 0)

    -- 苍红回响
    -- local x, y = myFindColor({965, 77, 1022, 127},
    -- {
    --   {x=0,y=0,color=0xedc4bb},
    --   {x=-19,y=-9,color=0x755824},
    --   {x=-9,y=16,color=0x3a3a3a},
    --   {x=16,y=7,color=0x2b2b2b}
    -- },
    -- 95, 0, 0, 0)

    -- --  夜色下的归途
    -- local x, y = myFindColor({0, 0, 1135, 638},
    -- {
    --   {x=0,y=0,color=0xdaebfd},
    --   {x=23,y=5,color=0xd9ecff},
    --   {x=106,y=184,color=0x252536},
    --   {x=106,y=226,color=0x252536},
    --   {x=649,y=403,color=0xeadac6},
    --   {x=978,y=20,color=0x252841}
    -- },
    -- 95, 0, 0, 0)

    --  穹顶下的圣咏曲
    -- local x, y = myFindColor({961, 75, 1021, 126},
    -- {
    --   {x=0,y=0,color=0xefe9f6},
    --   {x=-9,y=11,color=0xe2dfea},
    --   {x=13,y=26,color=0x707889},
    --   {x=-2,y=25,color=0xb69d6c},
    --   {x=-14,y=23,color=0x888899}
    -- },
    -- 95, 0, 0, 0)

    -- local x1, y1 = myFindColor({241, 199, 361, 286},
    -- {
    --   {x=0,y=0,color=0x252536},
    --   {x=0,y=44,color=0x252536},
    --   {x=55,y=21,color=0xffffff}
    -- },
    -- 95, 0, 0, 0)

    -- local x2, y2 = myFindColor({122,209,239,295},
    -- {
    --   {x=0,y=0,color=0x252536},
    --   {x=0,y=43,color=0x252536},
    --   {x=54,y=20,color=0xffffff}
    -- },
    -- 95, 0, 0, 0)

    -- 峡湾间的反击

    -- local  x, y = myFindColor({0, 0, 1135, 638},
    -- {
    --   {x=0,y=0,color=0x252536},
    --   {x=-22,y=21,color=0x252536},
    --   {x=-270,y=134,color=0x111111},
    --   {x=727,y=120,color=0xff2020},
    --   {x=-145,y=-388,color=0xdbedff},
    --   {x=-167,y=-384,color=0xd0e9ff}
    -- },
    -- 95, 0, 0, 0)
    -- return (x > -1) -- and ((x1 > -1) or (x2 > -1))

    -- 永夜幻光
    -- local x, y = myFindColor({968, 80, 1020, 126},
    -- {
    --   {x=0,y=0,color=0xd95236},
    --   {x=-2,y=17,color=0xd8b775},
    --   {x=-7,y=-1,color=0xfaf2c1},
    --   {x=-5,y=11,color=0x8b1310},
    --   {x=5,y=12,color=0xccae6a}
    -- },
    -- 95, 0, 0, 0)

    -- 铁血、音符与誓言
    -- x, y = myFindColor({967, 79, 1019, 127},
    -- {
    --   {x=0,y=0,color=0xfc4747},
    --   {x=4,y=6,color=0xebefef},
    --   {x=0,y=-6,color=0xcb3735},
    --   {x=-7,y=0,color=0xb7b6b6}
    -- },
    -- 95, 0, 0, 0)

    -- 蝶海梦花
    -- x, y = myFindColor({971, 83, 1016, 122},
    -- {
    --   {x=0,y=0,color=0xffffff},
    --   {x=11,y=6,color=0x031f61},
    --   {x=4,y=-12,color=0x5286b4},
    --   {x=-7,y=2,color=0x234180},
    --   {x=9,y=-2,color=0xffffff}
    -- },
    -- 95, 0, 0, 0)

    -- 划破海空之翼
    -- x, y = myFindColor({0, 0, 1135, 638},
    -- {
    --   {x=0,y=0,color=0xdaebfd},
    --   {x=23,y=4,color=0xdbedff},
    --   {x=106,y=183,color=0x252536},
    --   {x=30,y=480,color=0x092042},
    --   {x=1006,y=177,color=0xcbd3d3},
    --   {x=1005,y=198,color=0x174e77}
    -- },
    -- 95, 0, 0, 0)

    -- 激唱的Universe
    -- x, y = myFindColor({968, 75, 1021, 129},
    -- {
    --   {x=0,y=0,color=0xdbe3ff},
    --   {x=12,y=0,color=0xa3bbee},
    --   {x=-1,y=-4,color=0xe8ecfe},
    --   {x=-6,y=-11,color=0x3c4578}
    -- },
    -- 95, 0, 0, 0)

    -- 假日航线
    -- x, y =
    --     myFindColor(
    --     {967, 74, 1018, 124},
    --     {
    --         {x = 0, y = 0, color = 0xfff9ff},
    --         {x = -6, y = -7, color = 0xf777bc},
    --         {x = 0, y = 15, color = 0xd74369},
    --         {x = 13, y = -6, color = 0xfa82a4}
    --     },
    --     95,
    --     0,
    --     0,
    --     0
    -- )

    -- 负象限作战
    -- x, y =
    --     myFindColor(
    --     {963, 75, 1020, 126},
    --     {
    --         {x = 0, y = 0, color = 0x9a1918},
    --         {x = 18, y = -6, color = 0x343030},
    --         {x = 26, y = -12, color = 0xdb3030},
    --         {x = -3, y = 4, color = 0xb2b1ae}
    --     },
    --     95,
    --     0,
    --     0,
    --     0
    -- )
    -- return (x > -1)

    -- 破晓冰华
    -- local x, y =
    --     myFindColor(
    --     {970, 560, 1130, 630},
    --     {
    --         {x = 0, y = 0, color = 0x6eb3d4},
    --         {x = -27, y = 14, color = 0x31639d},
    --         {x = -2, y = 29, color = 0x49a9cf},
    --         {x = 26, y = 7, color = 0xd2e8fc},
    --         {x = 67, y = 12, color = 0xdaebfc},
    --         {x = 95, y = 2, color = 0xeaf2ff}
    --     },
    --     95,
    --     0,
    --     0,
    --     0
    -- )
    -- return (x > -1)

    -- 箱庭疗法
    -- local x, y =
    --     myFindColor(
    --     {961, 72, 1030, 136},
    --     {
    --         {x = 0, y = 0, color = 0xffffff},
    --         {x = 0, y = 9, color = 0x949591},
    --         {x = -12, y = 12, color = 0x75b9db},
    --         {x = -3, y = -8, color = 0xffffff}
    --     },
    --     95,
    --     0,
    --     0,
    --     0
    -- )

    -- 复兴的赞美诗
    -- local x, y =
    --     myFindColor(
    --     {966, 75, 1019, 130},
    --     {
    --         {x = 0, y = 0, color = 0xffffff},
    --         {x = 13, y = 11, color = 0xd1c8c0},
    --         {x = -3, y = -20, color = 0x698d65},
    --         {x = 3, y = -21, color = 0xdbd5c4},
    --         {x = 9, y = -21, color = 0xa63a41}
    --     },
    --     95,
    --     0,
    --     0,
    --     0
    -- )

    -- 镜位螺旋
    -- local x, y =
    --     myFindColor(
    --     {965, 78, 1019, 126},
    --     {
    --         {x = 0, y = 0, color = 0xc5cfee},
    --         {x = -10, y = -4, color = 0x91e9fc},
    --         {x = 10, y = -11, color = 0x8be0ff},
    --         {x = -2, y = -8, color = 0xfbfdff},
    --         {x = -5, y = 5, color = 0x74b8fc}
    --     },
    --     95,
    --     0,
    --     0,
    --     0
    -- )

    ---- 响彻碧海的偶像歌
    --local x, y = myFindColor({ 138, 194, 638, 436 },
    --        {
    --            { x = 0, y = 0, color = 0x252536 },
    --            { x = 0, y = 45, color = 0x252536 },
    --            { x = 387, y = 146, color = 0xee3333 },
    --            { x = 364, y = 164, color = 0xff44dd },
    --            { x = 385, y = 172, color = 0xffcc22 },
    --            { x = 392, y = 188, color = 0x22bb22 }
    --        },
    --        95, 0, 0, 0)

    ---- 碧海光粼
    --local x, y = myFindColor({ 963, 75, 1019, 124 },
    --        {
    --            { x = 0, y = 0, color = 0xd3dbe3 },
    --            { x = -6, y = -24, color = 0xf5fdff },
    --            { x = -17, y = -10, color = 0x9ccedf },
    --            { x = 6, y = -8, color = 0x9df8f8 }
    --        },
    --        95, 0, 0, 0)


    -- 穹顶下的圣咏曲
    --local x, y = myFindColor({ 961, 75, 1021, 126 },
    --        {
    --            { x = 0, y = 0, color = 0xefe9f6 },
    --            { x = -9, y = 11, color = 0xe2dfea },
    --            { x = 13, y = 26, color = 0x707889 },
    --            { x = -2, y = 25, color = 0xb69d6c },
    --            { x = -14, y = 23, color = 0x888899 }
    --        },
    --        95, 0, 0, 0)

    --杰诺瓦的焰火
    --local x, y = myFindColor({ 0, 0, 1135, 638 },
    --        {
    --            { x = 0, y = 0, color = 0xdaebfd },
    --            { x = 32, y = -2, color = 0xe9f3ff },
    --            { x = 189, y = 377, color = 0x252536 },
    --            { x = 289, y = 133, color = 0x553344 }
    --        },
    --        95, 0, 0, 0)
    -- 交汇世界的弧光
    --local x, y = myFindColor({ 965, 76, 1021, 123 },
    --        {
    --            { x = 0, y = 0, color = 0xeff4ff },
    --            { x = 23, y = 24, color = 0x346db6 },
    --            { x = 27, y = 11, color = 0x60c5e2 }
    --        },
    --        95, 0, 0, 0)

    -- 逆转彩虹之塔
    --local x, y = myFindColor({ 966, 75, 1019, 127 },
    --        {
    --            { x = 0, y = 0, color = 0x182030 },
    --            { x = 27, y = 4, color = 0x1e2636 },
    --            { x = 18, y = -10, color = 0x812f46 },
    --            { x = 9, y = 8, color = 0x32324b },
    --            { x = 24, y = -25, color = 0x333a6c }
    --        },
    --        95, 0, 0, 0)

    -- 深度回音
    --local x, y = myFindColor({ 968, 80, 1014, 125 },
    --        {
    --            { x = 0, y = 0, color = 0x29adde },
    --            { x = 0, y = 27, color = 0x50d4f5 },
    --            { x = -10, y = 17, color = 0x20afd1 }
    --        },
    --        95, 0, 0, 0)

    -- 虚像构筑之塔
    local x, y = myFindColor({ 968, 76, 1020, 129 },
            {
                { x = 0, y = 0, color = 0xd81b2c },
                { x = -2, y = 14, color = 0x34c2e8 },
                { x = -3, y = -11, color = 0x09294d },
                { x = 7, y = 13, color = 0x16559d }
            },
            95, 0, 0, 0)

    return (x > -1)

end

--  档案：围剿斯佩伯爵
function scanIsSpee()
    local x, y = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0x252536 },
                { x = -654, y = -204, color = 0xdbedff },
                { x = -656, y = -182, color = 0x5e8fa0 },
                { x = 328, y = -197, color = 0xdf8b8b },
                { x = -772, y = -187, color = 0x585882 }
            },
            95,
            0,
            0,
            0
    )

    -- 右上角密钥图标
    local x_key, y_key = myFindColor(
            { 999, 72, 1055, 128 },
            {
                { x = 0, y = 0, color = 0xe9ebeb },
                { x = 20, y = -2, color = 0xc4c7c9 },
                { x = 8, y = -7, color = 0x74a5c8 },
                { x = -6, y = -24, color = 0x4a7ebb }
            },
            95,
            0,
            0,
            0
    )

    return (x > -1) and (x_key > -1)
end

--  档案：异色格象
function scanIsOppcb()
    local x, y = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0xdaebfc },
                { x = 27, y = 6, color = 0xdeeefe },
                { x = -120, y = -13, color = 0x352352 },
                { x = 1006, y = 423, color = 0xbd3a79 },
                { x = 1005, y = 444, color = 0x22223b },
                { x = 892, y = 20, color = 0x477bb7 }
            },
            95,
            0,
            0,
            0
    )

    -- 右上角密钥图标
    local x_key, y_key = myFindColor(
            { 999, 72, 1055, 128 },
            {
                { x = 0, y = 0, color = 0xe9ebeb },
                { x = 20, y = -2, color = 0xc4c7c9 },
                { x = 8, y = -7, color = 0x74a5c8 },
                { x = -6, y = -24, color = 0x4a7ebb }
            },
            95,
            0,
            0,
            0
    )

    return (x > -1) and (x_key > -1)
end

--  档案：光与影的鸢尾之华
function scanIsIoLnD()
    local x_1, y_1 = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0xd0e9ff },
                { x = 27, y = -6, color = 0xdeeefe },
                { x = 61, y = 125, color = 0x252536 },
                { x = 115, y = 144, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )

    local x_2, y_2 = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0xd0e9ff },
                { x = 27, y = -6, color = 0xdeeefe },
                { x = 136, y = 138, color = 0x252536 },
                { x = 190, y = 153, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )

    -- 右上角密钥图标
    local x_key, y_key = myFindColor(
            { 999, 72, 1055, 128 },
            {
                { x = 0, y = 0, color = 0xe9ebeb },
                { x = 20, y = -2, color = 0xc4c7c9 },
                { x = 8, y = -7, color = 0x74a5c8 },
                { x = -6, y = -24, color = 0x4a7ebb }
            },
            95,
            0,
            0,
            0
    )

    return ((x_1 > -1) or (x_2 > -1)) and (x_key > -1)
end

--  关卡确认
function scanIsLevelConfirm()
    local colors = {
        { 927, 438, 0xffffff },
        { 837, 451, 0xffffff },
        { 762, 436, 0xd69652 }
    }
    return cmpColor(colors)
end

-- 关卡确认页面：自动寻敌
function scanLevelConfirmAutoSeek()
    local x, y = myFindColor(
            { 691, 508, 800, 550 },
            {
                { x = 0, y = 0, color = 0x9aea59 },
                { x = 6, y = 5, color = 0x9aea59 },
                { x = 0, y = 5, color = 0x9aea59 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

--  舰队选择
function scanIsFleetSelect()
    local x, y = myFindColor(
            { 833, 488, 1054, 597 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = -165, y = -1, color = 0xdb9551 },
                { x = -4, y = 4, color = 0xf7cb41 },
                { x = 3, y = 31, color = 0xf2bf41 }
            },
            95,
            0,
            0,
            0
    )

    return x > -1
end

function scanTeamChooseBtn_1()
    local x, y = myFindColor(
            { 882, 134, 965, 218 },
            {
                { x = 0, y = 0, color = 0xe9daca },
                { x = -49, y = -2, color = 0xbc9153 },
                { x = -52, y = 46, color = 0xcc904d },
                { x = 0, y = 46, color = 0xcb8e48 },
                { x = -12, y = 16, color = 0xfff9f3 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanTeamChooseBtn_2()
    local x, y = myFindColor(
            { 885, 255, 964, 336 },
            {
                { x = 0, y = 0, color = 0xe6dac9 },
                { x = -47, y = 0, color = 0xbf8f53 },
                { x = -50, y = 47, color = 0xcd8e47 },
                { x = -1, y = 47, color = 0xcb8d47 },
                { x = -12, y = 17, color = 0xfffaf4 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanSubmChooseBtn()
    local x, y = myFindColor(
            { 884, 385, 965, 469 },
            {
                { x = 0, y = 0, color = 0xe6dac9 },
                { x = -47, y = 0, color = 0xbf8f53 },
                { x = -50, y = 47, color = 0xcd8e47 },
                { x = -1, y = 47, color = 0xcb8d47 },
                { x = -12, y = 17, color = 0xfffaf4 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsAutoConfirm()
    -- inform 紧急委托等
    local colors = {
        { 292, 124, 0xffffff },
        { 835, 524, 0xffffff },
        { 794, 135, 0xce4542 },
        { 639, 504, 0x5a96de }
    }
    return cmpColor(colors)
end

--  三按钮对话框：退役
function scanIsInform3Confirm3()
    -- local colors = {{293,154,0xffffff},{828,495,0xffffff},{627,472,0x5a96de}}
    -- local colors_and = {{323,472,0x4a8ad6},{678,473,0x4a8ad6}}
    -- return (cmpColor(colors) and cmpColor(colors_and))
    local x, y = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 78, y = 27, color = 0xffffff },
                { x = 525, y = 279, color = 0x507cb6, offset = 0x0f0f0f },
                { x = 505, y = 322, color = 0x4f89d0, offset = 0x121212 },
                { x = 25, y = 277, color = 0x3b6193, offset = 0x131313 },
                { x = 29, y = 323, color = 0x4f89cf, offset = 0x121212 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

--  单按钮对话框
function scanIsInform1Confirm()
    -- local colors = {{293,154,0xffffff},{828,495,0xffffff},{627,472,0x5a96de},{499,471,0x4a8ad6}}
    -- local colors_not = {{323,472,0x4a8ad6},{678,473,0x4a8ad6}}
    -- return (cmpColor(colors) and not cmpColor(colors_not))

    local x, y = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 78, y = 27, color = 0xffffff },
                { x = 345, y = 279, color = 0x507db7, offset = 0x0f0f0f },
                { x = 203, y = 277, color = 0x3a6193, offset = 0x131313 },
                { x = 206, y = 323, color = 0x4e89cf, offset = 0x121212 }
            },
            95,
            0,
            0,
            0
    )

    --  因为单按钮的位置和三按钮中间一个按钮的位置重叠，所以要多一步判断
    if x > -1 and scanIsInform3Confirm3() == false then
        return true
    else
        return false
    end
end

--  对话框：确定、取消
function scanIsInform2Confirm1()
    -- 加入模糊识别机制
    local x, y = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 78, y = 27, color = 0xffffff },
                { x = 473, y = 283, color = 0x5181be, offset = 0x0f0f0f },
                { x = 341, y = 280, color = 0x3b6093, offset = 0x121212 },
                { x = 205, y = 284, color = 0x9f9f9f, offset = 0x0f0f0f },
                { x = 78, y = 282, color = 0x807f80, offset = 0x121212 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 大舰队任务信息：立即前往、我知道了
function scanIsFleetInform2Confirm1()
    local x, y = myFindColor(
            { 301, 130, 834, 525 },
            {
                { x = 0, y = 0, color = 0x4a6dd6 },
                { x = 169, y = 0, color = 0x4a6dd6 },
                { x = 166, y = 25, color = 0x4a6dd6 },
                { x = -1, y = 24, color = 0x4a6dd6 },
                { x = -39, y = 23, color = 0x737173 },
                { x = -207, y = 22, color = 0x737173 },
                { x = -208, y = -3, color = 0x737173 },
                { x = -38, y = -2, color = 0x737173 }
            },
            95,
            0,
            0,
            0
    )

    return x > -1
end

function scandnIsInform1ConfirmLarge()
    -- GoldBox info in formation
    local colors = {
        { 823, 105, 0xffd742 },
        { 309, 98, 0xfffbe7 },
        { 834, 201, 0xffb600 },
        { 501, 486, 0xffdb4a },
        { 644, 518, 0xffaa10 }
    }
    return cmpColor(colors)
end

function scanIsRepairInform()
    local colors = {
        { 735, 379, 0x5286c6 },
        { 410, 416, 0xa5a2a5 },
        { 293, 166, 0xffffff },
        { 836, 467, 0xffffff }
    }
    return cmpColor(colors)
end

function scanIsUpdateInform()
    local colors = {
        { 307, 186, 0xffffff },
        { 262, 154, 0xffffff },
        { 429, 455, 0xffffff },
        { 722, 457, 0xffffff }
    }
    return cmpColor(colors)
end

function scanIsCertificate()
    -- 应援资格验证码
    local colors = {
        { 979, 498, 0x3c72e7 },
        { 859, 526, 0xffffff },
        { 167, 130, 0xffffff },
        { 840, 528, 0x3c72e7 },
        { 976, 529, 0x3c72e7 },
        { 291, 207, 0x1f50c3 },
        { 232, 201, 0x2558be }
    }
    return cmpColor(colors)
end

function scanInformType()
    -- keepScreen(true)
    -- x, y = myFindColor({323, 242, 820, 332}, {{x=0,y=0,color=0x9cdb42}}, 95, 0, 0, 0)
    -- if x > -1 then
    --   keepScreen(false)
    --   return 'failed'
    -- else
    --   keepScreen(false)
    --   return 'other'
    -- end
    return "other"
end

function scanLootColor()
    local colors = {}
    keepScreen(true)

    colors = { { 913, 152, 0xffffff }, { 978, 151, 0xffffff } } -- 1: white 4: gold
    if cmpColor(colors) then
        keepScreen(false)
        STATS[1] = STATS[1] + 1
        setNumberConfig("1star", getNumberConfig("1star", 0) + 1)
        return 1
    end

    colors = { { 821, 369, 0x63a6ef }, { 904, 386, 0x6bd3f7 } }
    if cmpColor(colors) then
        keepScreen(false)
        STATS[2] = STATS[2] + 1
        setNumberConfig("2star", getNumberConfig("2star", 0) + 1)
        return 2
    end

    colors = { { 830, 388, 0xff55ef }, { 915, 388, 0xff55ef } }
    if cmpColor(colors) then
        keepScreen(false)
        STATS[3] = STATS[3] + 1
        setNumberConfig("3star", getNumberConfig("3star", 0) + 1)
        return 3
    end

    colors = { { 844, 378, 0xffe35a }, { 902, 384, 0xfff363 } }
    if cmpColor(colors) then
        keepScreen(false)
        STATS[4] = STATS[4] + 1
        setNumberConfig("4star", getNumberConfig("4star", 0) + 1)
        return 4
    end

    -- not identified = 1 star
    keepScreen(false)
    STATS[1] = STATS[1] + 1
    setNumberConfig("1star", getNumberConfig("1star", 0) + 1)
    return 1
end

-- keepsSreen outside
function scanChapter()
    local mode = nil
    local isSP = false
    local colors = {}

    isSP = scanIsSP()

    -- normal/hard

    if scanLevelNormalBtn() then
        mode = "hard"
    elseif scanLevelHardBtn() then
        mode = "normal"
    else
        mode = "other"
    end

    -- level 1
    colors = { { 122, 422, 0x212431 }, { 123, 454, 0x212431 } }
    if cmpColor(colors, 95) then
        return isSP, mode, 1
    end

    -- level 2
    colors = {
        { 141, 77, 0x4b5eaa },
        { 149, 81, 0x4b5da6 },
        { 713, 418, 0x252536 },
        { 140, 85, 0xccecff }
    }
    if cmpColor(colors, 95) then
        return isSP, mode, 2
    end

    -- level 3
    colors = {
        { 149, 73, 0xe6f1ff },
        { 127, 75, 0xd8ebff },
        { 149, 82, 0xd5eaff },
        { 347, 207, 0x252536 },
        { 140, 77, 0x4c5ea5 }
    }
    if cmpColor(colors, 95) then
        return isSP, mode, 3
    end

    -- level 4
    colors = { { 201, 287, 0x212431 }, { 201, 323, 0x212431 } }
    if cmpColor(colors, 95) and not cmpColor({ { 237, 381, 0x181829 } }) then
        return isSP, mode, 4
    end

    -- level 5
    colors = { { 204, 341, 0x212431 }, { 205, 375, 0x212431 } }
    if cmpColor(colors, 95) and not cmpColor({ { 237, 381, 0x181829 } }) then
        return isSP, mode, 5
    end

    -- level 6
    colors = { { 800, 458, 0x212431 }, { 802, 493, 0x212431 } }
    if cmpColor(colors, 95) then
        return isSP, mode, 6
    end

    -- level 7
    colors = { { 176, 446, 0x212431 }, { 176, 478, 0x212431 } }
    if cmpColor(colors, 95) then
        return isSP, mode, 7
    end

    -- level 8
    colors = {
        { 141, 75, 0xddeeff },
        { 162, 70, 0xf3f3ff },
        { 478, 178, 0x252536 },
        { 134, 82, 0xcdeeff },
        { 149, 81, 0xd8e9ff }
    }
    if cmpColor(colors, 95) then
        return isSP, mode, 8
    end

    -- level 9
    colors = {
        { 144, 70, 0xdcedff },
        { 143, 78, 0xddeeff },
        { 150, 77, 0xddeeff },
        { 135, 83, 0xd0e9ff },
        { 194, 234, 0x262532 }
    }
    if cmpColor(colors, 95) then
        return isSP, mode, 9
    end

    -- level 10
    colors = {
        { 147, 76, 0xdeefff },
        { 147, 80, 0xd9eaff },
        { 155, 82, 0xd3ecff },
        { 167, 226, 0x252536 },
        { 139, 72, 0xe0efff }
    }
    if cmpColor(colors) then
        return isSP, mode, 10
    end

    -- level 11
    colors = {
        { 152, 73, 0xe3f4ff },
        { 154, 82, 0xd3ebff },
        { 260, 220, 0x252536 },
        { 141, 72, 0xe0f1ff }
    }
    if cmpColor(colors, 97) then
        return isSP, mode, 11
    end

    -- level 12
    colors = {
        { 136, 74, 0xddeeff },
        { 141, 83, 0xd4e9ff },
        { 149, 77, 0x4c5eaa },
        { 150, 77, 0x4c5eaa },
        { 154, 83, 0x4b5d98 },
        { 155, 82, 0x4b5da1 },
        { 157, 81, 0x4c5ea5 },
        { 150, 81, 0xd7e8ff }
    }
    if cmpColor(colors, 97) then
        return isSP, mode, 12
    end

    -- level 13
    colors = {
        { 136, 74, 0xddeeff },
        { 141, 83, 0xd4e9ff },
        { 149, 77, 0xdcedff },
        { 150, 77, 0xdcedff },
        { 154, 83, 0xcfe3fc },
        { 155, 82, 0xd6e9ff },
        { 157, 81, 0xdbecfd },
        { 150, 81, 0x4b5da4 }
    }
    if cmpColor(colors) then
        return isSP, mode, 13
    end

    -- level 14
    colors = {
        { 154, 71, 0xeaf3ff },
        { 154, 84, 0xd0e9ff },
        { 147, 81, 0xd7e8ff },
        { 151, 78, 0x495fb7 },
        { 137, 77, 0x4558a4 },
        { 139, 71, 0xe0f1ff },
        { 135, 74, 0xddeeff },
        { 140, 84, 0xd8ebfd }
    }
    if cmpColor(colors) then
        return isSP, mode, 14
    end

    -- level tebieyanxi
    if scanIsTBYX() then
        return false, "hard", Lid_TBYXLevel
    end

    return isSP, mode, nil
end

function scanIsKeyanReady()
    return cmpColor({ { 678, 598, 0xef0021 } })
end

function scanIsTechAcademyReady()
    local md_ = GetRectColorAvg(432, 393, 441, 401)
    return md_.r > md_.g and md_.r > md_.b
end

function scanIsFirstWeituoFinished()
    return cmpColor({ { 357, 274, 0xffa610 } })
end

function scanIsLastWeituoEmpty()
    local colors = { { 299, 513, 0xffd3ef } }
    return cmpColor(colors)
end

function scanIsWeituoFinishedXY(n)
    local all_colors = {
        { { 357, 274, 0xffa610 } },
        { { 357, 356, 0xffa610 } },
        { { 358, 439, 0xffa610 } },
        { { 358, 522, 0xffa610 } }
    }
    local ret = cmpColor(all_colors[n])
    if ret then
        return all_colors[n][1][1], all_colors[n][1][2]
    else
        return nil, nil
    end
end

function scanFMT_RightArrow()
    local x, y = myFindColor(
            { 778, 267, 854, 337 },
            {
                { x = 0, y = 0, color = 0xeafbfd },
                { x = 10, y = 9, color = 0xd7f9ff },
                { x = 0, y = 19, color = 0xcdf3ff },
                { x = 7, y = 12, color = 0xd1f2ff },
                { x = 4, y = 5, color = 0xddfbff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanFMT_LeftArrow()
    local x, y = myFindColor(
            { 3, 269, 72, 339 },
            {
                { x = 0, y = 0, color = 0xebfcff },
                { x = -10, y = 10, color = 0xd2f4ff },
                { x = 0, y = 19, color = 0xccf3ff },
                { x = -8, y = 10, color = 0xd6f4ff },
                { x = -1, y = 3, color = 0xddeeff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanFMT_NoAwards()
    local x, y = myFindColor(
            { 857, 237, 959, 366 },
            {
                { x = 0, y = 0, color = 0x292c39 },
                { x = 53, y = -1, color = 0x292a38 },
                { x = 0, y = 55, color = 0x272935 },
                { x = 54, y = 55, color = 0x272835 },
                { x = -19, y = -39, color = 0xffffff },
                { x = 8, y = -32, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- keepScreen outside
function scanFormationType()
    local colors = { { 181, 568, 0xadd3f7 } }
    if not cmpColor(colors, 95) then
        return "normal"
    end

    if cmpColor({ { 735, 105, 0x314573 }, { 798, 103, 0x394573 } }, 90) then
        if not scanFMT_RightArrow() and not scanFMT_LeftArrow() and scanFMT_NoAwards() then
            return "beacon"
        else
            return "meiri"
        end
    else
        return "yanxi"
    end
end

-- keepScreen outside
function scanAvailableMeiriXY()
    local colors = { { 671, 188, 0xfffbff } }
    if not cmpColor(colors) then
        return 671, 188
    end

    local colors = { { 671, 313, 0xfffbff } }
    if not cmpColor(colors) then
        return 671, 313
    end

    local colors = { { 671, 439, 0xfffbff } }
    if not cmpColor(colors) then
        return 671, 439
    end

    local colors = { { 671, 561, 0xfffbff } }
    if not cmpColor(colors) then
        return 671, 561
    end

    return nil, nil
end

function scanRecycle1or2Rows()
    colors = {
        { 100, 66, 0x000000 },
        { 251, 66, 0x100800 },
        { 402, 66, 0x180c00 },
        { 553, 66, 0x211000 },
        { 704, 66, 0x211000 },
        { 855, 66, 0x100800 },
        { 1006, 66, 0x000008 }
    }
    -- for _,t in ipairs(colors) do
    --   printf('0x%x',myGetColor(t[1],t[2]))
    -- end
    if cmpColor(colors, 70) then
        return true
    end
    return false
end

function scanCardsForRecycle()
    -- 0xcecbce 白
    -- 0x7bc3de 蓝
    -- 0x8c71ce 紫
    -- 0xefa263 金
    -- 0xb58284 婚

    keepScreen(true)

    local yRow = {}
    if scanRecycle1or2Rows() then
        print("1,2 rows")
        yRow = { 75, 281, 487 }
    else
        print("3+ rows")
        yRow = { 66, 272, 478 }
    end

    local ret = {}
    for _, y in ipairs(yRow) do
        for _, x in ipairs({ 100, 251, 402, 553, 704, 855, 1006 }) do
            local r, g, b = myGetColorRGB(x, y)
            -- printf('%x', myGetColor(x,y))
            local level = 0
            if colorMatch(r, g, b, "0xcecbce") then
                level = 1
            elseif colorMatch(r, g, b, "0x7bc3de") then
                level = 2
            elseif colorMatch(r, g, b, "0x8c71ce") then
                level = 3
            elseif colorMatch(r, g, b, "0xefa263") then
                level = 4
            elseif colorMatch(r, g, b, "0xb58284") then
                level = 5
            end
            print(level)
            table.insert(ret, { x, y + 15, level })
        end
    end
    keepScreen(false)
    return ret
end

function scanWeituoBtn()
    local x, y = myFindColor(
            { 825, 554, 972, 627 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 2, y = -4, color = 0xffffff },
                { x = -6, y = 0, color = 0xffffff },
                { x = 6, y = 0, color = 0xffffff },
                { x = -3, y = 6, color = 0xfbfbfb },
                { x = 56, y = -1, color = 0xfbfbfb },
                { x = 59, y = 7, color = 0xffffff },
                { x = 66, y = 9, color = 0xfefefe }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function ws_scanWeituoBtn()
    local x, y = myFindColor(
            { 709, 513, 851, 584 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 2, y = -4, color = 0xffffff },
                { x = -6, y = 0, color = 0xffffff },
                { x = 6, y = 0, color = 0xffffff },
                { x = -3, y = 6, color = 0xfbfbfb },
                { x = 56, y = -1, color = 0xfbfbfb },
                { x = 59, y = 7, color = 0xffffff },
                { x = 66, y = 9, color = 0xfefefe }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanWeituoDot()
    r_, g_, b_ = myGetColorRGB(958, 567)
    return (r_ > 100 and g_ < 100 and b_ < 100)
end

function ws_scanWeituoDot()
    r_ws, g_ws, b_ws = myGetColorRGB(837, 521)
    return (r_ws > 100 and g_ws < 100 and b_ws < 100)
end

function scanCanConfirmWeituo()
    local colors = { { 916, 583, 0xffdb4a } }
    return cmpColor(colors)
end

function scanCanStartWeituo()
    local colors = { { 866, 382, 0xffd742 } }
    return cmpColor(colors)
end

function scanIsRenwuReady()
    return cmpColor({ { 844, 599, 0xb55542 } })
end

function scanIsFirstRenwuReady()
    return cmpColor({ { 1055, 93, 0xef0018 } })
end

function scanIsSidebarOpen()
    -- not 战略图标
    return not cmpColor({ { 1088, 368, 0xdedfe7 }, { 1100, 404, 0xdedfe7 } })
end

function scanIsFleetExpanded()
    -- 选队菜单是否展开
    local pt_ = myFindColors(
            { 873, 198, 1048, 648 },
            {
                { x = 0, y = 0, color = 0x2c2c2e },
                { x = -83, y = -11, color = 0xc7cfe7 },
                { x = 62, y = 15, color = 0xa7afc8 },
                { x = 26, y = 14, color = 0x9ba1b0 },
                { x = -20, y = 5, color = 0x2c2c2e }
            },
            95,
            0,
            0,
            0
    )
    return #pt_ > 0
end

function scanIsSubTeamCanSelect()
    -- 潜艇选队按钮是否可用
    local pt_ = myFindColors({ 898, 381, 979, 463 },
            {
                { x = 0, y = 0, color = 0xbd8e52 },
                { x = 42, y = -1, color = 0xefdbce },
                { x = 41, y = 37, color = 0xce9652 },
                { x = -6, y = 40, color = 0xc68a42 } }
    , 95, 0, 0, 0)
    return #pt_ > 0
end

function scanJunhuoshang()
    local table = {
        { x = 0, y = 0, color = 0xb5b2b5 },
        { x = -16, y = 19, color = 0xb5b2b5 },
        { x = 9, y = 8, color = 0xb5b2b5 }
    }

    local set = PosSet:new()
    local points = myFindColors({ 633, 535, 1055, 579 }, table, 97, 0, 0, 0)
    addToSet(set, points)
    return set
end

function scanWeituoExp()
    local table = {
        { x = 0, y = 0, color = 0x08fb00 },
        { x = 5, y = 28, color = 0x10ff00 },
        { x = 53, y = 7, color = 0x10ff00 },
        { x = 22, y = 36, color = 0x7bbeff }
    }

    local set = PosSet:new()
    local points = myFindColors({ 228, 83, 671, 607 }, table, 95, 0, 0, 0)
    addToSet(set, points)

    table = {
        { x = 0, y = 0, color = 0x75fb4c },
        { x = 5, y = 28, color = 0x75fb4c },
        { x = 53, y = 7, color = 0x75fb4c },
        { x = -241, y = 23, color = 0x636774 },
        { x = 22, y = 36, color = 0x89bbf2 }
    }
    points = myFindColors({ 228, 83, 671, 607 }, table, 95, 0, 0, 0)
    addToSet(set, points)

    return set
end

function scanWeituoScore(isNormal, box)
    local table = nil
    local minRank = nil

    table = {
        -- 进行中-绿色方块
        { x = 0, y = 0, color = 0x89bd69 },
        { x = 35, y = -9, color = 0x89bd69 },
        { x = 35, y = 6, color = 0x89bd69 },
        { x = -32, y = -8, color = 0x89bd68 },
        { x = -32, y = 6, color = 0x89bd68 },
        { x = 209, y = -32, color = 0x0cff04 },
        { x = -3, y = -1, color = 0xffffff }
    }
    local x, y = myFindColor(box, table, 95, 0, 0, 0)
    if x > -1 then
        return 99
    end -- 该委托正在执行

    if isNormal then
        minRank = SETTINGS.wtrank_other_normal
    else
        minRank = SETTINGS.wtrank_other_emergency
    end

    -- Setup score
    if isNormal then
        wtrank_oil = SETTINGS.wtrank_oil_normal
        wtrank_book = SETTINGS.wtrank_book_normal
        wtrank_cube = SETTINGS.wtrank_cube_normal
        wtrank_diamond = SETTINGS.wtrank_diamond_normal
        wtrank_coin = SETTINGS.wtrank_coin_normal
    else
        wtrank_oil = SETTINGS.wtrank_oil_emergency
        wtrank_book = SETTINGS.wtrank_book_emergency
        wtrank_cube = SETTINGS.wtrank_cube_emergency
        wtrank_diamond = SETTINGS.wtrank_diamond_emergency
        wtrank_coin = SETTINGS.wtrank_coin_emergency
    end

    table = {
        -- oil
        { x = 0, y = 0, color = 0x313431 },
        { x = 7, y = 9, color = 0x313431 },
        { x = 16, y = 3, color = 0x6b6d6b },
        { x = 20, y = 41, color = 0x313431 },
        { x = 30, y = 33, color = 0x6b696b }
    }
    local x, y = myFindColor(box, table, 95, 0, 0, 0)
    if x > -1 then
        print("oil")
    end
    if x > -1 and minRank > wtrank_oil then
        minRank = wtrank_oil
    end

    table = {
        -- oil ios
        { x = 0, y = 0, color = 0x363636 },
        { x = 7, y = 9, color = 0x363636 },
        { x = 16, y = 3, color = 0x6c6c6c },
        { x = 20, y = 41, color = 0x363636 },
        { x = 30, y = 33, color = 0x6c6c6c }
    }
    local x, y = myFindColor(box, table, 95, 0, 0, 0)
    if x > -1 then
        print("oil")
    end
    if x > -1 and minRank > wtrank_oil then
        minRank = wtrank_oil
    end

    table = {
        -- book
        { x = 0, y = 0, color = 0xa5a6a5 },
        { x = -5, y = 19, color = 0xa5a6a5 },
        { x = 26, y = 6, color = 0xa5a6a5 },
        { x = 30, y = -21, color = 0xffffff },
        { x = -13, y = -19, color = 0xefc36b }
    }
    local x, y = myFindColor(box, table, 95, 0, 0, 0)
    if x > -1 then
        print("book")
    end
    if x > -1 and minRank > wtrank_book then
        minRank = wtrank_book
    end

    table = {
        -- cube
        { x = 0, y = 0, color = 0xefbe63 },
        { x = 26, y = 23, color = 0xeffbd6 },
        { x = 13, y = 44, color = 0x7be7ef }
    }
    local x, y = myFindColor(box, table, 95, 0, 0, 0)
    if x > -1 then
        print("cube")
    end
    if x > -1 and minRank > wtrank_cube then
        minRank = wtrank_cube
        print("cube")
    end

    table = {
        -- diamond
        { x = 0, y = 0, color = 0xff456b },
        { x = 40, y = -13, color = 0xff4573 },
        { x = 23, y = -8, color = 0xff4573 }
    }
    local x, y = myFindColor(box, table, 95, 0, 0, 0)
    if x > -1 then
        print("diamond")
    end
    if x > -1 and minRank > wtrank_diamond then
        print(x, XX(x), YY(x))
        minRank = wtrank_diamond
        print("diamond")
    end

    table = {
        -- coin
        { x = 0, y = 0, color = 0xffffa5 },
        { x = -15, y = 10, color = 0xffffa5 },
        { x = 6, y = 4, color = 0xf7be52 },
        { x = -14, y = 17, color = 0xf7be52 }
    }
    local x, y = myFindColor(box, table, 95, 0, 0, 0)
    if x > -1 then
        print("coin")
    end
    if x > -1 and minRank > wtrank_coin then
        minRank = wtrank_coin
    end

    return minRank
end

function scanHouzhaiPerk()
    local table = {
        -- heart
        { x = 0, y = 0, color = 0xff95a3 },
        { x = -9, y = -6, color = 0xff879a },
        { x = -1, y = 5, color = 0xff8d9d },
        { x = 6, y = -5, color = 0xffa4b1 },
        { x = -1, y = -14, color = 0xfbfbf7 }
    }
    local x, y = myFindColor({ 0, 0, 1135, 639 }, table, 95, 0, 0, 0)
    if x > -1 then
        return x, y
    end

    -- heart 反
    table = {
        { x = 0, y = 0, color = 0xff94a0 },
        { x = 7, y = -4, color = 0xff879a },
        { x = -8, y = -4, color = 0xffa5af },
        { x = 0, y = 6, color = 0xff8c9d },
        { x = 0, y = -13, color = 0xfbfbf7 }
    }
    x, y = myFindColor({ 0, 0, 1135, 639 }, table, 95, 0, 0, 0)
    if x > -1 then
        return x, y
    end

    table = {
        -- coin
        { x = 0, y = 0, color = 0xffffaa },
        { x = 5, y = 7, color = 0xf4bd50 },
        { x = 0, y = 12, color = 0xcc8833 },
        { x = 0, y = -15, color = 0xfbfbf7 }
    }
    x, y = myFindColor({ 0, 0, 1135, 639 }, table, 95, 0, 0, 0)
    if x > -1 then
        return x, y
    end
    return -1, -1
end

function scanIsLivingAreaReady()
    return cmpColor({ { 555, 599, 0xbd554a } })
end

function scanIsHouzhai()
    local x, y = myFindColor(
            { 776, 524, 1017, 631 },
            {
                { x = 0, y = 0, color = 0xf4aa70 },
                { x = 9, y = -4, color = 0xfe6746 },
                { x = 26, y = 16, color = 0xff6847 },
                { x = -100, y = 32, color = 0xf7d786 }
            },
            95,
            0,
            0,
            0
    )
    return (x > -1)
end

-- function scanIsXueyuanReady()
--     if SETTINGS.JP then
--         return cmpColor({{387, 213, 0xf71029}})
--     else
--         return cmpColor({{370, 220, 0xef0021}})
--     end
-- end

function scanIsHouzhaiReady()
    return cmpColor({ { 671, 203, 0xb55142 } })
end

-- 后宅补充食物界面
function scanIsHouzhaiFoodSelect()
    return cmpColor(
            {
                { 352, 290, 0xf7f3f7 },
                { 357, 295, 0xffcb39 },
                { 363, 301, 0xd6d3d6 },
                { 1042, 302, 0xe7e7e7 },
                { 1049, 296, 0xffcb39 },
                { 1056, 291, 0xf7f3f7 }
            }
    )
end

-- 后宅购买食物界面（拿油买食物那个界面）
function scanIsHouzhaiFoodPurchase()
    return cmpColor(
            {
                { 294, 462, 0xf7f3ef },
                { 293, 186, 0xfff7ef },
                { 861, 186, 0xfff7ef },
                { 862, 464, 0xf7f3ef },
                { 489, 315, 0xf7c339 },
                { 670, 315, 0xf7c739 }
            }
    )
end

function scanIsStudyConfirm()
    return cmpColor(
            {
                { 274, 148, 0xeff3f7 },
                { 492, 420, 0xefebef },
                { 754, 426, 0xffd742 },
                { 736, 157, 0xeff3f7 }
            }
    )
end

function scanIsHouzhaiExpPage()
    return cmpColor(
            {
                { 641, 561, 0xfbfbfb },
                { 604, 100, 0xbe983e },
                { 299, 121, 0x252525 },
                { 974, 564, 0xffffff },
                { 959, 560, 0xffffff }
            }
    )
end

function scanIsWeituoExpanded()
    local x, y = myFindColor({ 1, 52, 466, 188 },
            {
                { x = 0, y = 0, color = 0xe8e8e8 },
                { x = -154, y = 7, color = 0x3c5a96 },
                { x = -67, y = 8, color = 0x477f6d },
                { x = -7, y = -13, color = 0xbe4030 }
            },
            95, 0, 0, 0)
    return x > -1
end

function scanIsSP()
    if scanIsEventChapter() then
        return true
    elseif scanIsSpee() then
        return true
    elseif scanIsOppcb() then
        return true
    elseif scanIsIoLnD() then
        return true
    end

    return false
end

function _scanIsMap()
    return cmpColor(
            {
                { 1114, 584, 0xffffff },
                { 986, 584, 0xffffff },
                { 811, 585, 0xffffff },
                { 1106, 586, 0xf7c342 }
            }
    )
end

function scanIsMap()
    return scanIsOilCoinGem() and _scanIsMap()
end

function scanOCGWidget()
    -- 油、物资、钻石控件
    -- 活动章节，增加额外识别
    if IsWorkingOnEventLevel() then
        return (scanIsOilCoinGem() or scanIsEventChapter())
    else
        return scanIsOilCoinGem()
    end
end

function scanIsChapter()
    return (scanLevelHardBtn() or scanLevelNormalBtn()) and scanWeighAnchor() and scanHomeBtn() and
            scanChapterCharacter()
end

function scanIsWorldLineSelect()
    -- 出击选择页面
    return (scanIsOilCoinGem() and scanWeighAnchor() and scanHomeBtn() and scanMainBattleLine())
end

function scanWeighAnchor()
    -- 章节左上角出击字样
    local x, y = myFindColor(
            { 103, 11, 157, 37 },
            {
                { x = 0, y = 0, color = 0xd2e8ff },
                { x = 26, y = 10, color = 0xd6eaff },
                { x = -8, y = 11, color = 0xd2e8ff },
                { x = 25, y = -7, color = 0xedf6ff }
            },
            95,
            0,
            0,
            0
    )
    if x <= -1 then
        local x2, y2 = myFindColor(
                { 98, 8, 206, 40 },
                {
                    { x = 0, y = 0, color = 0xd3e9ff },
                    { x = 1, y = 11, color = 0xd2e9ff },
                    { x = 10, y = 1, color = 0xd0e9ff },
                    { x = 14, y = 17, color = 0xd0e9ff },
                    { x = 29, y = 1, color = 0xe4f5ff },
                    { x = 34, y = 7, color = 0xe0f1ff },
                    { x = 37, y = 18, color = 0xdaebfc }
                },
                95,
                0,
                0,
                0
        )
        return (x2 > -1)
    else
        return (x > -1)
    end
end

function scanMainBattleLine()
    local x, y = myFindColor(
            { 150, 105, 454, 493 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = -193, y = -319, color = 0x232a3a },
                { x = -190, y = -131, color = 0xacb2cd },
                { x = 103, y = 43, color = 0x222734 },
                { x = -47, y = 32, color = 0xcbcbcb }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanHomeBtn()
    local x, y = myFindColor(
            { 1052, 0, 1135, 70 },
            {
                { x = 0, y = 0, color = 0xcff1ff },
                { x = 10, y = -14, color = 0xe8f9fb },
                { x = -9, y = -15, color = 0xdcf6ff },
                { x = 0, y = -23, color = 0xecfdfd },
                { x = -8, y = 0, color = 0xcceeff },
                { x = 9, y = 0, color = 0xd2f4ff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanHomeBtnRed()
    local x, y = myFindColor(
            { 1054, 3, 1133, 68 },
            {
                { x = 0, y = 0, color = 0xffdddf },
                { x = -10, y = 8, color = 0xffd6e0 },
                { x = -9, y = 23, color = 0xffccdd },
                { x = -4, y = 23, color = 0xffd0d8 },
                { x = 9, y = 23, color = 0xffccdd },
                { x = 11, y = 9, color = 0xffe0e0 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function _scanIsFormation()
    return cmpColor(
            {
                { 1095, 543, 0xffffff },
                { 928, 541, 0xde9652 },
                { 1084, 543, 0xf7cb42 }
            }
    )
end

-- 左上角：编队
function scanIsFormation()
    local x, y = myFindColor(
            { 97, 6, 161, 45 },
            {
                { x = 0, y = 0, color = 0xe3f4ff },
                { x = 0, y = 6, color = 0xddeeff },
                { x = -1, y = 12, color = 0xd7e9ff },
                { x = 11, y = -2, color = 0xeff7ff },
                { x = 31, y = -2, color = 0xf8fcff },
                { x = 33, y = 11, color = 0xd9ebff },
                { x = 18, y = 14, color = 0xd7e8ff },
                { x = 27, y = 13, color = 0xd8e9ff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 战斗中：停止自律
function scanStopAutomation()
    local x, y = myFindColor(
            { 10, 27, 54, 53 },
            {
                { x = 0, y = 0, color = 0xe3f4ff },
                { x = 1, y = 10, color = 0xdaebfc },
                { x = 8, y = 8, color = 0xdcecff },
                { x = 25, y = -2, color = 0xf3f8ff },
                { x = 24, y = 6, color = 0xe4f0ff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsAmbush()
    return cmpColor(
            {
                { 314, 302, 0xff5d6b },
                { 293, 338, 0xff5d6b },
                { 335, 338, 0xff5d6b },
                { 808, 405, 0x4a75b5 }
            }
    )
end

function scanInFilter()
    -- return cmpColor({{778,572,0x4271ad},{489,586,0xb5594a}})
    local x, y = myFindColor(
            { 27, 10, 241, 523 },
            {
                { x = 0, y = 0, color = 0x526da5 },
                { x = 0, y = 32, color = 0x4a6da5 },
                { x = 12, y = 4, color = 0xffffff },
                { x = 23, y = 14, color = 0xffffff },
                { x = 52, y = 21, color = 0xffffff },
                { x = 176, y = 33, color = 0x526dad },
                { x = 173, y = 3, color = 0x526dad }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsGetItems()
    local colors = {
        { 473, 202, 0x73ffff },
        { 650, 201, 0xbdffff },
        { 593, 212, 0xffffff }
    }
    return cmpColor(colors)
end

function scanIsGetItems2()
    -- 2 rows
    local colors = {
        { 474, 139, 0x73ffff },
        { 649, 141, 0xbdffff },
        { 594, 149, 0xffffff },
        { 510, 139, 0xffffff }
    }
    return cmpColor(colors)
end

function scanIsGetItems3()
    -- 2+ rows
    local colors = {
        { 473, 136, 0x73ffff },
        { 650, 140, 0xbdffff },
        { 594, 146, 0xffffff },
        { 552, 155, 0x7392ff }
    }
    return cmpColor(colors)
end

function scanIsHatchReady(slot)
    return cmpColor({ { 953, 207, 0xce614a } })
end

--  是否为出击页面对话框
function scanIsFormationDlg()
    local x, y = myFindColor(
            { 756, 142, 832, 202 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 22, y = -14, color = 0xc05350 },
                { x = -26, y = 18, color = 0xc05350 }
            },
            95,
            0,
            0,
            0
    )
    if x > -1 then
        local x2, y2 = myFindColor(
                { 907, 521, 1122, 617 },
                {
                    { x = 0, y = 0, color = 0x5b4c1d },
                    { x = -136, y = 26, color = 0x543f2e },
                    { x = -66, y = 6, color = 0x5f4721 }
                },
                95,
                0,
                0,
                0
        )
        if x2 > -1 then
            return true
        end
    end
    return false
end

function scanIsOperationFile()
    local x, y = myFindColor(
            { 65, 22, 124, 77 },
            {
                { x = 0, y = 0, color = 0xebeced },
                { x = 23, y = -2, color = 0xbfc4c7 },
                { x = 13, y = 3, color = 0x997711 },
                { x = -7, y = -26, color = 0x4577b9 }
            },
            95,
            0,
            0,
            0
    )
    return (x > -1)
end

function scanIsMoneyFullDlg()
    local result = true
    local x = -1
    local y = -1

    -- “物”
    x, y = myFindColor(
            { 254, 126, 869, 525 },
            {
                { x = 0, y = 0, color = 0xfffff7 },
                { x = -3, y = -2, color = 0xfefef7 },
                { x = 0, y = 10, color = 0xfffff7 },
                { x = 10, y = 4, color = 0xfafaf3 },
                { x = 13, y = 9, color = 0xfffff7 },
                { x = 6, y = -3, color = 0xfffff7 }
            },
            95,
            0,
            0,
            0
    )
    result = result and (x > -1)

    -- “资”
    x, y = myFindColor(
            { 254, 126, 869, 525 },
            {
                { x = 0, y = 0, color = 0xfdfdf5 },
                { x = -5, y = -1, color = 0xf3f4ed },
                { x = -3, y = -3, color = 0xfefef6 },
                { x = -9, y = 7, color = 0xfffff7 },
                { x = -4, y = 10, color = 0xfafaf3 },
                { x = 3, y = 9, color = 0xfffff7 }
            },
            95,
            0,
            0,
            0
    )
    result = result and (x > -1)
    return result
end

function scanIsRedfaceDlg()
    local bIsRedFaceDlg = true

    --  “处”
    local x, y = myFindColor(
            { 312, 202, 823, 409 },
            {
                { x = 0, y = 0, color = 0xfcfcf5, offset = 0x050505 },
                { x = 3, y = -7, color = 0xfffff7, offset = 0x050505 },
                { x = 7, y = -11, color = 0xfffff7, offset = 0x050505 },
                { x = 7, y = -7, color = 0xfffff7, offset = 0x050505 },
                { x = 7, y = -1, color = 0xfffff7, offset = 0x050505 },
                { x = -2, y = -8, color = 0xfffff7, offset = 0x050505 },
                { x = -3, y = -5, color = 0xfefef7, offset = 0x050505 }
            },
            95,
            0,
            0,
            0
    )
    bIsRedFaceDlg = bIsRedFaceDlg and (x > -1)

    --  “强”
    x, y = myFindColor(
            { 312, 202, 823, 409 },
            {
                { x = 0, y = 0, color = 0xfffff7, offset = 0x050505 },
                { x = 8, y = -8, color = 0xfffff7, offset = 0x050505 },
                { x = -3, y = 4, color = 0xfffff7, offset = 0x050505 },
                { x = -7, y = -3, color = 0xfefef6, offset = 0x050505 },
                { x = 0, y = 2, color = 0xfffff7, offset = 0x050505 },
                { x = 9, y = 6, color = 0xfafaf3, offset = 0x050505 }
            },
            95,
            0,
            0,
            0
    )
    bIsRedFaceDlg = bIsRedFaceDlg and (x > -1)

    return bIsRedFaceDlg
end

-- 战斗失败舰队实力提升提示
function scanIsBattleFailTip()
    local x, y = myFindColor(
            { 88, 234, 1038, 428 },
            {
                { x = 0, y = 0, color = 0xdae0e6 },
                { x = 46, y = 95, color = 0xffffff },
                { x = 313, y = -13, color = 0xd9dee7 },
                { x = 564, y = 53, color = 0xe3e7ef },
                { x = 859, y = 20, color = 0xdce3eb },
                { x = 433, y = 256, color = 0xcdcfd1 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

--  挑战券
function scanIsUsingTicketDlg()
    local x, y = myFindColor(
            { 0, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0x201208 },
                { x = -9, y = 11, color = 0x372417 },
                { x = 39, y = -20, color = 0x517fbb },
                { x = -372, y = 22, color = 0x9fa2a4 },
                { x = 60, y = -285, color = 0xcb4441 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsResearchItemExpanded()
    return cmpColor(
            {
                { 1104, 311, 0x95e8f7 },
                { 1110, 321, 0xc5f7fb },
                { 30, 312, 0x9ae9f7 },
                { 27, 323, 0xb6f2fb },
                { 641, 265, 0xb6b8bd },
                { 214, 24, 0xd0e9ff },
                { 134, 29, 0x4053a4 }
            }
    )
end

function scanIsResearchRunning()
    return cmpColor(
            {
                { 375, 499, 0xb8524e },
                { 435, 526, 0xffffff },
                { 523, 533, 0xda875b },
                { 488, 510, 0xffffff }
            }
    )
end

function scanIsWeituoBtnFinish()
    local x, y = myFindColor(
            { 326, 207, 460, 292 },
            {
                { x = 0, y = 0, color = 0xe49330 },
                { x = 43, y = 3, color = 0xfefeff },
                { x = 71, y = 2, color = 0xffffff },
                { x = 103, y = 0, color = 0xeca243 },
                { x = 51, y = 6, color = 0xe79d3f }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanLootShipNew()
    local x, y = myFindColor(
            { 143, 25, 294, 153 },
            {
                { x = 0, y = 0, color = 0xf2db4a },
                { x = -32, y = 2, color = 0xeaa03d },
                { x = -27, y = 40, color = 0xeea242 },
                { x = 24, y = 8, color = 0xeff560 },
                { x = 52, y = -10, color = 0xffffff },
                { x = 5, y = -8, color = 0xecd63f }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanLootShipUnlocked()
    local x, y = myFindColor(
            { 0, 294, 114, 638 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 0, y = 1, color = 0xffffff },
                { x = 12, y = 3, color = 0xffffff },
                { x = 12, y = 12, color = 0xffffff },
                { x = -2, y = 12, color = 0xffffff },
                { x = -2, y = 3, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

--function scanManeuversBtn()
--    local x, y = myFindColor({ 468, 547, 1135, 638 },
--        {
--            { x = 0, y = 0, color = 0xf3fda3 },
--            { x = 24, y = -7, color = 0xf9f9d7 },
--            { x = -77, y = -15, color = 0xff2120 },
--            { x = -32, y = -14, color = 0xffdd88 },
--            { x = -58, y = 7, color = 0xfbfbfb }
--        },
--        95,
--        0,
--        0,
--        0)
--    return x > -1
--end

function scanChapterCharacter()
    local x, y, tab_
    -- 第
    tab_ = {
        { x = 0, y = 0, color = 0xdaebff },
        { x = 7, y = 7, color = 0xd7e8ff },
        { x = -3, y = -4, color = 0xdcedfe },
        { x = -7, y = 5, color = 0xd2e3ff },
        { x = 0, y = 10, color = 0xd3e9ff }
    }

    x, y = myFindColor({ 106, 54, 188, 100 }, tab_, 95, 0, 0, 0)
    if x > -1 then
        -- 章
        tab_ = {
            { x = 0, y = 0, color = 0xedf5ff },
            { x = -3, y = 3, color = 0xe6f3ff },
            { x = 6, y = 8, color = 0xdfefff },
            { x = -6, y = 10, color = 0xdaebfc },
            { x = 0, y = 15, color = 0xd3e9ff }
        }
        x, y = myFindColor({ 106, 54, 188, 100 }, tab_, 95, 0, 0, 0)
        return x > -1
    end
    return false
end

function scanOutofFood()
    -- 后宅食物耗尽
    local x, y = myFindColor(
            { 269, 106, 867, 510 },
            {
                { x = 0, y = 0, color = 0x3e3e3e },
                { x = 296, y = 63, color = 0x9f4a4a },
                { x = -5, y = 166, color = 0x92a3a3 },
                { x = 241, y = 114, color = 0xfbebd3 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanFirstMatch(char_table_, rect_)
    local matched_name = ''
    for name_, char_ in pairs(char_table_) do
        local x, y = -1, -1
        x, y = myFindColor(rect_, char_, 95, 0, 0, 0)
        if x > -1 then
            matched_name = name_
            break
        end
    end
    return matched_name
end

function scanTechItemRarity()
    local x, y = -1, -1, -1
    local rarity_match = {
        ["r"] = {
            { x = 0, y = 0, color = 0x619ae5 },
            { x = 189, y = 0, color = 0x4486d8 },
            { x = -99, y = -274, color = 0xacbddf },
            { x = -103, y = -277, color = 0x7498ca }
        },
        ["sr"] = {
            { x = 0, y = 0, color = 0xa07fd9 },
            { x = 188, y = -1, color = 0x8669c8 },
            { x = -100, y = -274, color = 0xc6bedf },
            { x = -104, y = -278, color = 0x9a91c7 }
        },
        ["ssr"] = {
            { x = 0, y = 0, color = 0xeda348 },
            { x = 188, y = 1, color = 0xe6833f },
            { x = -98, y = -273, color = 0xffff75 },
            { x = -103, y = -277, color = 0xeba771 }
        },
        ["ur"] = {
            { x = 0, y = 0, color = 0xecbad3 },
            { x = 189, y = 1, color = 0x9accf5 },
            { x = -98, y = -273, color = 0xfffbcb },
            { x = -103, y = -277, color = 0xedc0a1 },
            { x = 237, y = 197, color = 0x81d6a3 }
        }
    }

    for k_, v_ in pairs(rarity_match) do
        x, y = myFindColor({ 236, 59, 654, 596 }, v_, 95, 0, 0, 0)
        if x > -1 then
            return k_
        end
    end
    return "r"
end

function scanTechTimeCost()
    local ret_ = 5
    local time_str = ""
    while ret_ > 0 do
        time_str = get_tech_time_str()
        if IsTimeValid(time_str) then
            break
        else
            ret_ = ret_ - 1
            sleep(1000)
        end
    end

    if ret_ > 0 then
        return time_str_to_hour(time_str)
    else
        return 12
    end
end

---@class TechItemData
---@field index number
---@field scale string
---@field type string
---@field ship_name string
---@field series number
---@field rarity string
---@field time_cost number
---@field ship_rarity string
TechItemData = {}

function scanTechItemData()
    local result = {
        index = 0,
        scale = "",
        type = "",
        ship_name = "",
        series = 1,
        rarity = "",
        time_cost = 12,
        ship_rarity = ""
    }

    keepScreen(true)

    -- series
    local seri_char = {
        -- 四期
        [4] = {
            { x = 0, y = 0, color = 0xffffff },
            { x = 0, y = 12, color = 0xffffff },
            { x = 14, y = 6, color = 0xffffff },
            { x = 16, y = 12, color = 0xffffff }
        },
        -- 三期
        [3] = {
            { x = 0, y = 0, color = 0xf7f7f7 },
            { x = 0, y = 19, color = 0xf7f7f7 },
            { x = 8, y = 19, color = 0xf7f7f7 },
            { x = 8, y = 0, color = 0xf7f7f7 },
            { x = 16, y = 0, color = 0xf7f7f7 },
            { x = 16, y = 19, color = 0xf7f7f7 }
        },
        -- 二期
        [2] = {
            { x = 0, y = 0, color = 0xf7f7f7 },
            { x = -10, y = 3, color = 0xf7f7f7 },
            { x = -10, y = -9, color = 0xf7f7f7 },
            { x = 0, y = 10, color = 0xf7f7f7 }
        }
    }

    for k_, v_ in pairs(seri_char) do
        local x, y = -1, -1
        x, y = myFindColor({ 244, 82, 294, 132 }, v_, 95, 0, 0, 0)
        if x > -1 then
            result.series = k_
            break
        end
    end

    -- scale
    if
    cmpColor(
            {
                { 410, 353, 0xffffff },
                { 400, 350, 0xffffff },
                { 430, 350, 0xffffff },
                { 423, 331, 0xf5f5f5 }
            }
    )
    then
        result.scale = 'small' -- 小
    elseif
    cmpColor(
            {
                { 408, 345, 0xfbfafb },
                { 424, 345, 0xfbfafb },
                { 401, 328, 0xffffff },
                { 431, 328, 0xffffff },
                { 410, 328, 0xffffff },
                { 421, 328, 0xffffff }
            }
    )
    then
        result.scale = 'middle' -- 中
    else
        result.scale = 'large' -- 大
    end

    -- type
    if cmpColor({ { 282, 275, 0xffffff } }) then
        -- has Top and Bot, not H
        if cmpColor({ { 291, 281, 0xffffff }, { 291, 297, 0xffffff } }) then
            -- Has top/bot right bar, not C/G
            if cmpColor({ { 284, 302, 0xffffff }, { 287, 306, 0xffffff } }) then
                -- has tail Q
                result.type = "Q"
            elseif cmpColor({ { 281, 288, 0xffffff } }) then
                -- middle bar, B
                result.type = "B"
            else
                result.type = "D"
            end
        else
            if cmpColor({ { 291, 294, 0xffffff } }) then
                -- has bot right bar G
                result.type = "G"
            elseif cmpColor({ { 285, 288, 0xffffff }, { 281, 288, 0xffffff } }) then
                -- has middle bar E
                result.type = "E"
            elseif
            cmpColor(
                    {
                        { 271, 277, 0xffffff },
                        { 271, 291, 0xffffff },
                        { 271, 300, 0xffffff }
                    }
            )
            then
                -- left bar C
                result.type = "C"
            else
                result.type = "T"
            end
        end
    else
        result.type = "H"
    end

    -- rarity
    result.rarity = scanTechItemRarity()

    -- time_cost
    result.time_cost = scanTechTimeCost()

    -- ship_name
    if result.series == 1 then
        local box = { 285, 392, 613, 472 }
        local ship_match = {
            ['Haiwangxing'] = {
                { x = 0, y = 0, color = 0xfefcf4 },
                { x = 13, y = -2, color = 0xa5425b },
                { x = 3, y = -11, color = 0xb1d2e3 },
                { x = 11, y = 13, color = 0x99ccdd },
                { x = -27, y = -2, color = 0x4173b6 }
            },
            ['Chuyun'] = {
                { x = 0, y = 0, color = 0xbc7cae },
                { x = 20, y = -5, color = 0xc583b5 },
                { x = -6, y = -5, color = 0xfbf3eb },
                { x = 12, y = -2, color = 0xfdf7f0 },
                { x = 2, y = 18, color = 0xfdfdf6 },
                { x = 1, y = 8, color = 0xa95f88 }
            },
            ['Luyi'] = {
                { x = 0, y = 0, color = 0xb7b6b3 },
                { x = 7, y = 8, color = 0xf56979 },
                { x = 0, y = -10, color = 0xdcdddb },
                { x = 9, y = 20, color = 0xc4c4c4 },
                { x = -30, y = 5, color = 0x4173b6 }
            },
            ['Junzhu'] = {
                { x = 0, y = 0, color = 0xeb8563 },
                { x = 5, y = -10, color = 0x787e79 },
                { x = 12, y = -7, color = 0x383041 },
                { x = -16, y = 14, color = 0xbd5746 },
                { x = 13, y = 1, color = 0xfefcf6 },
                { x = -29, y = 9, color = 0x4071b3 }
            },
            ['Luoen'] = {
                { x = 0, y = 0, color = 0xe7d5c6 },
                { x = -8, y = 14, color = 0x457fc1 },
                { x = 22, y = -10, color = 0xfaefe7 },
                { x = 10, y = 11, color = 0xf7d6cf },
                { x = 0, y = 18, color = 0x878787 },
                { x = -18, y = 11, color = 0x4278ba }
            },
            ['Yichui'] = {
                { x = 0, y = 0, color = 0x5e79b8 },
                { x = 5, y = -8, color = 0xc8d0e9 },
                { x = 17, y = -10, color = 0x8aa3cc },
                { x = 1, y = 27, color = 0xfffeff },
                { x = -25, y = 14, color = 0x4477bb }
            }
        }
        result.ship_name = scanFirstMatch(ship_match, box)

    elseif result.series == 2 then
        local box = { 285, 392, 613, 472 }
        local ship_match = {
            ['Dadi'] = {
                { x = 0, y = 0, color = 0x514f55 },
                { x = 7, y = 4, color = 0xfdfdf6 },
                { x = 20, y = -19, color = 0x5f5f61 },
                { x = 17, y = -6, color = 0xfdfdfd },
                { x = 24, y = -34, color = 0xb064e1 },
                { x = -31, y = 19, color = 0x55cfd8 }
            },
            ['Wuqi'] = {
                { x = 0, y = 0, color = 0x454059 },
                { x = 12, y = 1, color = 0x3b3b54 },
                { x = -7, y = 22, color = 0xfefef9 },
                { x = 12, y = -14, color = 0xb869eb },
                { x = -43, y = 39, color = 0x53cdd4 }
            },
            ['Zuozhiya'] = {
                { x = 0, y = 0, color = 0x55d9ee },
                { x = 13, y = -5, color = 0x6b5a5a },
                { x = 7, y = 14, color = 0x88bbd1 },
                { x = -8, y = 10, color = 0xcbba98 },
                { x = -22, y = 12, color = 0x4578ba }
            },
            ['Xiyatu'] = {
                { x = 0, y = 0, color = 0xf2d9cf },
                { x = -7, y = -9, color = 0x8c7b8c },
                { x = 9, y = -6, color = 0x968e9e },
                { x = 22, y = -24, color = 0xbacbcb },
                { x = 4, y = -27, color = 0xd4cccc },
                { x = -22, y = -6, color = 0x4477ba }
            },
            ['Beifeng'] = {
                { x = 0, y = 0, color = 0xfdfbf4 },
                { x = -6, y = 12, color = 0xbbddaa },
                { x = 12, y = 15, color = 0xb3d5b3 },
                { x = 2, y = 12, color = 0xfdfdf5 },
                { x = 0, y = -7, color = 0xf7eee9 },
                { x = -21, y = 14, color = 0x4477ba }
            },
            ['Jiasikenie'] = {
                { x = 0, y = 0, color = 0xafb6c4 },
                { x = 11, y = 2, color = 0xfdfdf5 },
                { x = 4, y = -17, color = 0xa8b0be },
                { x = -14, y = 15, color = 0x4780c2 },
                { x = -13, y = -9, color = 0xe8e8e8 }
            }
        }
        result.ship_name = scanFirstMatch(ship_match, box)

    elseif result.series == 3 then
        local box = { 285, 392, 613, 472 }
        local ship_match = {
            ['Chaijun'] = {
                { x = 0, y = 0, color = 0xfff7f6 },
                { x = -6, y = 16, color = 0x957c9d },
                { x = 16, y = -2, color = 0x9ec8d0 },
                { x = 8, y = 16, color = 0x4f89cb },
                { x = -1, y = -23, color = 0xfdfdfd }
            },
            ['Deleike'] = {
                { x = 0, y = 0, color = 0xfbf9f4 },
                { x = -13, y = 0, color = 0xdddddd },
                { x = 5, y = 9, color = 0x528cd1 },
                { x = 4, y = 18, color = 0x323232 },
                { x = 21, y = -34, color = 0xc270e3 },
                { x = -38, y = 17, color = 0x59e1d0 }
            },
            ['Meiyinci'] = {
                { x = 0, y = 0, color = 0xfffdf3 },
                { x = 8, y = 15, color = 0xd7d1d3 },
                { x = -14, y = 8, color = 0x427ec1 },
                { x = 5, y = -20, color = 0x717171 },
                { x = -37, y = -5, color = 0x3f70aa }
            },
            ['Aoding'] = {
                { x = 0, y = 0, color = 0xfffdf5 },
                { x = 15, y = -4, color = 0x121212 },
                { x = 1, y = -16, color = 0x292929 },
                { x = -15, y = 16, color = 0x3e82c6 },
                { x = 9, y = 9, color = 0xcccccc }
            },
            ['Xiangbin'] = {
                { x = 0, y = 0, color = 0xfdfbeb },
                { x = 11, y = -16, color = 0xabc4cc },
                { x = 21, y = -15, color = 0xafc8d0 },
                { x = -18, y = 2, color = 0x3b69ad },
                { x = 3, y = 5, color = 0xfdfbeb }
            }
        }
        result.ship_name = scanFirstMatch(ship_match, box)

    elseif result.series == 4 then
        local box = { 285, 392, 613, 472 }
        local ship_match = {
            ['Ankeleiqi'] = {
                { x = 0, y = 0, color = 0xfffff7 },
                { x = 9, y = -22, color = 0xefdfd6 },
                { x = 6, y = 10, color = 0x9c7563 },
                { x = -6, y = 7, color = 0x9c796b },
                { x = -25, y = 3, color = 0x4275bd }
            },
            ['Bailong'] = {
                { x = 0, y = 0, color = 0xfff7f7 },
                { x = 3, y = -22, color = 0xadaead },
                { x = -15, y = 8, color = 0xefebef },
                { x = 18, y = -10, color = 0xf7f3f7 },
                { x = 22, y = -33, color = 0xd68ef7 }
            },
            ['Aijier'] = {
                { x = 0, y = 0, color = 0xfffbf7 },
                { x = -20, y = 9, color = 0xede8e8 },
                { x = 10, y = -25, color = 0xece6e7 },
                { x = 22, y = -36, color = 0xdd85f6 }
            },
            ['Aogusite'] = {
                { x = 0, y = 0, color = 0xfffbf7 },
                { x = 8, y = -26, color = 0xcecfd6 },
                { x = 15, y = -8, color = 0x181818 },
                { x = -4, y = -13, color = 0xfffff7 },
                { x = -25, y = -1, color = 0x4275bd }
            },
            ['Makeboluo'] = {
                { x = 0, y = 0, color = 0xfefef6 },
                { x = 6, y = -18, color = 0xa5c5d3 },
                { x = 18, y = -18, color = 0x3b4343 },
                { x = 7, y = 11, color = 0x215487 },
                { x = -24, y = 5, color = 0x4477bb }
            }
        }
        result.ship_name = scanFirstMatch(ship_match, box)

    end

    keepScreen(false)
    local retrycount = 20
    while result.type == "D" and result.ship_name == "" and retrycount > 0 do
        -- 检测到定向研发但是没检测到具体类型，可能是ui特效导致识别误差，继续尝试识别

        local box = { 285, 392, 613, 472 }
        local ship_match = {
            ['Dadi'] = {
                { x = 0, y = 0, color = 0x514f55 },
                { x = 7, y = 4, color = 0xfdfdf6 },
                { x = 20, y = -19, color = 0x5f5f61 },
                { x = 17, y = -6, color = 0xfdfdfd },
                { x = 24, y = -34, color = 0xb064e1 },
                { x = -31, y = 19, color = 0x55cfd8 }
            },
            ['Wuqi'] = {
                { x = 0, y = 0, color = 0x454059 },
                { x = 12, y = 1, color = 0x3b3b54 },
                { x = -7, y = 22, color = 0xfefef9 },
                { x = 12, y = -14, color = 0xb869eb },
                { x = -43, y = 39, color = 0x53cdd4 }
            },
            ['Deleike'] = {
                { x = 0, y = 0, color = 0xfbf9f4 },
                { x = -13, y = 0, color = 0xdddddd },
                { x = 5, y = 9, color = 0x528cd1 },
                { x = 4, y = 18, color = 0x323232 },
                { x = 21, y = -34, color = 0xc270e3 },
                { x = -38, y = 17, color = 0x59e1d0 }
            },
            ['Bailong'] = {
                { x = 0, y = 0, color = 0xfff7f7 },
                { x = 3, y = -22, color = 0xadaead },
                { x = -15, y = 8, color = 0xefebef },
                { x = 18, y = -10, color = 0xf7f3f7 },
                { x = 22, y = -33, color = 0xd68ef7 }
            },
            ['Aijier'] = {
                { x = 0, y = 0, color = 0xfffbf7 },
                { x = -20, y = 9, color = 0xede8e8 },
                { x = 10, y = -25, color = 0xece6e7 },
                { x = 22, y = -36, color = 0xdd85f6 }
            },
        }
        result.ship_name = scanFirstMatch(ship_match, box)

        sleep(300)
        retrycount = retrycount - 1
    end

    -- ship_rarity
    local ship_rarity_tab = {
        ['Haiwangxing'] = 'pry',
        ['Chuyun'] = 'pry',
        ['Luyi'] = 'pry',
        ['Junzhu'] = 'pry',
        ['Luoen'] = 'pry',
        ['Yichui'] = 'pry',

        ['Dadi'] = 'dr',
        ['Wuqi'] = 'dr',
        ['Zuozhiya'] = 'pry',
        ['Xiyatu'] = 'pry',
        ['Beifeng'] = 'pry',
        ['Jiasikenie'] = 'pry',

        ['Chaijun'] = 'pry',
        ['Deleike'] = 'dr',
        ['Meiyinci'] = 'pry',
        ['Aoding'] = 'pry',
        ['Xiangbin'] = 'pry',

        ['Ankeleiqi'] = 'pry',
        ['Bailong'] = 'dr',
        ['Aijier'] = 'dr',
        ['Aogusite'] = 'pry',
        ['Makeboluo'] = 'pry',
    }

    result.ship_rarity = ship_rarity_tab[result.ship_name] or ''

    keepScreen(false)

    --print("TechItem: ", result.scale, result.type, result.ship_name, result.ship_rarity, result.series, result.rarity, result.time_cost)
    return result
end

-- 困难按钮
function scanLevelHardBtn()
    local tab = {
        { x = 0, y = 0, color = 0xffffff },
        { x = -61, y = -18, color = 0xba2a24 },
        { x = 48, y = 25, color = 0xd74f3e },
        { x = -61, y = 26, color = 0xb52121 }
    }
    local x, y = myFindColor({ 0, 550, 149, 647 }, tab, 95, 0, 0, 0)

    return x > -1
end

-- 普通按钮
function scanLevelNormalBtn()
    local tab = {
        { x = 0, y = 0, color = 0xfeffff },
        { x = -48, y = -14, color = 0x6187d3 },
        { x = 46, y = -14, color = 0x7596d8 },
        { x = 46, y = 30, color = 0x6d8ed8 },
        { x = -64, y = -11, color = 0xffffff }
    }
    local x, y = myFindColor({ 0, 550, 149, 647 }, tab, 95, 0, 0, 0)

    return x > -1
end

function scanIsTBZZ()
    local x, y = myFindColor(
            { 98, 6, 212, 43 },
            {
                { x = 0, y = 0, color = 0xd3e9ff },
                { x = 10, y = -11, color = 0xd0e9ff },
                { x = 57, y = -9, color = 0xf4fcff },
                { x = 84, y = 2, color = 0xd1e8ff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsTBYX()
    --local x, y = myFindColor(
    --        { 0, 0, 1135, 638 }, --  穿越水线
    --        {
    --            { x = 0, y = 0, color = 0xd3e9ff },
    --            { x = 16, y = 4, color = 0xd8e9ff },
    --            { x = 61, y = -2, color = 0xe5f2ff },
    --            { x = 12, y = -8, color = 0x4253a5 },
    --            { x = -30, y = 227, color = 0x121212 },
    --            { x = 915, y = 138, color = 0xffd929 }
    --        },
    --        95,
    --        0,
    --        0,
    --        0
    --)
    -- return x > -1

    -- --  战斗皇家女仆队2nd
    -- local x, y = myFindColor({339, 140, 416, 180},
    -- {
    --   {x=0,y=0,color=0xffffff},
    --   {x=-47,y=-4,color=0x961228},
    --   {x=0,y=11,color=0x961028},
    --   {x=-43,y=2,color=0xffffff}
    -- },
    -- 95, 0, 0, 0)
    -- return (x > -1 and scanIsTBZZ())

    -- -- 埃塞克斯级 复刻
    -- local x, y = myFindColor({624, 82, 907, 539},
    -- {
    --   {x=0,y=0,color=0xff515b},
    --   {x=-21,y=-7,color=0xfff8e9},
    --   {x=29,y=14,color=0x878cab},
    --   {x=62,y=188,color=0xc22b45},
    --   {x=69,y=-53,color=0xffb598}
    -- },
    -- 95, 0, 0, 0)
    --return (x > -1 and scanIsTBZZ())

    -- 神秘事件调查
    local x, y = myFindColor({ 962, 71, 1081, 192 },
            {
                { x = 0, y = 0, color = 0xdd7755 },
                { x = 2, y = 44, color = 0x42fff3 },
                { x = 3, y = 30, color = 0xfcffff },
                { x = 52, y = 45, color = 0x44ffee }
            },
            95, 0, 0, 0)

    return (x > -1 and scanIsTBZZ())

    --return false
end

function scanIsSubmarineUsed()
    -- button used
    local x, y = myFindColor(
            { 600, 500, 720, 605 },
            {
                { x = 0, y = 0, color = 0x858585 },
                { x = 20, y = 0, color = 0x858585 },
                { x = 2, y = 17, color = 0x858585 },
                { x = -23, y = 21, color = 0x858585 },
                { x = -27, y = 49, color = 0xffffff },
                { x = 49, y = 22, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )

    return x > -1
end

function scanSKIP()
    local x1, y1 = myFindColor(
            { 930, 23, 1011, 62 },
            {
                { x = 0, y = 0, color = 0x92fc63 },
                { x = 1, y = 8, color = 0x92fc63 },
                { x = -6, y = 1, color = 0x92fc63 },
                { x = -5, y = 9, color = 0x92fc63 }
            },
            95,
            0,
            0,
            0
    )
    if x1 == -1 then
        local x, y = myFindColor(
                { 1046, 27, 1090, 42 },
                {
                    { x = 0, y = 0, color = 0xefefef },
                    { x = -6, y = 5, color = 0xf7f7f9 },
                    { x = -12, y = -2, color = 0xe4e4e4 },
                    { x = -24, y = 5, color = 0xf3f3f3 }
                },
                95,
                0,
                0,
                0
        )
        return x > -1
    end
    return false
end

function scanPause()
    local x, y = myFindColor(
            { 215, 111, 917, 536 },
            {
                { x = 0, y = 0, color = 0x537fbb },
                { x = -380, y = 41, color = 0xc75f58 },
                { x = 99, y = -290, color = 0xd14b4b },
                { x = 100, y = -228, color = 0xe0e2e5 },
                { x = 105, y = -236, color = 0xe0e2e5 },
                { x = -456, y = -289, color = 0x4c6fae },
                { x = -460, y = -289, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 蝶海梦花 章节检测
function scanDiehaiMenghua()
    result = nil
    keepScreen(true)
    s_b = color_sum_b(577, 237, 596, 257)
    s_r = color_sum_r(577, 237, 596, 257)

    if s_b > 95000 and s_r < 65000 then
        result = "XianShi"
    elseif s_b < 85000 and s_r > 100000 then
        result = "MengJing"
    else
        result = nil
    end
    keepScreen(false)
    return result
end

-- 大舰队页面
function scanIsFleet()
    local x, y = myFindColor(
            { 94, 2, 187, 50 },
            {
                { x = 0, y = 0, color = 0xe3f0ff },
                { x = 0, y = 5, color = 0xdbedff },
                { x = -5, y = 12, color = 0xd3e9ff },
                { x = 6, y = 11, color = 0xd6eaff },
                { x = -7, y = 0, color = 0xdbedff },
                { x = 7, y = 0, color = 0xe8f3ff },
                { x = 52, y = -3, color = 0xeff7ff },
                { x = 52, y = 5, color = 0xddeeff },
                { x = 56, y = 11, color = 0xd3ecff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 大舰队：后勤
function scanIsFleetHouqin()
    local x, y = myFindColor(
            { 2, 101, 90, 636 },
            {
                { x = 0, y = 0, color = 0xf3f38a },
                { x = -12, y = -2, color = 0xf0f28b },
                { x = -19, y = -11, color = 0xf1f78d },
                { x = -16, y = -13, color = 0xf4ff8e },
                { x = -14, y = -23, color = 0xf1fe8c },
                { x = -19, y = -20, color = 0xf3fe90 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 大舰队：作战
function scanIsFleetZuozhan()
    local x, y = myFindColor(
            { 3, 513, 97, 610 },
            {
                { x = 0, y = 0, color = 0xf4fe8f },
                { x = -6, y = 4, color = 0xf2fc8f },
                { x = -15, y = 8, color = 0xf1f98e },
                { x = -21, y = -19, color = 0xf5fe8f },
                { x = 2, y = 7, color = 0xf4ff8e }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 大舰队：作战：提示红点_金色按钮
function scanFleetZuozhanRedDot_Gold()
    local x, y = myFindColor(
            { 94, 121, 1134, 546 },
            {
                { x = 0, y = 0, color = 0xcc5b3d },
                { x = -11, y = -10, color = 0xcf5d42 },
                { x = 10, y = -10, color = 0xd05f44 },
                { x = 0, y = -11, color = 0xfff5e8 }
            },
            95,
            0,
            0,
            0
    )
    return x, y
end

-- 大舰队：作战：提示红点_蓝色按钮
function scanFleetZuozhanRedDot_Blue()
    local x, y = myFindColor(
            { 94, 121, 1134, 546 },
            {
                { x = 0, y = 0, color = 0xc65a4a },
                { x = -10, y = -9, color = 0xc65b50 },
                { x = 10, y = -9, color = 0xc55a52 },
                { x = 0, y = -10, color = 0xfff5e8 }
            },
            95,
            0,
            0,
            0
    )
    return x, y
end

-- 大舰队：作战主页面（流程图页面）
function scanIsFleetZuozhanMain()
    local x, y = myFindColor(
            { 440, 527, 1136, 629 },
            {
                { x = 0, y = 0, color = 0x4a58bc },
                { x = 18, y = -33, color = 0x4850c1 },
                { x = 316, y = -31, color = 0x242d5b },
                { x = 298, y = -2, color = 0x293165 },
                { x = -332, y = -1, color = 0x494f7d },
                { x = -316, y = -33, color = 0x4b537c },
                { x = -23, y = -31, color = 0x1e2337 },
                { x = -41, y = -2, color = 0x23253a }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 作战：事件记录页面
function scanIsFleetZuozhanEvent()
    local x, y = myFindColor(
            { 981, 496, 1135, 545 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = -8, y = -4, color = 0xffffff },
                { x = 11, y = 3, color = 0x515edb },
                { x = -12, y = 10, color = 0x4f5cd6 },
                { x = -108, y = -1, color = 0xffffff },
                { x = -127, y = 10, color = 0x515edb }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 作战：事件记录页面_立即派遣
function scanFleetZuozhanEventPaiqian()
    local x, y = myFindColor(
            { 800, 547, 1135, 609 },
            {
                { x = 0, y = 0, color = 0x5060d6 },
                { x = 298, y = 0, color = 0x202857 },
                { x = 296, y = 31, color = 0x212959 },
                { x = -16, y = 32, color = 0x525bd9 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 大舰队作战：编队
function scanIsFleetFormation()
    local x, y = myFindColor(
            { 1042, 182, 1130, 351 },
            {
                { x = 0, y = 0, color = 0xc1c7d8 },
                { x = -51, y = 0, color = 0x284384 },
                { x = -52, y = 47, color = 0x416cc9 },
                { x = 0, y = 47, color = 0x416dc9 },
                { x = -1, y = 73, color = 0xcdcdce },
                { x = -51, y = 73, color = 0x555759 },
                { x = -52, y = 118, color = 0x838687 },
                { x = 0, y = 119, color = 0x858687 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 大型作战
function scanIsOPERATION()
    local x, y = myFindColor(
            { 113, 7, 240, 47 },
            {
                { x = 0, y = 0, color = 0xe3f4ff },
                { x = 0, y = 8, color = 0xdbeeff },
                { x = 19, y = 0, color = 0xf4feff },
                { x = 35, y = 2, color = 0xf8fcff },
                { x = 26, y = 17, color = 0xd6e9ff },
                { x = 54, y = 18, color = 0xd3e9ff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 信标烬按钮
function scanBeaconAshBtn()
    local x, y = myFindColor(
            { 824, 557, 963, 621 },
            {
                { x = 0, y = 0, color = 0x081421 },
                { x = 0, y = 54, color = 0x000810 },
                { x = 0, y = 27, color = 0x081421 },
                { x = 90, y = -1, color = 0x395d9c },
                { x = 131, y = 27, color = 0x18284a },
                { x = 79, y = 38, color = 0xf7f7f7 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 信标列表按钮
function scanBeaconListBtn()
    local x, y = myFindColor(
            { 13, 564, 202, 633 },
            {
                { x = 0, y = 0, color = 0xdf3a49 },
                { x = 168, y = 3, color = 0xffffff },
                { x = 1, y = 49, color = 0x6a3548 },
                { x = 168, y = 43, color = 0x414a69 },
                { x = 10, y = 6, color = 0xffffff },
                { x = 116, y = 26, color = 0xffffff },
                { x = 165, y = 7, color = 0x545274 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsBeaconListPage()
    local x, y = myFindColor(
            { 306, 3, 586, 50 },
            {
                { x = 0, y = 0, color = 0xffbfb0 },
                { x = 6, y = 12, color = 0xff9d8a },
                { x = 10, y = 3, color = 0xffac99 },
                { x = -11, y = 11, color = 0xffa08f },
                { x = 31, y = 23, color = 0x080808 },
                { x = 105, y = 3, color = 0xffc6b7 },
                { x = 95, y = 9, color = 0xffae99 },
                { x = 187, y = 25, color = 0x080808 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanFleetZuozhanPage()
    local page = ""
    keepScreen(true)
    if scanIsFleetZuozhanMain() then
        page = "zz_main"
    elseif scanIsFleetZuozhanEvent() then
        page = "zz_event"
    elseif scanIsFleetFormation() then
        page = "zz_formation"
    end

    keepScreen(false)
    return page
end

-- 阵容锁定
function scanIsFormationLockEnabled()
    local stand_color = { ["r"] = 85, ["g"] = 170, ["b"] = 119 }
    local c3i = GetRectColorAvg(1048, 469, 1135, 470)
    return (math.abs(c3i.r - stand_color.r) < stand_color.r * 0.1 and
            math.abs(c3i.g - stand_color.g) < stand_color.g * 0.1 and
            math.abs(c3i.b - stand_color.b) < stand_color.b * 0.1)
end

-- 自动寻敌
function scanIsAutoSeekEnabled()
    local stand_color = { ["r"] = 97, ["g"] = 142, ["b"] = 189 }
    local c3i = GetRectColorAvg(1048, 511, 1135, 512)
    return (math.abs(c3i.r - stand_color.r) < stand_color.r * 0.1 and
            math.abs(c3i.g - stand_color.g) < stand_color.g * 0.1 and
            math.abs(c3i.b - stand_color.b) < stand_color.b * 0.1)
end

-- 合计奖励页面
function scanIsTotalRewardsPage()
    local x, y = myFindColor(
            { 302, 84, 841, 601 },
            {
                { x = 0, y = 0, color = 0x5886c1 },
                { x = -122, y = 36, color = 0x4f89d1 },
                { x = -478, y = 0, color = 0x7e8082 },
                { x = -353, y = 38, color = 0xaaabad },
                { x = 16, y = 50, color = 0xffffff },
                { x = -469, y = -443, color = 0xffffff },
                { x = -410, y = -445, color = 0xffffff },
                { x = -384, y = -450, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )
    return (x > -1)
end

-- 合计奖励带研修页面
function scanIsTotalRewardsPage2()
    local x, y = myFindColor(
            { 241, 84, 905, 600 },
            {
                { x = 0, y = 0, color = 0x5381be },
                { x = -122, y = 36, color = 0x4f8ad2 },
                { x = -602, y = 0, color = 0x7e8184 },
                { x = -479, y = 38, color = 0xabadaf },
                { x = 16, y = 50, color = 0xffffff },
                { x = -593, y = -440, color = 0xffffff },
                { x = -432, y = -438, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )
    return (x > -1)
end

-- 地图：限时
function scanMapXianshi()
    local x, y = myFindColor(
            { 94, 4, 170, 49 },
            {
                { x = 0, y = 0, color = 0xe8f4ff },
                { x = 8, y = 0, color = 0xedf6ff },
                { x = 5, y = 12, color = 0xdaebfc },
                { x = 6, y = 14, color = 0xd7ebff },
                { x = 9, y = 12, color = 0xdcedff },
                { x = 17, y = 0, color = 0xf1f6ff },
                { x = 22, y = 14, color = 0xd8e9fd },
                { x = 33, y = -1, color = 0xfbfdff },
                { x = 32, y = 17, color = 0xd5eaff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 地图：无限时
function scanMapWuxianshi()
    local x, y = myFindColor({ 102, 7, 186, 39 },
            {
                { x = 0, y = 0, color = 0xdeefff },
                { x = 1, y = 18, color = 0xceebff },
                { x = 10, y = 16, color = 0xd6ebff },
                { x = 29, y = 3, color = 0xeff7ff },
                { x = 42, y = 5, color = 0xefffff },
                { x = 33, y = 16, color = 0xdeebff },
                { x = 49, y = 9, color = 0xefefff },
                { x = 60, y = 9, color = 0xdeefff },
                { x = 65, y = 9, color = 0xd6ebff }
            },
            95,
            0,
            0,
            0)
    return x > -1
end


-- 战斗计时器：0
function scanBattleTimer_0()
    local x, y = myFindColor(
            { 955, 32, 1005, 53 },
            {
                { x = 0, y = 0, color = 0x92fc63 },
                { x = 1, y = 0, color = 0x92fc63 },
                { x = 2, y = 0, color = 0x92fc63 },
                { x = 1, y = 1, color = 0x92fc63 },
                { x = 1, y = 4, color = 0x92fc63 },
                { x = 1, y = 7, color = 0x92fc63 },
                { x = 6, y = -1, color = 0x92fc63 },
                { x = 7, y = 3, color = 0x92fc63 },
                { x = 4, y = -3, color = 0x92fc63 },
                { x = 3, y = -3, color = 0x92fc63 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 战斗中
function scanIsCombat()
    local x, y = myFindColor(
            { 1014, 23, 1116, 59 },
            {
                { x = 0, y = 0, color = 0xeeeeee },
                { x = 5, y = -2, color = 0xf4f4f4 },
                { x = 8, y = 6, color = 0xebecef },
                { x = 18, y = -2, color = 0xf9f9ff },
                { x = 26, y = 7, color = 0xeeeeee },
                { x = 49, y = -8, color = 0xf2f2f2 },
                { x = 67, y = 9, color = 0xf3f3f3 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIslogin()
    local x, y = myFindColor({ 279, 6, 851, 624 },
            {
                { x = 0, y = 0, color = 0x23ade5 },
                { x = -5, y = -2, color = 0xffffff },
                { x = 203, y = 60, color = 0x23ade5 },
                { x = 113, y = -248, color = 0xfefef7 },
                { x = 111, y = -262, color = 0xacada5 }
            },
            95, 0, 0, 0)
    return x > -1
end

-- 战斗完成
function scanIsBattleComplete()
    local x, y = myFindColor(
            { 92, 36, 336, 116 },
            {
                { x = 0, y = 0, color = 0xf4f7ad },
                { x = 6, y = -1, color = 0xf5ed8f },
                { x = -8, y = 53, color = 0xffffa1 },
                { x = 45, y = 49, color = 0xffe170 },
                { x = 73, y = 39, color = 0xfdd670 },
                { x = 114, y = 55, color = 0xffff70 },
                { x = 193, y = 49, color = 0xf4c35b },
                { x = 195, y = 54, color = 0xffff63 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanBtnOverView()
    local x, y = myFindColor(
            { 533, 553, 1136, 637 },
            {
                { x = 0, y = 0, color = 0xffffff },
                { x = 4, y = -3, color = 0xd27d7d },
                { x = -170, y = -3, color = 0xbe5858 },
                { x = 3, y = 47, color = 0xc76e6d },
                { x = -117, y = 11, color = 0xffffff },
                { x = -20, y = 19, color = 0xffffff },
                { x = -75, y = 20, color = 0xffffff },
                { x = -8, y = 4, color = 0xce7979 }
            },
            95,
            0,
            0,
            0
    )
    return x, y
end

function scanIsOverView()
    local x1, y1 = myFindColor({ 502, 5, 573, 50 },
            {
                { x = 0, y = 0, color = 0xdbe2dd },
                { x = 1, y = 16, color = 0xd8e0d8 },
                { x = -5, y = 15, color = 0x3a3a3a },
                { x = 20, y = 6, color = 0x4e6bca }
            },
            95, 0, 0, 0)
    if x1 > -1 then
        local x, y = myFindColor(
                { 115, 7, 231, 45 },
                {
                    { x = 0, y = 0, color = 0xd3e9ff },
                    { x = 0, y = 14, color = 0xd1e9ff },
                    { x = 8, y = -2, color = 0xd5e9ff },
                    { x = 11, y = -2, color = 0xdaebfc },
                    { x = 10, y = 4, color = 0xd3e9ff },
                    { x = 14, y = 4, color = 0xd9eaff },
                    { x = 10, y = 14, color = 0xd1e9ff },
                    { x = 79, y = -4, color = 0xe6f3ff },
                    { x = 82, y = 10, color = 0xd3e9ff },
                    { x = 84, y = 13, color = 0xd2e9ff }
                },
                95,
                0,
                0,
                0
        )
        return x > -1
    else
        return false
    end

end

function scanQuickChallengePos()
    local x, y = myFindColor(
            { 327, 110, 1067, 574 },
            {
                { x = 0, y = 0, color = 0x5d8fc7 },
                { x = 66, y = -4, color = 0xffffff },
                { x = -58, y = -5, color = 0x618fc7 },
                { x = 70, y = 36, color = 0x4d7fb8 },
                { x = -584, y = -5, color = 0xf1a754 },
                { x = -461, y = -3, color = 0xffffff },
                { x = -458, y = 36, color = 0xefa755 },
                { x = -586, y = 34, color = 0xeb8b51 }
            },
            95,
            0,
            0,
            0
    )
    return x, y
end

function scanIsMeiri()
    local x, y = myFindColor(
            { 95, 8, 160, 43 },
            {
                { x = 0, y = 0, color = 0xe0f0ff },
                { x = -3, y = 2, color = 0xddeeff },
                { x = -1, y = 5, color = 0xdceeff },
                { x = 13, y = 16, color = 0xd8ebff },
                { x = 5, y = 9, color = 0xdceeff },
                { x = 24, y = 1, color = 0xf1ffff },
                { x = 38, y = 3, color = 0xf4feff },
                { x = 23, y = 10, color = 0xe2f1ff },
                { x = 37, y = 17, color = 0xd5ebff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsMeiriHome()
    -- meiri
    colors = {
        { 112, 26, 0xd6ebff },
        { 40, 590, 0xffffff },
        { 24, 319, 0xffffff },
        { 1110, 319, 0xffffff }
    }
    return cmpColor(colors)
end

function scanIsMeiriLevel()
    -- meiri:level
    colors = {
        { 137, 16, 0xf7fbff },
        { 1087, 126, 0xf7d342 },
        { 39, 629, 0x292839 }
    }
    return cmpColor(colors)
end

-- 扫描退役紫色船提示中的紫色船框框
function scanEliteConfirmInner()
    local x, y = myFindColor({ 358, 191, 775, 428 },
            {
                { x = 0, y = 0, color = 0xbfa3ff },
                { x = 0, y = 11, color = 0xbfa3ff },
                { x = 10, y = 0, color = 0xbfa3ff },
                { x = 70, y = 70, color = 0xbfa3ff },
                { x = 52, y = 70, color = 0xbfa3ff },
                { x = 70, y = 58, color = 0xbfa3ff }
            },
            95, 0, 0, 0)
    return x > -1
end

function scanRecycleConfirmInfo()
    -- 退役信息框
    local x, y = myFindColor({ 121, 12, 1008, 631 },
            {
                { x = 0, y = 0, color = 0x5381bc },
                { x = -329, y = 44, color = 0xca625c },
                { x = -833, y = -508, color = 0xffffff },
                { x = -185, y = 0, color = 0xb4635c }
            },
            95, 0, 0, 0)
    if x > -1 then
        -- 金币图标
        local x, y = myFindColor({ 151, 540, 631, 615 },
                {
                    { x = 0, y = 0, color = 0xfff361 },
                    { x = 9, y = 13, color = 0xefb545 },
                    { x = -3, y = 3, color = 0xcb8936 },
                    { x = 14, y = 10, color = 0xcb8c34 }
                },
                95, 0, 0, 0)
        if x > -1 then
            return true
        end
    end
    return false
end


-- 扫描退役紫色船提示的标题
function scanEliteConfirm()
    if not scanEliteConfirmInner() then
        return false
    end
    local x, y = myFindColor(
            { 252, 4, 882, 639 },
            {
                { x = 0, y = 0, color = 0x5171b1 },
                { x = 24, y = 25, color = 0xffffff },
                { x = 490, y = -1, color = 0xca5d5c },
                { x = 518, y = 15, color = 0xffffff },
                { x = 545, y = 33, color = 0xcc5f5e }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

-- 找到退役紫色船提示的确认按钮
function scanEliteConfirmButton()
    local x, y = myFindColor(
            { 252, 4, 882, 639 },
            {
                { x = 0, y = 0, color = 0xf7f7ff },
                { x = 34, y = 2, color = 0x39659c },
                { x = 51, y = 2, color = 0x5a8ece },
                { x = 8, y = 42, color = 0x4a86ce }
            },
            90, 0, 0, 0)
    return x, y
end

function scanEliteConfirm_1()
    local x, y = myFindColor(
            { 117, 21, 1007, 628 },
            {
                { x = 0, y = 0, color = 0x6b6b6b },
                { x = 134, y = 129, color = 0xffffff },
                { x = 511, y = 377, color = 0x3b6499 },
                { x = 510, y = 416, color = 0x4f8bd3 },
                { x = 349, y = 374, color = 0xa0a2a4 },
                { x = 350, y = 411, color = 0xabadaf }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanEliteConfirm_2()
    local x, y = myFindColor(
            { 117, 21, 1007, 628 },
            {
                { x = 0, y = 0, color = 0x6b6b6b },
                { x = 134, y = 97, color = 0xffffff },
                { x = 511, y = 407, color = 0x3c6399 },
                { x = 510, y = 447, color = 0x4f8cd5 },
                { x = 350, y = 406, color = 0xa0a2a3 },
                { x = 350, y = 442, color = 0xacaeb0 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsReadProtoc()
    local x, y = myFindColor(
            { 108, 22, 1027, 602 },
            {
                { x = 0, y = 0, color = 0x4fbdea },
                { x = -117, y = 61, color = 0x4fbdea },
                { x = -138, y = 1, color = 0xffffff },
                { x = -583, y = -386, color = 0xf2f4f6 },
                { x = 257, y = -384, color = 0xf2f4f6 }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function scanIsWeituoPage()
    local x, y = myFindColor(
            { 91, 5, 166, 44 },
            {
                { x = 0, y = 0, color = 0xdeefff },
                { x = -1, y = -7, color = 0xe7f2ff },
                { x = -5, y = -2, color = 0xddeeff },
                { x = 5, y = 5, color = 0xddeeff },
                { x = -3, y = 10, color = 0xd3e8ff },
                { x = 19, y = -5, color = 0xeff6ff },
                { x = 29, y = 1, color = 0xe7f3ff },
                { x = 35, y = 9, color = 0xd8e9ff }
            },
            95,
            0,
            0,
            0
    )
    return x > -1
end

function get_tech_time_str()
    local code, text = OCR.ky_time:getText(
            {
                rect = { 701, 255, 808, 286 },
                diff = { "0xffffff-0x0f0f0f" }
            }
    )
    if code == 0 then
        return trim(text)
    else
        return ""
    end
end

function scanYellowFaces()
    local point = myFindColors({ 95, 179, 800, 501 },
            {
                { x = 0, y = 0, color = 0xffc175 },
                { x = -6, y = -4, color = 0x2f1e08 },
                { x = 7, y = 1, color = 0x352413 },
                { x = -9, y = 12, color = 0xffc075 },
                { x = 11, y = -9, color = 0xffc175 }
            },
            95, 0, 0, 0)
    return point
end

function get_yellow_face_cells()
    local points_ = scanYellowFaces()
    local temps = {}
    for _, p_ in pairs(points_) do
        temps[get_pos_in_cell(p_.x, p_.y)] = true
    end
    local results = {}
    for i_, _ in pairs(temps) do
        table.insert(results, i_)
    end
    return results
end

function scanCVIconInCombat()
    local x, y = myFindColor({ 86, 0, 1135, 638 },
            {
                { x = 0, y = 0, color = 0xededed },
                { x = -2, y = -3, color = 0xfbfbfb },
                { x = 4, y = 5, color = 0xdddddd },
                { x = 6, y = -2, color = 0xfbfbfb },
                { x = -8, y = -3, color = 0x9980ba, offset = 0x404040 },
                { x = 16, y = 0, color = 0xf13c39 }
            },
            95, 0, 0, 0)
    return x > -1
end