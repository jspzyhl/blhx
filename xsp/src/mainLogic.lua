BattleRecovery = {}
function BattleRecovery:Init()
    self.RecoveryProc = nil -- function
    self.UnregistScanPath = nil -- string
end

function BattleRecovery:Regist(_recvproc, _unregist_scanpath)
    self.RecoveryProc = _recvproc
    self.UnregistScanPath = _unregist_scanpath
end

function BattleRecovery:doRecovery()
    if self.RecoveryProc ~= nil then
        self.RecoveryProc()
        self.RecoveryProc = nil
        return true
    else
        return false
    end
end

function BattleRecovery:UnregistByPath(_path)
    if self.UnregistScanPath == nil then
        return false
    end
    if self.UnregistScanPath == _path then
        self.RecoveryProc = nil
        self.UnregistScanPath = nil
        return true
    else
        return false
    end
end

--  刷图限制，返回：是否经历了等待
function FarmSpeedLimit()
    local bTimeWaited = false

    if SETTINGS.isSpeedLimiting == true then
        local round_per_hr = 0
        if STATS[8] ~= 0 then
            round_per_hr = STATS[5] / ((mTime() - STATS[8]) / 1000 / 3600)
            if round_per_hr > 0 then
                local waittime = ((3600000 * STATS[5]) / SETTINGS.max_round_per_h) + STATS[8] - mTime()
                if waittime > 0 then
                    bTimeWaited = true
                end
                while waittime > 0 do
                    showConsole("限制刷图效率，等待" .. string.format("%d", waittime / 1000) .. "秒")
                    mSleep(500)
                    waittime = ((3600000 * STATS[5]) / SETTINGS.max_round_per_h) + STATS[8] - mTime()
                end
            end
        end
    end

    if STATS[5] >= SETTINGS.round then
        showConsole("到达场数限制(" .. SETTINGS.round .. "场)，休息10min")
        sleep(60000)
        bTimeWaited = true
    else
        setNumberConfig("round", getNumberConfig("round", 0) + 1)
        if FREE and STATS[5] >= 2 then
            lua_restart()
        end
    end

    if bTimeWaited == true then
        hideConsole()
    end
    return bTimeWaited
end

function BuyCatboxLogic()
    -- 买猫
    click(440, 595, 560, 620) -- 生活区
    sleep(1000)
    click(780, 220, 950, 430) -- 指挥猫
    sleep(2500)
    click(880, 510, 955, 580) -- 订购
    for i = 1, 15 do
        -- +
        click(838, 256, 850, 269)
        sleep(1000)
    end
    click(761, 412, 852, 433) -- 结算
    sleep(1000)
    click(653, 389, 736, 411) -- 确认
    sleep(1000)

    local waitcount = 20
    local ordersuccess = false
    while waitcount > 0 do
        -- 等待交付
        if scanIsGetItems() then
            ordersuccess = true
            pressBack()
            sleep(1000)
            break
        end
        if scanIsGetItems2() then
            ordersuccess = true
            pressBack()
            sleep(1000)
            break
        end
        if scanIsGetItems3() then
            ordersuccess = true
            pressBack()
            sleep(1000)
            break
        end
        sleep(1000)
        waitcount = waitcount - 1
    end

    pressBack()
    sleep(1000)
    click(14, 9, 51, 39)
    sleep(2000)

    return ordersuccess
end

function recycleLogic()
    -- 退役过程
    if STATE.state == "" then
        print("Quit dockyard")
        click(762, 572, 906, 617) -- 退役页面取消按钮
        sleep(2000)
    else
        -- recycle
        if
        cmpColor(
                {
                    { 595, 38, 0xa8885a },
                    { 695, 9, 0x8e714a },
                    { 686, 29, 0xf5df8f },
                    { 622, 26, 0xce8461 }
                }
        )
        then
            -- 常用按钮被误开启
            click(595, 10, 705, 40) -- 点掉
            sleep(500)
        end

        click(981, 7, 1053, 39) -- filter
        while not scanInFilter() do
            sleep(200)
        end

        if not cmpColor({ { 419, 77, 0xbd8e52 } }) then
            click(396, 50, 511, 79) -- sort
            sleep(400)
        end

        if not cmpColor({ { 267, 138, 0x5279b5 } }) then
            click(256, 115, 370, 143) -- index
            sleep(400)
        end

        if not cmpColor({ { 267, 257, 0x4a75b5 } }) then
            click(258, 233, 369, 261) -- camp
            sleep(400)
        end

        if not cmpColor({ { 267, 379, 0x5279bd } }) then
            click(257, 351, 369, 379) -- rarity All
            sleep(400)
        end
        -- click(279, 400, 343, 417) -- rarity All
        -- sleep(400)

        -- if SETTINGS.isCleanWhite then
        --     click(403, 398, 500, 417) -- rarity white
        --     sleep(1000)
        -- end
        -- if SETTINGS.isCleanBlue then
        --     click(542, 395, 643, 418) -- rarity Blue
        --     sleep(1000)
        -- end

        if not cmpColor({ { 267, 445, 0x4a79b5 } }) then
            click(261, 419, 364, 441) -- index addition
            sleep(400)
        end

        click(643, 561, 771, 593) -- confirm
        sleep(1000)

        -- 手动选择被退役
        -- local selected = 0
        -- for _, y in ipairs({163, 369}) do
        --     for _, x in ipairs({148, 294, 442, 583, 732, 881, 1022}) do
        --         if selected > 9 then
        --             break
        --         else
        --             click(x - 3, y - 3, x + 3, y + 3)
        --             selected = selected + 1
        --         end
        --     end
        -- end

        -- 选完之后确定按钮还是暗色，判定为物资已满
        -- if
        --     cmpColor(
        --         {
        --             {998, 584, 0xa6a6a6},
        --             {1042, 602, 0xa6a6a6},
        --             {952, 572, 0x1d3659},
        --             {1025, 593, 0x24426a},
        --             {1098, 614, 0x325885}
        --         }
        --     )
        --  then
        --     showConsole("钱满，买猫")
        --     ForceHome()
        --     sleep(2000)
        --     if BuyCatboxLogic() then
        --         hideConsole()
        --         lua_restart()
        --     else
        --         showConsole("购买失败，停止刷图")
        --         SETTINGS.isDalao = false
        --         sleep(10000)
        --     end
        -- end

        click(570, 571, 714, 617) -- 一键退役
        sleep(1000)

        -- click(953, 572, 1097, 615) --  退役页面确定按钮
        -- sleep(1000)
        STATE.state = ""
    end
end

function shoppingLogic()
    click(21, 592, 128, 625) -- 商店
    sleep(1000)
    click(741, 429, 1032, 548) -- 补给商店
    sleep(1000)

    local table = {
        -- 军火商灰色图标
        { x = 0, y = 0, color = 0xb3b3b3 },
        { x = 1, y = -17, color = 0xb3b3b3 },
        { x = 21, y = 6, color = 0xb1b1b1 },
        { x = -15, y = 4, color = 0xb1b1b1 },
        { x = 10, y = 15, color = 0xb0b0b0 }
    }
    local x, y = myFindColor({ 395, 524, 1135, 638 }, table, 95, 0, 0, 0)
    if x > -1 then
        click(x - 40, y - 20, x + 40, y + 40) -- 点击军火商
        sleep(1000)
    end

    -- 447,256: gold
    for i = 0, 1 do
        y = 256 + 189 * i
        for j = 0, 4 do
            x = 447 + 138.5 * j
            if cmpColor({ { x, y, 0xfff363 } }) then
                -- gold
                if
                cmpColor({ { x, y - 61, { 0x73b2ff, 0xf7c363, 0xd6494a } } }) and
                        cmpColor({ { x, y - 86, { 0x73b2ff, 0xf7c363, 0xd6494a } } }) and
                        cmpColor({ { x + 55, y - 56, 0xffe37b } })
                then
                    click(x, y)
                    sleep(1000)
                    if scanIsInform2Confirm1() then
                        clickInform2Confirm()
                        sleep(1000)
                    end
                end
            end
        end
    end

    sleep(1500)
    ForceHome()
    sleep(500)
    return
end

local function hatchLogic()
    if not scanIsLivingAreaReady() then
        return
    end
    click(441, 591, 563, 626) -- living area
    sleep(2000)
    if not scanIsHatchReady() then
        pressBack()
        sleep(2000)
        return
    end
    click(797, 231, 944, 433) -- 指挥喵
    sleep(2500)

    -- 购买
    if cmpColor({ { 950, 593, 0xff5542 } }) then
        -- free
        click(902, 535, 934, 574)
        sleep(1000)
        for _ = 1, 20 do
            click(833, 250, 843, 264)
        end
        sleep(300)
        click(777, 417, 839, 433) -- buy
        sleep(1000)
        click(660, 388, 723, 413) -- 是
        sleep(1000)
        click(539, 415, 593, 435) -- confirm
        sleep(1000)
        click(539, 415, 593, 435) -- getitem
        sleep(1000)
        pressBack()
    end

    -- hatch
    if cmpColor({ { 1105, 594, 0xff5542 } }) then
        click(690, 483, 770, 507) -- complete all
        sleep(3000)
        for _ = 1, 30 do
            click(454, 132, 604, 150) -- 边框部分
        end
        sleep(2000)

        click(822, 483, 903, 510) -- train
        sleep(1000)
        click(690, 484, 769, 503) -- auto-select
        sleep(1000)
        click(822, 483, 900, 505) -- start-train
        sleep(1000)
        click(667, 388, 727, 410) -- confirm
        sleep(5000)
        pressBack()
        sleep(1000)
    end
    pressBack()
    sleep(1000)
end

local function _study_selectBook()
    local InitX = 209
    local InitY = 253
    local points = myFindColors(
            { 169, 236, 966, 466 },
            {
                { x = 0, y = 0, color = 0x92fc63 }, -- 绿色150%
                { x = -23, y = -9, color = 0x92fc63 },
                { x = 31, y = -3, color = 0x92fc63 },
                { x = 18, y = 17, color = 0xffffff },
                { x = -8, y = 16, color = 0xffffff }
            },
            95,
            0,
            0,
            0
    )

    local dx = 125
    local dy = 106
    local half = 45

    if #points > 0 then
        --默认第一行第一列的书
        local bestCell = { x = 0, y = 0 }

        local idv_best = 10000
        if SETTINGS.isBlueBookPreferred then
            idv_best = 0
        end

        for _, v in ipairs(points) do
            --找到的坐标位于书上部中间，对纵坐标加书标志半长调整为书中间
            local cellX = math.floor(((v.x - InitX) / dx))
            local cellY = math.floor(((v.y + half - InitY) / dy))

            --排序特征值，优先选择行最低，然后选择列最低
            local idv = cellX + cellY * 1000

            if
            (SETTINGS.isBlueBookPreferred and idv > idv_best) or
                    (not SETTINGS.isBlueBookPreferred and idv < idv_best)
            then
                bestCell.x = cellX
                bestCell.y = cellY
                idv_best = idv
            end
        end

        local cx = (InitX + half) + bestCell.x * dx
        local cy = (InitY + half) + bestCell.y * dy

        click(cx, cy)
        sleep(1000)
        click(917, 526, 1052, 568) -- 开始课程
    else
        click(917, 526, 1052, 568) -- 开始课程
    end
    sleep(1000)
end

local function _studyLogic()
    showConsole("自动学习")
    sleep(3000)
    if scanIsInform2Confirm1() then
        clickInform2Confirm() -- confirm
        sleep(3000)
        _study_selectBook()
        if scanIsInform2Confirm1() then
            -- 是否用书
            clickInform2Confirm() -- confirm
        end
        return true
    elseif scanIsInform1Confirm() then
        click(500, 440, 635, 471)
        sleep(1000)
    end
end

local function studyLogic()
    click(11, 132, 31, 157) -- click on extend
    sleep(1000)

    if cmpColor({ { 438, 367, 0xe79e31 } }) then
        click(345, 367, 441, 388)
        sleep(1000)
    else
        return
    end

    _studyLogic()
    _studyLogic()
    _studyLogic()
    _studyLogic()
    ForceHome()
    sleep(1000)
end

local function feedLogic()
    keepScreen(false)
    if not scanIsLivingAreaReady() then
        return
    end
    click(449, 600, 551, 622)

    sleep(2000)
    if not scanIsHouzhaiReady() then
        pressBack()
        sleep(2000)
        return
    end
    click(492, 232, 644, 416)

    for i = 1, 10 do
        if scanIsHouzhaiExpPage() then
            click(920, 542, 1045, 571)
            sleep(2000)
        elseif scanOutofFood() then
            click(391, 442, 529, 486)
            sleep(500)
        elseif scanIsHouzhai() then
            break
        else
            sleep(2000) -- 等待后宅加载
        end
    end

    sleep(1500)
    click(19, 590, 397, 606) -- food bar
    sleep(1000)

    -- 等待补充食物界面出现
    while not scanIsHouzhaiFoodSelect() do
        sleep(500)
    end

    -- pick food

    -- 可乐点十次
    for i = 1, 10 do
        click(270, 343, 357, 383) --  可乐
        sleep(300)
        if not checkThenClickFoodDlgCancel() then
            break
        end
        sleep(300)
    end

    if not SETTINGS.isRareFoodReserved then
        click(963, 346, 1049, 385) --  满汉全席
        sleep(300)
        checkThenClickFoodDlgCancel()
        sleep(300)

        click(824, 346, 910, 384) --  仰望星空
        sleep(300)
        checkThenClickFoodDlgCancel()
        sleep(300)
    end

    click(689, 344, 772, 381) --  海军咖喱
    sleep(300)
    checkThenClickFoodDlgCancel()
    sleep(300)

    if not SETTINGS.isTempuraFoodReserved then
        click(548, 345, 630, 381) --  天妇罗
        sleep(300)
        checkThenClickFoodDlgCancel()
        sleep(300)
    end

    click(408, 344, 494, 382) --  冷却水
    sleep(300)
    checkThenClickFoodDlgCancel()
    sleep(300)

    pressBack() -- quit bood bar
    sleep(300)

    local cnt = 0
    while true do
        cnt = cnt + 1
        if cnt > 6 then
            break
        end
        local x, y = scanHouzhaiPerk()
        if x > -1 then
            if x < 227 and y > 450 then
                break
            end -- 训练
            if x < 525 and y > 549 then
                break
            end -- 存粮
            if x > 1014 and y > 404 then
                break
            end -- 换层
            if x > 754 and y > 516 then
                break
            end -- 管理商店分享
            click(x, y)
            sleep(1000)
        else
            break
        end
    end

    pressBack() -- to main
    sleep(5000)
end

---@param tech_item_ TechItemData
function getResearchTypeStr(tech_item_)
    if tech_item_.scale == nil or tech_item_.type == nil or tech_item_.series == nil then
        return nil
    end

    local SIZE = {
        ['small'] = "小型",
        ['middle'] = "中型",
        ['large'] = "大型"
    }
    local CHAR = {
        ["Luoen"] = "罗恩",
        ["Luyi"] = "路易",
        ["Junzhu"] = "君主",
        ["Chuyun"] = "出云",
        ["Yichui"] = "伊吹",
        ["Haiwangxing"] = "海王星",
        ["Xiyatu"] = "西雅图",
        ["Zuozhiya"] = "佐治亚",
        ["Beifeng"] = "北风",
        ["Dadi"] = "腓特烈大帝",
        ["Jiasikenie"] = "加斯科涅",
        ["Wuqi"] = "吾妻",
        ["Chaijun"] = "柴郡",
        ["Deleike"] = "德雷克",
        ["Meiyinci"] = "美因茨",
        ["Aoding"] = "奥丁",
        ["Xiangbin"] = "香槟",
        ['Ankeleiqi'] = '安克雷奇',
        ['Bailong'] = '白龙',
        ['Aijier'] = '埃吉尔',
        ['Aogusite'] = '奥古斯特',
        ['Makeboluo'] = '马可波罗',
    }
    local RARITY = {
        ['r'] = '蓝',
        ['sr'] = '紫',
        ['ssr'] = '金',
        ['ur'] = '彩'
    }

    local SERIES = { "一期", "二期", "三期", "四期" }
    local str = tech_item_.index .. ": " .. SERIES[tech_item_.series] .. tech_item_.type .. "/" .. SIZE[tech_item_.scale] .. "/"
    if tech_item_.ship_name == "" or tech_item_.ship_name == nil then
        str = str .. "无蓝图"
    else
        str = str .. CHAR[tech_item_.ship_name]
    end
    str = str .. '/' .. RARITY[tech_item_.rarity]
    str = str .. '/' .. keepTwoDecimalPlaces(tech_item_.time_cost) .. 'H'
    return str
end

---@param tech_item_ TechItemData
function show_research_info(tech_item_)
    local str = getResearchTypeStr(tech_item_)
    showConsole(str)
end

local function ResearchLogic(_debugmode)
    _debugmode = _debugmode or false
    if _debugmode == true then
        showConsole("开始调试科研")
        sleep(2000)
    end

    click(576, 595, 703, 629) -- 主页科研按钮
    sleep(2000)
    if _debugmode == false then
        if not scanIsTechAcademyReady() then
            pressBack()
            sleep(2000)
            return
        end
    end

    click(254, 247, 437, 426) -- 军部研究室
    sleep(2000)

    -- start
    -- 检测已完成科研之前，要先打开一个科研，此时有可能碰巧是已完成的科研
    showConsole("检测已完成科研")
    clickTechMiddle() -- click on middle one
    sleep(1000)

    local retrycount = 8
    local research_finished = false
    local ResearchItemExpanded = false
    while retrycount > 0 do
        -- 等待获取物品
        if scanIsGetItems() then
            research_finished = true
            pressBack()
            sleep(1000)
            break
        end
        if scanIsGetItems2() then
            research_finished = true
            pressBack()
            sleep(1000)
            break
        end
        if scanIsGetItems3() then
            research_finished = true
            pressBack()
            sleep(1000)
            break
        end
        if scanIsResearchItemExpanded() then
            ResearchItemExpanded = true
            break
        end
        sleep(1000)
        retrycount = retrycount - 1
    end

    if research_finished then
        -- 再次点击中间的项目，展开该项目
        clickTechMiddle()
        sleep(1000)
    end

    -- 现在是一个科研被放大
    showConsole("扫描中...")

    local res_tab1 = ResTab:new()
    for i = 1, 5 do
        clickTechRightArrow() -- arror
        sleep(2000)
        ---@type TechItemData
        local tech_item = scanTechItemData()
        tech_item.index = i
        local str = getResearchTypeStr(tech_item)
        print(str)
        show_research_info(tech_item)
        res_tab1:add_item(ResData:make(i, tech_item.type, tech_item.scale, tech_item.series, tech_item.rarity, tech_item.time_cost, tech_item.ship_name, tech_item.ship_rarity))
    end
    sleep(2000)

    local picked_id_ = pick_best_item(res_tab1, res_sort, res_rule_data.group_by, res_rule_data.selection_rules, res_rule_data.order_by)
    --local BestItem = PriorityTable:SelectBestItem()
    if picked_id_ == nil then
        showConsole("无满足配置条件的项目，中止科研...")
        sleep(3000)
        ForceHome()
        sleep(1000)
        if _debugmode then
            showConsole("科研调试结束")
            sleep(2000)
            lua_restart()
        end
        return
    end

    local researchtype = getResearchTypeStr(res_tab1.item_data_tab[picked_id_])
    showConsole("选择: " .. researchtype)

    local leftMove = 5 - picked_id_
    if leftMove < picked_id_ then
        for i = 1, leftMove do
            clickTechLeftArrow()
            sleep(2000)
        end
    else
        for i = 1, picked_id_ do
            clickTechRightArrow()
            sleep(2000)
        end
    end

    ---- 开始研发
    --click(377, 500, 522, 532) -- 开始研发
    --sleep(1000)
    --if _debugmode == false then
    --    if scanIsInform2Confirm1() then
    --        clickInform2Confirm()
    --        sleep(1000)
    --    end
    --    STATS[7] = STATS[7] + 1
    --    ForceHome()
    --    sleep(1000)
    --else
    --    showConsole("科研调试结束")
    --    sleep(5000)
    --    lua_restart()
    --end


    -- 复检
    local tech_item = scanTechItemData()
    if res_data_equals(tech_item, res_tab1.item_data_tab[picked_id_]) then
        click(377, 500, 522, 532) -- 开始研发
        sleep(1000)
        if _debugmode == false then
            if scanIsInform2Confirm1() then
                clickInform2Confirm()
                sleep(1000)
            end
            STATS[7] = STATS[7] + 1
            ForceHome()
            sleep(1000)
        else
            showConsole("科研调试结束")
            sleep(5000)
            lua_restart()
        end
    else
        showConsole("选择项目与扫描结果不一致，中止科研...")
        sleep(3000)
        ForceHome()
        sleep(1000)
        if _debugmode then
            showConsole("科研调试结束")
            sleep(2000)
            lua_restart()
        end
    end
end

--- 特别作战章节选择逻辑  -----
-- 蝶海梦花
function ECS_Diehai()
    local exit_cond = false
    repeat
        local diehai_chapt = scanDiehaiMenghua()
        local working_on_xianshi = IsWorkingOn({ "14-1", "14-6" })
        local working_on_mengjing = IsWorkingOn({ "14-2", "14-3", "14-4", "14-5" })
        local at_mengjing = (diehai_chapt == "MengJing")
        local at_xianshi = (diehai_chapt == "XianShi")
        local exit_cond = (working_on_xianshi and at_xianshi) or (working_on_mengjing and at_mengjing)
        if exit_cond == false then
            if working_on_xianshi and at_mengjing then
                click(528, 242, 598, 304) -- 梦境切换按钮
                sleep(4000)
            elseif working_on_mengjing and at_xianshi then
                click(528, 242, 598, 304) -- 梦境切换按钮
                sleep(4000)
            end
        end
        sleep(600)
    until exit_cond
end

-- 如果活动图无特殊章节选择逻辑，此处应该设为nil
EVENT_CHAPTER_SELECT = nil

---------------------------------
local function levelLogic()
    if scanIsWorldLineSelect() then
        clickMainBattleLine()
    end

    local isSP, mode, chapter = scanChapter()
    print("mode=", mode, "chapter=", chapter, "isSP =", isSP)

    -- Select SP or regular
    if isSP then
        if SETTINGS.chapter < Lid_EventLevel_P1 then
            pressBack()
        end
    else
        --  活动图
        if IsWorkingOnEventLevel() then
            --  特别作战
            if scanIsEventChapter() == false then
                click(1048, 175, 1090, 199) -- 右上图标:特别作战（活动图）
                sleep(1000)
            end
        elseif IsWorkingOnTBYX() then
            if scanIsTBYX() == false then
                ForceHome()
                sleep(1000)
                click(540, 249, 593, 296) -- 点击特别演习
                sleep(2000)
            end
        elseif IsWorkingOnSpee() then
            --  档案：围剿斯佩伯爵
            clickOpFileBtn() --  点击作战档案
            sleep(2000)
            clickOpSPBtn()
            sleep(1000)
            click(656, 145, 1039, 275)
            sleep(1000)
        elseif IsWorkingOnOppcb() then
            --  档案：异色格象
            clickOpFileBtn() --  点击作战档案
            sleep(2000)
            clickOpEXBtn()
            sleep(1000)
            click(99, 408, 473, 537)
            sleep(1000)
        elseif IsWorkingOnIoLnD() then
            --  档案：光与影的鸢尾之华
            clickOpFileBtn() --  点击作战档案
            sleep(2000)
            clickOpEXBtn()
            sleep(1000)
            swipe(567, 484, 567, 265)
            sleep(1500)
            local pos_x = 231
            local pos_y = 507
            local x, y = myFindColor(
                    { 0, 0, 1135, 638 },
                    {
                        { x = 0, y = 0, color = 0xf80f1a },
                        { x = -61, y = 61, color = 0x5ec0e5 },
                        { x = -110, y = -17, color = 0xffde9c },
                        { x = 161, y = -29, color = 0xf6e8df },
                        { x = 253, y = 30, color = 0x67182e }
                    },
                    95,
                    0,
                    0,
                    0
            )
            if x > -1 then
                pos_x = x
                pos_y = y
            end
            click(pos_x, pos_y)
            sleep(1000)
        end
    end

    local level_xy = nil
    if IsWorkingOnTBYX() then
        level_xy = GetLevelPos(SETTINGS.chapter, SETTINGS.level, mode)
        RecordBeginTime()
    elseif SETTINGS.chapter >= 14 then
        isSP, mode, chapter = scanChapter()
        print("2-current chapter =", mode, chapter, "isSP =", isSP)

        -- --  用于缓解ui云层效果对识别的影响
        -- if isSP == true and mode == 'other' then
        --   local maxerr = 0.03           --  最大误差3%
        --   local x1 = 80                 --  困难切换按钮采样点
        --   local y1 = 575
        --   local lr,lg,lb = myGetColorRGB(x1, y1)
        --   local whiterr = math.abs(((lr+lb) / 2) - lg) / 255
        --   if whiterr > maxerr then
        --       if (lb-15) > lr  then   --  ui效果蓝色偏重，适当降低蓝色值，蓝色多于红色，判定当前为困难状态
        --           print('HARD')
        --           mode = 'hard'
        --       else
        --           print('NORM')
        --           mode = 'normal'
        --       end
        --   else
        --       print('UNKNOWN')
        --   end
        -- end

        -- mode
        if (mode == "normal" and SETTINGS.mode == "hard") or (mode == "hard" and SETTINGS.mode == "normal") then
            clickHardNormalswitch()
            sleep(1000)
        end

        if EVENT_CHAPTER_SELECT ~= nil then
            -- Click on Chapter arrow
            EVENT_CHAPTER_SELECT()
        elseif SETTINGS.chapter == Lid_EventLevel_P1 then
            prevChapter(2)
        elseif SETTINGS.chapter == Lid_EventLevel_P2 then
            -- prevChapter(2)
            nextChapter(2)
        end
        --  处理作战档案章节的切换
        if SETTINGS.chapter > Lid_TBYXLevel then
            local Chapter_offset = OPERATION_FILE_CHAPTER_OFFSET[SETTINGS.chapter]
            if Chapter_offset ~= nil then
                if Chapter_offset > 0 then
                    nextChapter(Chapter_offset)
                else
                    prevChapter(math.abs(Chapter_offset))
                end
            end
        end

        sleep(1000)
        -- Click on level
        level_xy = GetLevelPos(SETTINGS.chapter, SETTINGS.level, mode)
        if level_xy[1] == nil or level_xy[2] == nil then
            dialog("本关尚未制作(1型),请联系作者")
            lua_restart()
        end
    elseif chapter ~= nil then
        if mode == "hard" and SETTINGS.mode == "normal" then
            clickHardNormalswitch()
            sleep(1500)
        end

        if chapter < SETTINGS.chapter then
            nextChapter(SETTINGS.chapter - chapter)
        elseif chapter > SETTINGS.chapter then
            prevChapter(chapter - SETTINGS.chapter)
        end
        sleep(1500)

        if (mode == "normal" and SETTINGS.mode == "hard") then
            clickHardNormalswitch()
            sleep(1500)
        end

        -- Click on level
        level_xy = GetLevelPos(SETTINGS.chapter, SETTINGS.level, mode)
    else
        return -- cannot detect chapter
    end

    print("click level...")
    if level_xy == nil then
        dialog("您选择的关卡在游戏中不存在，请仔细检查")
        lua_restart()
    end
    click(level_xy[1] - 3, level_xy[2] - 3, level_xy[1] + 3, level_xy[2] + 3)
    MAP = Map:new()
end

local function sleep_yellow_face()
    ForceHome()
    sleep(1000)
    showConsole("黄脸，休息10min")
    sleep(600000)
end

local function formationLogic()
    local formation_type = scanFormationType()
    -- print('formation_type =', formation_type)
    if SETTINGS.rest_yellow_face then
        local yellow_faced = get_yellow_face_cells()
        for _, i_ in pairs(yellow_faced) do
            if SETTINGS.rest_yf[i_] == true then
                sleep_yellow_face()
                return
            end
        end
    end

    if formation_type == "yanxi" then
        if STATE.yanxied then
            pressBack(2)
        else
            STATE.yanxi_confirmed = STATE.yanxi_confirmed + 1
            if STATE.yanxi_confirmed >= 10 then
                STATE.yanxied = true
            end
            clickFormationWeightAnchor() -- 出击
            showConsole("演习第" .. STATE.yanxi_confirmed .. "次")
        end
    elseif formation_type == "beacon" then
        --
        showConsole("准备出击")
        sleep(500)
        MAP:applyAutoMode()
        clickFormationWeightAnchor() -- 出击
        STATE.beacon_battle_begin = true
    elseif formation_type == "meiri" then
        if SETTINGS.isMeiri and STATE.meiri_cycle <= 5 then
            for i_ = 1, 6 do
                -- body
                prevTeam(1)
                -- if not scanFMT_LeftArrow() then
                --     break
                -- else
                --     prevTeam(1)
                -- end
            end
            nextTeam(SETTINGS.meiri_team - 1)
            sleep(500)
            MAP:applyAutoMode()
            clickFormationWeightAnchor() -- 出击
            STATE.meiri_current_type_done = true
        elseif IsWorkingOnTBYX() then
            showConsole("准备出击")
            sleep(500)
            MAP:applyAutoMode()
            clickFormationWeightAnchor() -- 出击
            STATS[5] = STATS[5] + 1
        end
    elseif formation_type == "normal" then
        print("***XXX formation out of moveLogic!")
        showConsole("侦测到潜在卡顿/设定失误")
        MAP:applyAutoMode()
        MAP:applyRepair()
        MAP:autoSwitch()
        clickFormationWeightAnchor() -- 出击
        sleep(2500)
    end
    sleep(2500)
end

local function _weituoLogicPage(isNormal, target)
    target = target or -1
    local minRank = 99
    for i = 0, 1 do
        -- 每日
        if i > 0 then
            swipe(1070, 596, 1070, 78) -- next page
            sleep(2000)
        end
        local points = scanWeituoExp() -- get fix
        for _, p in ipairs(points) do
            local box = { 310, p.y - 30, 1024, p.y + 60 }
            local ret = scanWeituoScore(isNormal, box)
            drawDebugPriority(120, p.y, tostring(ret)) -- 绘制优先级
            print(ret)
            if target ~= -1 and ret == target then
                click(p.x - 10, p.y - 10, p.x + 10, p.y + 10)
                End_drawDebugPriority()
                return true
            end
            if minRank > ret then
                minRank = ret
            end
        end
        print("------")
        End_drawDebugPriority()
    end
    return minRank
end

function _weituoLogic()
    -- scan score
    showConsole("正在扫描委托优先级...")
    local minRank = 10 -- >=10, not do it
    click(23, 111, 67, 168) -- 每日
    local ret = _weituoLogicPage(true)
    if ret < minRank then
        minRank = ret
    end
    click(21, 192, 65, 255) -- 紧急
    sleep(1000)
    ret = _weituoLogicPage(false)
    if ret < minRank then
        minRank = ret
    end

    if minRank >= 10 then
        showConsole("无满足条件委托, 结束委托")
        return false
    end

    -- find the score and go inside and open dockyard
    showConsole("执行委托, 优先级" .. minRank)
    click(23, 111, 67, 168) -- 每日
    sleep(1000)
    local bool_isTargetFound = _weituoLogicPage(true, minRank)
    if bool_isTargetFound ~= true then
        click(21, 192, 65, 255) -- 紧急
        sleep(1000)
        _weituoLogicPage(false, minRank)
    end
    sleep(2500)

    showConsole("已找到委托, 优先级" .. minRank)
    if cmpColor({ { 1008, 292, 0xefc352 } }) then
        -- 误入进行中委托
        showConsole("误入进行中委托，退出")
    else
        click(777, 291, 881, 335) -- 推荐
        sleep(500)
        click(920, 295, 1020, 333)
        sleep(500)
        if scanIsInform2Confirm1() then
            clickInform2Confirm()
            sleep(1500)
        end
        STATS[6] = STATS[6] + 1
    end
    click(110, 95, 148, 252) -- 侧边收回委托框
    sleep(4000)
    return true
end

-- Now in "Main"
local function weituoLogic()
    STATE.weituo_first_done = true

    clickWeituoExpand() -- click on extend
    sleep(1000)
    click(84, 63, 134, 80) -- oil
    click(231, 63, 276, 83) -- gold
    click(390, 64, 433, 79) -- exp
    click(84, 63, 134, 80) -- oil
    click(231, 63, 276, 83) -- gold
    click(390, 64, 433, 79) -- exp
    sleep(1000)

    -- weituo完成
    local fetchWeituo_retyies = 0
    while scanIsWeituoBtnFinish() == true do
        clickWeituoBtn() -- 完成
        fetchWeituo_retyies = fetchWeituo_retyies + 1
        sleep(1500)
        if (fetchWeituo_retyies > 4) then
            --  多次尝试领取失败，判断钱满
            showConsole("钱满，买猫")
            ForceHome() -- 回主页
            sleep(2000)
            if BuyCatboxLogic() then
                showConsole("买好了，继续委托")
                clickWeituoExpand()
                sleep(1000)
                break
            else
                showConsole("购买失败，停止刷图")
                SETTINGS.isDalao = false
                sleep(10000)
                return
            end
        end
    end

    -- weituo:exp or getitems
    for i = 1, 12 do
        -- 12 instead of 8, to give it more time
        if scanIsWeituoExpanded() then
            break
        else
            click(222, 129, 263, 147)
            sleep(1500)
        end
    end

    local click_wt_btn_ret = 4
    while scanIsWeituoExpanded() and click_wt_btn_ret > 0 do
        clickWeituoBtn() -- 前往
        sleep(2000)
        if scanIsWeituoPage() then
            break
        end
        click_wt_btn_ret = click_wt_btn_ret - 1
    end

    if click_wt_btn_ret > 0 then
        for i = 1, 4 do
            -- (0/4)
            if
            cmpColor(
                    {
                        { 963, 21, 0x212431 },
                        { 963, 24, 0x212431 },
                        { 963, 28, 0x212431 }
                    }
            )
            then
                break
            end
            if not _weituoLogic() then
                break
            end
        end
    end
    ForceHome()
    sleep(2000)
end

function checkWeituo()
    -- 委托检测
    return SETTINGS.isWeituo and
            (scanWeituoBtn() and scanWeituoDot() or not STATE.weituo_first_done or mTime() - STATE.weituo_lasttime > 1800000)
end

function ws_checkWeituo()
    -- 委托检测
    return SETTINGS.isWeituo and
            (ws_scanWeituoBtn() and ws_scanWeituoDot() or not STATE.weituo_first_done or
                    mTime() - STATE.weituo_lasttime > 1800000)
end

local function gotoWeituo()
    -- 准备执行委托
    ------- 0 点重开
    if SETTINGS.reopen_after_midnight and daysSinceEpoch() > STATE.days_since_epoch then
        STATE.days_since_epoch = daysSinceEpoch()
        if DEVICE.isPrivate and SETTINGS.autoRestartApp then
            r = closeApp("com.bilibili.azurlane")
            showConsole("重启游戏刷新委托中。。。")
            STATE.team_selected = false
            STATE.form_selected = false
            STATE.accountability_setted = false
            sleep(5000)
        end
        STATE.meiri_cycle = 0
        STATE.yanxied = 0
        STATE.keyCollected = false
        STATE.pre_round = 0
        STATE.beacon_cycle = 0
        STATE.beacon_battle_retried = 0
        STATE.beacon_battle_begin = false
    else
        showConsole("开始委托")
        STATE.weituo_lasttime = mTime()
        ForceHome()
        sleep(1000)
        weituoLogic()
    end
end

-- 进图前可能的处理
function before_enter_level()
    local preround_modfied = false
    if STATE.pre_round < STATE.pre_round_setting then
        --  预刷次数+1
        STATE.pre_round = STATE.pre_round + 1
        preround_modfied = true
    end

    if not STATE.autoseek_enabled and not IsWorkingOnTBYX() then
        sleep(3000)
    end
end

function enter_level_redface()
    -- redface
    if SETTINGS.isSleepRedface and SETTINGS.chapter <= 15 then
        clickInform2Cancel() -- 左侧否
        if preround_modfied == true then
            --  恢复一次预刷次数
            STATE.pre_round = STATE.pre_round - 1
        end
        ForceHome()
        sleep(1000)
        showConsole("红脸，休息10min")
        sleep(600000)
        if SETTINGS.RestartAfterRest == true then
            r = closeApp("com.bilibili.azurlane")
            showConsole("休息完毕，重启游戏中")
            STATE.team_selected = false
            STATE.form_selected = false
            STATE.accountability_setted = false
            sleep(5000)
        end
    else
        clickInform2Confirm() -- 右侧是
    end
end

function in_map_redface()
    if SETTINGS.isSleepRedface then
        -- 红脸
        -- redface in formation
        if not STATE.autoseek_enabled then
            if not STATE.autoseek_enabled and MAP.initialized then
                MAP.numFighted = MAP.numFighted - 1 -- map模块刚刚战斗次数+1但是实际并没有战斗，所以-1
                if MAP.CurrentPlayer ~= nil then
                    MAP.CurrentPlayer.ammo = MAP.CurrentPlayer.ammo + 1 -- 退还弹药数量
                end
            end
        end
        clickInform2Cancel() -- 左侧否
        sleep(1000)
        ForceHome()
        sleep(1000)
        showConsole("红脸，休息10min")
        -- Click on level
        sleep(600000)
        --  注册恢复战斗
        BattleRecovery:Regist(
                function()
                    clickChuji() -- 主页出击
                    sleep(3000)
                    if not STATE.autoseek_enabled then
                        if
                        (SETTINGS.mob_team >= SETTINGS.boss_team) and (SETTINGS.autoRestartApp) and
                                (SETTINGS.RestartAfterRest)
                        then
                            --  道中队序号>=boss队序号的处理
                            click(843, 582, 987, 623) -- 切换
                            sleep(2000)
                        end
                        click(1023, 583, 1115, 621) -- 迎击
                        sleep(1500)
                        -- clickFormationWeightAnchor()
                        -- sleep(1500)
                        MAP.numFighted = MAP.numFighted + 1 -- 恢复战斗数
                        if MAP.CurrentPlayer ~= nil then
                            MAP.CurrentPlayer.ammo = MAP.CurrentPlayer.ammo - 1 -- 恢复弹药数量
                        end
                    end
                end,
                "map"
        ) --  取消恢复任务的条件：path == 'map'
        if SETTINGS.RestartAfterRest == true then
            showConsole("休息完毕，重启游戏中")
            r = closeApp("com.bilibili.azurlane")
            STATE.team_selected = false
            STATE.form_selected = false
            STATE.accountability_setted = false
            sleep(5000)
        end
    else
        clickInform2Confirm() --  右侧是，强制红脸出击
        sleep(2500)
    end
end

function set_fleet_accountability()
    if not STATE.accountability_setted and STATE.autoseek_enabled then
        STATE.accountability_setted = true
        local team1btn = scanTeamChooseBtn_1()
        local team2btn = scanTeamChooseBtn_2()
        local ret_cnt = 20
        while (team1btn or team2btn) and ret_cnt > 0 do
            clickFleetAccountability()
            sleep(200)
            ret_cnt = ret_cnt - 1
            team1btn = scanTeamChooseBtn_1()
            team2btn = scanTeamChooseBtn_2()
        end

        if SETTINGS.mob_team < SETTINGS.boss_team then
            clickTeam1_mob()
        elseif SETTINGS.mob_team > SETTINGS.boss_team then
            clickTeam1_boss()
        else
            if SETTINGS.mode == "hard" then
                if SETTINGS.mob_team > 1 then
                    clickTeam1_idle()
                else
                    clickTeam1_all()
                end
            end
        end
        sleep(500)
    end
end

-- 自动推荐舰队作战
function do_FleetZuozhan()
    local last_zz_page = ""

    while true do
        local zz_page = scanFleetZuozhanPage()
        if zz_page == "zz_main" then
            local bx, by = scanFleetZuozhanRedDot_Blue()
            if bx > -1 then
                click(bx, by)
                sleep(1000)
                bx, by = scanFleetZuozhanRedDot_Blue()
                click(bx, by - 50)
                sleep(1000)
            else
                local gx, gy = scanFleetZuozhanRedDot_Gold()
                if gx > -1 then
                    click(gx, gy)
                    sleep(1000)
                    gx, gy = scanFleetZuozhanRedDot_Gold()
                    click(gx, gy - 50)
                    sleep(1000)
                end
            end
        elseif zz_page == "zz_event" then
            -- pass
            if scanFleetZuozhanEventPaiqian() then
                click(858, 562, 1010, 593) -- 立即派遣
            else
                FleetZuozhanClose()
            end
            sleep(1000)
        elseif zz_page == "zz_formation" then
            -- pass
        end
        sleep(500)
    end
end

local function swipe_and_accept()
    swipe(568, 410, 568, -6000)
    sleep(2000)
    click(608, 512, 739, 561)
    sleep(1500)
end

--------**********************************************************

local path = "#"
local prev_cnt = 0
local login_tried = 0
local max_login_retries = 30

local function reset_states()
    STATE.team_selected = false
    STATE.form_selected = false
    STATE.accountability_setted = false
    STATE.combat_begin_time = 0
    prev_cnt = 0
    login_tried = 0
end

local function do_restart_game()
    r = closeApp("com.bilibili.azurlane")
    showConsole("游戏卡住, 重启中。。。")
    reset_states()
    sleep(5000)
end

function mainLogic()

    while true do
        ------- 防闪退
        if DEVICE.isPrivate and SETTINGS.autoRestartApp then
            if frontAppName() ~= "com.bilibili.azurlane" then
                r = runApp("com.bilibili.azurlane")
                showConsole("闪退重启中。。。")
                reset_states()
                sleep(30000)
            end
        end

        ------- loop
        STATE.last_scan_result = path
        path = scanMain()
        if STATE.last_scan_result == path then
            prev_cnt = prev_cnt + 1
        else
            prev_cnt = 0
        end
        keepScreen(false) -- in case returned early
        print("path =", path)
        -- showConsole('path ='..path)
        --if ((mTime() - STATE.combat_begin_time) / 1000) < 10 then

        if DEVICE.isPrivate and SETTINGS.autoRestartApp then
            if prev_cnt > 350 or login_tried >= max_login_retries or
                (STATE.combat_begin_time > 0 and ((mTime() - STATE.combat_begin_time) / 1000) > 350)
            then
                do_restart_game()
                -- end
            end
        end
        
        if path == "preload" then
            if login_tried < max_login_retries then
                click(451, 78, 1031, 385)
                login_tried = login_tried + 1
            end
        elseif path == "preload:startgame" then
            -- showConsole('60s后重新登录')
            -- sleep(60000)
            click(381, 319, 753, 413)
        elseif path == "preload:servers" then
            sleep(2000)
        elseif path == "login:bilibili" then
            click(597, 421, 795, 476)
            sleep(5000)
        elseif path == "star:4" then
            click(840, 502, 905, 565)
            sleep(300)
        elseif path == "announce" then
            -- 每日签到活动总览
            click(876, 60, 997, 69)
            sleep(1000)
            click(1035, 45, 1083, 75)
        elseif path == "activity" then
            ForceHome()
        elseif path == "main" then
            STATE.combat_begin_time = 0
            if BattleRecovery:doRecovery() == false then
                -- 用于恢复之前的一些过程
                clickChuji() --  主页出击
                sleep(1000)
            end
        elseif path == "weituo" then
            ForceHome()
            sleep(500)
        elseif path == "main:weituo" then
            pressBack()
        elseif path == "formation" then
            formationLogic()
        --elseif path == "Fleet" then
        --    --  大舰队页面
        --    if scanIsFleetHouqin() then
        --        click(951, 250, 1083, 281) -- 接受任务
        --        sleep(500)
        --        clickInform2Confirm()
        --    elseif scanIsFleetZuozhan() then
        --        -- do_FleetZuozhan()
        --    end
        --    sleep(500)
        --    pressBack()
        elseif path == "combat" then
            if scanBattleTimer_0() then
                if STATE.map_entered then
                    STATE.map_entered = false
                    STATE.combat_begin_time = 0
                    STATE.submarine_enabled = true
                    IncCombatCount()
                end

                local console_str = string.format("统计：紫:%d/金:%d/场次:%d/战斗次数:%d", STATS[3], STATS[4], STATS[5], STATS[9])
                showConsole(console_str)

                if STATE.combat_begin_time == 0 then
                    STATE.combat_begin_time = mTime()
                end

                if STATE.submarine_enabled == true then
                    if SETTINGS.sp4_special and IsWorkingOn({ '14-4', }) then
                        if ((mTime() - STATE.combat_begin_time) / 1000) < 10 then
                            if scanCVIconInCombat() then
                                if scanIsSubmarineUsed() == false then
                                    click(630, 527, 686, 578)
                                    sleep(500)
                                end
                            end
                        end
                    end

                    if SETTINGS.call_submarine then
                        if ((mTime() - STATE.combat_begin_time) / 1000) > SETTINGS.call_submarine_delay then
                            if scanIsSubmarineUsed() == false then
                                click(630, 527, 686, 578)
                                sleep(1000)
                            end
                        end
                    end
                end

                sleep(500)
            end
        elseif path == "ambush" then
            click(877, 403, 1013, 434)
            sleep(1000)
        elseif path == "fleetinform:2confirm1" then
            click(429, 407, 497, 423) -- 我知道了
            sleep(1000)
        elseif path == "auto:confirm" then
            pressAutoCombatOK()
        elseif path == "inform:3confirm3" then
            if SETTINGS.isCleanWhite or SETTINGS.isCleanBlue then
                click(319, 436, 460, 472) -- 整理
                sleep(1500)
                STATE.state = "recycle"
            end
        elseif path == "inform:2confirm1" then
            if STATE.last_scan_result == "formation" then
                -- 编队页面
                --  出击页面双选对话框
                if scanIsMoneyFullDlg() then
                    -- lua_restart()
                    -- elseif scanIsUsingTicketDlg() then
                    --   clickInform2Cancel()  -- 左侧否
                    --   sleep(2000)
                    showConsole("钱满，买猫")
                    clickInform2Cancel() -- 左侧否
                    sleep(1000)
                    ForceHome() -- 回主页
                    sleep(2000)
                    if BuyCatboxLogic() then
                        showConsole("买好了，继续刷图")
                        click(910, 269, 1008, 371) -- 主页出击
                        sleep(3000)
                        click(1023, 583, 1115, 621) -- 迎击
                        sleep(1000)
                        clickFormationWeightAnchor()
                        sleep(1000)
                    else
                        showConsole("购买失败，停止刷图")
                        SETTINGS.isDalao = false
                        sleep(10000)
                    end
                elseif scanIsRedfaceDlg() then
                    in_map_redface()
                else
                    --  出击页面未知类型的对话框，点否
                    clickInform2Cancel() -- 左侧否
                    sleep(1000)
                end
            elseif STATE.last_scan_result == "map" then
                -- 自律寻敌过程中出现的弹框
                -- body
                if scanIsRedfaceDlg() then
                    in_map_redface()
                end
            elseif STATE.enter_level_clicked then
                -- 非编队页面，如果已经点击进图判定为进图页面
                enter_level_redface() -- 处理进图红脸
            else
                clickInform2Confirm() -- 右侧是，其他情况的确定
            end
        elseif path == "inform1:confirm1" then
            clickInform1Confirm()
        elseif path == "inform:repair" then
            click(414, 402, 524, 434)
            sleep(1000) -- 左侧否, 不使用
        elseif path == "inform:update" then
            click(632, 425, 765, 463)
            sleep(1000) -- 确定
        elseif path == "inform:certificate" then
            pressBack() -- 应援资格验证码，点返回
            sleep(1000)
        elseif path == "lootship" then
            sleep(2000) -- wait for animation

            if scanLootShipNew() and scanLootShipUnlocked() then
                click(39, 495, 73, 528) -- 左下角锁定按钮
                sleep(1000)
            end

            local star = scanLootColor()
            showConsole(
                    "统计：白/蓝/紫/金 (场次) = " ..
                            STATS[1] .. "/" .. STATS[2] .. "/" .. STATS[3] .. "/" .. STATS[4] .. " (" .. STATS[5] .. ")"
            )
            print("loot star =", star)
            click(632, 435, 765, 468) -- 锁船确认位置

            -- inform:2confirm1, lock
            if scanIsInform2Confirm1() then
                clickInform2Confirm() -- 锁船确认位置
                sleep(500)
            end

            sleep(1000)
        elseif path == "dockyard" then
            -- 开始退役过程
            recycleLogic()
        elseif path == "recycle:confirm" then
            -- 退役信息框
            click(836, 557, 963, 589) -- confirm
            STATE.state = ""
            sleep(1000)
        elseif path == "recycle:confirm:elite" then
            local x, y = scanEliteConfirmButton();
            click(x + 10, y + 10, x + 140, y + 40) -- 确定
            sleep(1000)
        elseif path == "recycle:gear" then
            click(786, 465, 914, 499)
            sleep(300)
        elseif path == "recycle:gear:material" then
            click(646, 490, 788, 522)
            sleep(300)
            STATE.state = ""
        elseif path == "weituo:exp" then
            clickExpPage()
            sleep(500)
        elseif path == "BattleComplete" then
            clickBattleCompleteOK()
            if STATE.beacon_battle_begin then
                STATE.beacon_cycle = STATE.beacon_cycle - 1
                STATE.beacon_battle_begin = false
            end
        elseif path == "victory" or path == "victory:exp" then
            STATE.submarine_enabled = false

            clickExpPage()
            sleep(500)
            --  战斗结算后等待，防止将切换页面过程的背景识别为level页面
            if path == "victory:exp" then
                if STATE.autoseek_enabled == false then
                    sleep(SETTINGS.wait_after_rating)
                end
            end
        elseif path == "failtip" then
            -- 战斗失败提升提示页面，自动寻敌模式失败后也会出现这个页面
            click(463, 527, 651, 564)
            sleep(500)
        elseif path == "weituo:expended" then
            -- elseif path == "fail" then
            --     click(907, 520, 1063, 562)
            --     sleep(3000)
            -- elseif path == "fail:yanxi" then
            --     click(907, 520, 1063, 562)
            --     sleep(3000)
            click(907, 520, 1063, 562)
            sleep(1000)
        elseif path == "getitems" then
            click(907, 520, 1063, 562)
            sleep(500)
        elseif path == "yanxizuozhan" then
            if SETTINGS.isYanxizuozhan then
                showConsole("演习作战:" .. STATE.yanxizuozhan_round)
                if STATE.yanxizuozhan_round >= SETTINGS.yanxizuozhan_max then
                    pressBack()
                    sleep(1000)
                else
                    STATE.yanxizuozhan_round = STATE.yanxizuozhan_round + 1
                    if SETTINGS.yanxizuozhan_level == 3 then
                        click(860, 189, 1045, 445)
                    elseif SETTINGS.yanxizuozhan_level == 2 then
                        click(535, 249, 649, 400)
                    elseif SETTINGS.yanxizuozhan_level == 1 then
                        click(164, 221, 241, 436)
                    end
                    sleep(2000)
                    if STATE.yanxizuozhan_round == 1 then
                        prevTeam(6)
                        sleep(500)
                        nextTeam(SETTINGS.yanxizuozhan_team - 1)
                        sleep(1500)
                    end
                    click(827, 528, 1042, 593)
                    sleep(500)
                end
            else
                pressBack()
                sleep(500)
            end
        elseif path == "level" then
            local function doLevel()
                -- 主线刷图间歇检测委托完成
                if checkWeituo() then
                    gotoWeituo()
                elseif SETTINGS.isDalao then
                    --  经历过限速等待则应重新检测当前页面
                    if FarmSpeedLimit() == false then
                        showConsole("开始打捞")
                        process_SETTINGS()
                        levelLogic()
                    end
                end
            end

            STATE.combat_begin_time = 0
            if STATE.autoseek_enabled == true then
                if STATE.autoseek_round_end == true then
                    doLevel()
                end
            else
                doLevel()
            end
        elseif path == "world_line_select" then
            --    elseif path == 'tebieyanxi' then
            --      click(395,477,577,506)
            --      sleep(3000)
            --      if scanIsInform2Confirm1() then
            --        if SETTINGS.isSleepRedface then
            --          showConsole('红脸，休息10分钟。。。')
            --          click(359,421,511,459)
            --          sleep(600000)
            --        else
            --          click(623,422,780,458)
            --          sleep(3000)
            --        end
            --      end
            -- 防止卡作战档案页面
            local isSP, mode, chapter = scanChapter()

            if ws_checkWeituo() then
                gotoWeituo()
            elseif SETTINGS.isYanxi and not STATE.yanxied then
                if isSP then
                    pressBack(2)
                end
                showConsole("开始演习")
                ws_clickYanxi()
                sleep(1500)
            elseif ShouldBeaconBattle() then
                showConsole("开始信标挑战")
                clickOPERATION()
                sleep(3000)
            elseif SETTINGS.isMeiri and STATE.meiri_cycle <= 5 then
                if isSP then
                    pressBack(2)
                end
                showConsole("开始日常")
                ws_clickMeiri()
            elseif SETTINGS.isKeyan and mTime() - STATE.keyan_lasttime > 1800000 then
                showConsole("开始科研")
                STATE.keyan_lasttime = mTime()
                ForceHome()
                sleep(1000)
                ResearchLogic(SETTINGS.debug_research)
            elseif SETTINGS.isFeed and mTime() - STATE.feed_lasttime > 1800000 then
                showConsole("开始投喂+拾取")
                STATE.feed_lasttime = mTime()
                ForceHome()
                sleep(1000)
                feedLogic()
            elseif SETTINGS.isStudy and mTime() - STATE.study_lasttime > 1800000 then
                showConsole("开始学习技能")
                STATE.study_lasttime = mTime()
                ForceHome()
                sleep(1000)
                studyLogic()
            elseif SETTINGS.isRenwu and mTime() - STATE.renwu_lasttime > 1800000 then
                showConsole("开始任务")
                STATE.renwu_lasttime = mTime()
                STATE.award_received = false
                ForceHome()
                sleep(1000)

                if scanIsRenwuReady() then
                    click(727, 601, 825, 621)
                    sleep(1000)
                end
            elseif SETTINGS.isHatch and mTime() - STATE.hatch_lasttime > 1800000 then
                showConsole("开始孵喵")
                STATE.hatch_lasttime = mTime()
                ForceHome()
                sleep(1000)
                hatchLogic()
            elseif SETTINGS.isShopping and mTime() - STATE.shopping_lasttime > 10800000 then
                showConsole("开始买书")
                STATE.shopping_lasttime = mTime()
                ForceHome()
                sleep(1000)
                shoppingLogic()
            elseif SETTINGS.isCollectKey and not STATE.keyCollected then
                if isSP then
                    pressBack(2)
                end
                STATE.keyCollected = true
                showConsole("档案密匙")
                click(156, 521, 273, 565)
                sleep(1000)
                click(233, 40, 292, 58)
                sleep(1000)
                click(319, 35, 392, 63) -- 旁边空白
                sleep(1000)
                click(319, 35, 392, 63) -- 旁边空白
                sleep(1000)
                click(319, 35, 392, 63) -- 旁边空白
                sleep(1000)
            elseif SETTINGS.pre_SOS and not STATE.SOS_done then
                if isSP then
                    pressBack(2)
                end
                showConsole("开始救援")
                ws_clickRescue() -- 求救信号
                sleep(1000)

                click(769, 525, 886, 555) -- 搜索信号
                sleep(6000)

                if scanIsInform2Confirm1() then
                    clickInform2Cancel() -- 取消
                    sleep(1000)
                elseif cmpColor({ { 755, 519, 0xc67d10 }, { 899, 520, 0xde9629 } }) then
                    -- 没有前往
                    STATE.SOS_done = true
                    click(869, 73, 909, 104) -- close window
                else
                    local checkMap = scanMain()

                    if checkMap == "level" then
                        click(1046, 456, 1085, 494) -- 求救信号
                        sleep(1000)
                        -- 第一个搜索/当前关
                        click(764, 197, 864, 222) -- 前往
                        sleep(2000) -- wait for animation of 5th level

                        local isSP, mode, chapter = scanChapter()
                        if chapter ~= nil then
                            if chapter > tonumber(SETTINGS._chapter) + 1 then
                                showConsole("信号等级过高,停止救援")
                                STATE.SOS_done = true
                                sleep(3000)
                            else
                                SETTINGS.chapter = chapter
                                SETTINGS.level = 5
                                level_xy = CHAPTER_CONFIG[chapter][5]
                                click(level_xy[1] - 3, level_xy[2] - 3, level_xy[1] + 3, level_xy[2] + 3)
                                MAP = Map:new()
                            end
                        end
                    end
                end
            elseif SETTINGS.isDalao then
                --  经历过限速等待则应重新检测当前页面
                if FarmSpeedLimit() == false then
                    showConsole("开始打捞")
                    process_SETTINGS()
                    levelLogic()
                end
            else
                if SETTINGS.isFeed or SETTINGS.isWeituo or SETTINGS.isKeyan then
                    if not SETTINGS.isDalao then
                        showConsole("休息10分钟。。。")
                        sleep(600000)
                    else
                        showConsole("休息30分钟。。。")
                        sleep(1800000)
                    end
                else
                    lua_restart()
                end
            end
        elseif path == "OPERATION_MAP" then
            if ShouldBeaconBattle() then
                local btn_ov_x, btn_ov_y = scanBtnOverView()
                click(btn_ov_x - 70, btn_ov_y + 20)
                sleep(2000)
            else
                ForceHome()
            end
        elseif path == "OVERVIEW" then
            if ShouldBeaconBattle() then
                if scanBeaconAshBtn() then
                    clickBeaconAsh()
                end
            else
                ForceHome()
            end
        elseif path == "MYBEACON" then
            if ShouldBeaconBattle() then
                clickBeaconList()
            else
                ForceHome()
            end
        elseif path == "BEACONLIST" then
            if STATE.last_scan_result ~= "BEACONLIST" then
                STATE.beacon_battle_retried = 0
            end
            if ShouldBeaconBattle() then
                if STATE.beacon_battle_retried < 5 then
                    clickBeaconBattleStart()
                    STATE.beacon_battle_retried = STATE.beacon_battle_retried + 1
                    sleep(500)
                else
                    STATE.beacon_battle_begin = false
                    STATE.beacon_battle_retried = 0
                    STATE.beacon_cycle = 99
                    ForceHome()
                end
            else
                ForceHome()
            end
        elseif path == "OpFile" then
            pressBack()
            sleep(1000)
        elseif path == "renwu" then
            if STATE.award_received == false then
                if cmpColor({ { 919, 4, 0xd6b231 }, { 984, 3, 0xdeb231 } }) then
                    click(926, 9, 968, 30)
                    sleep(1000)
                elseif cmpColor({ { 1008, 81, 0x6b92ef }, { 1023, 91, 0x638ee7 } }) then
                    click(976, 109, 1036, 147)
                    sleep(1000)
                end
                STATE.award_received = true
            end
            pressBack()
            sleep(1000)
        elseif path == "yanxi" then
            if SETTINGS.yanxi_left then
                click(103, 102, 266, 330)
            else
                click(751, 101, 925, 333)
            end
            sleep(2000)
        elseif path == "yanxi:confirm" then
            click(487, 485, 649, 525)
            sleep(1500)
        elseif path == "meiri" then
            if not SETTINGS.isMeiri then
                ForceHome()
                sleep(500)
            else
                if scanIsMeiriHome() then
                    sleep(500)
                    STATE.meiri_cycle = STATE.meiri_cycle + 1
                    print("Meiri Cycle =", STATE.meiri_cycle)
                    click(755, 174, 855, 514) -- next
                    STATE.meiri_current_type_done = false
                    sleep(1000)

                    if STATE.meiri_cycle > 5 then
                        pressBack()
                        sleep(1000)
                    elseif
                    (not cmpColor(
                            {
                                { 505, 446, 0xffffff },
                                { 549, 427, 0xff9239 }
                            }
                    )) and not cmpColor({ { 526, 330, 0xad3839 }, { 547, 329, 0xad3839 } })
                    then
                        click(495, 174, 635, 553) -- 进入每日项目
                        sleep(2000)
                    end
                elseif scanIsMeiriLevel() then
                    if cmpColor({ { 165, 430, 0x42e7ff } }) then
                        if SETTINGS.yanxiu_mission == 1 then
                            click(457, 151, 778, 221)
                        elseif SETTINGS.yanxiu_mission == 2 then
                            click(466, 286, 798, 358)
                        else
                            click(476, 426, 776, 495)
                        end
                    else
                        click(449, 144, 614, 216)
                    end

                    sleep(2000)
                    if scanIsMeiriLevel() then
                        pressBack()
                        sleep(1000)
                        if STATE.meiri_current_type_done then
                            STATE.meiri_cycle = 0
                        end
                    else
                        -- formation
                        -- set STATE.meiri_current_type_done = true there
                    end
                end

                -- 点击快速挑战
                qc_pos_x, qc_pos_y = scanQuickChallengePos()
                if qc_pos_x > -1 then
                    click(qc_pos_x, qc_pos_y)
                    sleep(1000)
                end
            end
        elseif path == "total_rewards" or path == "total_rewards2" then
            -- 自律寻敌关卡结束
            STATE.autoseek_round_end = true
            if STATE.autoseek_enabled == true then
                if checkWeituo() then
                    clickLeave(path)
                    gotoWeituo()
                elseif SETTINGS.isDalao then
                    --  经历过限速等待则应重新检测当前页面
                    if FarmSpeedLimit() == false then
                        local old_chapt = SETTINGS.chapter
                        local old_lv = SETTINGS.level
                        local old_mode = SETTINGS.mode
                        local old_mobteam = SETTINGS.mob_team
                        local old_bossteam = SETTINGS.boss_team
                        local old_subteam = SETTINGS.sub_team
                        process_SETTINGS()
                        if
                        STATE.autoseek_enabled == true and
                                old_chapt == SETTINGS.chapter and
                                old_lv == SETTINGS.level and
                                old_mode == SETTINGS.mode and
                                old_mobteam == SETTINGS.mob_team and
                                old_bossteam == SETTINGS.boss_team and
                                old_subteam == SETTINGS.sub_team
                        then
                            showConsole("继续打捞")
                            clickAgain(path)
                            before_enter_level()
                        else
                            hideConsole()
                            clickLeave(path)
                        end
                    else
                        clickLeave(path)
                    end
                else
                    clickLeave(path)
                end
            else
                clickLeave(path)
            end
        elseif path == "level:confirm" then
            --  关卡确认
            sleep(1000)
            if STATE.autoseek_enabled then
                local is_auto_seek_found = scanLevelConfirmAutoSeek()
                if is_auto_seek_found ~= STATE.autoseek_enabled then
                    click(778, 528) -- 切换自律寻敌
                    sleep(500)
                end
            end
            clickLevelStart() --  按钮:立刻前往
            sleep(500)
        elseif path == "teamselection:moew" then
            click(1074, 143, 1097, 190)
            sleep(500)
        elseif path == "teamselection" then
            showConsole("选择队伍中")

            if SETTINGS.mode == "normal" and not IsWorkingOnTBYX() then
                local submbtn = scanSubmChooseBtn()
                clickClear_sub() -- clear sub
                if SETTINGS.sub_team > 0 and scanIsSubTeamCanSelect() then
                    clickChoose_sub() -- choose sub
                    showConsole("选择潜艇队伍")
                    while not scanIsFleetExpanded() and submbtn do
                        sleep(200)
                    end

                    clickTeam(3, SETTINGS.sub_team)
                    while scanIsFleetExpanded() and submbtn do
                        sleep(200)
                    end
                end
            end

            if SETTINGS.mode == "normal" and not STATE.team_selected and not IsWorkingOnTBYX() then
                local team1btn = scanTeamChooseBtn_1()
                local team2btn = scanTeamChooseBtn_2()
                local retry_cnt = 50

                while not team1btn and not team2btn and retry_cnt > 0 do
                    clickFleetBiandui()
                    sleep(200)
                    team1btn = scanTeamChooseBtn_1()
                    team2btn = scanTeamChooseBtn_2()
                    retry_cnt = retry_cnt - 1
                end

                STATE.team_selected = true
                clickClear_2() -- clear 2nd
                clickClear_1() -- clear 1st

                retry_cnt = 50
                for _ = 1, 2 do
                    clickChoose_1() -- choose 1st
                    showConsole("选择1队")
                    retry_cnt = 50
                    while retry_cnt > 0 and not scanIsFleetExpanded() and team1btn do
                        sleep(200)
                        retry_cnt = retry_cnt - 1
                    end
                    clickTeam(1, SETTINGS.mob_team)
                    retry_cnt = 50
                    while retry_cnt > 0 and scanIsFleetExpanded() and team1btn do
                        sleep(200)
                        retry_cnt = retry_cnt - 1
                    end
                    clickChoose_2() -- choose 2nd
                    showConsole("选择2队")
                    retry_cnt = 50
                    while retry_cnt > 0 and not scanIsFleetExpanded() and team2btn do
                        sleep(200)
                        retry_cnt = retry_cnt - 1
                    end
                    clickTeam(2, SETTINGS.boss_team)
                    retry_cnt = 50
                    while retry_cnt > 0 and scanIsFleetExpanded() and team2btn do
                        sleep(200)
                        retry_cnt = retry_cnt - 1
                    end
                    --sleep(500)
                end
            end
            sleep(200)
            if not IsWorkingOnTBYX() then
                set_fleet_accountability()
            end
            clickFleetStart() -- 按钮:立刻前往
            RecordBeginTime()
            before_enter_level()
        elseif path == "map" then
            -- 海域地图
            STATE.map_entered = true
            STATE.combat_begin_time = 0

            if STATE.enter_level_clicked == true then
                STATE.enter_level_clicked = false
                IncFarmedRound()
                ClearCombatCount()
            end

            if not STATE.autoseek_enabled then
                -- 关闭自动寻敌
                if scanIsAutoSeekEnabled() then
                    clickMapAutoSeek()
                    sleep(500)
                end
                -- 关闭阵容锁定
                if scanIsFormationLockEnabled() then
                    click(1094, 451)
                    sleep(500)
                end

                hideConsole()
                if scanIsSidebarOpen() then
                    print("Close Sidebar")
                    click(949, 371, 961, 412)
                    sleep(500)
                end
                MAP:nextStep()
            else
                STATE.autoseek_round_end = false
                showConsole("自律寻敌中...")
                -- 开启自律寻敌
                if not scanIsAutoSeekEnabled() then
                    clickMapAutoSeek()
                end

                sleep(800)
            end
        elseif path == "shop" then
            pressBack()
            sleep(500)
        elseif path == "commander" then
            pressBack()
            sleep(500)
        elseif path == "houzhai" then
            pressBack()
            sleep(500)
        elseif path == "houzhai:share" then
            click(681, 226, 703, 252)
            sleep(500)
        elseif path == "commander:share" then
            click(737, 167, 773, 193)
            sleep(500)
        elseif path == "houzhai:exp" then
            --elseif path == "oof" then
            --    click(391, 442, 529, 486)
            --    sleep(500)
            click(913, 536, 1046, 572)
            sleep(500)
        elseif path == "xueyuan" then
            pressBack()
            sleep(500)
        elseif path == "zhanshuxueyuan" then
            pressBack()
            sleep(500)
        elseif path == "supplies" then
            pressBack()
            sleep(2000)
        elseif path == "kaifachuanwu" then
            pressBack()
            sleep(1000)
            pressBack()
            sleep(500)
        elseif path == "book:selection" then
            pressBack()
            sleep(500)
        elseif path == "hatch" then
            pressBack()
            sleep(500)
        elseif path == "hatch:select" then
            click(891, 113, 916, 138)
            sleep(500)
        elseif path == "inputbox" then
            click(1019, 293, 1103, 350)
            sleep(3000)
        elseif path == "keyan_research" then
            pressBack()
            sleep(2000)
        elseif path == "update:reconnect" then
            click(618, 390, 732, 425)
            sleep(3000)
        elseif path == "SKIP" then
            click(1024, 22, 1123, 46) -- 右上角 skip
            sleep(1000)
        elseif path == "Pause" then
            click(857, 154) -- 右上角 x
            sleep(1000)
        elseif path == "ReadProtoc" then
            swipe_and_accept()
        elseif path == "" then
        else
            dialog("PATH=" .. path)
        end
        sleep(SETTINGS.waittime)
    end
end
