Pos = {}
function Pos:new (...)
  o = {
    type=TYPE_EMPTY,
    i = nil,
    j = nil,
    x = nil,
    y = nil,
    section = nil,
    region = nil,
    width = nil,
  }
  setmetatable(o, self)
  self.__index = self
  if #arg == 2 then
    o.x = arg[1]
    o.y = arg[2]
  elseif #arg == 1 then
    if arg[1].x ~= nil and arg[1].y~=nil then
      o.x = arg[1].x
      o.y = arg[1].y
    elseif arg[1][1] ~= nil and arg[1][2]~=nil then
      o.x = arg[1][1]
      o.y = arg[1][2]
    end
  end
  return o
end

function Pos:xy () return self.x, self.y end
function Pos:print() LL('pos =', self.x, self.y) end
function Pos:draw(color) drawCross(self.x, self.y, color) end
function Pos:drawPin() drawPin(self.x, self.y) end
function Pos:setWidth(w) self.width = w end
function Pos:similar(pos, offset, y_delta)
  local offset = offset or 20
  local y_delta = y_delta or 0
  return math.abs(self.x - pos.x) <= offset and math.abs(self.y - (pos.y + y_delta)) <= offset
end

function Pos:dist(pos)
  return math.abs(self.i - pos.i) + math.abs(self.j - pos.j)
end

function Pos:click(safe)
  if safe ~= true then safe = false end
  if safe then
    if self.x >= 580 and self.y >=557 then  -- 右下方问号开始
      click(self.x, 550)
      return
    elseif self.x >= 587 and self.y <= 50 then  -- 石油金币钻石
      click(self.x, 60)
      return
    elseif self.x >= 1050 and self.y > 355 and self.y < 427 then  -- 战略
      click(1049, self.y)
      return
    elseif self.x <= 393 and self.y <= 143 then
      click(self.x, 150)
      return
    end
  end
  click(self.x, self.y)
end
