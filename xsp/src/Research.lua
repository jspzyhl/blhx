---@param str_ string
---@return table
local function parse_str_into_table(str_)
    local script = "local ee={" .. str_ .. "}  return ee"
    return assert(loadstring(script))()
end

---@param str_ string
---@return table<string,string>
local function parse_str_to_sections(str_)
    local pos_ = 1
    local beg_, end_ = 0
    local temp_str = ''
    local temp_head = ''
    local temp_body = ''
    local result_tab = {}

    while beg_ ~= nil do
        beg_, end_ = string.find(str_, "^%[.-%].-%[", pos_)
        if beg_ ~= nil then
            temp_str = string.sub(str_, beg_, end_ - 1)
            temp_head = string.match(temp_str, "^%[(.-)%]")
            temp_body = string.gsub(temp_str, "^%[.-%]%s", "")
            result_tab[temp_head] = temp_body
            pos_ = end_
        end
    end

    if pos_ < #str_ then
        temp_str = string.match(str_, "^%[.-%].*", pos_)
        temp_head = string.match(temp_str, "^%[(.-)%]")
        temp_body = string.gsub(temp_str, "^%[.-%]%s", "")
        result_tab[temp_head] = temp_body
    end
    return result_tab
end

local function split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}
    i = 1
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

local function sort_by(t, str_)
    local a = split(str_, ',')
    table.sort(t, function(u, v)
        for i = 1, #a do
            if u[a[i]] or v[a[i]] then
                if (u[a[i]] == nil and v[a[i]] ~= nil) then
                    return false
                end
                if (u[a[i]] ~= nil and v[a[i]] == nil) then
                    return true
                end
                if u[a[i]] > v[a[i]] then
                    return false
                end
                if u[a[i]] < v[a[i]] then
                    return true
                end
            end
        end
    end)
end

local function t_dump(o)
    if type(o) == "table" then
        local s = "{"
        for k, v in pairs(o) do
            if type(k) ~= "number" then
                k = '"' .. k .. '"'
            end
            s = s .. "[" .. k .. "] = " .. t_dump(v) .. ","
        end
        return s .. "}\n"
    else
        return tostring(o)
    end
end

local function print_t(t_)
    print(t_dump(t_))
end

--------------------------------------------------------------------------
---@class ResData
---@field index number
---@field type string
---@field ship_name string
---@field scale string
---@field series number
---@field rarity number
---@field time_cost number
---@field ship_rarity string
ResData = {
    index = 0,
    type = "",
    ship_name = "",
    scale = "",
    series = 0,
    rarity = "",
    time_cost = 0,
    ship_rarity = ""
}

---@param index_ number
---@param type_ string
---@param scale_ string
---@param series_ number
---@param rarity_ string
---@param time_cost_ number
---@param ship_name_ string
---@param ship_rarity_ string
function ResData:make(index_, type_, scale_, series_, rarity_, time_cost_, ship_name_, ship_rarity_)
    ---@type ResData
    local result = {}
    setmetatable(result, { __index = self })
    result.index = index_
    result.type = type_
    result.scale = scale_
    result.series = series_
    result.rarity = rarity_
    result.time_cost = time_cost_
    result.ship_name = ship_name_
    result.ship_rarity = ship_rarity_
    return result
end

---@param Condition_ table
---@return boolean
function ResData:IsMatchCondition(Condition_)
    local result = true
    for c_k, c_v in pairs(Condition_) do
        if (self[c_k] ~= nil and self[c_k] ~= c_v) or self[c_k] == nil then
            result = false
            break
        end
    end
    return result
end

---@param data1 ResData
---@param data2 ResData
---@return boolean
function res_data_equals(data1, data2)
    return (data1.type == data2.type) and
            (data1.series == data2.series) and
            (data1.rarity == data2.rarity) and
            (data1.scale == data2.scale) and
            (data1.ship_name == data2.ship_name)
end

--------------------------------------------------------------------------


---@param str_ string
---@return table
local function make_selection_rule_table(str_)
    local rule_table_ = {}
    local cond_statements_ = split(str_, "\r\n")
    for _, s_ in pairs(cond_statements_) do
        if s_ ~= nil and #s_ > 0 then
            table.insert(rule_table_, parse_str_into_table(s_))
        end
    end
    return rule_table_
end

--------------------------------------------------------------------------

---@class ResTab
---@field item_data_tab table<number,ResData>
ResTab = {
    item_data_tab = {}
}

---@return ResTab
function ResTab:new()
    ---@type ResTab
    local result = {}
    setmetatable(result, { __index = self })
    result.item_data_tab = {}
    return result
end

---@param item_ ResData
function ResTab:add_item(item_)
    table.insert(self.item_data_tab, item_)
end

---@param ItemDataTab_ table
---@param RuleList_ table
function search_for_matched(ItemDataTab_, RuleList_)
    local out_items = {}
    if RuleList_ ~= nil then
        local itme_found = false
        for _, cond_ in pairs(RuleList_) do
            ---@param research_item_ ResData
            for _, research_item_ in pairs(ItemDataTab_) do
                if research_item_:IsMatchCondition(cond_) then
                    table.insert(out_items, research_item_)
                    itme_found = true
                end
            end
            if itme_found then
                break
            end
        end
    end
    return out_items
end

---@param item_data_tab_ table
---@param field_ string
function GroupBy(item_data_tab_, field_)
    local field_val_enum = {}
    for _, i_ in pairs(item_data_tab_) do
        if i_[field_] then
            field_val_enum[i_[field_]] = true
        end
    end
    local groups_ = {}
    for e_, _ in pairs(field_val_enum) do
        local rule_tab_ = {}
        if type(e_) == 'string' then
            rule_tab_ = make_selection_rule_table(field_ .. '=' .. "'" .. e_ .. "'")
        elseif type(e_) == 'number' then
            rule_tab_ = make_selection_rule_table(field_ .. '=' .. e_)
        end
        groups_[e_] = search_for_matched(item_data_tab_, rule_tab_)
    end
    return groups_
end

function GroupBySeries(ItemDataTab_)
    local series_enum = {}
    for _, i_ in pairs(ItemDataTab_) do
        if i_.series then
            series_enum[i_.series] = true
        end
    end
    local groups_ = {}
    for e_, _ in pairs(series_enum) do
        groups_[e_] = search_for_matched(ItemDataTab_, { { series = e_ } })
    end
    return groups_
end

---@param item_data_tab_ table
---@param rule_list_ table
---@param group_by_ string
function group_search_for_matched(item_data_tab_, rule_list_, group_by_)
    local gp_items_ = GroupBy(item_data_tab_, group_by_)
    local match_merged_ = {}
    for _, g_ in pairs(gp_items_) do
        local t_matched = search_for_matched(g_, rule_list_)
        for _, i_ in pairs(t_matched) do
            table.insert(match_merged_, i_)
        end
    end
    return match_merged_
end


--------------------------------------------------------------------------

---@class ResSorter
---@field k_map table
ResSorter = {
    k_map = {}
}

---@return ResSorter
function ResSorter:new()
    ---@type ResSorter
    local result = {}
    setmetatable(result, { __index = self })
    result.k_map = {}
    return result
end

---@param field_ string
---@param keyword_ any
---@param priority_ number
function ResSorter:regist_keyword(field_, keyword_, priority_)
    self.k_map = self.k_map or {}
    self.k_map[field_] = self.k_map[field_] or {}
    self.k_map[field_][keyword_] = priority_
end

---@param priority_def_ table
function ResSorter:regist_keyword_tab(priority_def_)
    self.k_map = self.k_map or {}
    self.k_map[priority_def_["field"]] = self.k_map[priority_def_["field"]] or {}
    self.k_map[priority_def_["field"]][priority_def_["keyword"]] = priority_def_["priority"]
end

---@param str_ string
function ResSorter:load_from_string(str_)
    local sec_tab = parse_str_to_sections(str_)
    if sec_tab['priority'] then
        local pri_statements_ = split(sec_tab['priority'], "\r\n")
        for _, statement_ in pairs(pri_statements_) do
            self:regist_keyword_tab(parse_str_into_table(statement_))
        end
    end
end

---@param file_name_ string
function ResSorter:load_from_file(file_name_)
    local file1 = io.open(file_name_, "r")
    local r1_txt = file1:read("*a")
    io.close(file1)
    self:load_from_string(r1_txt)
end

---@param research_item_table_ table
---@param keys_str_ string
function ResSorter:sort_table_by(research_item_table_, keys_str_)
    -- 关键词转数值，生成临时表，用于排序
    local temp_tab_ = {}
    for _, item_ in pairs(research_item_table_) do
        local new_item_data = {}
        for i_key_, i_val_ in pairs(item_) do
            local new_val_ = i_val_
            if i_val_ ~= nil and
                    self.k_map[i_key_] ~= nil and self.k_map[i_key_][i_val_] ~= nil then
                new_val_ = self.k_map[i_key_][i_val_]
            end
            new_item_data[i_key_] = new_val_
        end
        table.insert(temp_tab_, new_item_data)
    end
    --print(t_dump(temp_tab_))
    sort_by(temp_tab_, keys_str_)
    local sorted_index_ = {}
    for _, temp_item_ in pairs(temp_tab_) do
        table.insert(sorted_index_, temp_item_.index)
    end
    return sorted_index_
    --print(t_dump(sorted_index_))
end

--------------------------------------------------------------------------

---@class RuleData
---@field group_by string
---@field selection_rules table
---@field order_by string
RuleData = {
    group_by = nil,
    selection_rules = nil,
    order_by = nil
}

function RuleData:new()
    ---@type RuleData
    local result = {}
    setmetatable(result, { __index = self })
    return result
end

---@param str_ string
function RuleData:load_from_string(str_)
    local sec_tab = parse_str_to_sections(str_)
    if sec_tab['group'] then
        self.group_by = parse_str_into_table(sec_tab['group'])['group_by']
    end
    if sec_tab['select'] then
        self.selection_rules = make_selection_rule_table(sec_tab['select'])
    end
    if sec_tab['sort'] then
        self.order_by = parse_str_into_table(sec_tab['sort'])['order_by']
    end
end

---@param file_name_ string
function RuleData:load_from_file(file_name_)
    local file1 = io.open(file_name_, "r")
    local r1_txt = file1:read("*a")
    io.close(file1)
    self:load_from_string(r1_txt)
end

--------------------------------------------------------------------------

---@param res_tab_ ResTab
---@param sorter_ ResSorter
---@param group_by_ string
---@param select_rules_ table
---@param order_by_ string
---@return number
function pick_best_item(res_tab_, sorter_, group_by_, select_rules_, order_by_)
    local item_selected = {}
    if group_by_ and #group_by_ > 0 then
        item_selected = group_search_for_matched(res_tab_.item_data_tab, select_rules_, group_by_)
    else
        item_selected = search_for_matched(res_tab_.item_data_tab, select_rules_)
    end
    local sorted_ = sorter_:sort_table_by(item_selected, order_by_)
    if #sorted_ > 0 then
        return sorted_[1]
    else
        return nil
    end
end

--------------------------------------------------------------------------
