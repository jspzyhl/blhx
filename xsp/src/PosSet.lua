PosSet = {}
function PosSet:new (...)
	o = {}
	setmetatable(o, self)
	self.__index = self
	return o
end

function PosSet:add (new, offset)
	for _, p in ipairs(self) do
		if new:similar(p, offset) then
			if new.x < p.x then p = new end
			return
		end
	end
	self[#self+1] = new
end
function PosSet:print() for _,pos in ipairs(self) do pos:print() end end
function PosSet:draw(color) for _,pos in ipairs(self) do pos:draw(color) end end
function PosSet:shift(x,y) for _,pos in ipairs(self) do pos.x, pos.y = pos.x+x, pos.y+y end end
function PosSet:len() return #self end
function PosSet:merge(newSet, offset)
	if newSet == nil then return end
	for _, pos in ipairs(newSet) do
		self:add(pos, offset)
	end
end