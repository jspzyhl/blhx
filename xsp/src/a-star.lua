module("astar", package.seeall)

----------------------------------------------------------------
-- local variables
----------------------------------------------------------------

local INF = 1 / 0

----------------------------------------------------------------
-- local functions
----------------------------------------------------------------

local function IsMoveBlockByWall(_airwalls, _pos_a, _pos_b)
    local row = ''
    local col = ''

    if _airwalls == nil then return false end

    if _pos_a.i == _pos_b.i and _pos_a.j == _pos_b.j then return false end
    if _pos_a.i == _pos_b.i then
        row = 'r' .. _pos_a.i
        local lower_c = math.min(_pos_a.j, _pos_b.j)
        col = 'c' .. lower_c .. '_5'
    elseif _pos_a.j == _pos_b.j then
        col = 'c' .. _pos_a.j
        local lower_r = math.min(_pos_a.i, _pos_b.i)
        row = 'r' .. lower_r .. '_5'
    end
    return (_airwalls[row] ~= nil) and (_airwalls[row][col] ~= nil)
end

function dist_between_neighbor(curr, neighbor)
    local dist = 1
    if MAP.enemyM[neighbor.i][neighbor.j] > 0 then
        dist = SETTINGS.enemyBaseDist + MAP.enemyM[neighbor.i][neighbor.j]
    end

    --  -- preference down->up->right->left
    --  if curr.i == neighbor.i then --same row
    --    if curr.j < neighbor.j then -- right
    --      dist = dist
    --    else -- left
    --      dist = dist
    --    end
    --  else -- same column
    --    if curr.i < neighbor.i then -- down
    --      dist = dist-- - 0.01
    --    else -- up
    --      dist = dist-- - 0.01
    --    end
    --  end

    if IsMoveBlockByWall(MAP.config.airwalls, curr, neighbor) == true then
      dist = 4200000000
    end

    return dist
end

function heuristic_cost_estimate(nodeA, nodeB)
    return math.abs(nodeA.i - nodeB.i) * 1.01 + math.abs(nodeA.j - nodeB.j)
end

function lowest_f_score(set, f_score)

    local lowest, bestNode = INF, nil
    for _, node in ipairs(set) do
        local score = f_score[node]
        if score < lowest then lowest, bestNode = score, node end
    end
    return bestNode
end

function isValid(i, j)
    if i < 1 then return false end
    if j < 1 then return false end
    if i > MAP.numRow then return false end
    if j > MAP.numCol then return false end
    if MAP.config.blocks[i][j] == 1 then return false end
    if MAP.DynamicBlock[i][j] == true then return false end --  需要绕道的目标
    return true
end

function get_neighbors(current, nodes)
    local neighbors = {}
    local i, j
    i, j = current.i + 1, current.j
    if isValid(i, j) then table.insert(neighbors, MAP.grids[i][j]) end
    i, j = current.i - 1, current.j
    if isValid(i, j) then table.insert(neighbors, MAP.grids[i][j]) end
    i, j = current.i, current.j + 1
    if isValid(i, j) then table.insert(neighbors, MAP.grids[i][j]) end
    i, j = current.i, current.j - 1
    if isValid(i, j) then table.insert(neighbors, MAP.grids[i][j]) end
    return neighbors
end

function not_in(set, theNode)

    for _, node in ipairs(set) do if node == theNode then return false end end
    return true
end

function remove_node(set, theNode)

    for i, node in ipairs(set) do
        if node == theNode then
            set[i] = set[#set]
            set[#set] = nil
            break
        end
    end
end

function unwind_path(flat_path, map, current_node)

    if map[current_node] then
        table.insert(flat_path, 1, map[current_node])
        return unwind_path(flat_path, map, map[current_node])
    else
        return flat_path
    end
end

----------------------------------------------------------------
-- pathfinding functions
----------------------------------------------------------------

function a_star(start, goal, nodes)

    local closedset = {}
    local openset = {start}
    local came_from = {}

    local g_score, f_score = {}, {}
    g_score[start] = 0
    f_score[start] = g_score[start] + heuristic_cost_estimate(start, goal)
    while #openset > 0 do

        local current = lowest_f_score(openset, f_score)
        -- print('curr = ', current.i, current.j)
        if current == goal then
            local path = unwind_path({}, came_from, goal)
            table.insert(path, goal)
            return path
        end
        remove_node(openset, current)
        table.insert(closedset, current)
        local neighbors = get_neighbors(current, nodes)
        for _, neighbor in ipairs(neighbors) do
            -- print('neighbor =', neighbor.i, neighbor.j)
            if not_in(closedset, neighbor) then
                local tentative_g_score = g_score[current] + dist_between_neighbor(current, neighbor)
                if not_in(openset, neighbor) or tentative_g_score < g_score[neighbor] then
                    came_from[neighbor] = current
                    g_score[neighbor] = tentative_g_score
                    f_score[neighbor] = g_score[neighbor] + heuristic_cost_estimate(neighbor, goal)
                    if not_in(openset, neighbor) then
                        openset[#openset + 1] = neighbor
                    end
                end
            end
        end
    end
    return {} -- no valid path
end

----------------------------------------------------------------
-- exposed functions
----------------------------------------------------------------

function path(start, goal) return a_star(start, goal, MAP.grids_list) end
