function trim(s)
    return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

function t_dump(o)
    if type(o) == "table" then
        local s = "{"
        for k, v in pairs(o) do
            if type(k) ~= "number" then
                k = '"' .. k .. '"'
            end
            s = s .. "[" .. k .. "] = " .. t_dump(v) .. ","
        end
        return s .. "}\n"
    else
        return tostring(o)
    end
end

function print_t(t_)
    print(t_dump(t_))
end

function LF(fmt, ...)
    if not SETTINGS.debug_print then
        return
    end
    sysLog(string.format(fmt, ...))
end

function LL(...)
    if not SETTINGS.debug_print then
        return
    end
    local msg = ""
    for k, v in pairs({ ... }) do
        msg = msg .. tostring(v) .. " "
    end
    sysLog(msg)
end

print = LL
printf = LF

function XX(x)
    if x == -1 then
        return -1
    end
    -- print(x,'-x>',x * DEVICE.scale + DEVICE.offsetX)
    return x * DEVICE.scale + DEVICE.offsetX
end

function YY(y)
    if y == -1 then
        return -1
    end
    -- print(y,'-y>',y * DEVICE.scale + DEVICE.offsetY)
    return y * DEVICE.scale + DEVICE.offsetY
end

function XXR(x)
    if x == -1 then
        return -1
    end
    -- print(x,'-xr>',x * DEVICE.scale + DEVICE.offsetX)
    return (x - DEVICE.offsetX) / DEVICE.scale
end

function YYR(y)
    if y == -1 then
        return -1
    end
    -- print(y,'-yr>',y * DEVICE.scale + DEVICE.offsetY)
    return (y - DEVICE.offsetY) / DEVICE.scale
end

function myFindColor(region, tbl, degree, hdir, vdir, priority)
    if DEVICE.scale == 1 then
        return findColor(region, tbl, degree, hdir, vdir, priority)
    else
        local _tbl = {}
        for _, point in ipairs(tbl) do
            local p = {}
            p.x = point.x * DEVICE.scale
            p.y = point.y * DEVICE.scale
            p.color = point.color
            table.insert(_tbl, p)
        end

        local x, y = findColor({ XX(region[1]), YY(region[2]), XX(region[3]), YY(region[4]) }, _tbl, degree, hdir, vdir, priority)
        return XXR(x), YYR(y)
    end
end

function myFindColors(region, tbl, degree, hdir, vdir, priority)
    if DEVICE.scale == 1 then
        return findColors(region, tbl, degree, hdir, vdir, priority)
    else
        local _tbl = {}
        for _, point in ipairs(tbl) do
            local p = {}
            p.x = point.x * DEVICE.scale
            p.y = point.y * DEVICE.scale
            p.color = point.color
            table.insert(_tbl, p)
        end

        local points = findColors({ XX(region[1]), YY(region[2]), XX(region[3]), YY(region[4]) }, _tbl, degree, hdir, vdir, priority)
        for _, point in ipairs(points) do
            point.x = XXR(point.x)
            point.y = YYR(point.y)
        end
        return points
    end
end

function myGetColor(x, y)
    return getColor(XX(x), YY(y))
end

function myGetColorRGB(x, y)
    -- local r1,r2,r3 = getColorRGB(XX(x), YY(y))
    -- STATE.debug = STATE.debug .. XX(x)..'/'..YY(y)..':'..r1..'/'..r2..'/'..r3..'\n'
    return getColorRGB(XX(x), YY(y))
end

function myShowHUD(id, text, size, color, bg, pos, x, y, width, height)
    showHUD(id, text, size, color, bg, pos, XX(x), YY(y), width, height)
end

function hideAllHUD()
    local hasHUD = false
    for category, list in pairs(HUD) do
        for _, i in ipairs(list) do
            hideHUD(i)
            hasHUD = true
        end
        HUD[category] = {}
    end
    if hasHUD then
        sleep(100)
    end
end

function hidePathHUD()
    local hasHUD = false
    for _, i in ipairs(HUD.path) do
        hideHUD(i)
        hasHUD = true
    end
    HUD.path = {}
    if hasHUD then
        sleep(100)
    end
end

function drawPos(pos, color, offset)
    local offset = offset or 0
    return drawCross(pos[1] + offset, pos[2] + offset, color)
end

function drawPin(x, y, color)
    local id = createHUD()
    table.insert(HUD.path, id)
    myShowHUD(id, "", 12, "0x00000000", "pin.png", 0, x - 32, y - 52, 64, 64)
end

function drawCross(x, y, color)
    if color == nil then
        color = string.format("0x%x", 0xff000000 + myGetColor(x, y))
    end
    local len = math.floor(DEVICE.height / 30.0)
    local offset = math.floor(len / 2)
    local id1 = createHUD()
    local id2 = createHUD()
    table.insert(HUD.path, id1)
    table.insert(HUD.path, id2)
    myShowHUD(id1, "", 12, "0xffff0000", color, 0, x - offset, y - 1, len, 3)
    myShowHUD(id2, "", 12, "0xffff0000", color, 0, x - 1, y - offset, 3, len)
end

function drawDebugText(x, y, str)
    local id = createHUD()
    table.insert(HUD.weituo, id)
    myShowHUD(id, str, 15, "0xffff00ff", "0xffffffff", 0, x, y, 32, 32)
end

function drawDebugPriority(x, y, str)
    if SETTINGS.debug_weituo then
        drawDebugText(x, y, str)
    end
end

function End_drawDebugPriority()
    if SETTINGS.debug_weituo then
        sleep(4000)
        hideAllHUD()
        HUD.weituo = {}
    end
end

-- human sleep
sleep_EXTRA_MAX = 2000
function wait(t)
    local extra = math.floor(sleep_EXTRA_MAX / math.random(1, 10))
    sleep(t + extra)
end

function sleep(t)
    mSleep(t * SETTINGS.sleep_multiplier)
end

function click(x1, y1, x2, y2, slp)
    -- index = math.random(1,2)
    index = 1
    local mx = x1
    local my = y1

    if x2 ~= nil and y2 ~= nil then
        mx = math.random(x1, x2)
        my = math.random(y1, y2)
    end

    -- 显示指针
    if SETTINGS.cursor_duration ~= 0 then
        local id = createHUD()
        myShowHUD(id, "", 12, "0x00000000", "cursor.png", 0, mx, my, 45, 45)
        sleep(SETTINGS.cursor_duration)
        hideHUD(id)
        sleep(100)
    end

    local t = 300

    if slp ~= nil then
        t = slp
    else
        t = math.random(SETTINGS.cursor_click_duration - 10, SETTINGS.cursor_click_duration + 10)
    end

    touchDown(index, XX(mx), YY(my))
    sleep(t)
    touchUp(index, XX(mx), YY(my))

    if SETTINGS.cursor_wait_after ~= 0 then
        sleep(SETTINGS.cursor_wait_after)
    end
end

function pressResign()
    click(672, 590, 803, 618)
    sleep(1000)
    click(635, 435, 767, 472)
    sleep(1000)
end

function pressBack(num)
    local num = num or 1
    for i = 1, num do
        click(31, 28, 70, 66)
        sleep(300)
    end
end

function clickX()
    click(783, 161, 806, 183)
    sleep(300)
end

function pressHome()
    click(1090, 19, 1111, 40)
    sleep(300)
end

function pressAutoCombatOK()
    click(500, 465, 636, 500)
    sleep(500)
end

function clickTechMiddle()
    click(485, 105, 640, 330)
    sleep(500)
end

function clickTechRightArrow()
    click(1097, 308, 1111, 327)
    sleep(500)
end

function clickTechLeftArrow()
    click(20, 308, 39, 331)
    sleep(500)
end

function clickInform1Confirm()
    click(500, 440, 635, 471)
end

function clickInform2Confirm()
    click(630, 440, 765, 470)
end

function clickInform2Cancel()
    click(402, 452, 466, 472)
end

function clickFormationWeightAnchor()
    click(934, 545, 1090, 585)
    --STATE.combat_begin_time = 0
    --STATE.submarine_enabled = true
end

function clickFoodDlgCancel()
    click(363, 445, 507, 447)
end

-- 检查是否处于补充食物界面，如果否则会按取消，返回是否位于该界面
function checkThenClickFoodDlgCancel()
    local _ = scanIsHouzhaiFoodSelect()
    if not _ then
        clickFoodDlgCancel()
    end
    return _
end

--  展开委托边侧栏
function clickWeituoExpand()
    click(11, 132, 31, 157)
end

--  委托 完成、前往 按钮
function clickWeituoBtn()
    click(342, 236, 443, 264)
end

--  点击作战档案按钮
function clickOpFileBtn()
    click(177, 583, 329, 612)
end

--  点击作战档案页面S.P.按钮
function clickOpSPBtn()
    click(895, 36, 975, 61)
end

--  点击作战档案页面E.X.按钮
function clickOpEXBtn()
    click(800, 35, 883, 64)
end

function clickChoose_1()
    click(918, 161, 958, 179)
end

function clickChoose_2()
    click(919, 279, 958, 297)
end

function clickChoose_sub()
    click(919, 410, 958, 428)
end

function clickClear_1()
    click(992, 161, 1031, 179)
end

function clickClear_2()
    click(992, 279, 1031, 297)
end

function clickClear_sub()
    click(992, 410, 1031, 428)
end

-- 舰队选择：tab编队
function clickFleetBiandui()
    click(1061, 141, 1085, 198)
end

-- 舰队选择：职能
function clickFleetAccountability()
    click(1056, 329, 1085, 398)
end

-- 1队道中
function clickTeam1_mob()
    click(425, 172, 438, 184)
end

-- 1队boss
function clickTeam1_boss()
    click(549, 173, 560, 183)
end

-- 1队全部
function clickTeam1_all()
    click(724, 172, 735, 183)
end

-- 1队待机
function clickTeam1_idle()
    click(813, 172, 824, 183)
end

function prevChapter(num)
    for i = 1, num do
        click(37, 322, 50, 338)
        sleep(100)
    end
end

function nextChapter(num)
    for i = 1, num do
        click(1075, 321, 1090, 344)
        sleep(100)
    end
end

function prevTeam(num)
    for i = 1, num do
        click(24, 293, 42, 308)
        sleep(750)
    end
end

function nextTeam(num)
    for i = 1, num do
        click(806, 290, 821, 308)
        sleep(750)
    end
end

function daysSinceEpoch()
    return math.floor((mTime() / 1000 + 28800) / 86400)
end

function clickMishu(num)
    local num = num or 1
    for i = 1, num do
        click(129, 104, 505, 271)
        sleep(3000)
        click(176, 265, 449, 425)
        sleep(3000)
        click(167, 430, 456, 554)
        sleep(3000)
    end
end

function clickMingshi(num)
    local num = num or 1
    for i = 1, num do
        click(63, 139, 176, 493)
        sleep(3000)
    end
end

function clickChuji()
    click(910, 269, 1008, 371) -- fight 主页出击
end

function clickMainBattleLine()
    -- 主线
    click(156, 111, 448, 487)
end

function clickLevelStart()
    -- 关卡 立刻前往
    click(758, 434, 931, 484)
end

function clickFleetStart()
    -- 舰队选择页面 立刻前往
    click(900, 518, 1013, 544)
    STATE.enter_level_clicked = true
end

function clickCombatAutomation()
    click(6, 26, 137, 54)
end

function ws_clickYanxi()
    click(860, 519, 980, 565)
end

function ws_clickWeituo()
    click(720, 520, 839, 565)
end

function ws_clickMeiri()
    click(579, 520, 698, 565)
end

function ws_clickOpFile()
    -- 作战档案
    click(155, 521, 273, 563)
end

function ws_clickRescue()
    -- 求救信号
    click(908, 392, 968, 454)
end

function clickHardNormalswitch()
    -- 困难、普通模式切换按钮
    click(23, 570, 125, 609)
end

-- 地图：自律寻敌
function clickMapAutoSeek()
    click(1094, 494)
end

-- 点击战斗胜利页面
function clickExpPage()
    click(895, 568, 1036, 606)
end

-- 合计奖励：离开
function clickLeave(path_)
    if path_ == "total_rewards" then
        click(343, 540, 446, 566)
    elseif path_ == "total_rewards2" then
        click(285, 542, 384, 563)
    end
end

-- 合计奖励：再次前往
function clickAgain(path_)
    if path_ == "total_rewards" then
        click(694, 539, 803, 565)
    elseif path_ == "total_rewards2" then
        click(763, 541, 863, 565)
    end
    STATE.enter_level_clicked = true
end

-- 舰队战红叉
function FleetZuozhanClose()
    click(1092, 85, 1128, 121)
end

-- 点击大型作战
function clickOPERATION()
    click(485, 308, 855, 488)
end

-- 点击信标烬
function clickBeaconAsh()
    click(828, 560, 956, 614)
end

-- 点击信标列表
function clickBeaconList()
    click(22, 577, 190, 623)
end

-- 点击信标作战开始
function clickBeaconBattleStart()
    click(944, 566, 1114, 613)
end

-- 点击战斗完成
function clickBattleCompleteOK()
    click(890, 562, 1037, 609)
end

function ForceHome()
    local success = false
    local max_retries = 15
    local tried = 0
    local path = scanMain()
    --
    while path ~= "main" and tried < max_retries do
        if scanHomeBtn() then
            pressHome()
        elseif scanHomeBtnRed() then
            pressHome()
        elseif path == "inform1:confirm1" then
            clickInform1Confirm()
        elseif path == "inform:2confirm1" then
            clickInform2Cancel()
        elseif path == "inform:3confirm3" then
            clickX()
        else
            pressKey("BACK", false)
        end
        sleep(1500)
        path = scanMain()
        tried = tried + 1
    end

    if tried < max_retries then
        success = true
    end
    return success
end

function showConsole(str, bgColor)
    if str == nil then
        return
    end
    local bgColor = bgColor or "0xFF00FF00"
    if CONSOLE.id == nil then
        CONSOLE.id = createHUD()
    end
    --0xFF71f471
    myShowHUD(
            CONSOLE.id,
            str,
            CONSOLE.font,
            "0xFF00FF00",
            "console.png",
            0,
            CONSOLE.x,
            CONSOLE.y,
            CONSOLE.width,
            CONSOLE.height
    )
end

function showEyeProtector(x1, y1, x2, y2)
    local id = createHUD()
    table.insert(HUD.tmp, id)
    myShowHUD(id, "闪光防护罩", 30, "0xffffffff", "0x88000000", 0, x1, y1, x2 - x1 + 1, y2 - y1 + 1)
end

function hideConsole()
    if CONSOLE.id ~= nil then
        hideHUD(CONSOLE.id)
        CONSOLE.id = nil
        sleep(250)
    end
end

function colorMatch(lr, lg, lb, c, s)
    local s = s or 90
    s = math.floor(0xff * (100 - s) * 0.01)
    local rgb = c
    local r = math.floor(rgb / 0x10000)
    local g = math.floor(rgb % 0x10000 / 0x100)
    local b = math.floor(rgb % 0x100)
    if math.abs(lr - r) > s or math.abs(lg - g) > s or math.abs(lb - b) > s then
        return false
    end
    return true
end

function color_sum_r(x1, y1, x2, y2)
    sum_r = 0
    for y_ = y1, y2 do
        for x_ = x1, x2 do
            r_, g_, b_ = myGetColorRGB(x_, y_)
            sum_r = sum_r + r_
        end
    end
    return sum_r
end

function color_sum_g(x1, y1, x2, y2)
    sum_b = 0
    for y_ = y1, y2 do
        for x_ = x1, x2 do
            r_, g_, b_ = myGetColorRGB(x_, y_)
            sum_b = sum_b + g_
        end
    end
    return sum_b
end

function color_sum_b(x1, y1, x2, y2)
    sum_b = 0
    for y_ = y1, y2 do
        for x_ = x1, x2 do
            r_, g_, b_ = myGetColorRGB(x_, y_)
            sum_b = sum_b + b_
        end
    end
    return sum_b
end

-- 多点颜色对比，格式为{{x,y,color},{x,y,color}...}
-- 或者 {{x,y,{color1,color2}},{x,y,color3,color4}...}  color1 or color2 ...
-- s: similarity
function cmpColor(array, s, debug)
    local s = s or 90
    local debug = debug or false
    local str = ""
    s = math.floor(0xff * (100 - s) * 0.01)

    -- for each position
    for i = 1, #array do
        local lr, lg, lb = myGetColorRGB(array[i][1], array[i][2])
        str = str .. XX(array[i][1]) .. "," .. YY(array[i][2]) .. ":" .. lr .. " " .. lg .. " " .. lb .. "\n"
        if not debug then
            -- if it is a list
            if type(array[i][3]) == "table" then
                -- if it is a single color number
                local matched = false
                for i, rgb in ipairs(array[i][3]) do
                    local r = math.floor(rgb / 0x10000)
                    local g = math.floor(rgb % 0x10000 / 0x100)
                    local b = math.floor(rgb % 0x100)
                    if math.abs(lr - r) < s and math.abs(lg - g) < s and math.abs(lb - b) < s then
                        matched = true
                        break
                    end
                end
                if matched == false then
                    return false
                end
            else
                local rgb = array[i][3]
                local r = math.floor(rgb / 0x10000)
                local g = math.floor(rgb % 0x10000 / 0x100)
                local b = math.floor(rgb % 0x100)
                if math.abs(lr - r) > s or math.abs(lg - g) > s or math.abs(lb - b) > s then
                    return false
                end
            end
        end
    end

    if debug then
        return str
    else
        return true
    end
end

function swipe(x1, y1, x2, y2, no_down, no_up, stop, index)
    x2 = x1 + (x2 - x1) * DEVICE.swipeScaleX
    y2 = y1 + (y2 - y1) * DEVICE.swipeScaleY
    local step = 60 * DEVICE.scale
    if x2 > 1136 or y2 > 640 then
        step = step * 2
    end
    x1 = XX(x1)
    y1 = YY(y1)
    x2 = XX(x2)
    y2 = YY(y2)
    local index = index or 1

    local dx = x2 - x1
    local dy = y2 - y1
    local dist = math.sqrt(dx ^ 2 + dy ^ 2)
    if dist == 0 then
        return
    end
    local stepX = step / dist * dx
    local stepY = step / dist * dy

    if not no_down then
        touchDown(index, x1, y1)
    end
    sleep(50)

    local x, y = x1, y1
    while true do
        x = x + stepX
        y = y + stepY

        if (x - x1) / (x2 - x1) > 1 then
            x = x2
        end
        if (y - y1) / (y2 - y1) > 1 then
            y = y2
        end
        touchMove(index, x, y)
        sleep(20)
        if x == x2 and y == y2 then
            break
        end
    end

    if not no_up then
        if stop then
            sleep(200)
        end
        sleep(50)
        touchUp(index, x, y)
    end
end

PAD_X = 133
PAD_Y = 524
function pad_U()
    swipe(PAD_X, PAD_Y, 126, 399, false, true, false, 2)
    PAD_X, PAD_Y = 126, 399
end

function pad_D()
    swipe(PAD_X, PAD_Y, 132, 620, false, true, false, 2)
    PAD_X, PAD_Y = 132, 620
end

function pad_L()
    swipe(PAD_X, PAD_Y, 17, 521, false, true, false, 2)
    PAD_X, PAD_Y = 17, 521
end

function pad_R()
    swipe(PAD_X, PAD_Y, 258, 521, false, true, false, 2)
    PAD_X, PAD_Y = 258, 521
end

function pad_TL()
    swipe(PAD_X, PAD_Y, 46, 441, false, true, false, 2)
    PAD_X, PAD_Y = 46, 441
end

function pad_TR()
    swipe(PAD_X, PAD_Y, 225, 424, false, true, false, 2)
    PAD_X, PAD_Y = 225, 424
end

function pad_BL()
    swipe(PAD_X, PAD_Y, 54, 606, false, true, false, 2)
    PAD_X, PAD_Y = 54, 606
end

function pad_BR()
    swipe(PAD_X, PAD_Y, 208, 587, false, true, false, 2)
    PAD_X, PAD_Y = 208, 587
end

function pad_release()
    swipe(PAD_X, PAD_Y, 133, 524, true, false, false, 2)
    PAD_X, PAD_Y = 133, 524
end

function swipe2corner()
    swipe(310, 320, 2100, 2100)
    swipe(630, 320, 2100, 2100)
    sleep(300)
end

-- function swipe2bottom()
--   MAP.currRegion = 0
--   swipe(425,320,425,2000)
--   swipe(730,320,730,2000)
--   sleep(200)
-- end

function pos2xy(pos)
    return pos[1], pos[2]
end

function clickTeam(team, n)
    -- team 1-mob, 2-boss, 3-sub
    dy = 0
    if team == 2 then
        dy = 118
    elseif team == 3 then
        dy = 249
    end

    if n == 1 then
        click(905, 222 + dy, 1045, 242 + dy)
    elseif n == 2 then
        click(905, 260 + dy, 1045, 280 + dy)
    elseif n == 3 then
        click(905, 297 + dy, 1045, 317 + dy)
    elseif n == 4 then
        click(905, 334 + dy, 1045, 354 + dy)
    elseif n == 5 then
        click(905, 371 + dy, 1045, 391 + dy)
    elseif n == 6 then
        click(905, 409 + dy, 1045, 429 + dy)
    end
end

function split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}
    i = 1
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function ParseResearchConfig(_out_results, _rawstring)
    local t = split(_rawstring, "/")
    for i = 1, 4 do
        local num = tonumber(t[i])
        if num ~= nil then
            table.insert(_out_results, math.floor(num))
        else
            dialog("科研优先级参数存在格式错误，请检查并更正。")
            lua_restart()
        end
    end
end

function Y2W(y)
    if (SETTINGS.chapter == 2 and SETTINGS.level == 4) then
        return math.floor(94 + y / 22.5 + 0.5)
    elseif IsWorkingOn({ "7-1", "9-1", "18-3" }) then
        -- elseif (SETTINGS.chapter == 14 and SETTINGS.level == 3) then 鸢尾
        --   return math.floor(85 + y / 29.25 + 0.5)
        -- elseif (SETTINGS.chapter == 15 and SETTINGS.level == 3) then 鸢尾
        --   return math.floor(85.3 + y / 29.38 + 0.5)
        -- elseif IsWorkingOn({'14-1',}) then
        --   return math.floor(77.74 + y / 36.3 + 0.5)
        -- (SETTINGS.chapter == 14 and SETTINGS.level == 1) 鸢尾
        -- (SETTINGS.chapter == 15 and SETTINGS.level == 1) 鸢尾
        return math.floor(101.755 + y / 13.429 + 0.5)
    elseif IsWorkingOn({ "18-1", "18-2" }) then
        return math.floor(101.3 + y / 12 + 0.5)
    elseif IsWorkingOn({ "19-1" }) then
        return math.floor(101.8 + y / 13 + 0.5)
    elseif IsWorkingOn({ "18-4" }) then
        return math.floor(83.12 + y / 25.67 + 0.5)
    else
        return math.floor(94 + y / 15.95 + 0.5)
    end
end

function IsWorkingOnEventLevel()
    return SETTINGS.chapter >= Lid_EventLevel_P1 and SETTINGS.chapter <= Lid_EventLevel_P2
end

function IsWorkingOnEventLevel_Part1()
    return SETTINGS.chapter == Lid_EventLevel_P1
end

function IsWorkingOnEventLevel_Part2()
    return SETTINGS.chapter == Lid_EventLevel_P2
end

-- _LevelStrTab：例如 {'7-2',}
function IsWorkingOn(_LevelStrTab)
    for i, v in ipairs(_LevelStrTab) do
        local cs = split(v, "-")
        if (SETTINGS.chapter == tonumber(cs[1])) and (SETTINGS.level == tonumber(cs[2])) then
            return true
        end
    end
    return false
end

-- 支持关卡难度字符H、N，但不是必需
function IsWorkingOnEx(_LevelStrExTab)
    for i, v in ipairs(_LevelStrExTab) do
        local cs = split(v, "-")
        local chapter_str = cs[1]
        local level_str = cs[2]
        local mode_matched = (tonumber(chapter_str) ~= nil)

        if not mode_matched then
            local first_char = string.sub(chapter_str, 1, 1)
            if first_char == "H" or first_char == "h" then
                if SETTINGS.mode == "hard" then
                    mode_matched = true
                    chapter_str = string.sub(chapter_str, 2)
                end
            end
            if first_char == "N" or first_char == "n" then
                if SETTINGS.mode == "normal" then
                    mode_matched = true
                    chapter_str = string.sub(chapter_str, 2)
                end
            end
        end

        if mode_matched and SETTINGS.chapter == tonumber(chapter_str) and (SETTINGS.level == tonumber(level_str)) then
            return true
        end
    end
    return false
end

function IsWorkingOnTBYX()
    return SETTINGS.chapter == Lid_TBYXLevel
end

function IsWorkingOnSpee()
    return SETTINGS.chapter == Lid_OP_Spee
end

function IsWorkingOnOppcb()
    return (SETTINGS.chapter >= Lid_OP_Oppcb_P1) and (SETTINGS.chapter <= Lid_OP_Oppcb_P2)
end

function IsWorkingOnIoLnD()
    return (SETTINGS.chapter >= Lid_IoLnD_P1) and (SETTINGS.chapter <= Lid_IoLnD_P2)
end

function GetRectColorAvg(x1_, y1_, x2_, y2_)
    local pix_cnt = (x2_ - x1_ + 1) * (y2_ - y1_ + 1)
    local r_total, g_total, b_total = 0, 0, 0

    keepScreen(true)
    for row_ = y1_, y2_ do
        for col_ = x1_, x2_ do
            local r, g, b = getColorRGB(col_, row_)
            r_total = r_total + r
            g_total = g_total + g
            b_total = b_total + b
        end
    end
    keepScreen(false)

    return {
        ["r"] = math.modf(r_total / pix_cnt),
        ["g"] = math.modf(g_total / pix_cnt),
        ["b"] = math.modf(b_total / pix_cnt)
    }
end

-- 记录开始刷图时间
function RecordBeginTime()
    if (STATS[8] == 0) and (STATE.preround_selected == false) then
        STATS[8] = mTime()
    end
end

-- 增加一次已刷图次数
function IncFarmedRound()
    --  预刷状态不计数
    if STATE.preround_selected == false then
        STATS[5] = STATS[5] + 1
    end
end

-- 战斗次数+1
function IncCombatCount()
    STATS[9] = STATS[9] + 1
end

-- 战斗次数归零
function ClearCombatCount()
    STATS[9] = 0
end

function TestShift(shift)
    shift = shift or { 0, 0 }
    local N = 3
    swipe(1000, 500, 1000 + shift[1] * N, 500 + shift[2])
    sleep(5000)
    swipe(1000, 500, 1000 - shift[1] * N, 500 - shift[2])
    sleep(5000)
    lua_exit()
end

function TestShiftInit(shiftInit)
    shiftInit = shiftInit or { 0, 0 }
    swipe2corner()
    sleep(2000)
    swipe(1000, 500, 1000 + shiftInit[1], 500 + shiftInit[2])
    sleep(5000)
    lua_exit()
end

function ShouldBeaconBattle()
    return SETTINGS.isBeacon and STATE.beacon_cycle < 3
end

function IsTimeValid(time_str_)
    if #time_str_ > 0 then
        local time_splits = split(time_str_, ":")
        if #time_splits == 3 then
            local h_ = tonumber(time_splits[1])
            local m_ = tonumber(time_splits[2])
            local s_ = tonumber(time_splits[3])
            if (0 <= h_ and h_ <= 24) and (0 <= m_ and m_ <= 59) and (0 <= s_ and s_ <= 59) then
                return true
            end
        end
    end
    return false
end

function time_str_to_hour(time_str_)
    local time_split = split(time_str_, ":")
    return tonumber(time_split[1]) + tonumber(time_split[2]) / 60 + tonumber(time_split[3]) / 3600
end

function keepTwoDecimalPlaces(decimal)
    decimal = decimal * 100
    if decimal % 1 >= 0.5 then
        decimal = math.ceil(decimal)
    else
        decimal = math.floor(decimal)
    end
    return decimal * 0.01
end

function get_pos_in_cell(x_, y_)
    local function below_left(x1, y1)
        return y1 > (0.9171 * x1 - 12.2)
    end
    local function below_right(x1, y1)
        return y1 > (0.7655 * x1 - 124.3)
    end
    local function below_middle(x1, y1)
        return y1 > (-0.1988 * x1 + 417.8)
    end

    if below_left(x_, y_) and below_middle(x_, y_) then
        return 1
    elseif below_right(x_, y_) and below_middle(x_, y_) then
        return 2
    elseif not below_right(x_, y_) and below_middle(x_, y_) then
        return 3
    elseif below_left(x_, y_) and not below_middle(x_, y_) then
        return 4
    elseif below_right(x_, y_) and not below_middle(x_, y_) then
        return 5
    elseif not below_right(x_, y_) and not below_middle(x_, y_) then
        return 6
    end

end