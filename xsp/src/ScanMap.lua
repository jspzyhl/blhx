function addToSet(set, points)
    if #points ~= 0 then
        for i = 1, #points do
            if points[i].x < 519 and points[i].y < 92 then
                -- upper left corner
            else
                set:add(Pos:new(points[i]))
            end
        end
    end
    return set
end

function round(x)
    return x + 0.5 - (x + 0.5) % 1
end

function scale(table, zoom)
    for i, row in ipairs(table) do
        row.x = round(row.x * zoom)
        row.y = round(row.y * zoom)
    end
    return table
end

ScanMap = {}

ScanMap.scanEnemiesLv = function()
    local table = {
        {x = 0, y = 0, color = 0xffffff},
        {x = 0, y = -3, color = 0xffffff},
        {x = 0, y = -7, color = 0xffffff},
        {x = 0, y = -10, color = 0xffffff},
        {x = 0, y = -13, color = 0xffffff},
        {x = 0, y = -17, color = 0xffffff},
        {x = 12, y = 0, color = 0xffffff},
        {x = 9, y = -11, color = 0xffffff},
        {x = 15, y = -11, color = 0xffffff}
    }

    local set = PosSet:new()
    -- if true then return set end
    points = myFindColors({105, 49, 1135, 639}, table, 99, 0, 0, 0)
    addToSet(set, points)

    scale(table, 1.2)
    points = myFindColors({105, 49, 1135, 639}, table, 99, 0, 0, 0)
    addToSet(set, points)

    local tmp = PosSet:new()
    for _, p in ipairs(set) do
        -- 白色的撤退切换迎击字，忽略掉
        if p.x > 649 and p.y > 587 then
        else
            tmp:add(p)
        end
    end
    set = tmp

    if SETTINGS.debug_raw then
        set:draw(Color.YELLOW)
    end
    set:shift(-10, -35)
    if SETTINGS.debug_shift then
        set:draw(Color.YELLOW)
    end
    return set
end

ScanMap.scanEnemiesMark1 = function()
    if IsWorkingOn({"19-1"}) then
        --  活动图特殊缩放
        -- elseif IsWorkingOn({'14-1',}) then
        -- local table = {
        --   {x=0,y=0,color=0xffeb9a},
        --   {x=-4,y=3,color=0xfceb96},
        --   {x=0,y=-4,color=0xffee99},
        --   {x=4,y=3,color=0xffee99}
        -- }
        -- local set = PosSet:new()
        -- points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        -- addToSet(set,  points)
        -- local tmp = PosSet:new()
        -- local subSetRaw = nil
        -- if SETTINGS.sub_team > 0 then
        --   subSetRaw = ScanMap.scanSub()
        --   for _, p in ipairs(set) do
        --     local skip = false
        --     for __,t in ipairs(subSetRaw) do
        --       if t:similar(p, 10) then
        --         skip = true
        --         break
        --       end
        --     end
        --     if not skip then
        --       tmp:add(p)
        --     end
        --   end
        --   set = tmp
        -- end
        -- if SETTINGS.debug_raw then set:draw(Color.WHITE) end
        -- set:shift(Y2W(335)/2-10, Y2W(335)/2)
        -- if SETTINGS.debug_shift then set:draw(Color.WHITE) end
        -- return set
        --  底
        local table = {
            {x = 0, y = 0, color = 0xffffb0},
            {x = 15, y = 0, color = 0xfdf8b4},
            {x = 7, y = -15, color = 0xfff6b3},
            {x = 7, y = -6, color = 0xfeee99},
            {x = 7, y = -2, color = 0xfceb96}
        }

        local set = PosSet:new()

        local set1 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set1, points)

        --  顶
        local table = {
            {x = 0, y = 0, color = 0xffffb0},
            {x = 11, y = 0, color = 0xfae9a5},
            {x = 6, y = -11, color = 0xfbeaa6},
            {x = 6, y = -5, color = 0xfded9a},
            {x = 6, y = -1, color = 0xfceb96}
        }
        local set2 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set2, points)

        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_raw then
            set:draw(Color.ORANGE)
        end
        set1:shift(Y2W(350) / 2.2, Y2W(350) / 2.2)
        set2:shift(Y2W(350) / 2.2, Y2W(350) / 2.2)
        set = PosSet:new()
        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_shift then
            set:draw(Color.ORANGE)
        end
        return set
    else
        local table = {
            {x = 0, y = 0, color = 0xfff3a5},
            {x = 0, y = 2, color = 0xffeb94},
            {x = 0, y = 5, color = 0xffef9c},
            {x = 1, y = 7, color = 0xffef9c},
            {x = 1, y = 10, color = 0xffeb94},
            {x = 4, y = 10, color = 0xffeb9c},
            {x = 6, y = 10, color = 0xfff7b5},
            {x = -2, y = 10, color = 0xffeb94},
            {x = -5, y = 10, color = 0xffffb5},
            {x=-4,y=0,color=0x000000,offset=0x6f6f6f},
            {x=4,y=0,color=0x000000,offset=0x6f6f6f},
            {x=-2,y=20,color=0x000000,offset=0x6f6f6f},
            {x=2,y=20,color=0x000000,offset=0x6f6f6f}
        }

        local set = PosSet:new()
        points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set, points)

        local tmp = PosSet:new()
        local subSetRaw = nil
        if SETTINGS.sub_team > 0 then
            subSetRaw = ScanMap.scanSub()
            for _, p in ipairs(set) do
                local skip = false
                for __, t in ipairs(subSetRaw) do
                    if t:similar(p, 10) then
                        skip = true
                        break
                    end
                end
                if not skip then
                    tmp:add(p)
                end
            end
            set = tmp
        end

        if SETTINGS.debug_raw then
            set:draw(Color.WHITE)
        end
        set:shift(Y2W(360) / 2, Y2W(360) / 2)
        if SETTINGS.debug_shift then
            set:draw(Color.WHITE)
        end
        return set
    end
end

ScanMap.scanEnemiesMark2 = function()
    if IsWorkingOn({"18-4"}) then
        --  底
        local table = {
            {x = 0, y = 0, color = 0xfaf0ac},
            {x = 5, y = 0, color = 0xfff4a2},
            {x = 3, y = -5, color = 0xffee99},
            {x = 19, y = 0, color = 0xf8eca8},
            {x = 14, y = 0, color = 0xfff5a3},
            {x = 17, y = -6, color = 0xf8f3ab}
        }

        local set = PosSet:new()

        local set1 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set1, points)

        --  顶
        local table = {
            {x = 0, y = 0, color = 0xfaf0ac},
            {x = 5, y = 0, color = 0xfff4a2},
            {x = 3, y = -5, color = 0xffee99},
            {x = 19, y = 0, color = 0xf8eca8},
            {x = 14, y = 0, color = 0xfff5a3},
            {x = 17, y = -6, color = 0xf8f3ab}
        }
        local set2 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set2, points)

        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_raw then
            set:draw(Color.ORANGE)
        end
        set1:shift(Y2W(350) / 2.09, Y2W(350) / 2.09)
        set2:shift(Y2W(350) / 2.09, Y2W(350) / 2.09)
        set = PosSet:new()
        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_shift then
            set:draw(Color.ORANGE)
        end
        return set
    elseif IsWorkingOn({"19-1"}) then
        -- elseif IsWorkingOn({'14-1',}) then
        --   local table = {
        --     {x=0,y=0,color=0xffee99},
        --     {x=1,y=-3,color=0xffee99},
        --     {x=-12,y=0,color=0xf4e39f},
        --     {x=-10,y=-3,color=0xffee99},
        --     {x=3,y=0,color=0xf4e39f}
        --   }
        --   local set = PosSet:new()
        --   local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        --   addToSet(set,  points)
        --   scale(table, 1.3)
        --   points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        --   addToSet(set,  points)
        --   if SETTINGS.debug_raw then set:draw(Color.ORANGE) end
        --   set:shift(Y2W(335)/2-10, Y2W(335)/2)
        --   if SETTINGS.debug_shift then set:draw(Color.ORANGE) end
        --   return set
        --  底
        local table = {
            {x = 0, y = 0, color = 0xfef7b4},
            {x = 8, y = 0, color = 0xfff29e},
            {x = 4, y = -7, color = 0xffef9b},
            {x = 20, y = 0, color = 0xfff3a1},
            {x = 27, y = 0, color = 0xf8eba7},
            {x = 23, y = -7, color = 0xffee99}
        }

        local set = PosSet:new()

        local set1 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set1, points)

        --  顶
        local table = {
            {x = 0, y = 0, color = 0xf8eca8},
            {x = 5, y = 0, color = 0xffef9b},
            {x = 3, y = -6, color = 0xfdef9e},
            {x = 16, y = 0, color = 0xfff3a4},
            {x = 22, y = 0, color = 0xf5e6a2},
            {x = 19, y = -6, color = 0xfdef9e}
        }
        local set2 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set2, points)

        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_raw then
            set:draw(Color.ORANGE)
        end
        set1:shift(Y2W(355) / 2.1, Y2W(355) / 2.1)
        set2:shift(Y2W(355) / 2.1, Y2W(355) / 2.1)
        set = PosSet:new()
        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_shift then
            set:draw(Color.ORANGE)
        end
        return set
    else
        local table = {
            {x = 0, y = 0, color = 0xffefad},
            {x = -3, y = 6, color = 0xfff39c},
            {x = 1, y = 6, color = 0xffef9c},
            {x = 14, y = 1, color = 0xfffbb5},
            {x = 17, y = 6, color = 0xfff7a5},
            {x = 12, y = 5, color = 0xffef9c},
            {x=4,y=0,color=0x000000,offset=0x6f6f6f},
            {x=7,y=0,color=0x000000,offset=0x6f6f6f},
            {x=10,y=0,color=0x000000,offset=0x6f6f6f}
        }

        local set = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set, points)

        scale(table, 1.3)
        points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set, points)

        if SETTINGS.debug_raw then
            set:draw(Color.ORANGE)
        end
        set:shift(Y2W(360) / 2, Y2W(360) / 2)
        if SETTINGS.debug_shift then
            set:draw(Color.ORANGE)
        end
        return set
    end
end

ScanMap.scanEnemiesMark3 = function()
    if IsWorkingOn({"19-1"}) then
        -- elseif IsWorkingOn({'14-1',}) then
        --     local table = {
        --       {x=0,y=0,color=0xff8888},
        --       {x=-4,y=-13,color=0xff8787},
        --       {x=-9,y=0,color=0xff8382},
        --       {x=2,y=-3,color=0xff8989},
        --       {x=-6,y=-11,color=0xff8888}
        --     }
        --     local set = PosSet:new()
        --     points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        --     addToSet(set,  points)
        --     if SETTINGS.debug_raw then set:draw(Color.RED) end
        --     set:shift(Y2W(335)/2-10, Y2W(335)/2)
        --     if SETTINGS.debug_shift then set:draw(Color.RED) end
        --     return set
        --  底
        local table = {
            {x = 0, y = 0, color = 0xfd8888},
            {x = -13, y = 16, color = 0xf48e8e},
            {x = -6, y = 16, color = 0xfb8787},
            {x = 6, y = 16, color = 0xfd9797},
            {x = 13, y = 16, color = 0xfd9797},
            {x = 0, y = -7, color = 0xff8888}
        }

        local set = PosSet:new()

        local set1 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set1, points)

        --  顶
        local table = {
            {x = 0, y = 0, color = 0xfe8989},
            {x = -12, y = 13, color = 0xf68b8b},
            {x = -6, y = 13, color = 0xfb8484},
            {x = 5, y = 13, color = 0xfd9595},
            {x = 11, y = 13, color = 0xf99393},
            {x = -1, y = -6, color = 0xff8888}
        }
        local set2 = PosSet:new()
        local points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set2, points)

        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_raw then
            set:draw(Color.ORANGE)
        end
        set1:shift(Y2W(346) / 1.81 - 20, Y2W(346) / 1.81)
        set2:shift(Y2W(346) / 1.81 - 20, Y2W(346) / 1.81)
        set = PosSet:new()
        set:merge(set1)
        set:merge(set2)
        if SETTINGS.debug_shift then
            set:draw(Color.ORANGE)
        end
        return set
    else
        local table = {
            {x = 0, y = 0, color = 0xff8a8c},
            {x = -2, y = 4, color = 0xff8a8c},
            {x = 3, y = 4, color = 0xff8a8c},
            {x = -7, y = 11, color = 0xff8a8c},
            {x = -9, y = 16, color = 0xff8284},
            {x = -5, y = 16, color = 0xff8a8c},
            {x = 8, y = 11, color = 0xff8684},
            {x = 6, y = 16, color = 0xff8284},
            {x = 10, y = 16, color = 0xff8a8c}
        }
        local set = PosSet:new()
        points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
        addToSet(set, points)

        if SETTINGS.debug_raw then
            set:draw(Color.RED)
        end
        set:shift(Y2W(360) / 2, Y2W(360) / 2)
        if SETTINGS.debug_shift then
            set:draw(Color.RED)
        end
        return set
    end
end

ScanMap.scanEnemiesDD = function()
    local table = {
        {x = 0, y = 0, color = 0xe7e7e7},
        {x = 11, y = -9, color = 0xe7e7e7},
        {x = 33, y = -10, color = 0xf7efde},
        {x = 49, y = -21, color = 0x181421},
        {x = 34, y = 11, color = 0xa5aab5},
        {x = 38, y = -6, color = 0x000000},
        {x = 24, y = -27, color = 0xdedfde},
        {x = -16, y = 16, color = 0x847d94}
    }

    local set = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    scale(table, 1.2)
    points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    if SETTINGS.debug_raw then
        set:draw(Color.WHITE)
    end
    set:shift(20, 0)
    if SETTINGS.debug_shift then
        set:draw(Color.WHITE)
    end
    return set
end

ScanMap.scanEnemiesCV = function()
    local table = {
        {x = 0, y = 0, color = 0x18615a},
        {x = 11, y = -8, color = 0x21655a},
        {x = 22, y = -16, color = 0x31655a},
        {x = 15, y = -20, color = 0x106963},
        {x = 3, y = -12, color = 0x106963},
        {x = -8, y = -4, color = 0x106563},
        {x = 8, y = -3, color = 0xe7cf9c},
        {x = 8, y = -16, color = 0xe7c38c}
    }

    local set = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 90, 1, 1, 1)
    addToSet(set, points)

    scale(table, 1.2)
    points = myFindColors({105, 49, 1135, 639}, table, 90, 1, 1, 1)
    addToSet(set, points)

    if SETTINGS.debug_raw then
        set:draw(Color.DARK_GREEN)
    end
    set:shift(0, 10)
    if SETTINGS.debug_shift then
        set:draw(Color.DARK_GREEN)
    end
    return set
end

ScanMap.scanEnemiesBB = function()
    local table = {
        {x = 0, y = 0, color = 0x847d84},
        {x = -5, y = 5, color = 0x848284},
        {x = -14, y = 17, color = 0xd6d7de},
        {x = -35, y = -3, color = 0xc6c3c6},
        {x = -39, y = 1, color = 0xbdbabd},
        {x = -40, y = 10, color = 0x6b6973},
        {x = -34, y = 14, color = 0x736d73}
    }

    local set = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    scale(table, 1.2)
    points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    if SETTINGS.debug_raw then
        set:draw(Color.BLACK)
    end
    set:shift(-5, 0)
    if SETTINGS.debug_shift then
        set:draw(Color.BLACK)
    end
    return set
end

ScanMap.scanEnemiesGG = function()
    local table = {
        {x = 0, y = 0, color = 0xe7d784},
        {x = -5, y = -18, color = 0xe7db8c},
        {x = -10, y = 14, color = 0xd6b64a},
        {x = -26, y = 12, color = 0xd6b64a},
        {x = 21, y = -23, color = 0x5a5963},
        {x = 44, y = 1, color = 0xd6d7d6}
    }

    local set = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    -- scale(table, 1.2)
    -- points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    -- addToSet(set,  points)

    if SETTINGS.debug_raw then
        set:draw(Color.PINK)
    end
    set:shift(10, 0)
    if SETTINGS.debug_shift then
        set:draw(Color.PINK)
    end
    return set
end

ScanMap.scanEnemiesIoLnD = function()
    -- 档案：光与影的鸢尾之华 精英敌人
    local newset = PosSet:new()
    newset:merge(ScanMap.scanEnemiesZhuizong({"21-2"}))
    newset:merge(ScanMap.scanEnemiesLingyang({"21-2"}))
    newset:merge(ScanMap.scanEnemiesPojvzhe({"21-2"}))
    return newset
end

ScanMap.scanEnemiesMark4 = function()
    local newset = PosSet:new()
    newset:merge(ScanMap.scanEnemiesEvent())
    return newset
end

ScanMap.scanEnemiesEvent = function()
    -- 活动图特别敌人
    local newset = PosSet:new()
    -- newset:merge(ScanMap.scanEnemiesAC())
    -- newset:merge(ScanMap.scanEnemiesCarabiniere())
    -- newset:merge(ScanMap.scanEnemiesTrento())
    -- newset:merge(ScanMap.scanEnemiesLittorio())

    return newset
end

ScanMap.scanEnemiesEvent_Submarine = function()
    -- 活动图特别敌人：潜艇
    local newset = PosSet:new()
    -- newset:merge(ScanMap.scanEnemiesQianfu1())
    return newset
end

-- ScanMap.scanEnemiesU81 = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1',}) == false then
--       return nil
--     end
--   end

--   -- U81 -- 底
--   local table = {
--     {x=0,y=0,color=0xcd5c64},
--     {x=-11,y=-4,color=0xfffbeb},
--     {x=-9,y=9,color=0x765f5d},
--     {x=15,y=2,color=0xcc6161},
--     {x=-4,y=-17,color=0x756e92}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- U81  -- 顶
--   table = {
--     {x=0,y=0,color=0xcd5f66},
--     {x=-11,y=-4,color=0xfffbeb},
--     {x=-10,y=8,color=0x75605d},
--     {x=14,y=0,color=0xcf5e65},
--     {x=-3,y=-16,color=0x756e92}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- U81  -- 反 底
--   table = {
--     {x=0,y=0,color=0xcb5f68},
--     {x=11,y=-6,color=0xfffaea},
--     {x=12,y=10,color=0x75605d},
--     {x=-15,y=1,color=0xcf5e65},
--     {x=3,y=-18,color=0x756e92}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- U81  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xcc6466},
--     {x=11,y=-4,color=0xfffbeb},
--     {x=12,y=9,color=0x75605d},
--     {x=-13,y=2,color=0xcc5f67},
--     {x=3,y=-17,color=0x756e92}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/8.92)
--   set2:shift(0, Y2W(325)/8.92)
--   set3:shift(5, Y2W(325)/8.92)
--   set4:shift(5, Y2W(325)/8.92)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesU101 = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','15-1'}) == false then
--       return nil
--     end
--   end

--   -- U101 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-14,y=2,color=0x94defe},
--     {x=-45,y=-34,color=0x710414},
--     {x=-6,y=-61,color=0x484040},
--     {x=12,y=-24,color=0xfdfef9}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- U101  -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-13,y=2,color=0x95dfff},
--     {x=-41,y=-32,color=0x720414},
--     {x=-7,y=-53,color=0x484040},
--     {x=10,y=-21,color=0xfefff8}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- U101  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=12,y=2,color=0x97ddfe},
--     {x=-29,y=-46,color=0x5c0716},
--     {x=2,y=-62,color=0x474242},
--     {x=-13,y=-24,color=0xfdfef9}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- U101  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=11,y=2,color=0x97ddfe},
--     {x=-24,y=-40,color=0x5f0814},
--     {x=4,y=-54,color=0x474040},
--     {x=-11,y=-22,color=0xfdfef8}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.78)
--   set2:shift(0, Y2W(325)/1.78)
--   set3:shift(5, Y2W(325)/1.78)
--   set4:shift(5, Y2W(325)/1.78)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesU522 = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','15-1',}) == false then
--       return nil
--     end
--   end

--   -- U522 -- 底
--   local table = {
--     {x=0,y=0,color=0x9a243b},
--     {x=-15,y=-9,color=0x555455},
--     {x=-19,y=7,color=0x8e9596},
--     {x=22,y=-1,color=0x303640},
--     {x=-6,y=-22,color=0xfcfcfc}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- U522  -- 顶
--   table = {
--     {x=0,y=0,color=0x99243b},
--     {x=-13,y=-6,color=0x575455},
--     {x=-16,y=6,color=0x909798},
--     {x=20,y=0,color=0x323640},
--     {x=-4,y=-18,color=0xfcfcfc}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- U522  -- 反 底
--   table = {
--     {x=0,y=0,color=0x9a243b},
--     {x=15,y=-8,color=0x555455},
--     {x=19,y=7,color=0x8f9697},
--     {x=-22,y=-1,color=0x323740},
--     {x=5,y=-22,color=0xfcfcfc}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- U522  -- 反 顶
--   table = {
--     {x=0,y=0,color=0x9d243c},
--     {x=12,y=-6,color=0x555455},
--     {x=15,y=7,color=0x8e9596},
--     {x=-20,y=-1,color=0x303640},
--     {x=4,y=-18,color=0xfcfcfc}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/14.63)
--   set2:shift(0, Y2W(325)/14.63)
--   set3:shift(0, Y2W(325)/14.63)
--   set4:shift(0, Y2W(325)/14.63)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesScharnhorst = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2',}) == false then
--       return nil
--     end
--   end

--   -- 沙恩霍斯特 -- 底
--   local table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=-14,y=-9,color=0x3399cc},
--     {x=-14,y=-15,color=0x233a65},
--     {x=-18,y=-68,color=0xdf6482},
--     {x=-6,y=-70,color=0x454051}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 沙恩霍斯特  -- 顶
--   table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=-13,y=-9,color=0x3399cc},
--     {x=-12,y=-14,color=0x233b65},
--     {x=-16,y=-61,color=0xdf6582},
--     {x=-5,y=-62,color=0x454051}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 沙恩霍斯特  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=15,y=-10,color=0x3399cc},
--     {x=13,y=-15,color=0x253669},
--     {x=16,y=-71,color=0xdf6482},
--     {x=7,y=-72,color=0x454051}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 沙恩霍斯特  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=13,y=-8,color=0x3399cc},
--     {x=11,y=-13,color=0x253669},
--     {x=13,y=-61,color=0xdf6482},
--     {x=4,y=-62,color=0x454051}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/2)
--   set2:shift(0, Y2W(325)/2)
--   set3:shift(5, Y2W(325)/2)
--   set4:shift(5, Y2W(325)/2)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesDeutschland = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2',}) == false then
--       return nil
--     end
--   end

--   -- 德意志 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-10,y=3,color=0xc3fef6},
--     {x=-10,y=-3,color=0x093963},
--     {x=-6,y=-60,color=0x6a7fad},
--     {x=-2,y=-20,color=0x696e71}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 德意志  -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-9,y=-2,color=0x063761},
--     {x=-9,y=3,color=0xc2fef7},
--     {x=-6,y=-52,color=0x6880aa},
--     {x=-2,y=-17,color=0x696e71}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 德意志  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=13,y=3,color=0xbcfffe},
--     {x=11,y=-2,color=0x063761},
--     {x=8,y=-59,color=0x6880aa},
--     {x=4,y=-19,color=0x6a6f72}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 德意志  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=10,y=-1,color=0x0b3d66},
--     {x=10,y=3,color=0xc1fef7},
--     {x=6,y=-51,color=0x697fa6},
--     {x=2,y=-17,color=0x696e71}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.77)
--   set2:shift(0, Y2W(325)/1.77)
--   set3:shift(5, Y2W(325)/1.77)
--   set4:shift(5, Y2W(325)/1.77)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesGneisenau = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2',}) == false then
--       return nil
--     end
--   end

--   -- 格奈森瑙 -- 底
--   local table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=-14,y=0,color=0x233b65},
--     {x=7,y=26,color=0xbeabbe},
--     {x=-7,y=-59,color=0xdf6482},
--     {x=1,y=-57,color=0x444051}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 格奈森瑙  -- 顶
--   table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=-13,y=-2,color=0x233b65},
--     {x=7,y=22,color=0xbeabbe},
--     {x=-6,y=-53,color=0xdf6482},
--     {x=2,y=-51,color=0x454051}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 格奈森瑙  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=13,y=-1,color=0x243c66},
--     {x=-8,y=26,color=0xbeabbe},
--     {x=6,y=-59,color=0xdd6483},
--     {x=-2,y=-57,color=0x454051}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 格奈森瑙  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfff9e9},
--     {x=10,y=0,color=0x233b65},
--     {x=-9,y=23,color=0xbeabbe},
--     {x=4,y=-52,color=0xda6385},
--     {x=-2,y=-51,color=0x454051}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-2, Y2W(325)/1.61)
--   set2:shift(-2, Y2W(325)/1.61)
--   set3:shift(5, Y2W(325)/1.61)
--   set4:shift(5, Y2W(325)/1.61)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesU73 = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'15-1',}) == false then
--       return nil
--     end
--   end

--   -- U73 -- 底
--   local table = {
--     {x=0,y=0,color=0x333342},
--     {x=-13,y=-1,color=0x2a2932},
--     {x=-6,y=19,color=0x720d09},
--     {x=30,y=7,color=0x74788d},
--     {x=4,y=-7,color=0x740000}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- U73  -- 顶
--   table = {
--     {x=0,y=0,color=0x34343d},
--     {x=-10,y=-2,color=0x2a2932},
--     {x=-6,y=17,color=0x710c0a},
--     {x=26,y=7,color=0x72788b},
--     {x=3,y=-5,color=0x740000}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- U73  -- 反 底
--   table = {
--     {x=0,y=0,color=0x33343d},
--     {x=11,y=-1,color=0x2c2a34},
--     {x=4,y=19,color=0x730d08},
--     {x=-30,y=8,color=0x73788c},
--     {x=-3,y=-7,color=0x740000}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- U73  -- 反 顶
--   table = {
--     {x=0,y=0,color=0x30323c},
--     {x=10,y=-2,color=0x2c2a33},
--     {x=3,y=16,color=0x730d08},
--     {x=-25,y=6,color=0x72778b},
--     {x=-1,y=-6,color=0x740000}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/9.67)
--   set2:shift(0, Y2W(325)/9.67)
--   set3:shift(2, Y2W(325)/9.67)
--   set4:shift(2, Y2W(325)/9.67)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesZ18 = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','14-3'}) == false then
--       return nil
--     end
--   end

--   -- z18 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-12,y=2,color=0x74b8fc},
--     {x=0,y=-24,color=0xfff9d8},
--     {x=2,y=-38,color=0xe14c4c},
--     {x=2,y=-46,color=0x38363c}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- z18  -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-9,y=1,color=0x74b8fc},
--     {x=2,y=-22,color=0xfff9d8},
--     {x=4,y=-34,color=0xdf4b4e},
--     {x=2,y=-41,color=0x38363c}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- z18  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=11,y=2,color=0x76bafe},
--     {x=-3,y=-24,color=0xfff9d8},
--     {x=-4,y=-38,color=0xdf484c},
--     {x=-2,y=-46,color=0x38363c}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- z18  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=11,y=1,color=0x75b9fd},
--     {x=-1,y=-23,color=0xfff9d8},
--     {x=-1,y=-34,color=0xe04c4c},
--     {x=-2,y=-41,color=0x38363c}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/2.07)
--   set2:shift(0, Y2W(325)/2.07)
--   set3:shift(5, Y2W(325)/2.07)
--   set4:shift(5, Y2W(325)/2.07)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesZ2 = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2',}) == false then
--       return nil
--     end
--   end

--   -- z2 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-12,y=0,color=0x441111},
--     {x=6,y=-23,color=0xc29180},
--     {x=2,y=-58,color=0xb63e49},
--     {x=14,y=-51,color=0x4b4a70}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- z2  -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-11,y=0,color=0x441111},
--     {x=5,y=-20,color=0xc29180},
--     {x=1,y=-51,color=0xb63e49},
--     {x=12,y=-45,color=0x4b486f}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- z2  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=14,y=0,color=0x441111},
--     {x=-6,y=-24,color=0xbe9182},
--     {x=-1,y=-59,color=0xb63e49},
--     {x=-12,y=-53,color=0x4d4b72}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- z2  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=11,y=0,color=0x441111},
--     {x=-5,y=-21,color=0xc29180},
--     {x=-3,y=-51,color=0xb63e49},
--     {x=-11,y=-46,color=0x4c4a70}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.84)
--   set2:shift(0, Y2W(325)/1.84)
--   set3:shift(0, Y2W(325)/1.84)
--   set4:shift(0, Y2W(325)/1.84)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesZ19 = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-3',}) == false then
--       return nil
--     end
--   end

--   -- z19 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-10,y=0,color=0x656576},
--     {x=4,y=-12,color=0x65595e},
--     {x=6,y=-32,color=0xf33b58},
--     {x=15,y=-22,color=0x6d656d}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- z19  -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-10,y=0,color=0x626273},
--     {x=2,y=-10,color=0x65585f},
--     {x=5,y=-28,color=0xef4155},
--     {x=13,y=-20,color=0x6b656b}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- z19  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=10,y=0,color=0x666677},
--     {x=-5,y=-12,color=0x65595f},
--     {x=-7,y=-33,color=0xef4355},
--     {x=-16,y=-23,color=0x6d656d}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- z19  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=9,y=0,color=0x666676},
--     {x=-4,y=-10,color=0x65595e},
--     {x=-6,y=-28,color=0xef4255},
--     {x=-13,y=-19,color=0x6d656d}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/2.0)
--   set2:shift(0, Y2W(325)/2.0)
--   set3:shift(4, Y2W(325)/2.0)
--   set4:shift(4, Y2W(325)/2.0)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- 阿尔及利亚
-- ScanMap.scanEnemiesAlgerie = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'15-1','15-2'}) == false then
--       return nil
--     end
--   end

--   -- 阿尔及利亚 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-14,y=-3,color=0x4f4772},
--     {x=-42,y=-39,color=0x444040},
--     {x=2,y=-20,color=0xb9aeb5},
--     {x=-32,y=1,color=0xe9bfa7}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 阿尔及利亚  -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-12,y=-3,color=0x504871},
--     {x=-37,y=-34,color=0x453f3d},
--     {x=2,y=-17,color=0xbbafb5},
--     {x=-28,y=1,color=0xe8c0a5}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 阿尔及利亚  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=16,y=-2,color=0x433a64},
--     {x=43,y=-39,color=0x433f3e},
--     {x=-2,y=-20,color=0xb9aeb3},
--     {x=33,y=1,color=0xe9bfa7}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 阿尔及利亚  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=15,y=-3,color=0x463e6b},
--     {x=38,y=-35,color=0x433e3c},
--     {x=-1,y=-18,color=0xb9aeb5},
--     {x=30,y=0,color=0xe8c1a5}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.53)
--   set2:shift(0, Y2W(325)/1.53)
--   set3:shift(6, Y2W(325)/1.53)
--   set4:shift(6, Y2W(325)/1.53)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 拉·加利索尼埃
-- ScanMap.scanEnemiesLJLSN = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'15-1','15-3'}) == false then
--       return nil
--     end
--   end

--   -- 拉·加利索尼埃 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-17,y=-2,color=0x694061},
--     {x=0,y=-21,color=0xe7a0cd},
--     {x=-3,y=46,color=0xf3edf3}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 拉·加利索尼埃  -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-15,y=-1,color=0x694061},
--     {x=-1,y=-18,color=0xe7a0cd},
--     {x=-2,y=42,color=0xf3edf0}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 拉·加利索尼埃  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=18,y=1,color=0x694061},
--     {x=-1,y=-19,color=0xe7a0cd},
--     {x=3,y=49,color=0xf3eef1}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 拉·加利索尼埃  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=15,y=0,color=0x694061},
--     {x=-1,y=-18,color=0xe7a0cd},
--     {x=2,y=41,color=0xf3eef2}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-2, Y2W(325)/1.68)
--   set2:shift(-2, Y2W(325)/1.68)
--   set3:shift(2, Y2W(325)/1.68)
--   set4:shift(2, Y2W(325)/1.68)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 沃克兰
-- ScanMap.scanEnemiesVauquelin = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'15-2','15-3'}) == false then
--       return nil
--     end
--   end

--   -- 沃克兰 -- 底
--   local table = {
--     {x=0,y=0,color=0xfcece3},
--     {x=-17,y=1,color=0xd75c48},
--     {x=-3,y=-23,color=0xf3e5e3},
--     {x=-4,y=-30,color=0xffffff},
--     {x=-20,y=19,color=0xc27779}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 沃克兰  -- 顶
--   table = {
--     {x=0,y=0,color=0xfcece3},
--     {x=-14,y=0,color=0xd55b42},
--     {x=-1,y=-20,color=0xf3e5e2},
--     {x=-2,y=-27,color=0xffffff},
--     {x=-16,y=17,color=0xc27778}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 沃克兰  -- 反 底
--   table = {
--     {x=0,y=0,color=0xfcece3},
--     {x=17,y=0,color=0xdc6544},
--     {x=2,y=-22,color=0xf3e5e3},
--     {x=2,y=-30,color=0xffffff},
--     {x=20,y=19,color=0xc27678}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 沃克兰  -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfcece3},
--     {x=16,y=1,color=0xd75c49},
--     {x=3,y=-20,color=0xf3e5e3},
--     {x=3,y=-28,color=0xffffff},
--     {x=18,y=18,color=0xc27778}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-2, Y2W(325)/1.57)
--   set2:shift(-2, Y2W(325)/1.57)
--   set3:shift(3, Y2W(325)/1.57)
--   set4:shift(3, Y2W(325)/1.57)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 苍龙
-- ScanMap.scanEnemiesSoryo = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-3','15-3'}) == false then
--       return nil
--     end
--   end

--   -- 苍龙 -- 底
--   local table = {
--     {x=0,y=0,color=0xfff1d3},
--     {x=-15,y=22,color=0x2c6879},
--     {x=3,y=-24,color=0x41546d},
--     {x=-1,y=-35,color=0xfbf0d4},
--     {x=-14,y=2,color=0x66ddee}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 苍龙 -- 顶
--   table = {
--     {x=0,y=0,color=0xfff1d3},
--     {x=-14,y=19,color=0x2c6879},
--     {x=2,y=-22,color=0x41546d},
--     {x=-1,y=-31,color=0xfbefd3},
--     {x=-12,y=1,color=0x64d8eb}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 苍龙 -- 底_反
--   table = {
--     {x=0,y=0,color=0xfff1d3},
--     {x=15,y=22,color=0x2c6879},
--     {x=-5,y=-25,color=0x41546d},
--     {x=0,y=-34,color=0xfff2d3},
--     {x=14,y=3,color=0x66dbee}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 苍龙 -- 顶_反
--   table = {
--     {x=0,y=0,color=0xfff1d3},
--     {x=13,y=18,color=0x2c6879},
--     {x=-4,y=-21,color=0x41546d},
--     {x=1,y=-31,color=0xfdf4d7},
--     {x=11,y=2,color=0x66ddee}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)

--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-4, Y2W(325)/2.1)
--   set2:shift(-4, Y2W(325)/2.1)
--   set3:shift(6, Y2W(325)/2.1)
--   set4:shift(6, Y2W(325)/2.1)

--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 神通
-- ScanMap.scanEnemiesJintsu = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','14-2','14-3','15-3'}) == false then
--       return nil
--     end
--   end

--   -- 神通 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-13,y=24,color=0xffffff},
--     {x=-22,y=14,color=0xb79ea7},
--     {x=7,y=-25,color=0x96a0ec},
--     {x=-4,y=-37,color=0x676fb8}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 神通 -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-12,y=21,color=0xffffff},
--     {x=-19,y=13,color=0xb79ea7},
--     {x=6,y=-21,color=0x96a0ed},
--     {x=-3,y=-32,color=0x666eb4}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 神通 -- 底_反
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=12,y=25,color=0xffffff},
--     {x=21,y=16,color=0xb69ea9},
--     {x=-8,y=-23,color=0x96a0ed},
--     {x=3,y=-36,color=0x6870b5}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 神通 -- 顶_反
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=11,y=22,color=0xffffff},
--     {x=19,y=14,color=0xb69ea9},
--     {x=-7,y=-20,color=0x96a0ed},
--     {x=2,y=-31,color=0x6870b7}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)

--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/2.2)
--   set2:shift(0, Y2W(325)/2.2)
--   set3:shift(0, Y2W(325)/2.2)
--   set4:shift(0, Y2W(325)/2.2)

--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 鸟海
-- ScanMap.scanEnemiesChokai = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','14-2','14-3','15-3'}) == false then
--       return nil
--     end
--   end

--   -- 鸟海 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-13,y=0,color=0x493238},
--     {x=0,y=-27,color=0xba9782},
--     {x=-10,y=-36,color=0x454251}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 鸟海 -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-10,y=0,color=0x493238},
--     {x=0,y=-24,color=0xba9782},
--     {x=-7,y=-32,color=0x454251}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 鸟海 -- 底_反
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=11,y=-1,color=0x493238},
--     {x=-2,y=-28,color=0xba9782},
--     {x=5,y=-39,color=0x454251}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 鸟海 -- 顶_反
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=10,y=0,color=0x493238},
--     {x=-1,y=-23,color=0xba9782},
--     {x=5,y=-33,color=0x454251}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)

--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-3, Y2W(325)/2.0)
--   set2:shift(-3, Y2W(325)/2.0)
--   set3:shift(5, Y2W(325)/2.0)
--   set4:shift(5, Y2W(325)/2.0)

--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 雾岛
-- ScanMap.scanEnemiesKirishima = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2','14-3','15-3'}) == false then
--       return nil
--     end
--   end

--   -- 雾岛 -- 底
--   local table = {
--     {x=0,y=0,color=0xfbf5eb},
--     {x=-13,y=3,color=0xf08aff},
--     {x=-1,y=9,color=0x4d3649},
--     {x=2,y=-24,color=0xfbc9b2},
--     {x=-3,y=-35,color=0xb99ba2}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 雾岛 -- 顶
--   table = {
--     {x=0,y=0,color=0xfbf5eb},
--     {x=-12,y=3,color=0xf28cff},
--     {x=1,y=8,color=0x4d3449},
--     {x=1,y=-20,color=0xfbc9b2},
--     {x=-2,y=-32,color=0xb79ba2}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 雾岛 -- 底_反
--   table = {
--     {x=0,y=0,color=0xfbf5eb},
--     {x=-13,y=4,color=0xf48eff},
--     {x=2,y=9,color=0x4d3449},
--     {x=0,y=-24,color=0xfbc9b2},
--     {x=-3,y=-36,color=0xbf9ba2}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 雾岛 -- 顶_反
--   table = {
--     {x=0,y=0,color=0xfbf5eb},
--     {x=11,y=3,color=0xf48eff},
--     {x=-1,y=8,color=0x4d3449},
--     {x=0,y=-21,color=0xfbc9b2},
--     {x=3,y=-32,color=0xbd9ba2}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)

--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-3, Y2W(325)/2.03)
--   set2:shift(-3, Y2W(325)/2.03)
--   set3:shift(0, Y2W(325)/2.03)
--   set4:shift(0, Y2W(325)/2.03)

--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 伊势
-- ScanMap.scanEnemiesIse = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2','14-3'}) == false then
--       return nil
--     end
--   end

--   -- 伊势 -- 底
--   local table = {
--   	{x=0,y=0,color=0xfff1d8},
--     {x=-11,y=-4,color=0x8e4004},
--     {x=14,y=-4,color=0x8c4103},
--     {x=-17,y=-50,color=0xc3989f},
--     {x=6,y=-17,color=0xb63045}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 伊势 -- 顶
--   table = {
--   	{x=0,y=0,color=0xfff1d8},
--     {x=-10,y=-3,color=0x8e4004},
--     {x=12,y=-3,color=0x8e4004},
--     {x=-14,y=-43,color=0xc3989f},
--     {x=7,y=-18,color=0xbe3245}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 伊势 -- 反 底
--   table = {
--     {x=0,y=0,color=0xfff1d8},
--     {x=11,y=-4,color=0x8c4403},
--     {x=-13,y=-4,color=0x8e4004},
--     {x=16,y=-50,color=0xc399a1},
--     {x=-9,y=-22,color=0xbd3245}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 伊势 -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfff1d8},
--     {x=10,y=-4,color=0x8e4004},
--     {x=-12,y=-4,color=0x8b4202},
--     {x=15,y=-44,color=0xc3989f},
--     {x=-8,y=-19,color=0xbd3245}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/2.07)
--   set2:shift(0, Y2W(325)/2.07)
--   set3:shift(5, Y2W(325)/2.07)
--   set4:shift(5, Y2W(325)/2.07)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 摩耶
-- ScanMap.scanEnemiesMaya = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','14-2','14-3'}) == false then
--       return nil
--     end
--   end

--   -- 摩耶 -- 底
--   local table = {
--   	{x=0,y=0,color=0xffffff},
--     {x=-10,y=3,color=0xf9d7b5},
--     {x=0,y=17,color=0xfdfdfd},
--     {x=-11,y=31,color=0x454551}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 摩耶 -- 顶
--   table = {
--     {x=0,y=0,color=0xffffff},
--     {x=-10,y=3,color=0xf9d7b5},
--     {x=3,y=28,color=0x5c5454},
--     {x=10,y=8,color=0xfffbeb}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 摩耶 -- 反 底
--   table = {
--     {x=0,y=0,color=0xffffff},
--     {x=11,y=5,color=0xf9d7b5,offset=0x101010},
--     {x=15,y=-41,color=0xb7b7c8},
--     {x=12,y=-2,color=0x000000}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 摩耶 -- 反 顶
--   table = {
--   	{x=0,y=0,color=0xffffff},
--     {x=9,y=4,color=0xfad8b6,offset=0x101010},
--     {x=12,y=-38,color=0xb7b7c8},
--     {x=11,y=-3,color=0x000000}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/2.02)
--   set2:shift(0, Y2W(325)/2.02)
--   set3:shift(0, Y2W(325)/2.02)
--   set4:shift(0, Y2W(325)/2.02)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 鬼怒
-- ScanMap.scanEnemiesKinu = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','14-2','14-3'}) == false then
--       return nil
--     end
--   end

--   -- 鬼怒 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-13,y=-1,color=0x745b27},
--     {x=-27,y=3,color=0xeabea8},
--     {x=-4,y=-18,color=0x262626},
--     {x=-7,y=-56,color=0x282828}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 鬼怒 -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-10,y=-2,color=0x654c1a},
--     {x=-22,y=2,color=0xe9bfa7},
--     {x=-2,y=-16,color=0x252627},
--     {x=-3,y=-48,color=0x282828}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 鬼怒 -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=14,y=-1,color=0x6b521f},
--     {x=28,y=4,color=0xe9bfa7},
--     {x=5,y=-17,color=0x262626},
--     {x=7,y=-55,color=0x282828}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 鬼怒 -- 反 顶
--   table = {
--   	{x=0,y=0,color=0xfffbef},
--     {x=11,y=-1,color=0x6c5321},
--     {x=23,y=2,color=0xeac0a5},
--     {x=4,y=-16,color=0x252728},
--     {x=2,y=-48,color=0x282828}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.92)
--   set2:shift(0, Y2W(325)/1.92)
--   set3:shift(0, Y2W(325)/1.92)
--   set4:shift(0, Y2W(325)/1.92)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 恶毒u
-- ScanMap.scanEnemiesLeMalinU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','14-2'}) == false then
--       return nil
--     end
--   end

--   -- 恶毒u -- 底
--   local table = {
--     {x=0,y=0,color=0xffede7},
--     {x=-17,y=-3,color=0x654b7d},
--     {x=-15,y=22,color=0xcdc4d3},
--     {x=13,y=3,color=0x8cbfff}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 恶毒u -- 顶
--   table = {
--     {x=0,y=0,color=0xffede7},
--     {x=-15,y=-3,color=0x6c4c7e},
--     {x=-15,y=18,color=0xcbc1d1},
--     {x=11,y=3,color=0x97c7ff}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 恶毒u -- 反 底
--   table = {
--     {x=0,y=0,color=0xffede7},
--     {x=16,y=-4,color=0x674b7c},
--     {x=14,y=21,color=0xcec4d3},
--     {x=-13,y=3,color=0x8bbeff}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 恶毒u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xffede7},
--     {x=15,y=-3,color=0x665080},
--     {x=13,y=18,color=0xcdc3d3},
--     {x=-11,y=2,color=0x8bbeff}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-2, Y2W(325)/1.57)
--   set2:shift(-2, Y2W(325)/1.57)
--   set3:shift(3, Y2W(325)/1.57)
--   set4:shift(3, Y2W(325)/1.57)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 黛朵u
-- ScanMap.scanEnemiesDidoU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-1','14-2','14-3'}) == false then
--       return nil
--     end
--   end

--   -- 黛朵u -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-17,y=1,color=0x90376d},
--     {x=14,y=2,color=0x973c76},
--     {x=-4,y=-54,color=0x3c3869},
--     {x=-46,y=-8,color=0xfefcfe}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 黛朵u -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-16,y=-1,color=0x8f366d},
--     {x=12,y=0,color=0x913670},
--     {x=-5,y=-47,color=0x3c3869},
--     {x=-41,y=-8,color=0xfdfbfd}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 黛朵u -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=20,y=1,color=0x943371},
--     {x=-13,y=0,color=0x913670},
--     {x=4,y=-53,color=0x3c3869},
--     {x=48,y=-8,color=0xfcfafc}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 黛朵u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=18,y=1,color=0x933770},
--     {x=-10,y=1,color=0x913670},
--     {x=2,y=-47,color=0x3c3869},
--     {x=43,y=-8,color=0xfdfbfd}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.52)
--   set2:shift(0, Y2W(325)/1.52)
--   set3:shift(3, Y2W(325)/1.52)
--   set4:shift(3, Y2W(325)/1.52)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 大青花鱼u
-- ScanMap.scanEnemiesAlbacoreU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2','14-3'}) == false then
--       return nil
--     end
--   end

--   -- 大青花鱼u -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-16,y=-10,color=0x70a1c2},
--     {x=-1,y=11,color=0xfbb79f},
--     {x=24,y=-63,color=0x8bffff},
--     {x=-2,y=-56,color=0xfbf1cb}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 大青花鱼u -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-12,y=-10,color=0x77aacc},
--     {x=1,y=9,color=0xfab8a0},
--     {x=22,y=-55,color=0x8bffff},
--     {x=1,y=-49,color=0xfbf1cb}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 大青花鱼u -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=15,y=-10,color=0x77aacc},
--     {x=0,y=11,color=0xfab8a0},
--     {x=-24,y=-61,color=0x8bffff},
--     {x=2,y=-55,color=0xfbf1cb}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 大青花鱼u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=14,y=-9,color=0x77aaca},
--     {x=1,y=9,color=0xfab8a0},
--     {x=-21,y=-54,color=0x8bffff},
--     {x=1,y=-49,color=0xfbf1cb}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-4, Y2W(325)/1.55)
--   set2:shift(-4, Y2W(325)/1.55)
--   set3:shift(7, Y2W(325)/1.55)
--   set4:shift(7, Y2W(325)/1.55)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 巴尔的摩u
-- ScanMap.scanEnemiesBaltimoreU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-3','14-5'}) == false then
--       return nil
--     end
--   end

--   -- 巴尔的摩u -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-13,y=2,color=0xfffb45},
--     {x=18,y=1,color=0xfefc45},
--     {x=28,y=-66,color=0x404871},
--     {x=1,y=34,color=0xfffbeb}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 巴尔的摩u -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-12,y=1,color=0xfffb45},
--     {x=15,y=0,color=0xfdfa45},
--     {x=25,y=-58,color=0x404871},
--     {x=1,y=29,color=0xfffbeb}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 巴尔的摩u -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=16,y=2,color=0xfffb45},
--     {x=-15,y=0,color=0xfdfa45},
--     {x=-23,y=-67,color=0x404871},
--     {x=1,y=34,color=0xfffbeb}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 巴尔的摩u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=14,y=1,color=0xfffb45},
--     {x=-14,y=0,color=0xfdf945},
--     {x=-23,y=-58,color=0x404871},
--     {x=1,y=29,color=0xfffbeb}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.75)
--   set2:shift(0, Y2W(325)/1.75)
--   set3:shift(3, Y2W(325)/1.75)
--   set4:shift(3, Y2W(325)/1.75)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 希佩尔u
-- ScanMap.scanEnemiesAdmiralHipperU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-4'}) == false then
--       return nil
--     end
--   end

--   -- 希佩尔u -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-13,y=-5,color=0x3e572e},
--     {x=-19,y=-1,color=0xcec67b},
--     {x=-38,y=-44,color=0x240000},
--     {x=-8,y=30,color=0xfffffb}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 希佩尔u -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-11,y=-4,color=0x415729},
--     {x=-16,y=-1,color=0xc9c780},
--     {x=-34,y=-39,color=0x240000},
--     {x=-6,y=27,color=0xfffffb}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 希佩尔u -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=14,y=-5,color=0x3e572e},
--     {x=20,y=-1,color=0xccc479},
--     {x=39,y=-45,color=0x240000},
--     {x=9,y=30,color=0xfffffb}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 希佩尔u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=12,y=-4,color=0x3e572e},
--     {x=17,y=0,color=0xc9c176},
--     {x=34,y=-38,color=0x240000},
--     {x=7,y=27,color=0xfffffb}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-2, Y2W(325)/1.72)
--   set2:shift(-2, Y2W(325)/1.72)
--   set3:shift(3, Y2W(325)/1.72)
--   set4:shift(3, Y2W(325)/1.72)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 克利夫兰u
-- ScanMap.scanEnemiesClevelandU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-4'}) == false then
--       return nil
--     end
--   end

--   -- 克利夫兰u -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-17,y=-10,color=0x69285c},
--     {x=-13,y=-1,color=0xcd5347},
--     {x=6,y=-35,color=0xfffbeb},
--     {x=3,y=-62,color=0xd9a27d}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 克利夫兰u -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-16,y=-10,color=0x69285b},
--     {x=-13,y=-2,color=0xd0544c},
--     {x=3,y=-32,color=0xfffbeb},
--     {x=-1,y=-55,color=0xdaa37d}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 克利夫兰u -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=18,y=-8,color=0x69285b},
--     {x=14,y=0,color=0xcf544a},
--     {x=-4,y=-35,color=0xfffbeb},
--     {x=2,y=-62,color=0xdba57d}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 克利夫兰u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=16,y=-9,color=0x69285c},
--     {x=12,y=-1,color=0xcd5446},
--     {x=-4,y=-31,color=0xfffbeb},
--     {x=0,y=-55,color=0xd9a37d}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.68)
--   set2:shift(0, Y2W(325)/1.68)
--   set3:shift(0, Y2W(325)/1.68)
--   set4:shift(0, Y2W(325)/1.68)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 斯佩u
-- ScanMap.scanEnemiesGrafSpeeU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-4'}) == false then
--       return nil
--     end
--   end

--   -- 斯佩u -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-18,y=2,color=0x9ae5f5},
--     {x=15,y=3,color=0x95e2f3},
--     {x=-1,y=-46,color=0xededed},
--     {x=-7,y=-49,color=0xc6c6c6}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 斯佩u -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-15,y=1,color=0x9ae5f5},
--     {x=12,y=1,color=0x99eeee},
--     {x=-1,y=-40,color=0xededed},
--     {x=-6,y=-43,color=0xc6c6c6}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 斯佩u -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=18,y=2,color=0x9ee8f9},
--     {x=-13,y=1,color=0x99eeee},
--     {x=1,y=-47,color=0xededed},
--     {x=8,y=-50,color=0xc6c6c6}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 斯佩u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=17,y=1,color=0xa0e5f6},
--     {x=-11,y=1,color=0x99ecee},
--     {x=2,y=-41,color=0xededed},
--     {x=6,y=-44,color=0xc7c7cb}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-2, Y2W(325)/1.63)
--   set2:shift(-2, Y2W(325)/1.63)
--   set3:shift(4, Y2W(325)/1.63)
--   set4:shift(4, Y2W(325)/1.63)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 罗恩u
-- ScanMap.scanEnemiesRoonU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-5'}) == false then
--       return nil
--     end
--   end

--   -- 罗恩u -- 底
--   local table = {
--     {x=0,y=0,color=0xf7efe3},
--     {x=-17,y=1,color=0xaf966d},
--     {x=-14,y=-1,color=0x000000},
--     {x=-31,y=-25,color=0xa83230},
--     {x=-6,y=-50,color=0xf7e1d3}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 罗恩u -- 顶
--   table = {
--     {x=0,y=0,color=0xf7efe3},
--     {x=-14,y=1,color=0xb3976a},
--     {x=-12,y=-2,color=0x000000},
--     {x=-27,y=-21,color=0xa93230},
--     {x=-6,y=-46,color=0xf7e1d2}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 罗恩u -- 反 底
--   table = {
--     {x=0,y=0,color=0xf7efe3},
--     {x=18,y=2,color=0xaf966d},
--     {x=15,y=-1,color=0x000000},
--     {x=34,y=-28,color=0xa8302f},
--     {x=7,y=-51,color=0xf7e1d3}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 罗恩u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xf7efe3},
--     {x=15,y=1,color=0xb0966d},
--     {x=13,y=-2,color=0x000000},
--     {x=27,y=-27,color=0xa93230},
--     {x=6,y=-44,color=0xf7e1d3}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-2, Y2W(325)/1.57)
--   set2:shift(-2, Y2W(325)/1.57)
--   set3:shift(3, Y2W(325)/1.57)
--   set4:shift(3, Y2W(325)/1.57)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 光辉u
-- ScanMap.scanEnemiesIllustriousU = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-5'}) == false then
--       return nil
--     end
--   end

--   -- 光辉u -- 底
--   local table = {
--     {x=0,y=0,color=0xfff7eb},
--     {x=-17,y=-3,color=0x33448e},
--     {x=14,y=-1,color=0x334494},
--     {x=-6,y=26,color=0xfffaea},
--     {x=0,y=-50,color=0xf3f0f4}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 光辉u -- 顶
--   table = {
--     {x=0,y=0,color=0xfff7eb},
--     {x=-14,y=-3,color=0x33448e},
--     {x=13,y=-2,color=0x334496},
--     {x=-3,y=22,color=0xfffaeb},
--     {x=0,y=-44,color=0xf3f0f4}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 光辉u -- 反 底
--   table = {
--     {x=0,y=0,color=0xfff7eb},
--     {x=17,y=-3,color=0x33448e},
--     {x=-13,y=-2,color=0x324397},
--     {x=5,y=25,color=0xfff8e8},
--     {x=2,y=-49,color=0xf3f1f6}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 光辉u -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfff7eb},
--     {x=16,y=-1,color=0x324392},
--     {x=-11,y=-2,color=0x324395},
--     {x=5,y=23,color=0xfffae9},
--     {x=2,y=-41,color=0xf3eff3}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-1, Y2W(325)/1.73)
--   set2:shift(-1, Y2W(325)/1.73)
--   set3:shift(3, Y2W(325)/1.73)
--   set4:shift(3, Y2W(325)/1.73)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 玛莉萝丝
-- ScanMap.scanEnemiesMarieRose = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2'}) == false then
--       return nil
--     end
--   end

--   -- 玛莉萝丝 -- 底
--   local table = {
--     {x=0,y=0,color=0xfbf1eb},
--     {x=-12,y=-3,color=0x2f2f48},
--     {x=13,y=-2,color=0x41415b},
--     {x=-8,y=-44,color=0xe7bd99},
--     {x=-2,y=31,color=0x483737}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 玛莉萝丝 -- 顶
--   table = {
--     {x=0,y=0,color=0xfbf1eb},
--     {x=-10,y=-3,color=0x373750},
--     {x=11,y=-2,color=0x3d3d58},
--     {x=-4,y=-40,color=0xe7be9b},
--     {x=-3,y=26,color=0x473636}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 玛莉萝丝 -- 反 底
--   table = {
--     {x=0,y=0,color=0xfbf1eb},
--     {x=12,y=-3,color=0x3c3c55},
--     {x=-13,y=-3,color=0x3e3e58},
--     {x=6,y=-45,color=0xe7bd99},
--     {x=3,y=30,color=0x473636}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 玛莉萝丝 -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfbf1eb},
--     {x=9,y=-3,color=0x3d3d57},
--     {x=-12,y=-3,color=0x3e3e58},
--     {x=5,y=-39,color=0xe7bd97},
--     {x=2,y=25,color=0x473636}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.95)
--   set2:shift(0, Y2W(325)/1.95)
--   set3:shift(0, Y2W(325)/1.95)
--   set4:shift(0, Y2W(325)/1.95)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 海咲
-- ScanMap.scanEnemiesMisaki = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-3','14-4'}) == false then
--       return nil
--     end
--   end

--   -- 海咲 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-14,y=-4,color=0x8e715c},
--     {x=13,y=-3,color=0x89705f},
--     {x=-2,y=29,color=0x454e51},
--     {x=25,y=-25,color=0xf4f46c}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 海咲 -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-12,y=-3,color=0x8a705c},
--     {x=11,y=-2,color=0x846b5a},
--     {x=-2,y=26,color=0x454e51},
--     {x=21,y=-22,color=0xe8e871}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 海咲 -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=13,y=-4,color=0x91705f},
--     {x=-14,y=-3,color=0x89705f},
--     {x=2,y=30,color=0x454e51},
--     {x=-26,y=-25,color=0xf1f169}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 海咲 -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=11,y=-3,color=0x89705f},
--     {x=-12,y=-3,color=0x89705f},
--     {x=1,y=26,color=0x464f50},
--     {x=-22,y=-22,color=0xefef71}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(-1, Y2W(325)/1.95)
--   set2:shift(-1, Y2W(325)/1.95)
--   set3:shift(3, Y2W(325)/1.95)
--   set4:shift(3, Y2W(325)/1.95)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 霞
-- ScanMap.scanEnemiesKasumi  = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-3','14-4'}) == false then
--       return nil
--     end
--   end

--   -- 霞 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-8,y=-4,color=0x4c332b},
--     {x=15,y=-4,color=0x4f2d2d},
--     {x=15,y=-29,color=0xde9c6b},
--     {x=1,y=32,color=0xfbf5f7}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 霞 -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-8,y=-4,color=0x482f27},
--     {x=13,y=-3,color=0x4f2d2d},
--     {x=13,y=-25,color=0xde9c6b},
--     {x=1,y=28,color=0xfbf5f7}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 霞 -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=9,y=-4,color=0x472e26},
--     {x=-15,y=-4,color=0x4c332b},
--     {x=-15,y=-29,color=0xdc9e6b},
--     {x=-1,y=32,color=0xfbf5f7}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 霞 -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=8,y=-4,color=0x4a3129},
--     {x=-12,y=-3,color=0x4e2f28},
--     {x=-13,y=-25,color=0xdd9d6b},
--     {x=-1,y=28,color=0xfbf5f7}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/2.29)
--   set2:shift(0, Y2W(325)/2.29)
--   set3:shift(4, Y2W(325)/2.29)
--   set4:shift(4, Y2W(325)/2.29)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- -- 凪咲
-- ScanMap.scanEnemiesNagisa  = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-4'}) == false then
--       return nil
--     end
--   end

--   -- 凪咲 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-13,y=-3,color=0x6b423c},
--     {x=11,y=-3,color=0x6c4a39},
--     {x=-2,y=25,color=0xe8e8e8},
--     {x=2,y=38,color=0xab1212}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 凪咲 -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=-12,y=-3,color=0x6b423c},
--     {x=9,y=-3,color=0x6b4939},
--     {x=-3,y=22,color=0xe5e5e5},
--     {x=1,y=33,color=0xab1313}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 凪咲 -- 反 底
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=14,y=-3,color=0x6a423c},
--     {x=-10,y=-3,color=0x6c4a39},
--     {x=3,y=25,color=0xe5e5e5},
--     {x=0,y=40,color=0xb01a19}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 凪咲 -- 反 顶
--   table = {
--     {x=0,y=0,color=0xfffbeb},
--     {x=12,y=-2,color=0x6a423c},
--     {x=-9,y=-2,color=0x6c4a39},
--     {x=2,y=22,color=0xe8e8e8},
--     {x=0,y=34,color=0xaf1a1a}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.98)
--   set2:shift(0, Y2W(325)/1.98)
--   set3:shift(3, Y2W(325)/1.98)
--   set4:shift(3, Y2W(325)/1.98)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- 破局者1型
ScanMap.scanEnemiesPojvzhe = function(lvID_tab)
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn(lvID_tab) == false then
            return nil
        end
    end

    -- 破局者1型 -- 底
    local table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = 17, y = -2, color = 0xc8863b},
        {x = -10, y = 34, color = 0xfbede7},
        {x = -7, y = -33, color = 0xfbfbff},
        {x = -4, y = 39, color = 0x3d3d55}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 破局者1型  -- 顶
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = 15, y = -1, color = 0xc8863b},
        {x = -9, y = 29, color = 0xfbede7},
        {x = 5, y = -28, color = 0xfefcff},
        {x = -4, y = 35, color = 0x3d3d55}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 破局者1型  -- 反 底
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = -18, y = -1, color = 0xc8863b},
        {x = 7, y = 35, color = 0xfbede7},
        {x = -6, y = -31, color = 0xfbfcff},
        {x = 3, y = 40, color = 0x3d3d55}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 破局者1型  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = -15, y = -3, color = 0xc8863b},
        {x = 5, y = 29, color = 0xfbede7},
        {x = -3, y = -28, color = 0xfdfdff},
        {x = 2, y = 34, color = 0x3d3d55}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-4, Y2W(325) / 1.5)
    set2:shift(-4, Y2W(325) / 1.5)
    set3:shift(4, Y2W(325) / 1.5)
    set4:shift(4, Y2W(325) / 1.5)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 追踪者I
ScanMap.scanEnemiesZhuizong = function(lvID_tab)
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn(lvID_tab) == false then
            return nil
        end
    end

    -- 追踪者I -- 底
    local table = {
        {x = 0, y = 0, color = 0xe7c0b6},
        {x = -19, y = -2, color = 0xc58132},
        {x = -6, y = 26, color = 0x454059},
        {x = -6, y = 40, color = 0xfcecec},
        {x = -11, y = -39, color = 0xffffff}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 追踪者I -- 顶
    table = {
        {x = 0, y = 0, color = 0xe8c2b7},
        {x = -16, y = -2, color = 0xc48034},
        {x = -4, y = 23, color = 0x454059},
        {x = -6, y = 36, color = 0xfbedeb},
        {x = -10, y = -33, color = 0xfeffff}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 追踪者I -- 反_底
    table = {
        {x = 0, y = 0, color = 0xe7c1b6},
        {x = 17, y = -2, color = 0xc58233},
        {x = 4, y = 26, color = 0x454059},
        {x = 5, y = 41, color = 0xfbedeb},
        {x = 9, y = -39, color = 0xffffff}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 追踪者I -- 反_顶
    table = {
        {x = 0, y = 0, color = 0xe8c2b7},
        {x = 15, y = -2, color = 0xc48032},
        {x = 3, y = 24, color = 0x434159},
        {x = 4, y = 37, color = 0xfbedeb},
        {x = 8, y = -33, color = 0xfffeff}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-4, Y2W(325) / 1.7)
    set2:shift(-4, Y2W(325) / 1.7)
    set3:shift(5, Y2W(325) / 1.7)
    set4:shift(5, Y2W(325) / 1.7)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 领洋者I
ScanMap.scanEnemiesLingyang = function(lvID_tab)
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn(lvID_tab) == false then
            return nil
        end
    end
    -- 领洋者I -- 底
    local table = {
        {x = 0, y = 0, color = 0x3b3a54},
        {x = -13, y = -37, color = 0xbabad3},
        {x = 29, y = -39, color = 0xfff39a},
        {x = 35, y = -62, color = 0xfbedeb}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 领洋者I -- 顶
    table = {
        {x = 0, y = 0, color = 0x373650},
        {x = -12, y = -35, color = 0xbfbdd8},
        {x = 26, y = -35, color = 0xfff498},
        {x = 30, y = -56, color = 0xfbedeb}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 领洋者I -- 反_底
    table = {
        {x = 0, y = 0, color = 0x3c3a55},
        {x = 11, y = -39, color = 0xc1bfd8},
        {x = -32, y = -38, color = 0xfff498},
        {x = -37, y = -61, color = 0xfbedeb}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 领洋者I -- 反_顶
    table = {
        {x = 0, y = 0, color = 0x3b3a54},
        {x = 9, y = -35, color = 0xbebbd4},
        {x = -28, y = -34, color = 0xfff599},
        {x = -32, y = -54, color = 0xfbedeb}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(6, Y2W(325) / 9)
    set2:shift(6, Y2W(325) / 9)
    set3:shift(-5, Y2W(325) / 9)
    set4:shift(-5, Y2W(325) / 9)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 探索者2型
ScanMap.scanEnemiesTansuozhe2 = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-1", "15-2", "15-3"}) == false then
            return nil
        end
    end

    -- 探索者2型 -- 底
    local table = {
        {x = 0, y = 0, color = 0xe3bfb2},
        {x = -14, y = 2, color = 0x5da9ae},
        {x = 16, y = 2, color = 0x60a7aa},
        {x = -12, y = 7, color = 0x99ffff},
        {x = -6, y = -48, color = 0xefefff}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 探索者2型 -- 顶
    table = {
        {x = 0, y = 0, color = 0xe3bfb2},
        {x = -12, y = 2, color = 0x5ca7af},
        {x = 14, y = 2, color = 0x5ca7af},
        {x = -10, y = 6, color = 0x99ffff},
        {x = -2, y = -41, color = 0xefefff}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 探索者2型 -- 反 底
    table = {
        {x = 0, y = 0, color = 0xe3bfb4},
        {x = 15, y = 3, color = 0x5ca7af},
        {x = -13, y = 2, color = 0x66aaaa},
        {x = 12, y = 7, color = 0x99ffff},
        {x = 3, y = -48, color = 0xefefff}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 探索者2型 -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xe3bfb5},
        {x = 13, y = 1, color = 0x5ca8af},
        {x = -13, y = 2, color = 0x60a8ad},
        {x = 9, y = 5, color = 0x9affff},
        {x = 0, y = -42, color = 0xefefff}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(0, Y2W(325) / 1.53)
    set2:shift(0, Y2W(325) / 1.53)
    set3:shift(0, Y2W(325) / 1.53)
    set4:shift(0, Y2W(325) / 1.53)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 执棋者2型
ScanMap.scanEnemiesZhiqizhe2 = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-6"}) == false then
            return nil
        end
    end

    -- 执棋者2型 -- 底
    local table = {
        {x = 0, y = 0, color = 0xe7c3ba},
        {x = -13, y = 1, color = 0x78ffff},
        {x = -1, y = -34, color = 0xe7c3b6},
        {x = 9, y = 20, color = 0x2e2e42},
        {x = -2, y = 32, color = 0xfbebe7}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 执棋者2型  -- 顶
    table = {
        {x = 0, y = 0, color = 0xe7c3ba},
        {x = -11, y = 1, color = 0x78ffff},
        {x = -1, y = -30, color = 0xe7c3b6},
        {x = 8, y = 17, color = 0x302f43},
        {x = -1, y = 28, color = 0xfbebe7}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 执棋者2型 -- 反 底
    table = {
        {x = 0, y = 0, color = 0xe7c3b7},
        {x = 15, y = 2, color = 0x77ffff},
        {x = 4, y = -32, color = 0xe7c3b6},
        {x = -7, y = 21, color = 0x2f2f42},
        {x = 4, y = 33, color = 0xfbebe7}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 执棋者2型  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xe7c2b7},
        {x = 11, y = 2, color = 0x77ffff},
        {x = 2, y = -28, color = 0xe7c3b6},
        {x = -7, y = 19, color = 0x2f2e43},
        {x = 2, y = 30, color = 0xfbebe7}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-4, Y2W(325) / 1.57)
    set2:shift(-4, Y2W(325) / 1.57)
    set3:shift(8, Y2W(325) / 1.57)
    set4:shift(8, Y2W(325) / 1.57)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 执棋者_P
ScanMap.scanEnemiesZhiqizhe_P = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-3"}) == false then
            return nil
        end
    end

    -- 执棋者_P -- 底
    local table = {
        {x = 0, y = 0, color = 0xe6c3b9},
        {x = -14, y = 1, color = 0xe07dff},
        {x = -13, y = 18, color = 0x363449},
        {x = 54, y = 2, color = 0xe579fd},
        {x = 14, y = -90, color = 0xf474ff}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 执棋者_P  -- 顶
    table = {
        {x = 0, y = 0, color = 0xe7c3b9},
        {x = -12, y = 1, color = 0xe07dff},
        {x = -12, y = 16, color = 0x343449},
        {x = 49, y = 1, color = 0xe579fd},
        {x = 12, y = -79, color = 0xf374ff}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 执棋者_P -- 反 底
    table = {
        {x = 0, y = 0, color = 0xe7c3ba},
        {x = 14, y = 1, color = 0xe07dff},
        {x = 14, y = 18, color = 0x353449},
        {x = 66, y = 2, color = 0xfb72ff},
        {x = -14, y = -90, color = 0xf374ff}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 执棋者_P  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xe6c3b9},
        {x = 12, y = 1, color = 0xe07dff},
        {x = 11, y = 16, color = 0x373449},
        {x = 57, y = 2, color = 0xf973ff},
        {x = -12, y = -79, color = 0xf374ff}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-3, Y2W(325) / 1.63)
    set2:shift(-3, Y2W(325) / 1.63)
    set3:shift(6, Y2W(325) / 1.63)
    set4:shift(6, Y2W(325) / 1.63)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 破局者2型
ScanMap.scanEnemiesPojvzhe2 = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-6"}) == false then
            return nil
        end
    end

    -- 破局者2型 -- 底
    local table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = -14, y = 5, color = 0x5bffff},
        {x = -62, y = -48, color = 0x343249},
        {x = -4, y = 53, color = 0xffffff},
        {x = -11, y = 55, color = 0xfbede7}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 破局者2型  -- 顶
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = -10, y = 2, color = 0x6affff},
        {x = -54, y = -43, color = 0x343249},
        {x = -4, y = 46, color = 0xffffff},
        {x = -10, y = 46, color = 0xfbede7}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 破局者2型  -- 反 底
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = 12, y = 3, color = 0x5fffff},
        {x = 60, y = -51, color = 0x343349},
        {x = 2, y = 52, color = 0xfffffe},
        {x = 9, y = 52, color = 0xfbede7}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 破局者2型  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = 12, y = 3, color = 0x65ffff},
        {x = 55, y = -44, color = 0x343449},
        {x = 5, y = 46, color = 0xffffff},
        {x = 11, y = 47, color = 0xfbede7}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-3, Y2W(325) / 1.36)
    set2:shift(-3, Y2W(325) / 1.36)
    set3:shift(5, Y2W(325) / 1.36)
    set4:shift(5, Y2W(325) / 1.36)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 破局者_P
ScanMap.scanEnemiesPojvzhe_P = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-3"}) == false then
            return nil
        end
    end

    -- 破局者_P -- 底
    local table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = -18, y = -2, color = 0xbf5db1},
        {x = -15, y = -61, color = 0x363647},
        {x = -20, y = 25, color = 0xb3b3dc}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 破局者_P  -- 顶
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = -16, y = -1, color = 0xc05db1},
        {x = -13, y = -53, color = 0x363647},
        {x = -18, y = 24, color = 0xb3b3dc}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 破局者_P  -- 反 底
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = 19, y = 0, color = 0xc05db1},
        {x = 15, y = -60, color = 0x363647},
        {x = 20, y = 29, color = 0xb3b3dc}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 破局者_P  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xebc7be},
        {x = 17, y = 0, color = 0xc05eb1},
        {x = 14, y = -51, color = 0x363647},
        {x = 18, y = 26, color = 0xb3b3dc}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-4, Y2W(325) / 1.60)
    set2:shift(-4, Y2W(325) / 1.60)
    set3:shift(8, Y2W(325) / 1.60)
    set4:shift(8, Y2W(325) / 1.60)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 破局者_噩梦结晶
ScanMap.scanEnemiesPojvzhe_NM = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-4"}) == false then
            return nil
        end
    end

    -- 破局者_NM -- 底
    local table = {
        {x = 0, y = 0, color = 0x292931},
        {x = 10, y = 66, color = 0xeb5679},
        {x = 6, y = 121, color = 0xb3b3b3},
        {x = -1, y = 120, color = 0xfbede7},
        {x = 13, y = 24, color = 0xe3e2f7}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 破局者_NM  -- 顶
    table = {
        {x = 0, y = 0, color = 0x292931},
        {x = 7, y = 57, color = 0xeb5679},
        {x = 5, y = 105, color = 0xb3b3b3},
        {x = -1, y = 105, color = 0xfbede7},
        {x = 7, y = 18, color = 0xe2e1f8}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 破局者_NM  -- 反 底
    table = {
        {x = 0, y = 0, color = 0x292931},
        {x = -6, y = 66, color = 0xeb5779},
        {x = -4, y = 121, color = 0xb3b3b3},
        {x = 2, y = 122, color = 0xfbede7},
        {x = -9, y = 22, color = 0xe3e1f7}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 破局者_NM  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0x292931},
        {x = -7, y = 58, color = 0xeb5879},
        {x = -5, y = 105, color = 0xb3b3b3},
        {x = 1, y = 105, color = 0xfbede7},
        {x = -10, y = 19, color = 0xe3e1f7}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(6, Y2W(325) / 0.80)
    set2:shift(6, Y2W(325) / 0.80)
    set3:shift(-2, Y2W(325) / 0.80)
    set4:shift(-2, Y2W(325) / 0.80)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 领洋者II
ScanMap.scanEnemiesLingyang2 = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-1", "15-2", "15-3"}) == false then
            return nil
        end
    end
    -- 领洋者II -- 底
    local table = {
        {x = 0, y = 0, color = 0x323244},
        {x = -52, y = 22, color = 0x7dffff},
        {x = -8, y = 37, color = 0xbabad3},
        {x = 40, y = 65, color = 0x45425b},
        {x = 5, y = 85, color = 0x2e2d49}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 领洋者II -- 顶
    table = {
        {x = 0, y = 0, color = 0x303048},
        {x = -44, y = 17, color = 0x7dffff},
        {x = 34, y = 53, color = 0x454259},
        {x = -7, y = 29, color = 0xc3c0dc},
        {x = 4, y = 71, color = 0x302e49}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 领洋者II -- 反
    table = {
        {x = 0, y = 0, color = 0x303044},
        {x = 45, y = 20, color = 0x7dffff},
        {x = 6, y = 33, color = 0xbbbbd3},
        {x = -4, y = 77, color = 0x302e49},
        {x = 20, y = 0, color = 0x303045}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(0, Y2W(325) / 1.44)
    set2:shift(0, Y2W(325) / 1.44)
    set3:shift(0, Y2W(325) / 1.44)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 领洋者_紫
ScanMap.scanEnemiesLingyang_P = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-3", "15-3"}) == false then
            return nil
        end
    end
    -- 领洋者_P -- 底
    local table = {
        {x = 0, y = 0, color = 0xebc5ba},
        {x = -14, y = 2, color = 0xe486ff},
        {x = -87, y = 55, color = 0xe37eff},
        {x = -47, y = 64, color = 0xb6b7ce},
        {x = 1, y = -39, color = 0xf1f1ff}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 领洋者_P -- 顶
    table = {
        {x = 0, y = 0, color = 0xebc5ba},
        {x = -13, y = 3, color = 0xe790ff},
        {x = -77, y = 49, color = 0xe37eff},
        {x = -41, y = 55, color = 0xbab9d3},
        {x = -3, y = -34, color = 0xf1f1ff}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 领洋者_P -- 反 底
    table = {
        {x = 0, y = 0, color = 0xebc5ba},
        {x = 15, y = 1, color = 0xe489fe},
        {x = 89, y = 55, color = 0xe37eff},
        {x = 49, y = 62, color = 0xbab9d3},
        {x = 1, y = -40, color = 0xf1f1ff}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 领洋者_P  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xebc5ba},
        {x = 13, y = 2, color = 0xe488ff},
        {x = 77, y = 49, color = 0xe37eff},
        {x = 42, y = 56, color = 0xb5b6cd},
        {x = 3, y = -35, color = 0xf1f1ff}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-27, Y2W(325) / 1.06)
    set2:shift(-27, Y2W(325) / 1.06)
    set3:shift(27, Y2W(325) / 1.06)
    set4:shift(27, Y2W(325) / 1.06)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 领洋者_噩梦结晶
ScanMap.scanEnemiesLingyang_NM = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-2", "14-3"}) == false then
            return nil
        end
    end
    -- 领洋者_NM -- 底
    local table = {
        {x = 0, y = 0, color = 0x26262e},
        {x = -19, y = 5, color = 0xfb708c},
        {x = -8, y = 33, color = 0x86868e},
        {x = 36, y = 2, color = 0xfbedeb},
        {x = 50, y = 48, color = 0xfb7084}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 领洋者_NM -- 顶
    table = {
        {x = 0, y = 0, color = 0x26262e},
        {x = -17, y = 4, color = 0xfb708b},
        {x = -7, y = 28, color = 0x86868e},
        {x = 31, y = 1, color = 0xfbedeb},
        {x = 43, y = 41, color = 0xfb7082}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 领洋者_NM -- 反 底
    table = {
        {x = 0, y = 0, color = 0x26262e},
        {x = 19, y = 6, color = 0xfb708b},
        {x = 8, y = 33, color = 0x86868e},
        {x = -36, y = 0, color = 0xfcecec},
        {x = -50, y = 48, color = 0xfb7083}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 领洋者_NM  -- 反 顶
    table = {
        {x = 0, y = 0, color = 0x26262e},
        {x = 14, y = 5, color = 0xfb7191},
        {x = 5, y = 28, color = 0x888790},
        {x = -34, y = 1, color = 0xfbedeb},
        {x = -45, y = 41, color = 0xf97183}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(3, Y2W(325) / 1.45)
    set2:shift(3, Y2W(325) / 1.45)
    set3:shift(-3, Y2W(325) / 1.45)
    set4:shift(-3, Y2W(325) / 1.45)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 追踪者II
ScanMap.scanEnemiesZhuizong2 = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-1"}) == false then
            return nil
        end
    end

    -- 追踪者II -- 底
    local table = {
        {x = 0, y = 0, color = 0xfbedeb},
        {x = -18, y = -44, color = 0x2697c1},
        {x = 18, y = -45, color = 0x2397bb},
        {x = 16, y = -64, color = 0xf5cfc6},
        {x = -7, y = -92, color = 0xefefff},
        {x = 1, y = -34, color = 0xfbebe7}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 追踪者II -- 顶
    table = {
        {x = 0, y = 0, color = 0xfef0f0},
        {x = -15, y = -38, color = 0x2798c2},
        {x = 17, y = -39, color = 0x2795bc},
        {x = -3, y = -81, color = 0xefefff},
        {x = 15, y = -57, color = 0xf6d0c6}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 追踪者II -- 反
    table = {
        {x = 0, y = 0, color = 0xfbede9},
        {x = 12, y = -41, color = 0x2595be},
        {x = -21, y = -41, color = 0x2896bd},
        {x = -19, y = -59, color = 0xf6cec5},
        {x = 1, y = -81, color = 0xefefff},
        {x = 6, y = -74, color = 0xffffff}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-3, Y2W(325) / 3.9)
    set2:shift(-3, Y2W(325) / 3.9)
    set3:shift(0, Y2W(325) / 3.9)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 追踪者_P
ScanMap.scanEnemiesZhuizong_P = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-3"}) == false then
            return nil
        end
    end

    -- 追踪者_P -- 底
    local table = {
        {x = 0, y = 0, color = 0xe5c1b8},
        {x = -21, y = -1, color = 0xbf26b9},
        {x = 16, y = -2, color = 0xbe27b8},
        {x = 2, y = -54, color = 0x252435},
        {x = -6, y = -55, color = 0xebebff}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 追踪者_P -- 顶
    table = {
        {x = 0, y = 0, color = 0xe5c1b8},
        {x = -18, y = -1, color = 0xbe26b9},
        {x = 14, y = -1, color = 0xbe27ba},
        {x = 1, y = -48, color = 0x242334},
        {x = -5, y = -48, color = 0xebebff}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 追踪者_P -- 反 底
    table = {
        {x = 0, y = 0, color = 0xe6c2b9},
        {x = 21, y = 0, color = 0xc026ba},
        {x = -16, y = -1, color = 0xbe27b8},
        {x = -1, y = -54, color = 0x242334},
        {x = 5, y = -53, color = 0xeaeaff}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 追踪者_P -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xe5c1b7},
        {x = 18, y = -1, color = 0xbf26b9},
        {x = -14, y = -1, color = 0xbe27ba},
        {x = -1, y = -49, color = 0x222233},
        {x = 5, y = -48, color = 0xebebff}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-7, Y2W(325) / 1.53)
    set2:shift(-7, Y2W(325) / 1.53)
    set3:shift(7, Y2W(325) / 1.53)
    set4:shift(7, Y2W(325) / 1.53)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 追踪者_噩梦结晶
ScanMap.scanEnemiesZhuizong_NM = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-2", "14-3"}) == false then
            return nil
        end
    end

    -- 追踪者_NM -- 底
    local table = {
        {x = 0, y = 0, color = 0xeb5679},
        {x = 12, y = -1, color = 0xeb7d5b},
        {x = 27, y = -35, color = 0x373740},
        {x = 22, y = -24, color = 0xf7d0c7},
        {x = 36, y = -22, color = 0xebebff}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 追踪者_NM -- 顶
    table = {
        {x = 0, y = 0, color = 0xeb5679},
        {x = 9, y = -1, color = 0xeb7c5c},
        {x = 23, y = -31, color = 0x36363e},
        {x = 18, y = -20, color = 0xf6cfc6},
        {x = 30, y = -18, color = 0xebe9fb}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 追踪者_NM -- 反 底
    table = {
        {x = 0, y = 0, color = 0xeb5779},
        {x = -9, y = -1, color = 0xeb7c5c},
        {x = -23, y = -36, color = 0x36363e},
        {x = -19, y = -22, color = 0xf5cfc6},
        {x = -33, y = -22, color = 0xebebff}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 追踪者_NM -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xeb5879},
        {x = -7, y = 0, color = 0xeb7c5d},
        {x = -20, y = -30, color = 0x373740},
        {x = -16, y = -20, color = 0xf7cfc7},
        {x = -28, y = -18, color = 0xebeaff}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(0, Y2W(325) / 1.51)
    set2:shift(0, Y2W(325) / 1.51)
    set3:shift(5, Y2W(325) / 1.51)
    set4:shift(5, Y2W(325) / 1.51)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 侵扰者_B
ScanMap.scanEnemiesQinrao_B = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-1", "15-2", "15-3"}) == false then
            return nil
        end
    end

    -- 侵扰者_B -- 底
    local table = {
        {x = 0, y = 0, color = 0xebebfc},
        {x = -19, y = 36, color = 0x618fdb},
        {x = 18, y = 35, color = 0x618fdb},
        {x = -27, y = -25, color = 0x69bde7},
        {x = -4, y = 63, color = 0x434059}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 侵扰者_B -- 顶
    table = {
        {x = 0, y = 0, color = 0xebebff},
        {x = -16, y = 33, color = 0x6091d9},
        {x = 16, y = 31, color = 0x618fdb},
        {x = -24, y = -20, color = 0x69bde7},
        {x = -3, y = 55, color = 0x454059}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 侵扰者_B -- 反 底
    table = {
        {x = 0, y = 0, color = 0xebebfe},
        {x = 20, y = 36, color = 0x618eda},
        {x = -16, y = 36, color = 0x618fdb},
        {x = 27, y = -24, color = 0x69bde7},
        {x = 7, y = 64, color = 0x424058}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 侵扰者_B -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xebebfc},
        {x = 15, y = 33, color = 0x618fdb},
        {x = -17, y = 31, color = 0x618fdb},
        {x = 24, y = -20, color = 0x69bde7},
        {x = 2, y = 56, color = 0x414058}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-2, Y2W(325) / 1.03)
    set2:shift(-2, Y2W(325) / 1.03)
    set3:shift(3, Y2W(325) / 1.03)
    set4:shift(3, Y2W(325) / 1.03)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 侵扰者_P
ScanMap.scanEnemiesQinrao_P = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-3"}) == false then
            return nil
        end
    end

    -- 侵扰者_P -- 底
    local table = {
        {x = 0, y = 0, color = 0xfbede7},
        {x = -14, y = -7, color = 0xdf73f7},
        {x = 13, y = -8, color = 0xe075f9},
        {x = -23, y = -67, color = 0xe07afe},
        {x = -5, y = 12, color = 0x444059}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 侵扰者_P -- 顶
    table = {
        {x = 0, y = 0, color = 0xfbede7},
        {x = -12, y = -5, color = 0xdf73f7},
        {x = 12, y = -7, color = 0xe979f7},
        {x = -21, y = -57, color = 0xe07aff},
        {x = -4, y = 12, color = 0x454059}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 侵扰者_P -- 反 底
    table = {
        {x = 0, y = 0, color = 0xfbede7},
        {x = 14, y = -8, color = 0xdf73f7},
        {x = -14, y = -9, color = 0xee7af6},
        {x = 21, y = -67, color = 0xe07aff},
        {x = 6, y = 13, color = 0x454059}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 侵扰者_P -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xfbede7},
        {x = 11, y = -7, color = 0xe579fb},
        {x = -13, y = -8, color = 0xe075f9},
        {x = 20, y = -58, color = 0xe07afe},
        {x = 3, y = 11, color = 0x454059}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-3, Y2W(325) / 2.04)
    set2:shift(-3, Y2W(325) / 2.04)
    set3:shift(4, Y2W(325) / 2.04)
    set4:shift(4, Y2W(325) / 2.04)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 潜伏者I
ScanMap.scanEnemiesQianfu1 = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-1", "15-2", "15-3"}) == false then
            return nil
        end
    end

    -- 潜伏者I -- 底
    local table = {
        {x = 0, y = 0, color = 0xd0a99e},
        {x = -17, y = -4, color = 0xbe722c},
        {x = -15, y = 2, color = 0xfaff9b},
        {x = -8, y = 29, color = 0xb7b7c9},
        {x = -11, y = 37, color = 0xfbedeb}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 潜伏者I -- 顶
    table = {
        {x = 0, y = 0, color = 0xd2aa9f},
        {x = -15, y = -4, color = 0xbe722c},
        {x = -13, y = 1, color = 0xfaff9d},
        {x = -6, y = 25, color = 0xb2b2c2},
        {x = -8, y = 30, color = 0xfbedeb}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-5, Y2W(325) / 1.45)
    set2:shift(-5, Y2W(325) / 1.45)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 潜伏者II
ScanMap.scanEnemiesQianfu2 = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-1", "14-6"}) == false then
            return nil
        end
    end

    -- 潜伏者II -- 底
    local table = {
        {x = 0, y = 0, color = 0xfbedeb},
        {x = 24, y = 19, color = 0x3d3b52},
        {x = -71, y = 58, color = 0x2a2a42},
        {x = 12, y = -64, color = 0xefefff},
        {x = -15, y = -50, color = 0xddb8ad}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 潜伏者II -- 顶
    table = {
        {x = 0, y = 0, color = 0xfbedeb},
        {x = 22, y = 15, color = 0x3d3b52},
        {x = -62, y = 51, color = 0x2b2840},
        {x = 10, y = -57, color = 0xefefff},
        {x = -14, y = -45, color = 0xdfb9ae}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 潜伏者II -- 反 底
    table = {
        {x = 0, y = 0, color = 0xfbedeb},
        {x = -24, y = 19, color = 0x3c3c54},
        {x = 72, y = 55, color = 0x2b2a42},
        {x = -22, y = -65, color = 0xefefff},
        {x = 12, y = -53, color = 0xdebab0}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 潜伏者II -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xfbedeb},
        {x = -21, y = 17, color = 0x3c3b53},
        {x = 62, y = 49, color = 0x262540},
        {x = -18, y = -56, color = 0xefefff},
        {x = 10, y = -45, color = 0xdcb8ae}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(3, Y2W(325) / 2.52)
    set2:shift(3, Y2W(325) / 2.52)
    set3:shift(-3, Y2W(325) / 2.52)
    set4:shift(-3, Y2W(325) / 2.52)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- 潜伏者_噩梦结晶
ScanMap.scanEnemiesQianfu_NM = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-4"}) == false then
            return nil
        end
    end

    -- 潜伏者_NM -- 底
    local table = {
        {x = 0, y = 0, color = 0xeb5679},
        {x = 17, y = 63, color = 0x242128},
        {x = -56, y = 88, color = 0x3c3c45},
        {x = -8, y = 36, color = 0xfbedeb},
        {x = -17, y = -31, color = 0xe6c0b5}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 潜伏者_NM -- 顶
    table = {
        {x = 0, y = 0, color = 0xeb5679},
        {x = 12, y = 55, color = 0x212127},
        {x = -52, y = 76, color = 0x3c3c45},
        {x = -10, y = 31, color = 0xfbedeb},
        {x = -16, y = -27, color = 0xe7c1b6}
    }
    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 潜伏者_NM -- 反 底
    table = {
        {x = 0, y = 0, color = 0xeb5679},
        {x = -12, y = 62, color = 0x1f1f25},
        {x = 58, y = 87, color = 0x3d3e47},
        {x = 10, y = 34, color = 0xfbedeb},
        {x = 19, y = -30, color = 0xe7c1b6}
    }
    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 潜伏者_NM -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xeb5679},
        {x = -14, y = 54, color = 0x202128},
        {x = 48, y = 77, color = 0x3e3e47},
        {x = 8, y = 30, color = 0xfbedeb},
        {x = 18, y = -25, color = 0xe7c1b6}
    }
    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(-3, Y2W(325) / 1.46)
    set2:shift(-3, Y2W(325) / 1.46)
    set3:shift(5, Y2W(325) / 1.46)
    set4:shift(5, Y2W(325) / 1.46)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- ScanMap.scanEnemiesSheffield = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-1", "14-2", "14-3"}) == false then
--             return nil
--         end
--     end

--     -- 谢菲尔德 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbf7},
--         {x = -9, y = 22, color = 0x45424d},
--         {x = 13, y = -26, color = 0xfffbf7},
--         {x = -2, y = -37, color = 0xd3c7b6},
--         {x = 13, y = -1, color = 0xcb7632}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 谢菲尔德 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbf7},
--         {x = -8, y = 19, color = 0x45424d},
--         {x = 11, y = -23, color = 0xfffbf7},
--         {x = -2, y = -33, color = 0xd3c7b6},
--         {x = 11, y = -2, color = 0xc87532}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 谢菲尔德 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbf7},
--         {x = 10, y = 21, color = 0x45424d},
--         {x = -11, y = -27, color = 0xfffbf7},
--         {x = 3, y = -38, color = 0xd3c7b6},
--         {x = -11, y = -2, color = 0xcb7632}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 谢菲尔德 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbf7},
--         {x = 8, y = 19, color = 0x45424d},
--         {x = -10, y = -23, color = 0xfffbf7},
--         {x = 3, y = -33, color = 0xd3c7b6},
--         {x = -10, y = -1, color = 0xc7722e}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-2, Y2W(325) / 2.07)
--     set2:shift(-2, Y2W(325) / 2.07)
--     set3:shift(5, Y2W(325) / 2.07)
--     set4:shift(5, Y2W(325) / 2.07)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesNurnberg = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"15-1"}) == false then
--             return nil
--         end
--     end

--     -- 纽伦堡 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfff7f3},
--         {x = -13, y = -3, color = 0x530d22},
--         {x = 12, y = -4, color = 0x520e1f},
--         {x = -4, y = -41, color = 0xc4c4c4},
--         {x = -2, y = 23, color = 0x898089}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 纽伦堡 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfff7f3},
--         {x = -12, y = -2, color = 0x520d22},
--         {x = 10, y = -3, color = 0x510c24},
--         {x = -4, y = -36, color = 0xc3c3c3},
--         {x = -2, y = 20, color = 0x898089}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 纽伦堡 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfff7f3},
--         {x = 13, y = -2, color = 0x520c20},
--         {x = -12, y = -3, color = 0x510c24},
--         {x = 4, y = -40, color = 0xc1c1c1},
--         {x = 2, y = 24, color = 0x898089}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 纽伦堡 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfff7f3},
--         {x = 12, y = -1, color = 0x510c22},
--         {x = -10, y = -3, color = 0x520e1f},
--         {x = 4, y = -35, color = 0xc2c2c2},
--         {x = 2, y = 22, color = 0x898089}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 2.02)
--     set2:shift(-1, Y2W(325) / 2.02)
--     set3:shift(6, Y2W(325) / 2.02)
--     set4:shift(6, Y2W(325) / 2.02)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesZ24 = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"15-1", "15-2"}) == false then
--             return nil
--         end
--     end

--     -- z24 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbef},
--         {x = -12, y = -1, color = 0xffffcc},
--         {x = -2, y = -48, color = 0x302b2a},
--         {x = -12, y = 18, color = 0x353535},
--         {x = 16, y = -56, color = 0xa20404}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- z24 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbef},
--         {x = -11, y = 0, color = 0xffffcc},
--         {x = -1, y = -42, color = 0x2e2a2a},
--         {x = -11, y = 18, color = 0x363636},
--         {x = 14, y = -48, color = 0xa20404}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- z24 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbef},
--         {x = 12, y = -1, color = 0xffffcc},
--         {x = 2, y = -48, color = 0x302b2a},
--         {x = 14, y = 21, color = 0x373334},
--         {x = 32, y = -58, color = 0xa20404}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- z24 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbef},
--         {x = 9, y = 0, color = 0xffffcc},
--         {x = 1, y = -41, color = 0x302b2a},
--         {x = 11, y = 19, color = 0x363435},
--         {x = 28, y = -50, color = 0xa20404}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 2.29)
--     set2:shift(-1, Y2W(325) / 2.29)
--     set3:shift(6, Y2W(325) / 2.29)
--     set4:shift(6, Y2W(325) / 2.29)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesYamashiro = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-3", "15-2"}) == false then
--             return nil
--         end
--     end

--     -- 山城 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -13, y = -4, color = 0x4b2a4b},
--         {x = -25, y = -42, color = 0xd92046},
--         {x = 26, y = -38, color = 0xe0afb7}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 山城 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -11, y = -4, color = 0x4a294a},
--         {x = -22, y = -37, color = 0xdb2145},
--         {x = 22, y = -33, color = 0xdfaeb6}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 山城 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 14, y = -4, color = 0x4b2a4b},
--         {x = 26, y = -42, color = 0xd71f47},
--         {x = -25, y = -37, color = 0xe0afb7}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 山城 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 11, y = -4, color = 0x4a284a},
--         {x = 22, y = -37, color = 0xd81e47},
--         {x = -22, y = -33, color = 0xe0afb7}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 2.07)
--     set2:shift(-1, Y2W(325) / 2.07)
--     set3:shift(6, Y2W(325) / 2.07)
--     set4:shift(6, Y2W(325) / 2.07)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesShokaku = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-3"}) == false then
--             return nil
--         end
--     end

--     -- 翔鹤 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -13, y = 1, color = 0x44dddd},
--         {x = -33, y = -39, color = 0xf97565},
--         {x = -22, y = 15, color = 0xc4b3c4}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 翔鹤 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -12, y = 0, color = 0x44dcdc},
--         {x = -30, y = -34, color = 0xf97565},
--         {x = -20, y = 12, color = 0xc2b2c2}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 翔鹤 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -11, y = -1, color = 0x4ee7e7},
--         {x = 36, y = -40, color = 0xfc7564},
--         {x = 23, y = 14, color = 0xc4b3c4}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 翔鹤 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -9, y = 0, color = 0x3ed7d7},
--         {x = 31, y = -34, color = 0xfe766d},
--         {x = 20, y = 13, color = 0xc4b3c4}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 1.95)
--     set2:shift(-1, Y2W(325) / 1.95)
--     set3:shift(6, Y2W(325) / 1.95)
--     set4:shift(6, Y2W(325) / 1.95)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesZuikaku = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-3"}) == false then
--             return nil
--         end
--     end

--     -- 瑞鹤 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -12, y = 3, color = 0xe1c082},
--         {x = -3, y = 34, color = 0xcb3a38},
--         {x = -4, y = -41, color = 0x784f51}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 瑞鹤 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -10, y = 2, color = 0xe1c084},
--         {x = -1, y = 31, color = 0xcb3a38},
--         {x = -4, y = -37, color = 0x795051}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 瑞鹤 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 13, y = 2, color = 0xe1c083},
--         {x = 3, y = 36, color = 0xcb3a38},
--         {x = 6, y = -42, color = 0x754f51}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 瑞鹤 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 11, y = 2, color = 0xe1c086},
--         {x = 1, y = 31, color = 0xcb3a38},
--         {x = 7, y = -37, color = 0x784f50}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 1.93)
--     set2:shift(-1, Y2W(325) / 1.93)
--     set3:shift(6, Y2W(325) / 1.93)
--     set4:shift(6, Y2W(325) / 1.93)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesKongo = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-3", "15-2"}) == false then
--             return nil
--         end
--     end

--     -- 金刚 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -12, y = 2, color = 0xb0dfae},
--         {x = -14, y = 15, color = 0xa66641},
--         {x = -3, y = 30, color = 0x3c4055},
--         {x = -13, y = -26, color = 0xfdf5d5}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 金刚 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -10, y = 1, color = 0xafe2af},
--         {x = -11, y = 13, color = 0xa66641},
--         {x = -2, y = 27, color = 0x3c4055},
--         {x = -9, y = -23, color = 0xfdf5d4}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 金刚 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 12, y = 1, color = 0xb0e0ae},
--         {x = 13, y = 14, color = 0xa66641},
--         {x = 3, y = 31, color = 0x3c4055},
--         {x = 12, y = -26, color = 0xfdf5d4}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 金刚 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 10, y = 1, color = 0xb1dead},
--         {x = 11, y = 12, color = 0xa66641},
--         {x = 2, y = 26, color = 0x3c4055},
--         {x = 11, y = -23, color = 0xfdf5d4}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-3, Y2W(325) / 2.07)
--     set2:shift(-3, Y2W(325) / 2.07)
--     set3:shift(4, Y2W(325) / 2.07)
--     set4:shift(4, Y2W(325) / 2.07)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesArkRoyal = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-2"}) == false then
--             return nil
--         end
--     end

--     -- 皇家方舟 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfff1d3},
--         {x = 0, y = 29, color = 0xaa4455},
--         {x = -2, y = -42, color = 0x453841},
--         {x = 13, y = 23, color = 0x6f6f6f},
--         {x = 2, y = -25, color = 0x826f6f}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 皇家方舟 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfff1d3},
--         {x = 0, y = 25, color = 0xaa4455},
--         {x = -3, y = -37, color = 0x453841},
--         {x = 12, y = 20, color = 0x707070},
--         {x = 2, y = -21, color = 0x826e71}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 皇家方舟 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfff1d3},
--         {x = 0, y = 29, color = 0xaa4455},
--         {x = 2, y = -38, color = 0x453841},
--         {x = -13, y = 23, color = 0x6f6f6f},
--         {x = -4, y = -24, color = 0x826f71}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 皇家方舟 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfff1d3},
--         {x = 0, y = 26, color = 0xaa4455},
--         {x = 3, y = -35, color = 0x453841},
--         {x = -11, y = 20, color = 0x6f6f6f},
--         {x = -2, y = -21, color = 0x827071}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 1.95)
--     set2:shift(-1, Y2W(325) / 1.95)
--     set3:shift(4, Y2W(325) / 1.95)
--     set4:shift(4, Y2W(325) / 1.95)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesRodney = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-2"}) == false then
--             return nil
--         end
--     end

--     -- 罗德尼 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -11, y = -1, color = 0x4d4269},
--         {x = 13, y = -1, color = 0x4c4465},
--         {x = -4, y = -25, color = 0xffffff},
--         {x = -6, y = -37, color = 0xeff1ff}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 罗德尼 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -9, y = -1, color = 0x4d4269},
--         {x = 12, y = -1, color = 0x4c4465},
--         {x = -4, y = -22, color = 0xffffff},
--         {x = -5, y = -32, color = 0xeff1ff}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 罗德尼 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 12, y = -1, color = 0x4d4269},
--         {x = -12, y = -1, color = 0x4c4465},
--         {x = 4, y = -25, color = 0xffffff},
--         {x = 6, y = -36, color = 0xeff1ff}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 罗德尼 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 10, y = -2, color = 0x4d4269},
--         {x = -11, y = -2, color = 0x484269},
--         {x = 4, y = -22, color = 0xffffff},
--         {x = 5, y = -32, color = 0xeff1ff}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 2.13)
--     set2:shift(-1, Y2W(325) / 2.13)
--     set3:shift(5, Y2W(325) / 2.13)
--     set4:shift(5, Y2W(325) / 2.13)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesDorsetshire = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-2"}) == false then
--             return nil
--         end
--     end

--     -- 多塞特郡 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -13, y = 0, color = 0x9e96fe},
--         {x = -7, y = -17, color = 0xfffbeb},
--         {x = 7, y = -22, color = 0xfbe1cf},
--         {x = -19, y = -55, color = 0x59484d}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 多塞特郡 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = -11, y = 1, color = 0xa095fa},
--         {x = -6, y = -14, color = 0xfffbeb},
--         {x = 6, y = -19, color = 0xfbe1cf},
--         {x = -17, y = -47, color = 0x59484c}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 多塞特郡 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 13, y = 1, color = 0x9d91fa},
--         {x = 7, y = -17, color = 0xfffbeb},
--         {x = -7, y = -22, color = 0xfbe1cf},
--         {x = 20, y = -54, color = 0x5a494c}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 多塞特郡 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfffbeb},
--         {x = 11, y = 1, color = 0x9990fa},
--         {x = 5, y = -14, color = 0xfffbeb},
--         {x = -6, y = -19, color = 0xfbe1cf},
--         {x = 17, y = -47, color = 0x59484d}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 2.11)
--     set2:shift(-1, Y2W(325) / 2.11)
--     set3:shift(5, Y2W(325) / 2.11)
--     set4:shift(5, Y2W(325) / 2.11)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesRenown = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-2", "14-3"}) == false then
--             return nil
--         end
--     end

--     -- 声望 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xffeedd},
--         {x = -12, y = -3, color = 0x24428a},
--         {x = -2, y = -29, color = 0xffffff},
--         {x = -27, y = -43, color = 0x5b5b58},
--         {x = -37, y = -34, color = 0xe5bb9a}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 声望 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xffeedd},
--         {x = -11, y = -3, color = 0x24428a},
--         {x = 0, y = -26, color = 0xffffff},
--         {x = -23, y = -38, color = 0x5c5c54},
--         {x = -31, y = -30, color = 0xe5bb9a}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 声望 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xffeedd},
--         {x = 13, y = -4, color = 0x24428a},
--         {x = 1, y = -30, color = 0xffffff},
--         {x = 28, y = -45, color = 0x595951},
--         {x = 37, y = -36, color = 0xe5bb9a}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 声望 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xffeedd},
--         {x = 10, y = -3, color = 0x24438a},
--         {x = 0, y = -26, color = 0xffffff},
--         {x = 23, y = -39, color = 0x5b5b52},
--         {x = 31, y = -31, color = 0xe5bb9a}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-2, Y2W(325) / 2.0)
--     set2:shift(-2, Y2W(325) / 2.0)
--     set3:shift(4, Y2W(325) / 2.0)
--     set4:shift(4, Y2W(325) / 2.0)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesWales = function()
--     if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--         if IsWorkingOn({"14-3"}) == false then
--             return nil
--         end
--     end

--     -- 威尔士 -- 底
--     local table = {
--         {x = 0, y = 0, color = 0xfff1e3},
--         {x = -11, y = -4, color = 0x2a0000},
--         {x = -12, y = 8, color = 0xfcc9a7},
--         {x = -12, y = -25, color = 0xfff1e3},
--         {x = -17, y = -30, color = 0xfffde2}
--     }
--     local set = PosSet:new()
--     local set1 = PosSet:new()
--     local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set1, points)

--     -- 威尔士 -- 顶
--     table = {
--         {x = 0, y = 0, color = 0xfff1e3},
--         {x = -11, y = -3, color = 0x2a0000},
--         {x = -12, y = 7, color = 0xfcc9a7},
--         {x = -12, y = -21, color = 0xfff1e3},
--         {x = -16, y = -26, color = 0xfefce4}
--     }

--     local set2 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set2, points)

--     -- 威尔士 -- 反 底
--     table = {
--         {x = 0, y = 0, color = 0xfff1e3},
--         {x = 12, y = -4, color = 0x2a0000},
--         {x = 11, y = 8, color = 0xfcc9a7},
--         {x = 12, y = -25, color = 0xfff1e3},
--         {x = 16, y = -30, color = 0xfefde4}
--     }

--     local set3 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set3, points)

--     -- 威尔士 -- 反 顶
--     table = {
--         {x = 0, y = 0, color = 0xfff1e3},
--         {x = 10, y = -4, color = 0x2a0000},
--         {x = 10, y = 7, color = 0xffccaa},
--         {x = 12, y = -22, color = 0xfff1e3},
--         {x = 16, y = -27, color = 0xfffce1}
--     }

--     local set4 = PosSet:new()
--     points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--     addToSet(set4, points)

--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_raw then
--         set:draw(Color.PURPLE)
--     end
--     set1:shift(-1, Y2W(325) / 2.0)
--     set2:shift(-1, Y2W(325) / 2.0)
--     set3:shift(4, Y2W(325) / 2.0)
--     set4:shift(4, Y2W(325) / 2.0)
--     set = PosSet:new()
--     set:merge(set1)
--     set:merge(set2)
--     set:merge(set3)
--     set:merge(set4)
--     if SETTINGS.debug_shift then
--         set:draw(Color.PURPLE)
--     end
--     return set
-- end

-- ScanMap.scanEnemiesHipper = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2','14-3','14-5'}) == false then
--       return nil
--     end
--   end

--   -- 希佩尔 -- 底
--   local table = {
--   {x=0,y=0,color=0xffffff},
--   {x=-24,y=-8,color=0x240000},
--   {x=8,y=63,color=0x524343},
--   {x=0,y=-17,color=0xfad78d},
--   {x=-4,y=24,color=0x425832}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 希佩尔  -- 顶
--   table = {
--     {x=0,y=0,color=0xffffff},
--   {x=-22,y=-7,color=0x240000},
--   {x=6,y=54,color=0x514343},
--   {x=1,y=-14,color=0xf9d88e},
--   {x=-3,y=21,color=0x445829}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 希佩尔  -- 反 底
--   table = {
--   {x=0,y=0,color=0xffffff},
--   {x=34,y=-8,color=0x240000},
--   {x=9,y=63,color=0x514343},
--   {x=8,y=-15,color=0xf9d88e},
--   {x=15,y=24,color=0x415a31}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 希佩尔  -- 反 顶
--   table = {
--   {x=0,y=0,color=0xffffff},
--   {x=30,y=-6,color=0x240000},
--   {x=8,y=56,color=0x514244},
--   {x=9,y=-13,color=0xfad88e},
--   {x=9,y=47,color=0xfffffb}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(2, Y2W(325)/1.38)
--   set2:shift(2, Y2W(325)/1.38)
--   set3:shift(5, Y2W(325)/1.38)
--   set4:shift(5, Y2W(325)/1.38)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesCleveland = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-2','14-3','14-4','14-5'}) == false then
--       return nil
--     end
--   end

--   -- 克利夫兰 -- 底
--   local table = {
--   {x=0,y=0,color=0xfffbeb},
--   {x=2,y=-22,color=0xdea780},
--   {x=-12,y=20,color=0x69285c},
--   {x=-9,y=-14,color=0xfbd5a6},
--   {x=-3,y=55,color=0xfdfdfd}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 克利夫兰  -- 顶
--   table = {
--   {x=0,y=0,color=0xfffbeb},
--   {x=2,y=-18,color=0xdea680},
--   {x=-11,y=17,color=0x69285c},
--   {x=-9,y=-15,color=0xfbd5a6},
--   {x=-4,y=47,color=0xf8f8f8}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 克利夫兰  -- 反 底
--   table = {
--   {x=0,y=0,color=0xfffbeb},
--   {x=-2,y=-21,color=0xdba47d},
--   {x=13,y=20,color=0x69285c},
--   {x=4,y=55,color=0xfefefe},
--   {x=10,y=-17,color=0xfbd5a6}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 克利夫兰  -- 反 顶
--   table = {
--   {x=0,y=0,color=0xfffbeb},
--   {x=-1,y=-19,color=0xdba47e},
--   {x=9,y=17,color=0x69285b},
--   {x=1,y=46,color=0xf7f7f7},
--   {x=7,y=-12,color=0xfbd5a6}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.36)
--   set2:shift(0, Y2W(325)/1.36)
--   set3:shift(2, Y2W(325)/1.36)
--   set4:shift(2, Y2W(325)/1.36)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

-- ScanMap.scanEnemiesGascogne = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-3','14-4'}) == false then
--       return nil
--     end
--   end

--   -- 加斯科涅 -- 底
--   local table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=-4,y=-38,color=0x7a839b},
--   {x=-12,y=0,color=0x865d2c},
--   {x=-7,y=32,color=0xefe6ed},
--   {x=4,y=40,color=0x514343}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 加斯科涅   -- 顶
--   table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=-1,y=-30,color=0x868da7},
--   {x=-5,y=27,color=0xefe7ef},
--   {x=3,y=35,color=0x514342},
--   {x=-11,y=-1,color=0x865d2c}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 加斯科涅   -- 反 底
--   table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=4,y=-37,color=0x7a849b},
--   {x=13,y=0,color=0x865d2c},
--   {x=7,y=32,color=0xefe7ef},
--   {x=-3,y=42,color=0x4e4040}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 加斯科涅   -- 反 顶
--   table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=7,y=-33,color=0x7b839c},
--   {x=11,y=1,color=0x865d2c},
--   {x=5,y=28,color=0xefe7ef},
--   {x=-3,y=35,color=0x514344}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.86)
--   set2:shift(0, Y2W(325)/1.86)
--   set3:shift(0, Y2W(325)/1.86)
--   set4:shift(0, Y2W(325)/1.86)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

ScanMap.scanEnemiesSpee = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-2"}) == false then
            return nil
        end
    end

    -- 斯佩伯爵 -- 底
    local table = {
        {x = 0, y = 0, color = 0xfffbeb},
        {x = -11, y = 4, color = 0x98e6f6},
        {x = 30, y = -23, color = 0xff3f37},
        {x = -19, y = -48, color = 0xc7c7cb},
        {x = 3, y = -41, color = 0xededed}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 斯佩伯爵  -- 顶
    table = {
        {x = 0, y = 0, color = 0xfffbeb},
        {x = -11, y = 3, color = 0x9ae5f5},
        {x = 26, y = -18, color = 0xff3e38},
        {x = -18, y = -42, color = 0xc7c7cb},
        {x = 1, y = -33, color = 0xededed}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    -- 斯佩伯爵   -- 反 底
    table = {
        {x = 0, y = 0, color = 0xfffbeb},
        {x = 13, y = 4, color = 0x9ae5f5},
        {x = -30, y = -22, color = 0xff3e38},
        {x = 23, y = -46, color = 0xc7c7cb},
        {x = -1, y = -40, color = 0xededed}
    }

    local set3 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set3, points)

    -- 斯佩伯爵   -- 反 顶
    table = {
        {x = 0, y = 0, color = 0xfffbeb},
        {x = 12, y = 4, color = 0x9ae5f5},
        {x = -26, y = -18, color = 0xff3e38},
        {x = 21, y = -40, color = 0xc7c7cb},
        {x = 0, y = -34, color = 0xededed}
    }

    local set4 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set4, points)

    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(0, Y2W(325) / 1.73)
    set2:shift(0, Y2W(325) / 1.73)
    set3:shift(5, Y2W(325) / 1.73)
    set4:shift(5, Y2W(325) / 1.73)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    set:merge(set3)
    set:merge(set4)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end

-- ScanMap.scanEnemiesAkagi = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'14-5'}) == false then
--       return nil
--     end
--   end

--   -- 赤城 -- 底
--   local table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=-35,y=-34,color=0x342024},
--   {x=25,y=-51,color=0xa26665},
--   {x=2,y=41,color=0x342828},
--   {x=-14,y=-1,color=0x771111}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- 赤城   -- 顶
--   table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=-32,y=-28,color=0x342024},
--   {x=19,y=-45,color=0xa26665},
--   {x=2,y=36,color=0x342828},
--   {x=-14,y=0,color=0x7a0c10}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   -- 赤城   -- 反 底
--   table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=37,y=-33,color=0x342024},
--   {x=-22,y=-53,color=0xa26665},
--   {x=-2,y=41,color=0x342828},
--   {x=16,y=0,color=0x780f11}
--   }

--   local set3 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set3,  points)

--   -- 赤城   -- 反 顶
--   table = {
--   {x=0,y=0,color=0xfffbef},
--   {x=32,y=-29,color=0x342124},
--   {x=-21,y=-44,color=0xa26665},
--   {x=-1,y=35,color=0x342828},
--   {x=14,y=-1,color=0x790e10}
--   }

--   local set4 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set4,  points)

--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--   set1:shift(0, Y2W(325)/1.97)
--   set2:shift(0, Y2W(325)/1.97)
--   set3:shift(5, Y2W(325)/1.97)
--   set4:shift(5, Y2W(325)/1.97)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)
--   set:merge(set3)
--   set:merge(set4)
--   if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--   return set
-- end

--ScanMap.scanEnemiesAmazon = function ()
--  if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--    if IsWorkingOn({'14-1','14-3'}) == false then
--      return nil
--    end
--  end
--
--  -- 女将 -- 底
--  local table = {
--    {x=0,y=0,color=0xfff9df},
--    {x=0,y=16,color=0xffe9bf},
--    {x=-13,y=23,color=0x2a3967},
--    {x=-1,y=38,color=0xffc8ba},
--    {x=14,y=32,color=0xfdfdec}}
--
--  local set = PosSet:new()
--  local set1 = PosSet:new()
--  local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--  addToSet(set1, points)
--
--  -- 女将 -- 顶
--  table = {
--    {x=0,y=0,color=0xfff9df},
--    {x=2,y=20,color=0xffe9bf},
--    {x=-11,y=27,color=0x2a3967},
--    {x=0,y=39,color=0xffc9ba},
--    {x=7,y=38,color=0xfffcec}
--  }
--
--  local set2 = PosSet:new()
--  points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--  addToSet(set2,  points)
--
--  -- 女将 -- 反
--  table = {
--    {x=0,y=0,color=0xfff9df},
--    {x=0,y=20,color=0xffe9bf},
--    {x=14,y=29,color=0x2b3a67},
--    {x=2,y=43,color=0xffc9ba},
--    {x=-12,y=38,color=0xfcfceb}
--  }
--
--  local set3 = PosSet:new()
--  points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--  addToSet(set3,  points)
--
--  set:merge(set1)
--  set:merge(set2)
--  set:merge(set3)
--  if SETTINGS.debug_raw then set:draw(Color.PURPLE) end
--  set1:shift(-3, Y2W(325)/1.29)
--  set2:shift(-2, Y2W(325)/1.29)
--  set3:shift(5, Y2W(325)/1.29)
--  set = PosSet:new()
--  set:merge(set1)
--  set:merge(set2)
--  set:merge(set3)
--  if SETTINGS.debug_shift then set:draw(Color.PURPLE) end
--  return set
--end

ScanMap.scanEnemiesAC = function()
    -- 神圣的悲喜剧 C2飞机

    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOnEventLevel_Part1() == false then
            return nil
        end
    end

    local table = {
        {x = 0, y = 0, color = 0x5c5c5c},
        {x = 9, y = -5, color = 0x866b6e},
        {x = 34, y = 12, color = 0xd6d6b5},
        {x = 44, y = -9, color = 0x9fa286},
        {x = -23, y = 11, color = 0xdedebc},
        {x = -3, y = 21, color = 0x346db7}
    }

    local set = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    scale(table, 0.9)
    points = myFindColors({105, 49, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    if SETTINGS.debug_raw then
        set:draw(Color.CYAN)
    end
    set:shift(3, 10)
    if SETTINGS.debug_shift then
        set:draw(Color.CYAN)
    end

    return set
end

ScanMap.scanEnemiesTrento = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOnEventLevel_Part2() == false then
            return nil
        end
    end

    -- 特伦托
    local table = {
        {x = 0, y = 0, color = 0x9ea7e0},
        {x = 12, y = 28, color = 0xc85c59},
        {x = -12, y = 27, color = 0xa67e9c},
        {x = 12, y = 41, color = 0x625996},
        {x = -13, y = 41, color = 0x635a97},
        {x = 1, y = 63, color = 0x252524}
    }

    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    scale(table, 1.1)
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 特伦托_反
    table = {
        {x = 0, y = 0, color = 0x9ea7e0},
        {x = 0, y = -6, color = 0xbac1ef},
        {x = -10, y = 14, color = 0xc45a56},
        {x = 12, y = 13, color = 0xaa809b},
        {x = -7, y = 28, color = 0x615896},
        {x = -6, y = -4, color = 0xfffbef}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    scale(table, 1.1)
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(0, Y2W(325) / 1.375)
    set2:shift(5, Y2W(325) / 1.7)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end

    return set
end

ScanMap.scanEnemiesCarabiniere = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"15-1", "15-2"}) == false then
            return nil
        end
    end

    -- 龙骑兵
    local table = {
        {x = 0, y = 0, color = 0x495a6d},
        {x = 15, y = 13, color = 0xebab8e},
        {x = -14, y = -25, color = 0x495a6d},
        {x = 13, y = 18, color = 0xf3e5be},
        {x = 23, y = 16, color = 0xfffbef}
    }

    local set = PosSet:new()
    local set1 = PosSet:new()

    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    scale(table, 1.1)
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 龙骑兵_反
    table = {
        {x = 0, y = 0, color = 0x495a6d},
        {x = -17, y = 13, color = 0xebab8e},
        {x = -16, y = 19, color = 0xf3e4be},
        {x = -23, y = 17, color = 0xfffbef},
        {x = -4, y = 48, color = 0x405061}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    scale(table, 1.1)
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(5, Y2W(326) / 1.39)
    set2:shift(-5, Y2W(326) / 1.39)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end

    return set
end

ScanMap.scanEnemiesLittorio = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOnEventLevel_Part2() == false or SETTINGS.level < 2 then
            return nil
        end
    end
    -- 利托里奥
    local table = {
        {x = 0, y = 0, color = 0x89a991},
        {x = -11, y = -6, color = 0x89aa91},
        {x = -29, y = 9, color = 0xb0c79e},
        {x = -4, y = 14, color = 0xfffbef},
        {x = 5, y = 74, color = 0xfbfbfb},
        {x = -3, y = 78, color = 0xf4f4f4}
    }

    local set = PosSet:new()
    local set1 = PosSet:new()

    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    table = {
        {x = 0, y = 0, color = 0x89aa91},
        {x = -11, y = -6, color = 0x89aa91},
        {x = -27, y = 9, color = 0xb2c89f},
        {x = -4, y = 14, color = 0xfffbef},
        {x = 5, y = 70, color = 0xfbfbfb},
        {x = 14, y = 17, color = 0xdbe7b6}
    }

    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 利托里奥_反
    table = {
        {x = 0, y = 0, color = 0x89aa91},
        {x = -15, y = 17, color = 0xdbe7b6},
        {x = -1, y = 15, color = 0xfffbef},
        {x = -16, y = 37, color = 0xda6352},
        {x = 9, y = 33, color = 0x7c3a52},
        {x = -6, y = 67, color = 0xfbfbfb}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    scale(table, 1.1)
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    set1:shift(0, Y2W(326) / 1.25)
    set2:shift(0, Y2W(326) / 1.29)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end

    return set
end

-- -- BOSS白上吹雪
-- ScanMap.scanBossFubuki = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'15-3'}) == false then
--       return nil
--     end
--   end

--   -- BOSS白上吹雪 -- 底
--   local table = {
--     {x=0,y=0,color=0xfff7eb},
--     {x=-19,y=-5,color=0x609393},
--     {x=-18,y=26,color=0x1e1526},
--     {x=4,y=-19,color=0xefeff7},
--     {x=22,y=3,color=0xb9eca8}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- BOSS白上吹雪 -- 顶
--   table = {
--     {x=0,y=0,color=0xfff7eb},
--     {x=-16,y=-4,color=0x609393},
--     {x=-15,y=22,color=0x171021},
--     {x=18,y=3,color=0xbbeeaa},
--     {x=4,y=-17,color=0xefeff7}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   set:merge(set1)
--   set:merge(set2)
--   if SETTINGS.debug_raw then set:draw(Color.DARK_RED) end
--   set1:shift(0, Y2W(325)/1.22)
--   set2:shift(0, Y2W(325)/1.22)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)

--   if SETTINGS.debug_shift then set:draw(Color.DARK_RED) end
--   return set
-- end

-- -- BOSS紫咲诗音
-- ScanMap.scanBossShion = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'15-2'}) == false then
--       return nil
--     end
--   end

--   -- BOSS紫咲诗音 -- 底
--   local table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-12,y=-3,color=0x8e5423},
--     {x=-5,y=-44,color=0xf7f1f7},
--     {x=-56,y=15,color=0xafaff1},
--     {x=16,y=-4,color=0x8e5423}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- BOSS紫咲诗音   -- 顶
--   table = {
--     {x=0,y=0,color=0xfffbef},
--     {x=-12,y=-4,color=0x8e5423},
--     {x=-1,y=-43,color=0xf7f1f7},
--     {x=-52,y=11,color=0xafaff1},
--     {x=15,y=-3,color=0x8e5423}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   set:merge(set1)
--   set:merge(set2)
--   if SETTINGS.debug_raw then set:draw(Color.DARK_RED) end
--   set1:shift(-2, Y2W(325)/1.57)
--   set2:shift(-2, Y2W(325)/1.57)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)

--   if SETTINGS.debug_shift then set:draw(Color.DARK_RED) end
--   return set
-- end

-- -- BOSS夏色祭
-- ScanMap.scanBossMatsuri = function ()
--   if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
--     if IsWorkingOn({'15-1'}) == false then
--       return nil
--     end
--   end

--   -- BOSS夏色祭 -- 底
--   local table = {
--     {x=0,y=0,color=0xfff5e7},
--     {x=-14,y=-3,color=0x4488aa},
--     {x=2,y=12,color=0xf9bfb7},
--     {x=-6,y=-53,color=0x866a71},
--     {x=-27,y=28,color=0xdbdbe7}
--   }
--   local set = PosSet:new()
--   local set1 = PosSet:new()
--   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set1, points)

--   -- BOSS夏色祭   -- 顶
--   table = {
--     {x=0,y=0,color=0xfff5e7},
--     {x=-13,y=-2,color=0x4487a8},
--     {x=2,y=11,color=0xf9bfb7},
--     {x=-6,y=-46,color=0x866a71},
--     {x=-23,y=23,color=0xdadbe6}
--   }

--   local set2 = PosSet:new()
--   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
--   addToSet(set2,  points)

--   set:merge(set1)
--   set:merge(set2)
--   if SETTINGS.debug_raw then set:draw(Color.DARK_RED) end
--   set1:shift(-3, Y2W(325)/1.57)
--   set2:shift(-3, Y2W(325)/1.57)
--   set = PosSet:new()
--   set:merge(set1)
--   set:merge(set2)

--   if SETTINGS.debug_shift then set:draw(Color.DARK_RED) end
--   return set
-- end

-- 人形BOSS小图标
ScanMap.scanBoss_Small = function()
    if SETTINGS.debug_raw == false and SETTINGS.debug_shift == false then
        if IsWorkingOn({"14-1", "14-2", "14-3", "15-1", "15-2", "15-3"}) == false then
            return nil
        end
    end

    -- 人形BOSS小图标 -- 底
    local table = {
        {x = 0, y = 0, color = 0x342a30},
        {x = -7, y = -1, color = 0xfa4c50},
        {x = 16, y = -7, color = 0xfc5252},
        {x = 0, y = 10, color = 0x750410},
        {x = -1, y = -8, color = 0x3e232b}
    }
    local set = PosSet:new()
    local set1 = PosSet:new()
    local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set1, points)

    -- 人形BOSS小图标 -- 顶
    table = {
        {x = 0, y = 0, color = 0x342930},
        {x = -6, y = 0, color = 0xfa4c50},
        {x = 13, y = -5, color = 0xf94f4f},
        {x = 0, y = 10, color = 0x750310},
        {x = 0, y = -6, color = 0x3c232c}
    }

    local set2 = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set2, points)

    set:merge(set1)
    set:merge(set2)
    if SETTINGS.debug_raw then
        set:draw(Color.DARK_RED)
    end
    set1:shift(-23, Y2W(345) / 110)
    set2:shift(-21, Y2W(345) / 110)
    set = PosSet:new()
    set:merge(set1)
    set:merge(set2)

    if SETTINGS.debug_shift then
        set:draw(Color.DARK_RED)
    end
    return set
end

--  一般BOSS
ScanMap.scanBoss_Normal = function()
    if true == false then
        -- 活动图特殊缩放
        -- elseif IsWorkingOnEventLevel() then
        --   local set = PosSet:new()
        --   local table = {
        --     {x=0,y=0,color=0x342930},
        --     {x=-19,y=-6,color=0xfb4c51},
        --     {x=22,y=-9,color=0xfc4d51},
        --     {x=0,y=-27,color=0x302e30},
        --     {x=18,y=-33,color=0x2e262e}
        --   }
        --   local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
        --   addToSet(set, points)
        --   -- scale(table, 0.85)
        --   -- points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
        --   -- addToSet(set, points)
        --   if SETTINGS.debug_raw then set:draw(Color.DARK_RED) end
        --   return set
        assert(0)
    else
        local set = PosSet:new()
        local table = {
            {x = 0, y = 0, color = 0x31272d},
            {x = -10, y = 1, color = 0xf94f4f},
            {x = 26, y = -2, color = 0xfb4d51},
            {x = -23, y = -41, color = 0x363636},
            {x = -24, y = 26, color = 0xb94e55}
        }
        local points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
        addToSet(set, points)

        scale(table, 0.85)
        points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
        addToSet(set, points)

        if SETTINGS.debug_raw then
            set:draw(Color.DARK_RED)
        end
        return set
    end
end

--  boss识别
ScanMap.scanBoss = function()
    local newset = PosSet:new()
    newset:merge(ScanMap.scanBoss_Normal())
    newset:merge(ScanMap.scanBoss_Small())
    return newset
end

ScanMap.scanPlayer = function()
    local table = {
        {x = 0, y = 0, color = 0x18ff6b},
        {x = 3, y = 0, color = 0x7bffb5},
        {x = 3, y = 3, color = 0x73ffad},
        {x = 0, y = 3, color = 0x10ff63}
    }

    local set = PosSet:new()
    points = myFindColors({100, 50, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    if SETTINGS.debug_raw then
        set:draw(Color.GREEN)
    end
    set:shift(0, Y2W(360) * 1.5)
    if SETTINGS.debug_shift then
        set:draw(Color.GREEN)
    end
    return set
end

ScanMap.scanSub = function()
    local table = {
        {x = 0, y = 0, color = 0xfff39c},
        {x = 0, y = 7, color = 0xb56918},
        {x = -6, y = 7, color = 0xad6518},
        {x = 6, y = 7, color = 0x945518},
        {x = -2, y = 12, color = 0xfff79c},
        {x = 2, y = 12, color = 0xfff79c}
    }

    local set = PosSet:new()
    points = myFindColors({100, 50, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    scale(table, 1.2)
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set, points)

    if SETTINGS.debug_raw then
        set:draw(Color.GREEN)
    end
    set:shift(0, 0)
    if SETTINGS.debug_shift then
        set:draw(Color.GREEN)
    end
    return set
end

ScanMap.scanQuestion = function()
    local table = {
        {x = 0, y = 0, color = 0x94fff7},
        {x = 0, y = -17, color = 0x94fff7},
        {x = 0, y = -46, color = 0x84fff7},
        {x = -16, y = -8, color = 0xefebef},
        {x = 15, y = -9, color = 0xefebef}
    }

    local set = PosSet:new()
    points = myFindColors({100, 50, 1135, 639}, table, 90, 0, 0, 0)
    addToSet(set, points)

    if SETTINGS.debug_raw then
        set:draw(Color.BLUE)
    end
    set:shift(0, 50)
    if SETTINGS.debug_shift then
        set:draw(Color.BLUE)
    end
    return set
end

ScanMap.scanForm = function(submarineExist)
    local colors1 = {{1012, 367, 0xffffff}, {1012, 422, 0xffffff}}
    local colors2 = {{1001, 377, 0xffffff}, {1022, 412, 0xffffff}}
    local colors3 = {{995, 394, 0xffffff}, {1030, 394, 0xffffff}}

    if submarineExist then
        colors1[1][2] = colors1[1][2] - 83
        colors1[2][2] = colors1[2][2] - 83
        colors2[1][2] = colors2[1][2] - 83
        colors2[2][2] = colors2[2][2] - 83
        colors3[1][2] = colors3[1][2] - 83
        colors3[2][2] = colors3[2][2] - 83
    end

    if cmpColor(colors1) then
        return 1
    elseif cmpColor(colors2) then
        return 2
    elseif cmpColor(colors3) then
        return 3
    else
        return 0
    end
end

ScanMap.scanSubmarineExist = function()
    return cmpColor({{1010, 379, 0xffffff}, {1005, 404, 0xffffff}, {1088, 403, 0xffffff}, {1094, 382, 0xffffff}})
end

ScanMap.scanSubmarineClosed = function()
    return cmpColor({{1043, 413, 0xffffff}})
end

ScanMap.scanEnemiesMark4_5 = function()
    -- 苍红回响 平时
    -- local table = {
    --   {x=0,y=0,color=0xf7b2b5},
    --   {x=0,y=3,color=0xf7b2b5},
    --   {x=-15,y=0,color=0xef717b},
    --   {x=13,y=2,color=0xef717b}
    -- }

    -- 新UI红框
    local table = {
        {x = 0, y = 0, color = 0x9c1029},
        {x = -9, y = 0, color = 0x9c1029},
        {x = -23, y = 0, color = 0x9c1029},
        {x = -37, y = 0, color = 0xa51429},
        {x = -45, y = 0, color = 0xa51429},
        {x = 17, y = 0, color = 0x9c1429},
        {x = 37, y = 0, color = 0x9c1029}
    }

    local set = PosSet:new()
    points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    addToSet(set, points)

    -- -- AI智障 普通
    -- table = {
    --   {x=0,y=0,color=0xffbabd},
    --   {x=0,y=-5,color=0xffbabd},
    --   {x=-12,y=1,color=0xff8284},
    --   {x=10,y=2,color=0xff8284}
    -- }

    -- points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    -- addToSet(set,  points)

    -- if SETTINGS.chapter == 17 and SETTINGS.level == 4 then
    --   -- AI智障 特殊
    --   table = {
    --     {x=0,y=0,color=0x525152},
    --     {x=-15,y=1,color=0x525152},
    --     {x=1,y=-17,color=0xffffff},
    --     {x=4,y=-49,color=0xfffbef},
    --     {x=4,y=-80,color=0xada2a5},
    --     {x=-15,y=-74,color=0xd6beb5}
    --   }

    --   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    --   addToSet(set,  points)

    --   scale(table, 0.8)
    --   points = myFindColors({105, 49, 1135, 639}, table, 95, 0, 0, 0)
    --   addToSet(set,  points)
    -- end

    if SETTINGS.debug_raw then
        set:draw(Color.PURPLE)
    end
    -- set:shift(0, 100)
    if SETTINGS.debug_shift then
        set:draw(Color.PURPLE)
    end
    return set
end
