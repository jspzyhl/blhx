require("a-star")

i2A = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"}

NO_SCAN = 0
ENEMY_SCAN = 1
PLAYER_SCAN = 2

Map = {}
function Map:new()
    o = {
        initialized = false,
        config = nil,
        sub_closed = false,
        numRow = 0,
        numCol = 0,
        numSec = 0,
        numReg = 0,
        secWidth = 0,
        regHeight = 0,
        secWidthRem = 0,
        regHeightRem = 0,
        currSection = -1,
        currRegion = -1,
        anchor = {-1, -1}, -- coord of bottom right corner
        grids = {},
        grids_list = {},
        grids_region = {}, -- region_grids[i][j] = grids in section i, region j
        grids_top = {}, -- grids_top[i] = {}, means top grids in section i
        player = nil,
        playerBoss = nil,
        playerMob = nil,
        playerIdle = nil, --上场但不使用的队伍
        CurrentPlayer = nil,
        enemyM = {},
        DynamicBlock = {}, --  非静态障碍(比如：某个需要绕行的敌人、随机出现的障碍)
        question_list = {},
        elite_list = {},
        elite_list_fixed = {},
        boss = nil, -- {i,j}
        ammo = nil, -- {i,j}
        numRound = 0, -- each nextStep()
        numFighted = 0, -- the fight needed before boss, ambush doesn't count
        noenemy_last = false, --  上次扫描无敌人
        noenemy_retyies = 0, --  连续无敌人次数
        noenemy_retyies_forboss = 3, --  连续无敌人次数尝试boss
        possible_boss_list = {},
        boss_list_tried = 0,
        fixed_path_index = 1,
        fixed_path_index_prev = 1,
        fixed_path_finished = false,
        -- 0: no need to scan
        -- 1: scan enemies only
        -- 2: scan enemies + player (withtop)
        next_scan = 2,
        dumbMode = false,
        -- starting from bottom right, go up
        -- coordFixed = {{1017,506},{996,412},{977,327},{959,249},{943,177}},
        coordFixed = {{1012, 501}, {992, 408}, {972, 323}, {955, 246}, {939, 173}},
        maxRegSize = {5, 7},
        shiftX = 0,
        shiftY = 0,
        shiftXInit = 0,
        shiftYInit = 0,
        initAnchor = {3, 4}, -- initial anchor after shiftX/YInit
        topHeight = 3 -- # of rows to scan for top
    }
    setmetatable(o, self)
    self.__index = self
    return o
end

function Map:showConsole(str, player, target)
    showConsole(str .. " " .. player.i .. i2A[player.j] .. " -> " .. target[1] .. i2A[target[2]])
end

function Map:GetNumFight()
    local numFight = self.config.numFight
    if SETTINGS.mode == "normal" and self.config.numFight_N ~= nil then
        numFight = self.config.numFight_N
    end
    if SETTINGS.mode == "hard" and self.config.numFight_H ~= nil then
        numFight = self.config.numFight_H
    end
    return numFight
end

function Map:printState()
    local str = " |"
    for i = 1, self.numCol do
        str = str .. string.char(64 + i)
    end
    print(str)

    for i = 1, self.numRow do
        local str = i .. "|"
        for j = 1, self.numCol do
            if self.config.blocks[i][j] == 1 then
                str = str .. "-"
            elseif self.enemyM[i][j] > 0 and self.enemyM[i][j] < 10 then
                str = str .. self.enemyM[i][j]
            elseif self.DynamicBlock[i][j] == true then -- 需要绕行的目标
                str = str .. "x"
            elseif self.enemyM[i][j] == 10 then
                str = str .. "E"
            else
                str = str .. "o"
            end
        end
        print(str)
    end
    printf(
        "Player Boss = (%d,%d) %d/%d",
        self.playerBoss.i,
        self.playerBoss.j,
        self.playerBoss.ammo,
        self.playerBoss.ammoMax
    )
    printf(
        "Player Mob  = (%d,%d) %d/%d",
        self.playerMob.i,
        self.playerMob.j,
        self.playerMob.ammo,
        self.playerMob.ammoMax
    )
    if self.boss ~= nil then
        printf("Boss  = (%d,%d)", self.boss[1], self.boss[2])
    end
    if #self.question_list then
        for _, p in ipairs(self.question_list) do
            printf("Question  = (%d,%d)", p.i, p.j)
        end
    end
    if #self.elite_list then
        for _, p in ipairs(self.elite_list) do
            printf("Elite  = (%d,%d)", p.i, p.j)
        end
    end
end

function Map:getBlocker(i1, j1, i2, j2)
    -- printf('get blocker (%d,%d) -> (%d,%d)',i1,j1,i2,j2)
    local path = astar.path(self.grids[i1][j1], self.grids[i2][j2])
    assert(#path ~= 0, i1 .. j1 .. "->" .. i2 .. j2 .. " No path, BLK")
    self:printPath(path)
    for _, pos in ipairs(path) do
        if pos.i == i1 and pos.j == j1 then
        elseif pos.i == i2 and pos.j == j2 then
        elseif self.enemyM[pos.i][pos.j] > 0 or (self.boss ~= nil and self.boss[1] == pos.i and self.boss[2] == pos.j) then
            return pos
        end
    end
    return nil
end

function Map:getPath(i1, j1, i2, j2)
    local path = astar.path(self.grids[i1][j1], self.grids[i2][j2])
    assert(#path ~= 0, i1 .. j1 .. "->" .. i2 .. j2 .. " no path, NUM")
    return path
end

function Map:drawPath(path)
    for _, pos in ipairs(path) do
        if pos.section == self.currSection and pos.region == self.currRegion then
            pos:drawPin()
        end
    end
end

function Map:getStep(i1, j1, i2, j2)
    -- printf('Find path (%d,%d) -> (%d,%d)',i1,j1,i2,j2)
    local path = astar.path(self.grids[i1][j1], self.grids[i2][j2])
    -- self:printPath(path)
    assert(#path ~= 0, i1 .. j1 .. "->" .. i2 .. j2 .. " No path, STEP")
    assert(#path > 1, i1 .. j1 .. "->" .. i2 .. j2 .. " start=end, STEP")
    return path[2]
end

function Map:printPath(path)
    if #path == 0 then
        print("path = nil")
    end
    str = "path = "
    for _, pos in ipairs(path) do
        str = str .. "(" .. pos.i .. "," .. pos.j .. ")"
    end
    print(str)
end

function Map:switch()
    if self.player == self.playerIdle then
        self.player = self.playerMob -- which is also self.playerBoss
    elseif self.player == self.playerMob then
        self.player = self.playerBoss
    else
        self.player = self.playerMob
    end
    self.currSection = -1
    self.currRegion = -1
    sleep(500)
    click(846, 585, 981, 619) -- 切换

    -- is SP, the other team might be in fight after switch
    local state = self:getStableState(1000)
    if state == "formation" then
        self.player.ammo = self.player.ammo - 1
        clickFormationWeightAnchor() --出击
        sleep(7000)
        return false -- in fight after switch, cannot scanmap after return
    else
        return true -- not in fight after switch
    end
end

function Map:init()
    RecordBeginTime()

    if not STATE.autoseek_enabled then
        self.config = MAP_CONFIG[SETTINGS.chapter .. "-" .. SETTINGS.level]
        if self.config == nil then
            dialog("本关尚未制作(1型),仅支持自律寻敌模式")
            lua_restart()
        end
        if self.config.shift == nil then
            dialog("本关尚未制作(2型),仅支持自律寻敌模式")
            lua_restart()
        end
        if self.config.coordFixed ~= nil then
            self.coordFixed = self.config.coordFixed
        end
        if self.config.maxRegSize ~= nil then
            self.maxRegSize = self.config.maxRegSize
        end
        if self.config.initAnchor ~= nil then
            self.initAnchor = self.config.initAnchor
        end
        if self.config.scanTopHeight ~= nil then
            self.scanTopHeight = self.config.scanTopHeight
        end
        if self.config.topHeight ~= nil then
            self.topHeight = self.config.topHeight
        end

        self.numRow = #self.config.blocks
        self.numCol = #self.config.blocks[1]
        self.numSec = math.ceil(self.numCol / self.maxRegSize[2])
        self.numReg = math.ceil(self.numRow / self.maxRegSize[1])
        self.secWidth = math.floor(self.numCol / self.numSec)
        self.regHeight = math.floor(self.numRow / self.numReg)
        self.secWidthRem = self.numCol % self.numSec
        self.regHeightRem = self.numRow % self.numReg
        self.possible_boss_list = {unpack(self.config.boss)}
        self.boss_list_tried = 0
        self.shiftX = self.config.shift[1]
        self.shiftY = self.config.shift[2]
        self.shiftXInit = self.config.shiftInit[1]
        self.shiftYInit = self.config.shiftInit[2]

        printf("Map Size = %dx%d", self.numRow, self.numCol)
        printf("Num Reg = %dx%d", self.numReg, self.numSec)
        printf("Reg First Size = %dx%d", self.regHeight, self.secWidth)
        printf("Reg Last Size = %dx%d", self.regHeight + self.regHeightRem, self.secWidth + self.secWidthRem)

        -- init enemyM, ammo, grids
        for i = 1, self.numRow do
            self.enemyM[i] = {}
            self.DynamicBlock[i] = {}
            self.grids[i] = {}
            for j = 1, self.numCol do
                self.enemyM[i][j] = 0
                self.DynamicBlock[i][j] = false
                if self.config.blocks[i][j] == 9 then
                    self.ammo = {i, j}
                end
            end
        end

        -- setup player object
        self.player = Player:new()
        if SETTINGS.boss_team == SETTINGS.mob_team then
            self.playerBoss = self.player
            self.playerMob = self.player
        else
            self.playerMob = self.player
            self.playerBoss = Player:new()
        end
        self.playerMob.ammo = SETTINGS.mob_team_ammo
        self.playerMob.ammoMax = SETTINGS.mob_team_ammo
        self.playerMob.step = SETTINGS.mob_team_step
        self.playerBoss.step = SETTINGS.boss_team_step

        -- init grids_region
        for i = 1, self.numSec do
            self.grids_region[i] = {}
            self.grids_top[i] = {}
            for j = 1, self.numReg do
                self.grids_top[i][j] = self:initGrid(i, j, true)
                self.grids_region[i][j] = self:initGrid(i, j, false)
            end
        end

        -- 困难模式，只用第二队，靠此方法开局换队
        if SETTINGS.mode == "hard" and SETTINGS.boss_team == SETTINGS.mob_team and SETTINGS.mob_team == 2 then
            self.playerIdle = Player:new()
            self.player = self.playerIdle
        end

        self:scanPlayersAndMap(true)
    end

    self.initialized = true
    STATE.form_selected = true
    -- IncFarmedRound()
    print("Map initialized!")
end

function Map:scanPlayersAndMap(first)
    self:scanMap(true)
    if
        self.config.playerInit ~= nil and first and #self.config.playerInit == 2 and
            (SETTINGS.boss_team ~= SETTINGS.mob_team or self.playerIdle ~= nil)
     then
        local otheri = -1
        local otherj = -1
        local teammate = nil
        print("scanPlayersAndMap")
        print(self.player.i, self.player.j)
        if self.config.playerInit[1][1] == self.player.i and self.config.playerInit[1][2] == self.player.j then
            otheri = self.config.playerInit[2][1]
            otherj = self.config.playerInit[2][2]
        else
            otheri = self.config.playerInit[1][1]
            otherj = self.config.playerInit[1][2]
        end
        print(otheri, otherj)

        if self.playerIdle ~= nil then
            teammate = self.playerIdle
        elseif self.player ~= self.playerBoss then
            teammate = self.playerBoss
        elseif self.player ~= self.playerMob then
            teammate = self.playerMob
        end
        teammate.i = otheri
        teammate.j = otherj
    elseif SETTINGS.boss_team ~= SETTINGS.mob_team or self.playerIdle ~= nil then
        local inMap = self:switch() -- if false, means in fight.
        if inMap then
            self:scanMap(true)
        else
            -- After return, the outer function also returns to mainLogic
            -- So we are safe to return here
            print("WHAT!")
            return
        end
    end
end

function Map:calcGrid(secI, regJ, isTop)
    -- calc height of current region
    local currRegHeight = self.regHeight
    local currSecWidth = self.secWidth
    if regJ == self.numReg then
        currRegHeight = self.regHeight + self.regHeightRem
    end
    if secI == self.numSec then
        currSecWidth = self.secWidth + self.secWidthRem
    end

    if isTop and currRegHeight > self.topHeight then
        currRegHeight = self.topHeight
    end

    if regJ == 1 and currRegHeight < self.initAnchor[1] then
        currRegHeight = self.initAnchor[1]
    end
    if secI == 1 and currSecWidth < self.initAnchor[2] then
        currSecWidth = self.initAnchor[2]
    end

    -- topleft/bottomright coord {iABS, jABS}, of current region
    local anchorTL = {(regJ - 1) * self.regHeight + 1, (secI - 1) * self.secWidth + 1}
    local anchorBR = {(regJ - 1) * self.regHeight + currRegHeight, (secI - 1) * self.secWidth + currSecWidth}
    return currSecWidth, currRegHeight, anchorTL, anchorBR
end

function Map:initGrid(secI, regJ, isTop)
    local currSecWidth, currRegHeight, anchorTL, anchorBR = self:calcGrid(secI, regJ, isTop)

    -- check if region is too small so we don't need to scan top
    if isTop then
        local _, topRegHeight, _, _ = self:calcGrid(secI, regJ, False)
    end
    if topRegHeight == currRegHeight then
        return nil
    end

    local grid = {}
    for i = 1, currRegHeight do -- i-th row in region
        grid[i] = {}
        local iABS = anchorTL[1] + i - 1
        for j = 1, currSecWidth do -- j-th column in region
            -- rowAnchor = (i,currSecWidth)
            -- rowAnchor is (currRegHeight+1-i) th element in coordFixed
            -- (i,j) is rowAnchor shift left by (currSecWidth - j) * width
            local xy = self.coordFixed[currRegHeight + 1 - i]
            local y = xy[2]
            local x = xy[1] - (currSecWidth - j) * Y2W(y)
            local pos = Pos:new(x, y)
            pos:setWidth(Y2W(y))

            local jABS = anchorTL[2] + j - 1
            pos.i = iABS
            pos.j = jABS
            pos.section = secI
            pos.region = regJ
            grid[i][j] = pos
            if not isTop then
                self.grids[iABS][jABS] = pos
            end
            table.insert(self.grids_list, pos)
        end
    end
    return grid
end

function Map:scanMap(withTop)
    hideConsole()
    if withTop then
        self.next_scan = 0
    end

    -- 切阵型/关闭潜艇
    local submarineExist = SETTINGS.sub_team > 0
    if SETTINGS.mob_form == 0 and SETTINGS.boss_form == 0 then
        STATE.form_selected = true
    end

    if (not STATE.form_selected) or (submarineExist and not self.sub_closed) then
        click(1102, 374, 1128, 405) --open sidebar
        self.sub_closed = true
        sleep(750)

        -- 选了潜艇，实际上不存在
        if submarineExist and not ScanMap.scanSubmarineExist() then
            submarineExist = false
            SETTINGS.sub_team = 0
        end

        if submarineExist and not ScanMap.scanSubmarineClosed() then
            click(985, 370, 1037, 419) -- 潜艇
            sleep(750)
        end

        local setting_form = nil
        if self.player == self.playerMob then
            setting_form = SETTINGS.mob_form
        else
            setting_form = SETTINGS.boss_form
        end

        -- -- 阵型
        if setting_form ~= nil and setting_form > 0 then
            for _ = 1, 4 do
                if ScanMap.scanForm(submarineExist) == setting_form then
                    break
                end
                if submarineExist then
                    click(985, 329, 1035, 378)
                else
                    click(987, 371, 1034, 418)
                end
                sleep(750)
            end
        end

        click(949, 371, 961, 412) -- close sidebar
        sleep(100)
    end

    -- reset enemyM DynamicBlock
    for i = 1, self.numRow do
        for j = 1, self.numCol do
            self.enemyM[i][j] = 0
            self.DynamicBlock[i][j] = false
        end
    end

    -- whether we should reposition whole map
    local reposition = false
    reposition = withTop or self.numSec ~= 1 or self.numReg ~= 1 or self.numFighted >= self:GetNumFight()

    hideConsole()
    self.question_list = {}
    self.elite_list = {}

    if reposition then -- reposition in next map-move
        self.currSection = -1
        self.currRegion = -1
        self.anchor = self.initAnchor
    end

    for i = 1, self.numSec do
        for j = 1, self.numReg do
            if reposition and self.grids_top[i][j] ~= nil then
                self:moveToRegion(i, j, true)
                -- sleep(5000)
                self:scanGrid(self.grids_top[i][j])
            end
            self:moveToRegion(i, j, false)
            -- sleep(10000)
            self:scanGrid(self.grids_region[i][j])
            -- sleep(1000000)
        end
        -- sleep(1000000)
    end
    -- self:printState()
    -- sleep(100000)
end

function Map:scanGrid(grid, draw)
    keepScreen(true)
    local enemySetRawLv = {} -- ScanMap.scanEnemiesLv()
    local enemySetRaw1 = ScanMap.scanEnemiesMark1()
    local enemySetRaw2 = ScanMap.scanEnemiesMark2()
    local enemySetRaw3 = ScanMap.scanEnemiesMark3()
    local enemySetRaw4 = {}
    local enemySetRaw5 = {}
    -- local enemySetRaw_AsBlock = {}
    local enemySetRawBB = {}
    local enemySetRawCV = {}
    local enemySetRawGG = {}
    local enemySetRawDD = {}
    if SETTINGS.scanEnemyType then
        enemySetRawBB = ScanMap.scanEnemiesBB()
        enemySetRawCV = ScanMap.scanEnemiesCV()
        enemySetRawGG = ScanMap.scanEnemiesGG()
        enemySetRawDD = ScanMap.scanEnemiesDD()
    end

    if IsWorkingOnIoLnD() then
        enemySetRaw4 = ScanMap.scanEnemiesIoLnD()
    elseif IsWorkingOnEventLevel() then
        enemySetRaw4 = ScanMap.scanEnemiesMark4()
        enemySetRaw5 = ScanMap.scanEnemiesEvent_Submarine()
    end

    local playerSetRaw = ScanMap.scanPlayer()
    local bossSetRaw = ScanMap.scanBoss()
    local questionSetRaw = ScanMap.scanQuestion()
    keepScreen(false)
    for i = 1, #grid do
        for j = 1, #grid[1] do
            if draw then
                grid[i][j]:drawPin()
            end
            if self.config.blocks[grid[i][j].i][grid[i][j].j] ~= 1 then
                local similarity = grid[i][j].width * 0.3

                for _, p in ipairs(bossSetRaw) do
                    if grid[i][j]:similar(p, similarity) then
                        self.boss = {}
                        self.boss[1] = grid[i][j].i
                        self.boss[2] = grid[i][j].j
                        self.enemyM[grid[i][j].i][grid[i][j].j] = 0
                    end
                end

                for _, p in ipairs(enemySetRawLv) do -- regard as 2 star
                    if grid[i][j]:similar(p, similarity) then
                        self.enemyM[grid[i][j].i][grid[i][j].j] = -2
                    end
                end
                for _, p in ipairs(enemySetRaw1) do
                    if grid[i][j]:similar(p, similarity) then
                        self.enemyM[grid[i][j].i][grid[i][j].j] = -1
                    end
                end
                for _, p in ipairs(enemySetRaw2) do
                    if grid[i][j]:similar(p, similarity) then
                        self.enemyM[grid[i][j].i][grid[i][j].j] = -2
                    end
                end
                for _, p in ipairs(enemySetRaw3) do
                    if grid[i][j]:similar(p, similarity) then
                        self.enemyM[grid[i][j].i][grid[i][j].j] = -3
                    end
                end
                for _, p in ipairs(enemySetRaw4) do
                    if grid[i][j]:similar(p, similarity) then
                        self.enemyM[grid[i][j].i][grid[i][j].j] = -4
                        local seen = false
                        for _, e in ipairs(self.elite_list) do
                            if e.i == grid[i][j].i and e.j == grid[i][j].j then
                                seen = true
                                break
                            end
                        end

                        if
                            not seen and
                                (self.boss == nil or (self.boss[1] ~= grid[i][j].i and self.boss[2] ~= grid[i][j].j))
                         then
                            table.insert(self.elite_list, grid[i][j])
                        end
                    end
                end
                for _, p in ipairs(enemySetRaw5) do
                    if grid[i][j]:similar(p, similarity) then
                        self.enemyM[grid[i][j].i][grid[i][j].j] = -5
                    end
                end

                -- --  识别动态障碍
                -- for _, p in ipairs(enemySetRaw_AsBlock) do
                --   if grid[i][j]:similar(p, similarity) then
                --     self.DynamicBlock[grid[i][j].i][grid[i][j].j] = true
                --   end
                -- end

                --  敌人战力定义
                if true or SETTINGS.scanEnemyType then
                    for _, p in ipairs(enemySetRawBB) do
                        if grid[i][j]:similar(p, similarity) then
                            if self.enemyM[grid[i][j].i][grid[i][j].j] == -1 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.BB1
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -2 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.BB2
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -3 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.BB3
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == 0 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.BB2
                            end
                        end
                    end
                    for _, p in ipairs(enemySetRawCV) do
                        if grid[i][j]:similar(p, similarity) then
                            if self.enemyM[grid[i][j].i][grid[i][j].j] == -1 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.CV1
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -2 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.CV2
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -3 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.CV3
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == 0 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.CV2
                            end
                        end
                    end
                    for _, p in ipairs(enemySetRawDD) do
                        if grid[i][j]:similar(p, similarity) then
                            if self.enemyM[grid[i][j].i][grid[i][j].j] == -1 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.DD1
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -2 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.DD2
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -3 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.DD3
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == 0 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.DD2
                            end
                        end
                    end
                    for _, p in ipairs(enemySetRawGG) do
                        if grid[i][j]:similar(p, similarity) then
                            if self.enemyM[grid[i][j].i][grid[i][j].j] == -1 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.GG1
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -2 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.GG2
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -3 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.GG3
                            elseif self.enemyM[grid[i][j].i][grid[i][j].j] == 0 then
                                self.enemyM[grid[i][j].i][grid[i][j].j] = SETTINGS.GG2
                            end
                        end
                    end
                end

                if self.enemyM[grid[i][j].i][grid[i][j].j] < 0 then
                    if SETTINGS.scanEnemyType then
                        if self.enemyM[grid[i][j].i][grid[i][j].j] == -1 then
                            self.enemyM[grid[i][j].i][grid[i][j].j] = 4
                        elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -2 then
                            self.enemyM[grid[i][j].i][grid[i][j].j] = 5
                        elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -3 then
                            self.enemyM[grid[i][j].i][grid[i][j].j] = 6
                        elseif self.enemyM[grid[i][j].i][grid[i][j].j] == -4 then
                            self.enemyM[grid[i][j].i][grid[i][j].j] = 10 --  精英权重
                        end
                    else
                        self.enemyM[grid[i][j].i][grid[i][j].j] = -self.enemyM[grid[i][j].i][grid[i][j].j]
                    end
                end

                for _, p in ipairs(questionSetRaw) do
                    if grid[i][j]:similar(p, similarity) then
                        table.insert(self.question_list, grid[i][j])
                    end
                end
                -- for _, p in ipairs(bossSetRaw) do
                --   if grid[i][j]:similar(p, similarity) then
                --     self.boss = {}
                --     self.boss[1] = grid[i][j].i
                --     self.boss[2] = grid[i][j].j
                --     self.enemyM[grid[i][j].i][grid[i][j].j] = 0
                --   end
                -- end
                for _, p in ipairs(playerSetRaw) do
                    if grid[i][j]:similar(p, similarity) then
                        self.player.i = grid[i][j].i
                        self.player.j = grid[i][j].j
                        self.enemyM[grid[i][j].i][grid[i][j].j] = 0
                        if self.boss ~= nil and self.boss[1] == self.player.i and self.boss[2] == self.player.j then
                            self.boss = nil
                        end
                    end
                end
            end
        end
    end
end

function Map:pickQuestion(_player)
    local min_dist = 100
    local min_pos = nil
    local path = {}
    for _, p in ipairs(self.question_list) do
        if self:getBlocker(_player.i, _player.j, p.i, p.j) == nil then
            path = self:getPath(_player.i, _player.j, p.i, p.j)
            if #path < min_dist then
                min_dist = #path
                min_pos = p
            end
        end
    end
    if min_pos ~= nil then
        for _, p in ipairs(self.question_list) do -- 同一列上方存在问号优先考虑，先吃下方可能阻挡上方问号的识别
            if self:getBlocker(_player.i, _player.j, p.i, p.j) == nil then
                if (p.j == min_pos.j) and (p.i < min_pos.i) then
                    min_pos = p
                end
            end
        end
    end

    return min_pos
end

function Map:pickElite(_player)
    local min_dist = 100
    local min_pos = nil
    local path = {}
    for _, p in ipairs(self.elite_list) do
        if self:getBlocker(_player.i, _player.j, p.i, p.j) == nil then
            path = self:getPath(_player.i, _player.j, p.i, p.j)
            if #path < min_dist then
                min_dist = #path
                min_pos = p
            end
        end
    end
    return min_pos
end

function Map:pickPossibleBoss(_player)
    if _player.i == self.possible_boss_list[1][1] and _player.j == self.possible_boss_list[1][2] then
        local taken = table.remove(self.possible_boss_list, 1)
        table.insert(self.possible_boss_list, taken)
        self.boss_list_tried = self.boss_list_tried + 1
        if self.boss_list_tried >= #self.possible_boss_list then -- 已经尝试过所有可能boss点仍然没有遇到boss，继续攻击其他敌人
            self.boss_list_tried = 0 --  恢复boss重试次数
            local fight_sub_1 = self.numFighted - 1 --  战斗次数-1
            self.numFighted = (fight_sub_1 > 0) and fight_sub_1 or 0
            self.noenemy_retyies = 0 --  无敌人重试归零
            return nil
        end
    end
    return self.possible_boss_list[1]
end

function Map:pickNearestEnemy(_player)
    local min = 100
    local imin, jmin = -1, -1
    for i = 1, self.numRow do
        for j = 1, self.numCol do
            if
                self.enemyM[i][j] > 0 and
                    ((SETTINGS.elite_strategy == 2 and self.enemyM[i][j] < 4) or (SETTINGS.elite_strategy ~= 2))
             then --  躲避精英策略：若启用躲避则优先选择非精英敌人，否则选择精英
                if _player.i ~= i or _player.j ~= j then
                    local path = self:getPath(_player.i, _player.j, i, j)
                    if #path < min then
                        min = #path
                        imin = i
                        jmin = j
                    end
                else
                    -- player on top of enemy, give it a chance to rescan later,
                    -- instead of return "no enemy" (I think not necessary anymore)
                    if min == 100 then
                        imin = i
                        jmin = j
                    end
                end
            end
        end
    end

    --  全都是精英，选一个最近的精英
    local minEnemyMk = 99999
    if imin == -1 and jmin == -1 then
        for i = 1, self.numRow do
            for j = 1, self.numCol do
                if self.enemyM[i][j] > 0 then
                    if _player.i ~= i or _player.j ~= j then
                        local path = self:getPath(_player.i, _player.j, i, j)
                        if (#path < min) and (self.enemyM[i][j] <= minEnemyMk) then -- 选择威胁最低、距离最短的精英
                            min = #path
                            minEnemyMk = self.enemyM[i][j]
                            imin = i
                            jmin = j
                        end
                    else
                        -- player on top of enemy, give it a chance to rescan later,
                        -- instead of return "no enemy" (I think not necessary anymore)
                        if min == 100 then
                            imin = i
                            jmin = j
                        end
                    end
                end
            end
        end
    end

    return {imin, jmin}
end

function Map:pickWeakestEnemy(_player)
    local minStar = 100
    local minDist = 100
    local imin, jmin = -1, -1

    for i = 1, self.numRow do
        for j = 1, self.numCol do
            if self.enemyM[i][j] > 0 then
                if (_player.i ~= i or _player.j ~= j) and (self:getBlocker(_player.i, _player.j, i, j) == nil) then
                    if self.enemyM[i][j] < minStar then
                        local path = self:getPath(_player.i, _player.j, i, j)
                        minStar = self.enemyM[i][j]
                        minDist = #path
                        imin = i
                        jmin = j
                    elseif self.enemyM[i][j] == minStar then
                        local path = self:getPath(_player.i, _player.j, i, j)
                        if #path < minDist then
                            minDist = #path
                            imin = i
                            jmin = j
                        end
                    end
                else
                    -- player on top of enemy, give it a chance to rescan later,
                    -- instead of return "no enemy" (I think not necessary anymore)
                    if minDist == 100 then
                        imin = i
                        jmin = j
                    end
                end
            end
        end
    end
    return {imin, jmin}
end

function Map:moveToRegion(SecI, RegJ, isTop)
    local delta = {0, 0}
    if self.currSection <= 0 or self.currRegion <= 0 then
        swipe2corner()
        self.anchor = self.initAnchor
        delta = {self.shiftXInit, self.shiftYInit} -- negative
    end
    local currSecWidth, currRegHeight, anchorTL, anchorBR = self:calcGrid(SecI, RegJ, isTop)
    delta[1] = delta[1] + self.shiftX * (self.anchor[2] - anchorBR[2])
    delta[2] = delta[2] + self.shiftY * (self.anchor[1] - anchorBR[1])
    if delta[1] == 0 and delta[2] == 0 then
        return
    end

    printf(
        "-Move: (%d,%d)->(%d,%d), (%d,%d)->(%d,%d) %s",
        self.anchor[1],
        self.anchor[2],
        anchorBR[1],
        anchorBR[2],
        self.currRegion,
        self.currSection,
        RegJ,
        SecI,
        tostring(isTop)
    )
    swipe(1000, 500, 1000 + delta[1], 500 + delta[2])
    self.currSection = SecI
    self.currRegion = RegJ
    self.anchor = anchorBR
    sleep(100)
end

function Map:isMap()
    -- includes bottom right corner (ambush)
    -- includes arrow on right (air attack)
    if scanIsMap() then
        if scanIsSidebarOpen() then
            click(949, 371, 961, 412)
            sleep(100)
            return true
        else
            return true
        end
    end
    return false
end

function Map:isPreAmbush()
    local ret = false
    local colors = {{434, 356, 0xff0000}, {531, 357, 0xff0000}, {628, 350, 0xff0000}, {768, 350, 0xff0400}}
    for i = 1, 3 do
        keepScreen(true)
        ret = cmpColor(colors, 75)
        keepScreen(false)
        if ret then
            printf(" - preAmbush-%d", i)
            sleep(3000)
            return true
        end
        sleep(300)
    end
    return false
end

function Map:applyAutoMode()
    colors = {{670, 107, 0xef796b}}
    if cmpColor(colors) then -- auto off
        if not SETTINGS.isManualMode then
            click(732, 96, 800, 115)
            sleep(1500)
            if scanIsAutoConfirm() then
                pressAutoCombatOK()
            end
        end
    else -- auto on
        if SETTINGS.isManualMode then
            click(732, 96, 800, 115)
        end
    end
end

function Map:scanHP()
    local color_red = nil
    local color_green = nil
    color_red = 0xef0000 --red
    color_green = 0x94eb42 --green

    local ret = {-1, -1, -1, -1, -1, -1}

    for i, xy in ipairs({{42, 618}, {136, 618}, {229, 618}, {330, 618}, {421, 618}, {513, 618}}) do
        local x, y = xy[1], xy[2]
        for hp = 0, 3 do
            if cmpColor({{x + 5.8 * hp, y, color_red}}) then
                ret[i] = hp
            end
        end
        for hp = 3, 10 do
            if cmpColor({{x + 5.8 * hp, y, color_green}}) then
                ret[i] = hp
            end
        end
    end
    return ret
end

function Map:applyRepair()
    if not SETTINGS.autoRepair then
        return
    end
    local color_red = nil
    local color_green = nil

    if not cmpColor({{110, 470, 0xffffff}}) then
        return --no repair
    end

    local hps = self:scanHP()

    local total = 0
    local cnt1 = 0
    local cnt2 = 0
    for i, hp in ipairs(hps) do
        if hp ~= -1 then
            total = total + 1
            if hp < SETTINGS.repairHP1 then
                cnt1 = cnt1 + 1
            end
            if hp < SETTINGS.repairHP2 then
                cnt2 = cnt2 + 1
            end
        end
    end
    if total == 0 then
        return
    end
    if cnt1 / total >= SETTINGS.repairVote1 or cnt2 / total >= SETTINGS.repairVote2 then
        click(92, 461, 120, 490)
        sleep(500)
        click(606, 381, 736, 413)
        sleep(500)

        -- full health / no repair tool
        if scanIsRepairInform() then
            click(405, 378, 524, 411) --取消
        end
        sleep(500)
        self:applyRepair()
    end
end

function Map:autoSwitch()
    if not SETTINGS.autoSwitch then
        return
    end

    local hps = self:scanHP()
    local ship_xy = {{434, 194}, {596, 168}, {251, 224}, {363, 342}, {556, 301}, {728, 257}}

    local switchShip = function(high_pos, low_pos, diff, min)
        if hps[high_pos] == -1 or hps[low_pos] == -1 then
            return
        end
        if (hps[low_pos] - hps[high_pos] >= diff) and hps[low_pos] >= min then
            swipe(ship_xy[high_pos][1], ship_xy[high_pos][2], 679, 518, false, true)
            swipe(679, 518, ship_xy[low_pos][1], ship_xy[low_pos][2], true, false)
            local tmp = hps[high_pos]
            hps[high_pos] = hps[low_pos]
            hps[low_pos] = tmp
            sleep(1000)
        end
    end
    switchShip(1, 2, SETTINGS.switchDiff2, SETTINGS.switchMin2)
    switchShip(1, 3, SETTINGS.switchDiff2, SETTINGS.switchMin2)
    switchShip(3, 2, SETTINGS.switchDiff2, SETTINGS.switchMin2)
    switchShip(4, 5, SETTINGS.switchDiff1, SETTINGS.switchMin1)
    switchShip(4, 6, SETTINGS.switchDiff1, SETTINGS.switchMin1)
    switchShip(6, 5, SETTINGS.switchDiff1, SETTINGS.switchMin1)
end

function Map:getStableState(walkingTime)
    walkingTime = walkingTime or 0
    if walkingTime > 0 and walkingTime < 1000 then
        walkingTime = 1000
    end

    local t = mTime()
    for i = 1, 15 do
        -- print(i, mTime(), mTime() - t, walkingTime)
        -- if self:isPreAmbush() then
        --   return 'ambush'
        -- else
        if self:isMap() and mTime() - t > walkingTime then
            return "map"
        elseif scanIsAmbush() then
            sleep(1500) -- wait for ambush to fully pop out
            return "ambush"
        elseif scanIsFormation() then
            self:applyAutoMode()
            self:applyRepair()
            self:autoSwitch()
            return "formation"
        elseif scanIsGetItems() or scanIsGetItems2() then
            click(288, 434, 957, 509)
            sleep(500)
            return "item"
        elseif scanIsInform1Confirm() then
            click(500, 440, 635, 471)
            sleep(1000)
        elseif scanIsAutoConfirm() then
            pressAutoCombatOK()
        elseif scanIsInform2Confirm1() then
            -- elseif scanIsInform1ConfirmLarge() then
            --   click(492,479,647,524)
            --   sleep(1000)
            click(364, 435, 505, 475) -- 左侧否
            sleep(1000)
        elseif scanIsRepairInform() then
            click(398, 383, 539, 413)
            sleep(1000) -- 左侧否, 不使用]
        else
            sleep(1000) -- because we disabled pre-ambush
        end
    end
    return "wrong"
end

function Map:smartMove(_player, target)
    -- calc path to target
    orig_target = {target[1], target[2]}
    printf("(%d,%d)->(%d,%d)", _player.i, _player.j, target[1], target[2])
    local path = self:getPath(_player.i, _player.j, target[1], target[2])
    local dist = #path -- dist is just an estimation, 1 means at target already
    if dist == 1 then
        showConsole("异常：已经在目标，10s后尝试恢复。")
        print("TERGET=PLAYER, RECOVERY")
        sleep(10000)
        self:scanPlayersAndMap()
        return NO_SCAN
    end

    -- find teammate
    local teammate = nil
    if self.playerIdle ~= nil then
        teammate = self.playerIdle
    elseif _player ~= self.playerBoss then
        teammate = self.playerBoss
    elseif _player ~= self.playerMob then
        teammate = self.playerMob
    end

    -- extra_waiting for Walk, SP, Submarine
    local extra_waiting = 1500
    extra_waiting = extra_waiting + #self.elite_list * 3 * 500 -- elite speed = 500ms
    if SETTINGS.sub_team > 0 and SETTINGS.isSubAnime then
        extra_waiting = extra_waiting + 5000
    end
    if SETTINGS.isCatAnime then
        extra_waiting = extra_waiting + 3000
    end

    -- step size [SP], calc new target position
    if _player.step ~= 0 and dist - 1 > _player.step then
        local new_path = {}
        for i = 1, _player.step + 1 do
            if teammate ~= nil and teammate.i == path[i].i and teammate.j == path[i].j then
                print("Teammate found")
            else
                table.insert(new_path, path[i])
            end
        end
        path = new_path
        dist = #path
        target = {path[dist].i, path[dist].j}
        printf("     ->(%d,%d)", target[1], target[2])
    end

    -- target type information
    local isEnemy = self.enemyM[target[1]][target[2]] > 0
    local isBoss = false
    if self.boss ~= nil and self.boss[1] == target[1] and self.boss[2] == target[2] then
        isBoss = true
    end
    local isAmmo = self.ammo ~= nil and target[1] == self.ammo[1] and target[2] == self.ammo[2]
    local isQuestion = false
    for _, pos in ipairs(self.question_list) do
        if target[1] == pos.i and target[2] == pos.j then
            isQuestion = true
        end
    end
    local isEmpty = not (isEnemy or isBoss or isAmmo or isQuestion)

    -- if target = teammate
    if teammate ~= nil and teammate.i == target[1] and teammate.j == target[2] then
        showConsole("异常：目标为队友，切换并战斗")
        self:switch()
        local pos = self.grids[target[1]][target[2]]
        self:moveToRegion(pos.section, pos.region)
        pos:click(true)
        sleep(2000)
        state = self:getStableState()

        if state == "formation" then
            _player.ammo = _player.ammo - 1
            self.numFighted = self.numFighted + 1
            self.enemyM[pos.i][pos.j] = 0
            sleep(2000)
            clickFormationWeightAnchor() --出击
            sleep(5000)
            return ENEMY_SCAN
        else
            showConsole("异常：切换队友后没有进入战斗，10s后恢复")
            print("TERGET=TEAMMATE, RECOVERY")
            sleep(10000)
            self:scanPlayersAndMap()
            return NO_SCAN
        end
    end

    -- check whether player has moved
    local moved = false
    local first_try = true

    local state = ""
    while true do
        -- Check state before click
        if not self:getStableState() == "map" then
            showConsole("初始状态异常")
            return PLAYER_SCAN
        end

        if _player.i == target[1] and _player.j == target[2] then
            return ENEMY_SCAN
        end

        print("???? =", moved, isEmpty, dist)

        -----------------------------------------------------------------------
        ---------------------------------------------------------------NORMAL TARGET
        if (first_try or moved or isAmmo or isQuestion or isEnemy or isBoss) and (not isEmpty) and dist > 0 then
            --------------------------------------------------------------------
            -- draw and click
            local pos = self.grids[target[1]][target[2]]
            self:moveToRegion(pos.section, pos.region)
            self:drawPath(path)
            pos:click(true)
            first_try = false
            hidePathHUD()
            dist = dist - 2

            state = self:getStableState(SETTINGS.move_speed * dist + extra_waiting)
            print("state =", state)

            if state == "formation" then
                _player.ammo = _player.ammo - 1
                self.numFighted = self.numFighted + 1
                _player.i = target[1]
                _player.j = target[2]
                self.enemyM[pos.i][pos.j] = 0
                sleep(2000)
                clickFormationWeightAnchor() --出击
                sleep(5000)
                return ENEMY_SCAN
            elseif state == "ambush" then
                moved = true
                if SETTINGS.isFightAmbush then
                    click(677, 402, 811, 435)
                else
                    click(877, 403, 1013, 434)
                end
                dist = dist + 1
                state = self:getStableState(3000)
                print("after ambush =", state)
                if state == "formation" then
                    _player.ammo = _player.ammo - 1
                    sleep(2000)
                    clickFormationWeightAnchor() --出击
                    sleep(5000)
                    return PLAYER_SCAN
                elseif state == "map" then
                    print("Ambush avoided")
                else
                    return PLAYER_SCAN
                end
            elseif state == "item" then
                moved = true
                if isQuestion then
                    _player.i = target[1]
                    _player.j = target[2]
                    return ENEMY_SCAN
                end
            elseif state == "map" then
                -- attacked/blocked
            else --wrong
                return PLAYER_SCAN
            end

            -- check whether moved
            -- save old i,j into tmp, scan again to see if it is changed
            local old_i = _player.i
            local old_j = _player.j
            _player.i = -1
            _player.j = -1
            self:scanGrid(self.grids_region[self.currSection][self.currRegion])
            if _player.i ~= -1 and (_player.i ~= old_i or _player.j ~= old_j) then
                moved = true
                path = self:getPath(_player.i, _player.j, target[1], target[2])
                dist = #path
                if self.ammo ~= nil and _player.i == self.ammo[1] and _player.j == self.ammo[2] then
                    self.ammo = nil
                end
            else
                _player.i = old_i
                _player.j = old_j
            end
        elseif isEmpty and dist > 0 then ----------------------------------SP EMPTY
            ---------------------------------------------------------------------
            print("SP")
            -- draw and click
            local pos = self.grids[target[1]][target[2]]
            self:moveToRegion(pos.section, pos.region)
            self:drawPath(path)
            pos:click(true)
            hidePathHUD()
            dist = dist - 2

            state = self:getStableState(SETTINGS.move_speed * dist + extra_waiting)

            if state == "formation" then
                _player.ammo = _player.ammo - 1
                self.numFighted = self.numFighted + 1
                _player.i = target[1]
                _player.j = target[2]
                self.enemyM[pos.i][pos.j] = 0
                sleep(2000)
                clickFormationWeightAnchor() --出击
                sleep(5000)
                return ENEMY_SCAN
            elseif state == "ambush" then -- we don't know where we are
                if SETTINGS.isFightAmbush then
                    click(677, 402, 811, 435)
                else
                    click(877, 403, 1013, 434)
                end
                dist = dist + 1
                state = self:getStableState(3000)
                if state == "formation" then
                    _player.ammo = _player.ammo - 1
                    sleep(2000)
                    clickFormationWeightAnchor() --出击
                    sleep(5000)
                    return PLAYER_SCAN
                elseif state == "map" then
                    return PLAYER_SCAN
                else
                    return PLAYER_SCAN
                end
            elseif state == "item" then -- we don't know where we are
                moved = true
                dist = 1
                return PLAYER_SCAN
            elseif state == "map" then --map, don't know whether we moved
            else
                return PLAYER_SCAN
            end

            -- check whether moved
            -- save old i,j into tmp, scan again to see if it is changed
            local old_i = _player.i
            local old_j = _player.j
            _player.i = -1
            _player.j = -1
            self:scanGrid(self.grids_region[self.currSection][self.currRegion])
            if _player.i ~= -1 and (_player.i ~= old_i or _player.j ~= old_j) then --moved
                if
                    #self.elite_list > 0 and SETTINGS.elite_strategy > 0 and
                        self.enemyM[orig_target[1]][orig_target[2]] <= 0
                 then
                    return ENEMY_SCAN -- because we want to chase moved elite, but if target is enemy then not scan to save time
                else
                    return NO_SCAN -- because we don't care about elite
                end
            else
                -- not moved, dist -= 2, may go into step mode
                _player.i = old_i
                _player.j = old_j
            end
        else ----------------------------------------STEP MODE
            local pos = self:getStep(_player.i, _player.j, target[1], target[2])

            -- pos is teammate
            if teammate ~= nil and teammate.i == pos.i and teammate.j == pos.j then
                _player.i = pos.i
                _player.j = pos.j
            else
                -- draw and click
                self:moveToRegion(pos.section, pos.region)
                self:drawPath(path)
                pos:click(true)
                hidePathHUD()
                dist = dist - 1
                _player.i = pos.i
                _player.j = pos.j
                self.enemyM[pos.i][pos.j] = 0
                if self.ammo ~= nil and pos.i == self.ammo[1] and pos.j == self.ammo[2] then
                    self.ammo = nil
                end

                state = self:getStableState(SETTINGS.move_speed + extra_waiting)
                if state == "formation" then
                    _player.ammo = _player.ammo - 1
                    self.numFighted = self.numFighted + 1
                    sleep(2000)
                    clickFormationWeightAnchor() --出击
                    sleep(5000)
                    return ENEMY_SCAN
                elseif state == "ambush" then
                    moved = true
                    if SETTINGS.isFightAmbush then
                        click(677, 402, 811, 435)
                    else
                        click(877, 403, 1013, 434)
                    end
                    state = self:getStableState(3000)
                    if state == "formation" then
                        _player.ammo = _player.ammo - 1
                        sleep(2000)
                        clickFormationWeightAnchor() --出击
                        sleep(5000)
                        return ENEMY_SCAN
                    elseif state == "map" then
                        print("Ambush avoided")
                    else
                        return PLAYER_SCAN
                    end
                elseif state == "wrong" then
                    return PLAYER_SCAN
                end
            end
        end
    end

    return PLAYER_SCAN
end

function Map:smartLogic()
    -- self.numRow = 5
    -- self.numCol = 7
    -- self.enemyM = {
    --   {0,0,2,0,0,0,0},
    --   {0,0,0,0,0,0,0},
    --   {0,0,0,0,2,0,2},
    --   {0,0,0,0,0,0,0},
    --   {0,0,0,0,0,0,0},
    -- }
    -- self.ammo = {2,6}
    -- self.playerMob.i = 3
    -- self.playerMob.j = 3
    -- self.playerBoss.i = 3
    -- self.playerBoss.j = 4
    -- self.boss = {4,7}

    -- self.playerMob.ammo = 1
    -- self.question_list = {self.grids[3][3]}
    -- self.numFighted = 4

    self:printState()


    -- player for next move
    -- decide whether switch at move time
    local _player

    if self.playerMob.ammo > 0 then
        _player = self.playerMob
    else
        _player = self.playerBoss
    end
    self.CurrentPlayer = _player

    -- use smart logic only if both player locations are found
    if (self.playerMob.i == -1 or self.playerBoss.i == -1)  then
        showConsole("未找到己方,2s后重新扫描")
        sleep(2000)
        self:scanPlayersAndMap()
        return
    end

    local target = {}

    -- check if there is any enemy, before fighting boss
    local hasEnemyBeforeBoss = false
    for i = 1, self.numRow do
        for j = 1, self.numCol do
            if self.enemyM[i][j] > 0 then
                hasEnemyBeforeBoss = true
                break
            end
        end
        if hasEnemyBeforeBoss then
            break
        end
    end

    if SETTINGS.fixed_path_enabled == false then --  未开启固定路线，直接标记为已完成
        self.fixed_path_finished = true
    end
    --  判断是否需要走固定路线
    local bShouldMovetoFixed = false
    if self.fixed_path_finished == false then
        bShouldMovetoFixed = true
        local last_fix_target = SETTINGS.fixed_path[self.fixed_path_index_prev] --  上一次固定目标
        local final_fix_target = SETTINGS.fixed_path[#SETTINGS.fixed_path]
        if self.playerMob:IsAtPosT(last_fix_target) or self.playerBoss:IsAtPosT(last_fix_target) then --  已抵达上一次的目标
            self.fixed_path_index = self.fixed_path_index + 1 --  切换为下一个固定目标
            if self.fixed_path_index > #SETTINGS.fixed_path then --  clamp 避免超过最大index
                self.fixed_path_index = #SETTINGS.fixed_path
            end
        end
        if
            self.fixed_path_index >= #SETTINGS.fixed_path and --  已抵达固定路线终点，将固定移动标记为完成
                (self.playerMob:IsAtPosT(final_fix_target) or self.playerBoss:IsAtPosT(final_fix_target))
         then
            self.fixed_path_finished = true
            bShouldMovetoFixed = false
        end
    end

    -- IF 1. there is fixed path to walk
    if bShouldMovetoFixed == true then
        -- IF 1. Setting.chase_elite 2. Map has elite 3. _player not blocked to any of the elite
        print("FIXED PATH")
        self.fixed_path_index_prev = self.fixed_path_index --  记录目标index
        print(tostring(self.fixed_path_index))
        target = SETTINGS.fixed_path[self.fixed_path_index]
        self:showConsole("目标: 固定走位", _player, target)
    elseif SETTINGS.elite_strategy == 1 and #self.elite_list > 0 and self:pickElite(_player) ~= nil then
        -- IF 1. Map has ammo 2. Setting eat ammo 3. _player.ammo <= _player.ammoMax-3 4. _player not blocked to ammo
        print("ELITE")
        local elite = self:pickElite(_player)
        target = {elite.i, elite.j}
        self:showConsole("目标: 精英", _player, target)
    elseif
        self.ammo ~= nil and SETTINGS.eat_ammo and _player.ammo <= _player.ammoMax - SETTINGS.eatAmmoAfter and
            self:getBlocker(_player.i, _player.j, self.ammo[1], self.ammo[2]) == nil
     then
        -- IF 1. has question 2. Setting eat question 3. _player not blocked to any of the question
        print("AMMO")
        target = {self.ammo[1], self.ammo[2]}
        self:showConsole("目标: 弹药", _player, target)
    elseif
        #self.question_list > 0 and SETTINGS.eat_question and self.numFighted >= SETTINGS.eatQuestionAfter and
            self:pickQuestion(_player) ~= nil
     then
        -- IF has enough fights for boss
        print("QUESTION")
        local question = self:pickQuestion(_player)
        target = {question.i, question.j}
        self:showConsole("目标: 问号", _player, target)
    elseif
        SETTINGS.isFightBoss and (not SETTINGS.isClearMap or not hasEnemyBeforeBoss) and
            (self.numFighted >= self:GetNumFight() or self.boss ~= nil or
                self.noenemy_retyies >= self.noenemy_retyies_forboss)
     then
        -- ELSE has no enough fights for boss
        local _boss = {}
        local _status_str = ""
        -- IF boss found, go to boss
        if self.boss ~= nil then
            -- ELSE, go to possible_boss, shift possible_boss_list
            _boss = self.boss
            print("BOSS")
            _status_str = "目标: BOSS"
        else
            _boss = self:pickPossibleBoss(self.playerBoss)
            if _boss ~= nil then
                print("PREDICT BOSS (%d,%d)", _boss[1], _boss[2])
                _status_str = "目标: 预测BOSS点"
            else
                self:scanMap(true)
                return
            end
        end

        local blocker = self:getBlocker(self.playerBoss.i, self.playerBoss.j, _boss[1], _boss[2])
        -- playerBoss can reach boss
        if blocker == nil then
            -- playerBoss cannot reach boss
            -- move playerBoss to boss
            _player = self.playerBoss
            self.CurrentPlayer = _player
            target = {_boss[1], _boss[2]}
            self:showConsole(_status_str, _player, target)
        else
            -- move _player to blocker
            print("BLOCKER")
            target = {blocker.i, blocker.j}
            self:showConsole("目标: 阻挡BOSS的敌人", _player, target)
        end
    else
        if self.numFighted >= SETTINGS.resignBefore then
            pressResign()
            sleep(2000)
            return
        end

        local blocker = nil
        for i, _boss in ipairs(self.possible_boss_list) do
            blocker = self:getBlocker(self.playerBoss.i, self.playerBoss.j, _boss[1], _boss[2])
            if blocker ~= nil then
                break
            end
        end

        -- IF playerBoss blocked to potential boss
        if blocker ~= nil then
            local new_blocker = nil
            new_blocker = self:getBlocker(self.playerMob.i, self.playerMob.j, blocker.i, blocker.j)
            -- IF playerMob blocked to that blocker
            if new_blocker ~= nil then
                -- ELSE
                -- move _player to new_blocker
                print("PREDICT BLOCKER OF BLOCKER")
                target = {new_blocker.i, new_blocker.j}
                self:showConsole("目标: 挡住-挡住boss出生点的敌人", _player, target)
            else
                -- move _player to blocker
                print("PREDICT BLOCKER")
                target = {blocker.i, blocker.j}
                self:showConsole("目标: 挡住boss出生点的敌人", _player, target)
            end
        else
            -- move _player to nearest enemy
            if SETTINGS.isWeightedEnemy then
                print("WEAKEST")
                target = self:pickWeakestEnemy(_player)
            else
                print("NEAREST")
                target = self:pickNearestEnemy(_player)
            end

            -- if no nearest enemy
            if target[1] == -1 then
                if self.noenemy_last == true then
                    self.noenemy_retyies = self.noenemy_retyies + 1
                end
                self.noenemy_last = true
                if SETTINGS.isFightBoss then
                    showConsole("无敌人,2s后重新扫描")
                    sleep(2000)
                    self:scanMap(true) -- rescan with top
                else
                    showConsole("无敌人,不打boss,2s后撤退")
                    pressResign()
                    sleep(2000)
                end
                return
            else
                self.noenemy_last = false
                self.noenemy_retyies = 0
                if SETTINGS.isWeightedEnemy then
                    self:showConsole("目标: 最弱敌人(战斗力" .. self.enemyM[target[1]][target[2]] .. ")", _player, target)
                else
                    self:showConsole("目标: 最近敌人", _player, target)
                end
            end
        end
    end

    if _player ~= self.player then
        self:switch()
    end
    -- sleep(100000)
    self.next_scan = self:smartMove(_player, target)
end

function Map:nextStep()
    print("----------------------------")
    self.numRound = self.numRound + 1
    if self.numRound > 30 then
        showConsole("时间太久，10s后撤退")
        sleep(10000)
        pressResign()
        return
    end

    if not self.initialized then
        self:init()
    else
        print("next_scan =", self.next_scan)
        if self.next_scan == 0 then
        elseif self.next_scan == 1 then
            self:scanMap()
        elseif self.next_scan == 2 then
            self:scanMap(true)
        else
            assert(false, "Invalid Scan Mode")
        end
    end

    self:smartLogic()
end
