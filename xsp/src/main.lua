require("Config")
require("Utils")

require("ScanMain")
require("ScanMap")
require("Settings")

require("Pos")
require("PosSet")
require("Color")
require("Player")
require("Map")
require("Act")

require("mainLogic")
require("Research")

JSON = require("JSON")
SHA = require("sha2")

DEVICE = {}
CONSOLE = {}
HUD = {
    tmp = {},
    path = {},
    side = {},
    top = {},
    weituo = {}
}
SETTINGS = {}
STATE = {
    state = "",
    pre_round = 0,
    pre_round_setting = 0,
    preround_selected = false,
    recycled = false,
    yanxied = false,
    yanxi_confirmed = 0,
    yanxiued = false,
    keyCollected = false,
    meiri_cycle = 0,
    meiri_current_type_done = false,
    beacon_cycle = 0,
    beacon_battle_retried = 0,
    beacon_battle_begin = false,
    weituo_first_done = false,
    weituo_lasttime = 0,
    feed_lasttime = 0,
    study_lasttime = 0,
    keyan_lasttime = 0,
    renwu_lasttime = 0,
    award_received = false,
    hatch_lasttime = 0,
    hatch_rarity = 3,
    shopping_lasttime = 0,
    yanxizuozhan_round = 0,
    team_selected = false,
    form_selected = false,
    accountability_setted = false,
    SOS_done = false,
    days_since_epoch = 0,
    money_full = false,
    combat_begin_time = 0,
    submarine_enabled = false,
    autoseek_enabled = false,
    autoseek_round_end = true,
    enter_level_clicked = false,
    map_entered = false,
    last_scan_result = ""
}
STATS = { 0, 0, 0, 0, 0, 0, 0, 0, 0 } --1:白/2:蓝/3:紫/4:金/5:场次/6:委托/7:科研/8:任务启动时间/9:战斗次数
MAP = nil
OCR = {
    ky_time = nil
}

res_sort = ResSorter:new()
res_rule_data = RuleData:new()

-- FREE = true
VER = "2018-6-23"

function load_ocr()
    local ocr, msg = createOCR(
            {
                type = "tesseract",
                path = "res/",
                lang = "ky_time"
            }
    )
    assert(ocr, msg)
    OCR.ky_time = ocr
end

function initialize()
    --  setSysConfig("isLogFile","1")
    math.randomseed(os.time())
    STATE.days_since_epoch = daysSinceEpoch()
    DEVICE.width, DEVICE.height = getScreenSize()
    DEVICE.DPI = getScreenDPI()
    DEVICE.OS = getOSType()
    DEVICE.isPrivate = isPrivateMode()
    if DEVICE.width > DEVICE.height then
        DEVICE.width, DEVICE.height = DEVICE.height, DEVICE.width
    end
    print("Resolution =", DEVICE.height, "x", DEVICE.width)
    -- print(frontAppName())

    DEVICE.devWidth = 639
    DEVICE.devHeight = 1136
    DEVICE.devRatio = 16 / 9

    DEVICE.offsetX = 0
    DEVICE.offsetY = 0
    DEVICE.scale = 1
    DEVICE.swipeScaleX = 1 * DEVICE.devHeight / DEVICE.height
    DEVICE.swipeScaleY = 1 * DEVICE.devWidth / DEVICE.width
    local ratio = DEVICE.height / DEVICE.width -- dev ratio = 1.775
    DEVICE.rev_ratio = DEVICE.width / DEVICE.height -- dev rev_ratio = 0.5625
    print("rev_ratio=", rev_ratio)
    if DEVICE.rev_ratio < 0.5625 then
        -- ipx, 左右，height
        DEVICE.scale = DEVICE.width / DEVICE.devWidth
        DEVICE.UI_scale = DEVICE.scale
        -- DEVICE.swipeScaleX = DEVICE.height / (DEVICE.devHeight * DEVICE.scale)
        DEVICE.offsetX = (DEVICE.height - DEVICE.width * DEVICE.devRatio) / 2
        print("OffsetX =", DEVICE.offsetX, "scale =", DEVICE.scale)
    elseif DEVICE.rev_ratio > 0.5625 then
        -- pad, 上下, width
        DEVICE.scale = DEVICE.height / DEVICE.devHeight
        DEVICE.UI_scale = DEVICE.scale
        -- DEVICE.swipeScaleY = DEVICE.width / (DEVICE.devWidth * DEVICE.scale)
        DEVICE.offsetY = (DEVICE.width - DEVICE.height / DEVICE.devRatio) / 2
        print("OffsetY =", DEVICE.offsetY, "scale =", DEVICE.scale)
    else
        setScreenScale(DEVICE.devWidth, DEVICE.devHeight, 0)
        DEVICE.UI_scale = DEVICE.width / DEVICE.devWidth
    end

    init("0", 1)
    setUIOrientation(0)

    -- CONSOLE
    CONSOLE.width = math.floor(DEVICE.devHeight / 2) * DEVICE.scale
    CONSOLE.height = math.floor(DEVICE.devWidth * 0.05) * DEVICE.scale
    CONSOLE.font = 18 * DEVICE.width / DEVICE.devWidth
    if DEVICE.rev_ratio < 0.5625 then
        -- ipx, 左右，height
        CONSOLE.font = 18 * DEVICE.width / DEVICE.devWidth
    elseif DEVICE.rev_ratio > 0.5625 then
        -- pad, 上下, width
        CONSOLE.font = 18 * DEVICE.height / DEVICE.devHeight
    end
    CONSOLE.x = math.floor(DEVICE.devHeight * 0.25)
    CONSOLE.y = math.floor(DEVICE.devWidth / 13)
    printf("console: loc: %d,%d size=%dx%d", CONSOLE.x, CONSOLE.y, CONSOLE.width, CONSOLE.height)

    load_ocr()
end

function checkScreenDirection()
    local direction = getScreenDirection()
    print("direction =", direction)
    while direction ~= 1 do
        if DEVICE.OS == "iOS" then
            dialog("请先横屏,Home键在右侧,再开脚本")
        else
            dialog("请先横屏,再开脚本")
        end
    end
end

function _scaleUI(json, scale)
    scale = DEVICE.UI_scale
    if json.width ~= nil then
        json.width = json.width * scale
    end
    if json.height ~= nil then
        json.height = json.height * scale
    end
    if json.size ~= nil then
        json.size = json.size * scale
    end
    if json.rect ~= nil then
        local x, y, w, h = json.rect:match("([^,]+),([^,]+),([^,]+),([^,]+)")
        x = math.floor(x * scale)
        y = math.floor(y * scale)
        w = math.floor(w * scale)
        h = math.floor(h * scale)
        json.rect = x .. "," .. y .. "," .. w .. "," .. h
    end
    if json.views ~= nil then
        for _, v in ipairs(json.views) do
            _scaleUI(v, scale)
        end
    end
end

local function showFree()
    local json_str = getUIContent("free.json")
    local json_obj = JSON:decode(json_str)

    local scale = DEVICE.width / 640
    _scaleUI(json_obj, scale)

    json_str = JSON:encode(json_obj)
    local ret, settings = showUI(json_str)
    if ret == 0 then
        lua_exit()
    end
end

local function showAnnounce(file)
    local json_str = getUIContent(file)
    local json_obj = JSON:decode(json_str)

    local scale = DEVICE.width / 640
    _scaleUI(json_obj, scale)

    json_str = JSON:encode(json_obj)
    local ret, settings = showUI(json_str)
    if ret == 0 then
        lua_exit()
    end
end

local function loadStats(json_obj)
    for i = 1, #json_obj.views do
        if json_obj.views[i].text == "统计" then
            json_obj.views[i].views[1].text = "白皮：" .. getNumberConfig("1star", 0)
            json_obj.views[i].views[2].text = "蓝皮：" .. getNumberConfig("2star", 0)
            json_obj.views[i].views[3].text = "紫皮：" .. getNumberConfig("3star", 0)
            json_obj.views[i].views[4].text = "金皮：" .. getNumberConfig("4star", 0)
            json_obj.views[i].views[5].text = "场次：" .. getNumberConfig("round", 0)
            json_obj.views[i].views[6].text = "" .. DEVICE.height .. "/" .. DEVICE.width .. "/" .. DEVICE.rev_ratio .. "\n"
        end
    end

    -- json_obj.views[7].views[7].text = cmpColor({{63,335,0xffffff},{51,508,0xffffff},{60,592,0xffffff},{973,553,0xffffff}},nil,true)
end

local function showSettings()
    --  resetUIConfig("settings.dat")
    local json_str = getUIContent("ui.json")
    local json_obj = JSON:decode(json_str)

    -- modify content
    loadStats(json_obj)
    print(getStringConfig("settings.dat", "nothing"))
    -- json_obj.views[1].views[3].select = ""

    local scale = DEVICE.width / 640
    _scaleUI(json_obj, scale)

    json_str = JSON:encode(json_obj)
    local ret, settings = showUI(json_str)
    if ret == 0 then
        lua_exit()
    end
    SETTINGS = settings
    process_SETTINGS()
    if DEVICE.OS == "android" then
        sleep(2000)
    end
end

local function autoResetSetting()
    local ver = getStringConfig("ver", "0")
    if ver ~= VER then
        resetUIConfig("settings.dat")
        setStringConfig("ver", VER)
    end
end

function onBeforeUserExit()
    local round_per_hr = 0
    if STATS[8] ~= 0 then
        round_per_hr = STATS[5] / ((mTime() - STATS[8]) / 1000 / 3600)
        round_per_hr = string.format("%.2f", round_per_hr)
    end
    if SETTINGS.isShowSummary then
        dialog(
                "本次打了：" ..
                        STATS[5] ..
                        "场\n" ..
                        round_per_hr ..
                        "场/小时\n白蓝紫金 = " ..
                        STATS[1] ..
                        "/" ..
                        STATS[2] ..
                        "/" ..
                        STATS[3] ..
                        "/" ..
                        STATS[4] ..
                        "\n完成委托：" .. STATS[6] .. "次\n完成科研：" .. STATS[7] .. "次",
                0
        )
    end
end

function main()
    autoResetSetting()
    initialize()
    checkScreenDirection()
    -- act()
    showAnnounce("announce.json")
    if FREE then
        showFree()
    end
    showSettings()

    if SETTINGS.resetSettings then
        resetUIConfig("settings.dat")
        lua_restart()
    end

    -- hotfix map
    -- MAP_CONFIG.hotfix()
    MAP = Map:new() -- must be after hotfix

    if SETTINGS.debug_raw or SETTINGS.debug_shift then
        while true do
            keepScreen(true)
            ScanMap.scanEnemiesLv()
            ScanMap.scanEnemiesMark1()
            ScanMap.scanSub()
            ScanMap.scanEnemiesMark2()
            ScanMap.scanEnemiesMark3()
            ScanMap.scanEnemiesMark4()
            ScanMap.scanEnemiesEvent_Submarine()
            ScanMap.scanPlayer()
            ScanMap.scanQuestion()
            ScanMap.scanBoss()
            ScanMap.scanEnemiesDD()
            ScanMap.scanEnemiesCV()
            ScanMap.scanEnemiesBB()
            ScanMap.scanEnemiesGG()

            ScanMap.scanEnemiesIoLnD()

            keepScreen(false)
            sleep(2000)
            hideAllHUD()
            sleep(500)
        end
    end

    local shift = { 176, 125 } --  0.70984455958549222797927461139896  常见： 193,137 195,138 199,141   188,133  185,131  184,131 181,128
    -- TestShift(shift)

    local shiftInit = { -258, -242 } -- shiftInit = {-280,-127}
    -- TestShiftInit(shiftInit)

    --my_other_test(500)

    showConsole("欢迎来到碧蓝之神")
    mainLogic()
end

function test_tesser_num()
    local code, text = OCR.ky_time:getText(
            {
                rect = { 701, 255, 808, 286 },
                diff = { "0xffffff-0x0f0f0f" }
            }
    )
    if code == 0 then
        return trim(text)
    else
        return ''
    end
end

function my_other_test(interval)
    while true do
        --md_ = GetRectColorAvg(432,393,441,401)
        --printf("r: %d, g: %d, b: %d", md_.r, md_.g, md_.b)
        -- p_ = scanMain()
        -- print("path = " .. p_)
        --print(get_pos_in_cell(238, 286))
        --local path = scanMain()
        --print(path)
        --print(scanIsTechAcademyReady())

        sleep(1000)
    end
end

main()
