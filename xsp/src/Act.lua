local function showActUI(key)
	resetUIConfig("act.dat")
	local json_str = getUIContent('act.json')
	local json_obj = JSON:decode(json_str)

	local scale = DEVICE.width / 640
	_scaleUI(json_obj, scale)
	json_obj.views[3].text = key
	json_str = JSON:encode(json_obj)
	local ret, settings = showUI(json_str)
	if ret == 0 then lua_exit() end
	return settings.ans
end

local function saveAct(deviceInfo,tk)
	setStringConfig('a', SHA.sha256(deviceInfo..'D!@#$%^&'))
end

function checkAct(deviceInfo)
	local ret = getStringConfig('a', '') == SHA.sha256(deviceInfo..'D!@#$%^&')
	if not ret then
		setStringConfig('a', SHA.sha256('bla'))
	end
	return true
	-- return ret
end

function act()
	local aaa,bbb,ccc,ddd,eee,fff,ggg,hhh = 1,1,1,1,1,1,1,1
	local w, h = getScreenSize()
	local t = getNetTime()
	local ver = getEngineVersion()
	local o = getOSType()
	local m = isPrivateMode()
	local arch = getSystemProperty('ro.arch') --Android
	local aprod = getSystemProperty('ro.build.product') --Android
	local loc = getLocalInfo()
	local uid = getUserID()
	local sid = getScriptID()
	local dpi = getScreenDPI() --Android
	local imei = getDeviceIMEI() --Android
	local imsi = getDeviceIMSI() --Android
	local buyState,validTime,res=getUserCredit()
	local rtm = getRuntimeMode()
	local prod = getProduct()
	if t == 0 then
		dialog('无法连接网络')
		lua_exit()
	end

	local deviceInfo = tostring(w) .. tostring(h) .. tostring(ver) .. tostring(o) ..
		tostring(m) .. tostring(arch) .. tostring(aprod) .. tostring(loc) .. tostring(uid) ..
		tostring(sid) .. tostring(dpi) .. tostring(imei) .. tostring(imsi) .. tostring(buyState) .. tostring(validTime) ..
		tostring(res) .. tostring(rtm) .. tostring(prod) .. tostring(r)

	math.randomseed(t)
	local key = SHA.sha256(deviceInfo..tostring(t)..math.random())
	prev_t = getNumberConfig('t',0)
	prev_key = getStringConfig('k','')

	local hard_limit = 1565676034 + 10 * 24 * 3600
	local key_max_dur = 24 * 3600
	local hr_left = (hard_limit - t) / 3600

	if prev_t ~= 0 and prev_key~='' and t - prev_t < key_max_dur then
		key = prev_key
	else
		setNumberConfig('t',t)
		setStringConfig('k',key)
	end

	if checkAct(deviceInfo) and t < hard_limit then
		dialog('剩余'..tostring(hr_left)..'小时')
		return
	end

	ans = SHA.sha256(key..'&)*(&)')
	-- sysLog(key)
	-- sysLog(ans)

	user_ans = showActUI(key)
	if true or ans == user_ans then
		saveAct(deviceInfo)
		dialog('剩余'..tostring(hr_left)..'小时')
	else
		dialog('激活失败')
		lua_exit()
	end
end