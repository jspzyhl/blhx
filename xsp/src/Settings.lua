local function parse_fixed_path(rule)
    local ret = {}
    for _, coord in ipairs(split(rule, "/")) do
        local xy = split(coord, ",")
        table.insert(ret, { tonumber(xy[1]), tonumber(xy[2]) })
    end
    return ret
end

function parse_round(rule)
    return tonumber(split(rule, "/")[5])
end

function parse_level_list(level_list_str)
    return split(level_list_str, ",")
end

function parse_rule_lv(rule)
    local tbl = split(rule, "/")
    return tbl[4]
end

local function apply_rule(rule)
    local tbl = split(rule, "/")
    SETTINGS.mob_team = tonumber(tbl[1])
    SETTINGS.boss_team = tonumber(tbl[2])
    SETTINGS.sub_team = tonumber(tbl[3])
    -- SETTINGS._preround_round1 = tonumber(tbl[5])
    if tbl[4]:sub(1, 1) == "N" then
        SETTINGS.mode = "normal"
    else
        SETTINGS.mode = "hard"
    end
    local lv = split(tbl[4]:sub(2, -1), "-")
    SETTINGS.chapter = tonumber(lv[1])
    SETTINGS.level = tonumber(lv[2])
    STATE.team_selected = false
    STATE.form_selected = false
    STATE.accountability_setted = false
end

function test_level_rules(level_str)
    if SETTINGS.as_level_mode == 0 then
        for k_, v_ in ipairs(SETTINGS.as_blacklist) do
            if level_str == v_ then
                return false
            end
        end
        return true
    elseif SETTINGS.as_level_mode == 1 then
        for k_, v_ in ipairs(SETTINGS.as_whitelist) do
            if level_str == v_ then
                return true
            end
        end
        return false
    end
end

function test_autoseek_state()
    local level_str = ""
    if SETTINGS.mode == "normal" then
        level_str = level_str .. "N"
    elseif SETTINGS.mode == "hard" then
        level_str = level_str .. "H"
    end
    level_str = level_str .. tostring(SETTINGS.chapter) .. "-" .. tostring(SETTINGS.level)
    return test_level_rules(level_str)
end

function process_SETTINGS()
    SETTINGS.isDalao = SETTINGS._isDalao == "0"
    SETTINGS.isTebieyanxi = SETTINGS._isDalao == "3"
    SETTINGS.isWeituo = SETTINGS._isWeituo == "0"
    SETTINGS.isKeyan = SETTINGS._isKeyan == "0"
    SETTINGS.isYanxi = SETTINGS._isYanxi == "0"
    SETTINGS.isMeiri = SETTINGS._isMeiri == "0"
    SETTINGS.isStudy = SETTINGS._isStudy == "0"
    SETTINGS.isFeed = SETTINGS._isFeed == "0"
    SETTINGS.isRenwu = SETTINGS._isRenwu == "0"
    SETTINGS.isHatch = SETTINGS._isHatch == "0"
    SETTINGS.isShopping = SETTINGS._isShopping == "0"
    SETTINGS.isCollectKey = SETTINGS._isCollectKey == "0"
    SETTINGS.isBeacon = SETTINGS._isBeacon == "0"
    SETTINGS.isYanxizuozhan = false

    -- 自律寻敌
    SETTINGS.as_normal = (SETTINGS.ui_as_normal == "0")
    SETTINGS.as_preround = (SETTINGS.ui_as_preround == "0")
    SETTINGS.as_recycle_1 = (SETTINGS.ui_as_recycle_1 == "0")
    SETTINGS.as_recycle_2 = (SETTINGS.ui_as_recycle_2 == "0")
    SETTINGS.as_level_mode = tonumber(SETTINGS.ui_as_level_mode) -- 0:黑名单 1:白名单
    SETTINGS.as_blacklist = parse_level_list(SETTINGS.ui_as_blacklist)
    SETTINGS.as_whitelist = parse_level_list(SETTINGS.ui_as_whitelist)

    -- 打捞设置
    if SETTINGS._mode == "0" and (tonumber(SETTINGS._chapter) + 1 ~= 17) then
        SETTINGS.mode = "normal"
    else
        SETTINGS.mode = "hard"
    end

    SETTINGS.round = tonumber(SETTINGS._round)
    if SETTINGS.round == 0 then
        SETTINGS.round = 99999
    end
    SETTINGS.chapter = tonumber(SETTINGS._chapter) + 1
    SETTINGS.level = tonumber(SETTINGS._level) + 1
    SETTINGS.boss_team = tonumber(SETTINGS._boss_team) + 1
    SETTINGS.mob_team = tonumber(SETTINGS._mob_team) + 1
    SETTINGS.sub_team = tonumber(SETTINGS._sub_team)
    SETTINGS.mob_team_ammo = tonumber(SETTINGS._mob_team_ammo) + 1
    SETTINGS.mob_team_step = tonumber(SETTINGS._mob_team_step)
    SETTINGS.boss_team_step = tonumber(SETTINGS._boss_team_step)
    SETTINGS.eat_ammo = SETTINGS._eat_ammo == "0"
    SETTINGS.eat_question = SETTINGS._eat_question == "0"
    SETTINGS.isCleanWhite = SETTINGS._isCleanWhite == "0"
    SETTINGS.isCleanBlue = SETTINGS._isCleanBlue == "0"
    SETTINGS.mob_form = tonumber(SETTINGS._mob_form)
    SETTINGS.boss_form = tonumber(SETTINGS._boss_form)
    SETTINGS.isSleepRedface = SETTINGS._isSleepRedface == "0"
    SETTINGS.isWeightedEnemy = SETTINGS._isWeightedEnemy == "0"
    SETTINGS.chase_elite = SETTINGS._chase_elite == "0"
    SETTINGS.elite_strategy = tonumber(SETTINGS._elite_strategy) -- 0:无差别 1:追杀 2:躲避
    SETTINGS.isNotUseSub = SETTINGS._isNotUseSub == "0"
    SETTINGS.isSubAnime = SETTINGS._isSubAnime == "0"
    SETTINGS.isCatAnime = SETTINGS._isCatAnime == "0"
    SETTINGS.isDumbFirst = SETTINGS._isDumbFirst == "0"
    SETTINGS.rest_yellow_face = SETTINGS.ui_rest_yellow_face == '0'
    SETTINGS.rest_yf = {}
    SETTINGS.rest_yf[1] = SETTINGS.ui_rest_yf_1 == '0'
    SETTINGS.rest_yf[2] = SETTINGS.ui_rest_yf_2 == '0'
    SETTINGS.rest_yf[3] = SETTINGS.ui_rest_yf_3 == '0'
    SETTINGS.rest_yf[4] = SETTINGS.ui_rest_yf_4 == '0'
    SETTINGS.rest_yf[5] = SETTINGS.ui_rest_yf_5 == '0'
    SETTINGS.rest_yf[6] = SETTINGS.ui_rest_yf_6 == '0'
    SETTINGS.sp4_special = SETTINGS.ui_sp4_special == '0'

    -- Advanced
    SETTINGS.resetSettings = SETTINGS._resetSettings == "0"
    SETTINGS.scanEnemyType = SETTINGS._scanEnemyType == "0"
    if SETTINGS.scanEnemyType then
        SETTINGS.isWeightedEnemy = true
    end
    SETTINGS.BB3 = tonumber(SETTINGS._BB3) + 1
    SETTINGS.BB2 = tonumber(SETTINGS._BB2) + 1
    SETTINGS.BB1 = tonumber(SETTINGS._BB1) + 1
    SETTINGS.CV3 = tonumber(SETTINGS._CV3) + 1
    SETTINGS.CV2 = tonumber(SETTINGS._CV2) + 1
    SETTINGS.CV1 = tonumber(SETTINGS._CV1) + 1
    SETTINGS.DD3 = tonumber(SETTINGS._DD3) + 1
    SETTINGS.DD2 = tonumber(SETTINGS._DD2) + 1
    SETTINGS.DD1 = tonumber(SETTINGS._DD1) + 1
    SETTINGS.GG3 = tonumber(SETTINGS._GG3) + 1
    SETTINGS.GG2 = tonumber(SETTINGS._GG2) + 1
    SETTINGS.GG1 = tonumber(SETTINGS._GG1) + 1
    SETTINGS.enemyBaseDist = tonumber(SETTINGS._enemyBaseDist) + 1
    SETTINGS.isFightBoss = SETTINGS._isNoFightBoss ~= "0"
    SETTINGS.isManualMode = SETTINGS._isManualMode == "0"
    SETTINGS.isFightAmbush = SETTINGS._isFightAmbush == "0"
    SETTINGS.isClearMap = SETTINGS._isClearMap == "0"

    SETTINGS.autoRepair = SETTINGS._autoRepair == "0"
    SETTINGS.repairVote1 = (tonumber(SETTINGS._repairVote1) + 1) / 10
    SETTINGS.repairHP1 = (tonumber(SETTINGS._repairHP1) + 1)
    SETTINGS.repairVote2 = (tonumber(SETTINGS._repairVote2) + 2) / 10
    SETTINGS.repairHP2 = (tonumber(SETTINGS._repairHP2) + 2)

    SETTINGS.autoSwitch = SETTINGS._autoSwitch == "0"
    SETTINGS.switchDiff1 = tonumber(SETTINGS._switchDiff1) + 1
    SETTINGS.switchDiff2 = tonumber(SETTINGS._switchDiff2) + 1
    SETTINGS.switchMin1 = tonumber(SETTINGS._switchMin1) + 1
    SETTINGS.switchMin2 = tonumber(SETTINGS._switchMin2) + 1

    SETTINGS.eatQuestionAfter = tonumber(SETTINGS._eatQuestionAfter)
    SETTINGS.eatAmmoAfter = tonumber(SETTINGS._eatAmmoAfter)
    SETTINGS.resignBefore = tonumber(SETTINGS._resignBefore)
    SETTINGS.fixed_path_enabled = SETTINGS._fixed_path_enabled == "1"
    SETTINGS.call_submarine = SETTINGS._call_submarine == "0"
    SETTINGS.call_submarine_delay = tonumber(SETTINGS._call_submarine_delay)
    SETTINGS.pre_SOS = SETTINGS._pre_SOS == "0"

    if SETTINGS.pre_SOS and not STATE.SOS_done then
        apply_rule(SETTINGS._preround_rule3)
        SETTINGS.level = 5
        SETTINGS.mode = "normal"
        SETTINGS.resignBefore = 20
        SETTINGS.fixed_path_enabled = false
    else
        print("Round = ", STATS[5])
        STATE.preround_selected = false
        if SETTINGS._preround_mode == "1" or SETTINGS._preround_mode == "3" then
            if parse_round(SETTINGS._preround_rule0) == nil then
                dialog("预刷规则不正确,请选关闭")
                lua_restart()
            end
            STATE.pre_round_setting = parse_round(SETTINGS._preround_rule0)

            if STATE.pre_round < STATE.pre_round_setting then
                showConsole("进入预刷模式")
                STATE.preround_selected = true
                print("进入预刷模式")

                apply_rule(SETTINGS._preround_rule0)
                STATE.autoseek_enabled = SETTINGS.as_preround and test_autoseek_state()
                SETTINGS.fixed_path_enabled = false
            elseif STATE.pre_round == STATE.pre_round_setting then
                -- 为循环或正常模式做准备
                STATE.team_selected = false
                STATE.form_selected = false
                STATE.accountability_setted = false
            end
        end
        if (SETTINGS._preround_mode == "2" or SETTINGS._preround_mode == "3") and not STATE.preround_selected then
            local round1 = parse_round(SETTINGS._preround_rule1)
            local round2 = parse_round(SETTINGS._preround_rule2)
            if round1 == nil or round2 == nil then
                dialog("循环规则不正确,请选关闭")
                lua_restart()
            end
            if STATS[5] % (round1 + round2) < round1 then
                showConsole("进入循环模式1")
                print("进入循环模式1")
                apply_rule(SETTINGS._preround_rule1)
                STATE.autoseek_enabled = SETTINGS.as_recycle_1 and test_autoseek_state()
            else
                showConsole("进入循环模式2")
                print("进入循环模式2")
                apply_rule(SETTINGS._preround_rule2)
                STATE.autoseek_enabled = SETTINGS.as_recycle_2 and test_autoseek_state()
            end
        end
    end

    if SETTINGS.fixed_path_enabled then
        SETTINGS.fixed_path = parse_fixed_path(SETTINGS._fixed_path)
    else
        if SETTINGS.mode == "hard" and IsWorkingOnEventLevel() == false then
            SETTINGS.resignBefore = 20
        end

        SETTINGS.fixed_path = {}
    end

    STATE.autoseek_enabled = SETTINGS.as_normal and test_autoseek_state()

    -- 委托设置
    SETTINGS.wtrank_diamond_normal = tonumber(SETTINGS._wtrank_diamond_normal) + 1
    SETTINGS.wtrank_oil_normal = tonumber(SETTINGS._wtrank_oil_normal) + 1
    SETTINGS.wtrank_cube_normal = tonumber(SETTINGS._wtrank_cube_normal) + 1
    SETTINGS.wtrank_book_normal = tonumber(SETTINGS._wtrank_book_normal) + 1
    SETTINGS.wtrank_coin_normal = tonumber(SETTINGS._wtrank_coin_normal) + 1
    SETTINGS.wtrank_other_normal = tonumber(SETTINGS._wtrank_other_normal) + 1

    SETTINGS.wtrank_diamond_emergency = tonumber(SETTINGS._wtrank_diamond_emergency) + 1
    SETTINGS.wtrank_oil_emergency = tonumber(SETTINGS._wtrank_oil_emergency) + 1
    SETTINGS.wtrank_cube_emergency = tonumber(SETTINGS._wtrank_cube_emergency) + 1
    SETTINGS.wtrank_book_emergency = tonumber(SETTINGS._wtrank_book_emergency) + 1
    SETTINGS.wtrank_coin_emergency = tonumber(SETTINGS._wtrank_coin_emergency) + 1
    SETTINGS.wtrank_other_emergency = tonumber(SETTINGS._wtrank_other_emergency) + 1
    SETTINGS.reopen_after_midnight = SETTINGS._reopen_after_midnight == "0"

    -- 其他设置
    SETTINGS.yanxi_left = SETTINGS._yanxi_opponent == "0"
    SETTINGS.meiri_team = tonumber(SETTINGS._meiri_team) + 1
    SETTINGS.yanxiu_mission = tonumber(SETTINGS._yanxiu_mission) + 1
    SETTINGS.yanxizuozhan_team = tonumber(SETTINGS._yanxizuozhan_team) + 1
    SETTINGS.yanxizuozhan_level = tonumber(SETTINGS._yanxizuozhan_level) + 1
    if SETTINGS.yanxizuozhan_level == 3 then
        SETTINGS.yanxizuozhan_max = 40
    else
        SETTINGS.yanxizuozhan_max = 20
    end
    SETTINGS.isShowSummary = SETTINGS._isShowSummary == "0"
    SETTINGS.autoRestartApp = SETTINGS._autoRestartApp == "0"
    SETTINGS.RestartAfterRest = SETTINGS._RestartAfterRest == "0"
    SETTINGS.isSpeedLimiting = SETTINGS._isSpeedLimiting == "0"
    SETTINGS.isBlueBookPreferred = SETTINGS._isBlueBookPreferred == "0"
    SETTINGS.isRareFoodReserved = SETTINGS._isRareFoodReserved == "0"
    SETTINGS.isTempuraFoodReserved = SETTINGS._isTempuraFoodReserved == "0"
    SETTINGS.max_round_per_h = tonumber(SETTINGS._max_round_per_h)

    --  debug
    SETTINGS.move_speed = tonumber(SETTINGS._move_speed) * 100 + 50
    SETTINGS.cursor_duration = tonumber(SETTINGS._cursor_duration) * 50
    SETTINGS.cursor_click_duration = tonumber(SETTINGS._cursor_click_duration) * 10 + 20
    SETTINGS.sleep_multiplier = tonumber(SETTINGS._sleep_multiplier) * 0.1 + 0.5
    SETTINGS.cursor_wait_after = tonumber(SETTINGS._cursor_wait_after) * 50
    SETTINGS.waittime = tonumber(SETTINGS._waittime) * 100
    SETTINGS.wait_after_rating = tonumber(SETTINGS._wait_after_rating) * 200 + 2100

    local buyState, validTime, res = getUserCredit()
    -- if getDeviceIMEI() == '112233445566778' then
    SETTINGS.debug_raw = SETTINGS._debug_raw == "0"
    SETTINGS.debug_shift = SETTINGS._debug_shift == "0"
    SETTINGS.debug_print = SETTINGS._debug_print == "0"
    SETTINGS.debug_path = SETTINGS._debug_path == "0"
    SETTINGS.debug_weituo = SETTINGS._debug_weituo == "0"
    -- end
    SETTINGS.debug_research = SETTINGS._debug_research == "0"

    -- if SETTINGS._JP == "0" then
    --     SETTINGS.JP = false
    -- else
    --     SETTINGS.JP = true
    -- end

    if SETTINGS.chapter <= 13 then
        SETTINGS.mob_team_step = 0
        SETTINGS.boss_team_step = 0
        SETTINGS.chase_elite = false
        SETTINGS.elite_strategy = 0
    end

    if SETTINGS.BB3 == 0 then
        SETTINGS.BB3 = 1
    end
    if SETTINGS.BB2 == 0 then
        SETTINGS.BB2 = 1
    end
    if SETTINGS.BB1 == 0 then
        SETTINGS.BB1 = 1
    end
    if SETTINGS.CV3 == 0 then
        SETTINGS.CV3 = 1
    end
    if SETTINGS.CV2 == 0 then
        SETTINGS.CV2 = 1
    end
    if SETTINGS.CV1 == 0 then
        SETTINGS.CV1 = 1
    end
    if SETTINGS.DD3 == 0 then
        SETTINGS.DD3 = 1
    end
    if SETTINGS.DD2 == 0 then
        SETTINGS.DD2 = 1
    end
    if SETTINGS.DD1 == 0 then
        SETTINGS.DD1 = 1
    end
    if SETTINGS.GG3 == 0 then
        SETTINGS.GG3 = 1
    end
    if SETTINGS.GG2 == 0 then
        SETTINGS.GG2 = 1
    end
    if SETTINGS.GG1 == 0 then
        SETTINGS.GG1 = 1
    end

    -- 处理科研数据
    local priority_str = getUIContent('priority.txt')
    local rule_str = ''
    -- 使用内置规则
    if SETTINGS.ui_rule_from == '0' then
        if SETTINGS.ui_builtin_rules == '0' then
            rule_str = getUIContent('rules_balance.txt')        -- 均衡
        elseif SETTINGS.ui_builtin_rules == '1' then
            rule_str = getUIContent('rules_ship_first.txt')     -- 舰船优先
        elseif SETTINGS.ui_builtin_rules == '2' then
            rule_str = getUIContent('rules_equip_first.txt')    -- 装备优先
        end
    elseif SETTINGS.ui_rule_from == '1' then
        local file_name_ = '[public]' .. SETTINGS.ui_custom_filename
        local file1 = io.open(file_name_, "r")
        if file1 then
            rule_str = file1:read("*a")
            io.close(file1)
        else
            dialog('科研规则文件读取失败，请确认文件是否存在')
            lua_exit()
        end
    end

    res_rule_data:load_from_string(rule_str)
    res_sort:load_from_string(priority_str)
    res_sort:load_from_string(rule_str)

    if SETTINGS.ui_series_redef == "0" then
        res_sort:regist_keyword('series', 1, tonumber(SETTINGS.ui_cb_pry_s1) + 1)
        res_sort:regist_keyword('series', 2, tonumber(SETTINGS.ui_cb_pry_s2) + 1)
        res_sort:regist_keyword('series', 3, tonumber(SETTINGS.ui_cb_pry_s3) + 1)
        res_sort:regist_keyword('series', 4, tonumber(SETTINGS.ui_cb_pry_s4) + 1)
    end

    if SETTINGS.ui_ship_redef == "0" then
        res_sort:regist_keyword('ship_name', 'Haiwangxing', tonumber(SETTINGS.ui_pry_Haiwangxing) + 1)
        res_sort:regist_keyword('ship_name', 'Chuyun', tonumber(SETTINGS.ui_pry_Chuyun) + 1)
        res_sort:regist_keyword('ship_name', 'Luyi', tonumber(SETTINGS.ui_pry_Luyi) + 1)
        res_sort:regist_keyword('ship_name', 'Junzhu', tonumber(SETTINGS.ui_pry_Junzhu) + 1)
        res_sort:regist_keyword('ship_name', 'Luoen', tonumber(SETTINGS.ui_pry_Luoen) + 1)
        res_sort:regist_keyword('ship_name', 'Yichui', tonumber(SETTINGS.ui_pry_Yichui) + 1)

        res_sort:regist_keyword('ship_name', 'Dadi', tonumber(SETTINGS.ui_pry_Dadi) + 1)
        res_sort:regist_keyword('ship_name', 'Wuqi', tonumber(SETTINGS.ui_pry_Wuqi) + 1)
        res_sort:regist_keyword('ship_name', 'Zuozhiya', tonumber(SETTINGS.ui_pry_Zuozhiya) + 1)
        res_sort:regist_keyword('ship_name', 'Xiyatu', tonumber(SETTINGS.ui_pry_Xiyatu) + 1)
        res_sort:regist_keyword('ship_name', 'Beifeng', tonumber(SETTINGS.ui_pry_Beifeng) + 1)
        res_sort:regist_keyword('ship_name', 'Jiasikenie', tonumber(SETTINGS.ui_pry_Jiasikenie) + 1)

        res_sort:regist_keyword('ship_name', 'Chaijun', tonumber(SETTINGS.ui_pry_Chaijun) + 1)
        res_sort:regist_keyword('ship_name', 'Deleike', tonumber(SETTINGS.ui_pry_Deleike) + 1)
        res_sort:regist_keyword('ship_name', 'Meiyinci', tonumber(SETTINGS.ui_pry_Meiyinci) + 1)
        res_sort:regist_keyword('ship_name', 'Aoding', tonumber(SETTINGS.ui_pry_Aoding) + 1)
        res_sort:regist_keyword('ship_name', 'Xiangbin', tonumber(SETTINGS.ui_pry_Xiangbin) + 1)

        res_sort:regist_keyword('ship_name', 'Ankeleiqi', tonumber(SETTINGS.ui_pry_Ankeleiqi) + 1)
        res_sort:regist_keyword('ship_name', 'Bailong', tonumber(SETTINGS.ui_pry_Bailong) + 1)
        res_sort:regist_keyword('ship_name', 'Aijier', tonumber(SETTINGS.ui_pry_Aijier) + 1)
        res_sort:regist_keyword('ship_name', 'Aogusite', tonumber(SETTINGS.ui_pry_Aogusite) + 1)
        res_sort:regist_keyword('ship_name', 'Makeboluo', tonumber(SETTINGS.ui_pry_Makeboluo) + 1)

    end

end
