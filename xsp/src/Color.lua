Color = {
	GREEN = '0xff00ff00',
	DARK_GREEN = '0xff00802b',
	WHITE = '0xffffffff',
	BLACK = '0xff000000',
	RED = '0xffff0000',
	DARK_RED = '0xff7d0000',
	BLUE = '0xff0000ff',
	YELLOW = '0xffffff00',
	ORANGE = '0xffffa500',
	PURPLE = '0xffff00ff',
	CYAN = '0xff00ffff',
	PINK = '0xffff00ff'
}