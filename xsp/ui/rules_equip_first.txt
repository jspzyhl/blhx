[group]
group_by='series'

[sort]
order_by='series,ship_rarity,ship_name,scale'

[select]
type='Q',rarity='ssr'
type='D',rarity='ur',ship_rarity='dr'
type='D',rarity='r',ship_rarity='dr'
type='Q',rarity='sr'
type='G',rarity='sr'
type='D',rarity='ssr',scale='small'
type='Q',rarity='r',time_cost=1
type='G',rarity='r',time_cost=1.5
type='G',rarity='r',time_cost=2.5
type='D',rarity='r'
type='E',rarity='sr',scale='small'
type='Q',rarity='r',time_cost=2
type='E',rarity='r'
type='D',rarity='sr',ship_rarity='dr'
type='D',rarity='sr'
type='D',rarity='ssr',ship_rarity='dr'
type='D',rarity='ssr'